package ru.xlv.head.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.event.GiveInsuredItemsPostEvent;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostAttachmentItemStack;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.util.PostFactory;

public class InsureEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(GiveInsuredItemsPostEvent event) {
        if (!event.getItemStackList().isEmpty()) {
            PostObject adminPostObject = PostFactory.createAdminPostObject("Ваши застрахованные вещи не поместились в ваш инвентарь и были прикреплены к этому письму.", event.getEntityPlayer().getCommandSenderName());
            event.getItemStackList().forEach(itemStack -> adminPostObject.getAttachments().add(new PostAttachmentItemStack(itemStack)));
            XlvsPostMod.INSTANCE.getPostHandler().sendPost(adminPostObject);
            event.getItemStackList().clear();
            event.setCanceled(true);
        }
    }
}
