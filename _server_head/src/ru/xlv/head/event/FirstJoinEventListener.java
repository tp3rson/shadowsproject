package ru.xlv.head.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.item.ItemStack;
import ru.xlv.core.event.ServerPlayerFirstLoginEvent;
import ru.xlv.mochar.player.kit.Kit;
import ru.xlv.mochar.player.kit.KitHandler;

public class FirstJoinEventListener {

    private final Kit kitStart = KitHandler.getKit("start");

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(ServerPlayerFirstLoginEvent event) {
        if (kitStart != null) {
            kitStart.getItems().forEach(itemStack -> {
                for (int i = 0; i < itemStack.stackSize; i++) {
                    ItemStack itemStack1 = itemStack.copy();
                    itemStack1.stackSize = 1;
                    event.getCharacter().getMatrixInventory().addItem(itemStack1);
                }
            });
        }
    }
}
