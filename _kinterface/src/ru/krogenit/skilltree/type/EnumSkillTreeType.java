package ru.krogenit.skilltree.type;

public enum EnumSkillTreeType {
    PASSIVE, ACTIVE;
}
