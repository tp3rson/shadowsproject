package ru.krogenit.skilltree;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.lwjgl.util.vector.Vector3f;

@Getter
@AllArgsConstructor
public class SkillAttribute {
    private final String name;
    private final Vector3f color;
}
