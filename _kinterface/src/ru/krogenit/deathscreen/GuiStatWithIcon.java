package ru.krogenit.deathscreen;

import lombok.AllArgsConstructor;
import lombok.Setter;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.xlv.customfont.FontType;

@AllArgsConstructor
public class GuiStatWithIcon {

    private final ResourceLocation texture;
    @Setter
    private float width, height;
    private final String name;
    @Setter
    private String value;

    public void draw(float x, float y) {
        GuiDrawUtils.drawCenter(texture, x, y, width, height);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, name, x + width + 10, y + 13, 30 / 32f, 0xb4b4b4);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrMedium, value, x + width + 295, y + 12, 40 / 32f, 0xffffff);
    }
}
