package ru.krogenit.deathscreen;

import lombok.AllArgsConstructor;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.util.DecimalUtils;
import ru.xlv.customfont.FontType;

@AllArgsConstructor
public class GuiDamageSource {

    private final ResourceLocation texture;
    private final float width, height;
    private final double damage, percent;

    public void draw(float x, float y) {
        float fs = 90 / 32f;
        GuiDrawUtils.drawCenter(texture, x, y, width, height);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrBlack, DecimalUtils.getFormattedStringWithOneDigit(damage), x + width + 10, y + 10, fs, 0xffffff);
        fs = 26 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "("+ DecimalUtils.getFormattedStringWithToDigits(percent)+"%)", x + width + 16, y + 40, fs, 0x7e7e7e);
    }
}
