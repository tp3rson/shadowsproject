package ru.krogenit.settings.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundCategory;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.settings.gui.slider.GuiSliderAudio;
import ru.xlv.customfont.FontType;

public class GuiAudioSettings extends AbstractGuiScreenAdvanced implements IGuiWithPopupConfirm {
    public final GuiScreen guiScreen;
    public final GameSettings gameSettings;
    private final String offString = I18n.format("options.off");
    private AbstractGuiScreenAdvanced popup;

    public GuiAudioSettings(GuiScreen guiScreen) {
        super(ScaleGui.SXGA);
        this.guiScreen = guiScreen;
        this.gameSettings = Minecraft.getMinecraft().gameSettings;
    }

    public void initGui() {
        super.initGui();
        this.buttonList.clear();

        float x = 75 + 960;
        float y = -300 + 540;
        float buttonWidth = 270;
        float buttonHeight = 39;
        GuiSliderAudio slider = new GuiSliderAudio(SoundCategory.MASTER.getCategoryId(), ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), SoundCategory.MASTER, this);
        slider.setTexture(TextureRegister.textureSettingsButton);
        this.buttonList.add(slider);
        SoundCategory[] asoundcategory = SoundCategory.values();
        float yOffset = buttonHeight + 20;
        y += yOffset;

        for (SoundCategory soundcategory : asoundcategory) {
            if (soundcategory != SoundCategory.MASTER && soundcategory != SoundCategory.RECORDS && soundcategory != SoundCategory.BLOCKS) {
                slider = new GuiSliderAudio(soundcategory.getCategoryId(), ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), soundcategory, this);
                slider.setTexture(TextureRegister.textureSettingsButton);
                this.buttonList.add(slider);
                y += yOffset;
            }
        }

        buttonWidth = 171;
        buttonHeight = 39;
        x = -120 + 960;
        y = 330 + 540;
        GuiButtonAnimated b = new GuiButtonAnimated(200, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ПОДТВЕРДИТЬ");
        b.setTexture(TextureRegister.textureTreeButtonAccept);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);
        x = 120 + 960;
        b = new GuiButtonAnimated(201, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТМЕНИТЬ");
        b.setTexture(TextureRegister.textureTreeButtonCancel);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);

        if (popup != null) popup.initGui();
    }

    public String getValue(float sliderValue) {
        return sliderValue == 0.0F ? offString : (int) (sliderValue * 100.0F) + "%";
    }

    protected void actionPerformed(GuiButton guiButton) {
        if (guiButton.enabled) {
            if (guiButton.id == 200) {
                popup = new GuiConfirm(this);
                popup.initGui();
            } else if (guiButton.id == 201) {
                this.mc.displayGuiScreen(this.guiScreen);
            }
        }
    }

    private void drawMiscElements() {
        float fsx = 2.4f;
        String s = "НАСТРОЙКИ ЗВУКА";
        float x = 960;
        float y = 110f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, s, x, y, fsx, 0x626262);

        fsx = 0.8f;
        x = 1866;
        y = 937;
        s = "VERSION 0.163 BETA";
        GuiDrawUtils.drawRightStringRightBot(FontType.HelveticaNeueCyrLight, s, x, y, fsx, 0xffffff);
        s = "Shadows Project - an original MMORPG-FPS\n" +
                "Massively Multiplayer Online Role-Playing First\n" +
                "Person Shooter) in a fantasy anti-utopian hi-tec future where the horrors\n" +
                "of space and all the “charms” of interplanetary travel await you.";
        fsx = 0.7f;
        y = 971;
        GuiDrawUtils.drawSplittedStringRightBot(FontType.DeadSpace, s, x, y, fsx, 1000f, -1f, 0x626262, EnumStringRenderType.RIGHT);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureSettingsBorderTop, 960, -346 + 540, 821, 78);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureSettingsBorderBot, 960, 230 + 540, 821, 78);
        GuiDrawUtils.drawBotCentered(TextureRegister.textureESCLogo, 137, 960, 184, 53);
    }

    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawDefaultBackground();
        GuiDrawUtils.drawMaskingBackgroundEffect(0, 0, width, height);
        drawMiscElements();
        drawButtons(mouseX, mouseY, partialTick);

        if (popup != null) popup.drawScreen(mouseX, mouseY, partialTick);
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (popup != null) {
            popup.mouseClicked(mouseX, mouseY, mouseButton);
        } else {
            super.mouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    @Override
    public void applySettings() {
        for (Object o : this.buttonList) {
            if (o instanceof GuiSliderAudio) {
                GuiSliderAudio slider = (GuiSliderAudio) o;
                gameSettings.setSoundLevel(slider.soundCategory, slider.value);
            }
        }

        gameSettings.saveOptions();
        this.mc.displayGuiScreen(this.guiScreen);
    }

    @Override
    public void cancel() {
        popup = null;
    }

    @Override
    public float getCurrentAspect() {
        return minAspect;
    }
}
