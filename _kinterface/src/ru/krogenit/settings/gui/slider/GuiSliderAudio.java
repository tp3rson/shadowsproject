package ru.krogenit.settings.gui.slider;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundCategory;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.settings.CoreSettings;
import ru.krogenit.settings.gui.GuiAudioSettings;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

import java.awt.*;

public class GuiSliderAudio extends GuiButtonAdvanced {
    public final SoundCategory soundCategory;
    private final String settingName;
    public float value;
    public boolean isMoving;
    private final GuiAudioSettings guiAudioSettings;
    private float blending;

    public GuiSliderAudio(int id, float x, float y, float width, float height, SoundCategory soundCategory, GuiAudioSettings guiAudioSettings) {
        super(id, x, y, width, height, "");
        this.soundCategory = soundCategory;
        this.settingName = I18n.format("soundCategory." + soundCategory.getCategoryName());
        this.displayString = guiAudioSettings.getValue(guiAudioSettings.gameSettings.getSoundLevel(soundCategory));
        this.value = guiAudioSettings.gameSettings.getSoundLevel(soundCategory);
        this.guiAudioSettings = guiAudioSettings;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            boolean hovered = isHovered(mouseX, mouseY);
            if (hovered) {
                blending += AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blending > 1) blending = 1;
            } else {
                blending -= AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blending < 0) blending = 0f;
            }

            Utils.bindTexture(texture);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height, 1f, 1f, 1f, 1f);

            if (blending > 0) {
                GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height, 1f, 1f, 1f, blending);
            }

            mouseDragged(mc, mouseX, mouseY);
            drawText(mc, 0, 0);
        }
    }

    protected void drawText(Minecraft mc, int xOffset, int yOffset) {
        float offsetY = mc.displayHeight / 400f;
        float textScale = 0.9f;

        String s = settingName.toUpperCase();
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, s, this.xPosition - this.width / 10.0f + xOffset - FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * ScaleGui.get(textScale),
                this.yPosition + this.height / 2.0f + yOffset - offsetY, textScale, 0xffffff);

        int r = ((0x959595 & 0xFF0000) >> 16) + (int) ((255 - ((0x959595 & 0xFF0000) >> 16)) * blending);
        int g = ((0x959595 & 0xFF00) >> 8) + (int) ((255 - ((0x959595 & 0xFF00) >> 8)) * blending);
        int b = (0x959595 & 0xFF) + (int) ((255 - ((0x959595 & 0xFF))) * blending);
        int j = (int) Long.parseLong(Integer.toHexString(new Color(r, g, b).getRGB()), 16);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrBold, displayString, this.xPosition + this.width / 2.0f + xOffset - (FontType.HelveticaNeueCyrBold.getFontContainer().width(displayString) * ScaleGui.get(textScale)) / 2.0f,
                this.yPosition + this.height / 2.0f + yOffset - offsetY, textScale, j);
    }

    /**
     * Returns 0 if the button is disabled, 1 if the mouse is NOT hovering over this button and 2 if it IS hovering
     * over this button.
     */
    public int getHoverState(boolean isHovering) {
        return 0;
    }

    /**
     * Fired when the mouse button is dragged. Equivalent of MouseListener.mouseDragged(MouseEvent e).
     */
    protected void mouseDragged(Minecraft mc, int mouseX, int mouseY) {
        if (this.visible) {
            if (this.isMoving) {
                this.value = (mouseX - (this.xPosition + 4)) / (this.width - 8);

                if (this.value < 0.0F) {
                    this.value = 0.0F;
                }

                if (this.value > 1.0F) {
                    this.value = 1.0F;
                }

                this.displayString = guiAudioSettings.getValue(this.value);
            }

            Utils.bindTexture(new ResourceLocation(CoreSettings.MODID, "textures/slider.png"));
            GL11.glColor4f(1f, 1f, 1f, 1f);

            float width = this.width / 16f;
            GuiDrawUtils.drawRect(this.xPosition + this.value * (this.width - width), this.yPosition, width, this.height);

            if (blending > 0) {
                GL11.glColor4f(1f, 1f, 1f, blending);
                GuiDrawUtils.drawRect(this.xPosition + this.value * (this.width - width), this.yPosition, width, this.height);
            }
        }
    }

    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of
     * MouseListener.mousePressed(MouseEvent e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        if (super.mousePressed(mc, mouseX, mouseY)) {
            this.value = (mouseX - (this.xPosition + 4)) / (this.width - 8);

            if (this.value < 0.0F) {
                this.value = 0.0F;
            }

            if (this.value > 1.0F) {
                this.value = 1.0F;
            }

            this.displayString = guiAudioSettings.getValue(this.value);
            this.isMoving = true;
            return true;
        } else {
            return false;
        }
    }

    public void playClickSound(SoundHandler sound) {
    }

    /**
     * Fired when the mouse button is released. Equivalent of MouseListener.mouseReleased(MouseEvent e).
     */
    public void mouseReleased(int mouseX, int mouseY) {
        if (this.isMoving) {
            if (this.soundCategory != SoundCategory.MASTER) {
                guiAudioSettings.gameSettings.getSoundLevel(this.soundCategory);
            }

            guiAudioSettings.mc.getSoundHandler().playSound(PositionedSoundRecord.func_147674_a(new ResourceLocation("gui.button.press"), 1.0F));
        }

        this.isMoving = false;
    }
}
