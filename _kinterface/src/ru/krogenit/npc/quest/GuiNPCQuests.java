package ru.krogenit.npc.quest;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.EnumChatFormatting;
import noppes.npcs.NoppesUtilPlayer;
import noppes.npcs.QuestLogData;
import noppes.npcs.client.gui.util.IGuiData;
import noppes.npcs.constants.EnumPlayerPacket;
import noppes.npcs.controllers.Quest;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.npc.GuiNPCButtonSwitcher;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiNPCQuests extends AbstractGuiScreenAdvanced implements IGuiData, IGuiWithPopup {

    protected float scroll, scrollAnim, lastScroll;
    protected float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    protected float scrollX, scrollY, scrollWidth, scrollHeight;
    protected boolean mouseScrolling;
    protected float mouseStartY;
    private AxisAlignedBB axisAlignedBB;

    private final EntityNPCInterface npc;
    private final List<GuiQuestElement> guiQuests = new ArrayList<>();
    private int selected = -1;
    private GuiConfirmPopup popup;

    public GuiNPCQuests(EntityNPCInterface npc) {
        super(ScaleGui.FULL_HD);
        this.npc = npc;
        NoppesUtilPlayer.sendData(EnumPlayerPacket.QuestLog);
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        initButtons();

        guiQuests.clear();
        float x = 308;
        float y = 183;
        float width = 1277;
        float height = 217;
        float offsetY = 237;
        float startY = y;
        while(guiQuests.size() < 4) {
            int id = guiQuests.size();
            guiQuests.add(new GuiQuestElement(id, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), new Quest(), this));
            y+=offsetY;
        }

        scrollViewHeight = ScaleGui.get(731);
        scrollTotalHeight = ScaleGui.get(y - startY + 40);

        if(popup != null) showConfirmPopup(null);
    }

    private void initButtons() {
        float xOffset = 74;
        float x = 1362;
        float y = 57;
        float buttonWidth = 47;
        float buttonHeight = 47;
        GuiNPCButtonSwitcher b = new GuiNPCButtonSwitcher(0, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonDialog);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(1, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonTrade);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(2, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonSave);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(3, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonLocked);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        b.enabled = false;
        buttonList.add(b);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {

    }

    public void showConfirmPopup(GuiQuestElement guiQuestElement) {
        List<GuiButton> buttons = new ArrayList<>();
        float buttonWidth = 171;
        float buttonHeight = 39;
        float x = 960;
        float y = -24 + 540;
        GuiButtonAnimated button = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ПОДТВЕРДИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttons.add(button);

        y += buttonHeight * 1.35f;
        button = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТМЕНИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonCancel);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttons.add(button);
        this.popup = new GuiConfirmPopup(this, buttons, "НАЧАТЬ ЗАДАНИЕ?", "Если вы возьметесь за задание, которое не сможете выполнить или откажетесь от него, " +
                "ваша репутация с персонажем, выдавшем задание испортится.");
    }

    public void showInfo(GuiQuestElement guiQuestElement) {

    }

    @Override
    public void popupAction(GuiButton button) {
        if(button.id == 0) {
            popup = null;
        } else if(button.id == 1) {
            popup = null;
        }
    }

    @Override
    public void setPopup(GuiPopup popup) {

    }

    private void drawScroll() {
        if (scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if (scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float x = ScaleGui.getCenterX(1604);
        float y = ScaleGui.getCenterY(183);
        float iconWidth = ScaleGui.get(7);
        float iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(TextureRegister.textureInvArrowTop, x, y, iconWidth, iconHeight);
        y += iconHeight + ScaleGui.get(3);
        scrollTextureHeight = ScaleGui.get(677);
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if (scrollValue > 1) scrollValue = 1f;
        if (scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += scrollAnim * scrollYValue;
        scrollX = x;
        scrollY = y;
        scrollWidth = iconWidth;
        scrollHeight = iconHeight;
        GuiDrawUtils.drawRect(TextureRegister.textureInvScrollBody, x, y, iconWidth, iconHeight);
        y = ScaleGui.getCenterY(871);
        iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(TextureRegister.textureInvArrowBot, x, y, iconWidth, iconHeight);
    }

    private void drawBackgroundThings() {
        GL11.glDisable(GL_TEXTURE_2D);
        GL11.glColor4f(0f, 0f, 0f, 0.8f);
        GuiDrawUtils.drawCenter(288, 163, 1342, 731);
        GuiDrawUtils.drawRect(ScaleGui.getCenterX(288), ScaleGui.getCenterY(914), ScaleGui.get(1342), ScaleGui.get(731));
        GL11.glEnable(GL_TEXTURE_2D);
        GL11.glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCDialogBorderBot, 115, 124, 1688, 78);
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCBorderBotIconsCover, 1231, 35, 498, 91);
        drawScroll();
        float x = 292;
        float y = 72;
        float fs = 60 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, npc.display.name.toUpperCase(), x, y, fs, 0xffffff);
        x = 960;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "ДОСТУПНЫЕ ЗАДАНИЯ", x, y, fs, 0xffffff);
        x = 292;
        fs = 30 / 32f;
        y += 28;
//        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ТОРГОВЕЦ ОРУЖИЕМ", x, y, fs, 0xffffff);

        x = 36;
        y = 106;
        fs = 18 / 32f;
        GuiDrawUtils.drawSplittedStringCenter(FontType.DeadSpace, "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure\n" +
                "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure\n", x, y, fs, ScaleGui.get(150), -1, 0x626262, EnumStringRenderType.DEFAULT);

        x = 1875;
        y = 259;
        GuiDrawUtils.drawSplittedStringCenter(FontType.DeadSpace, "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure\n" +
                "Lorem ipsum\u0003dolor sit amet, consec\n" +
                "tetur adipisicing\n" +
                "elit, sed do eiusmod\n" +
                "tempor in\n" +
                "cididunt ut lab\n" +
                "ore et \n" +
                "magna aliqua. U\n" +
                "t enim ad\n" +
                " minim veniam, q\n" +
                "uis nostrud exercit\n" +
                "ation ul\n" +
                "lamco labor\n" +
                "is nisi ut aliqui\n" +
                "p ex ea c\n" +
                "ommodo consequa\n" +
                "t. Dui\n" +
                "s aute ir\n" +
                "ure ", x, y, fs, ScaleGui.get(150), -1, 0x626262, EnumStringRenderType.RIGHT);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawDefaultBackground();

        glEnable(GL_BLEND);
        glAlphaFunc(GL_GREATER, 0.0001f);
        glDisable(GL_ALPHA_TEST);

        drawButtons(mouseX, mouseY, partialTick);
        drawBackgroundThings();

        for(GuiQuestElement questElement : guiQuests) {
            questElement.addYPosition(-scrollAnim);
        }

        float x = 304;
        float y = 934;
        float fs = 30 / 32f;
        if(mouseY > ScaleGui.getCenterY(920) && mouseY <= ScaleGui.getCenterY(920) + ScaleGui.get(35)) {
            selected = 1;
            GL11.glDisable(GL_TEXTURE_2D);
            GL11.glColor4f(0.172f, 0.172f, 0.172f, 0.8f);
            GuiDrawUtils.drawCenter(x - 4, y - 10, 1325, 25);
            GL11.glEnable(GL_TEXTURE_2D);
        } else {
            selected = -1;
        }
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, EnumChatFormatting.AQUA + "ESC. " + EnumChatFormatting.WHITE + "Покинуть доску заданий", x, y, fs, 0xffffff);

        GL11.glEnable(GL_SCISSOR_TEST);
        int sx = (int) (ScaleGui.getCenterX(288));
        int sy = (int) (ScaleGui.screenHeight - ScaleGui.getCenterY(892));
        int swidth = (int) (ScaleGui.get(1342));
        int sheight = (int) (ScaleGui.get(731));
        axisAlignedBB = AxisAlignedBB.getBoundingBox(sx, this.height - sy - sheight, -100, sx + swidth, this.height - sy,100);
        GL11.glScissor(sx, sy, swidth, sheight);
        for(GuiQuestElement guiQuestElement : guiQuests) {
            guiQuestElement.drawButton(mouseX, mouseY);
        }
        GL11.glDisable(GL_SCISSOR_TEST);

        if(popup != null) popup.drawPopup(mouseX, mouseY);
    }

    public void setGuiData(NBTTagCompound compound) {
        QuestLogData data = new QuestLogData();
        data.readNBT(compound);
        this.initGui();
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        if(mouseButton == 0) {
            if(popup != null) {
                if(popup.mouseClicked(mouseX, mouseY)) {
                    return;
                }
            }

            for(GuiQuestElement questElement : guiQuests) {
                if (questElement.mousePressed(mc, mouseX, mouseY, axisAlignedBB)) {
                    return;
                }
            }

            if (mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
                mouseStartY = mouseY;
                mouseScrolling = true;
                lastScroll = scroll;
            } else {
                if(selected == 1) {
                    mc.displayGuiScreen(null);
                }
            }
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        if (mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if (heightDiff > 0) {
                float diff = scrollTextureHeight / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }
            }
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        mouseScrolling = false;
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int d = Mouse.getEventDWheel();
        int mouseX = Mouse.getX();
        int mouseY = this.height - Mouse.getY() - 1;
        if (d != 0) {
            float x = ScaleGui.getCenterX(288);
            float y = ScaleGui.getCenterY(163);
            float width = ScaleGui.get(1342);
            float height = ScaleGui.get(731);
            if (mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height) {
                float diff = scrollTotalHeight - scrollViewHeight;
                if (diff > 0) {
                    scroll -= d / 2f;
                    if (scroll < 0) {
                        scroll = 0;
                    } else if (scroll > diff) {
                        scroll = diff;
                    }
                }
            }
        }
    }
}
