package ru.krogenit.npc;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.inventory.GuiMatrixSlot;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.lwjgl.opengl.GL11.*;

public class MatrixInventoryModule {

    private static final Minecraft mc = Minecraft.getMinecraft();
    protected IInventoryWithDraggedSlot parentInventory;

    protected float scroll, scrollAnim, lastScroll;
    protected float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    protected float scrollX, scrollY, scrollWidth, scrollHeight;
    protected boolean mouseScrolling;
    protected float mouseStartY;
    protected AxisAlignedBB inventoryAABB;

    protected List<GuiMatrixSlot> slots = new ArrayList<>();
    protected MatrixInventory matrixInventory;
    protected int matrixWidth, matrixHeight;

    protected float startX, startY;
    protected float cellWidth, cellHeight;
    protected float viewWidth, viewHeight;
    private final Vec3 mouseVector = Vec3.createVectorHelper(0,0,0);

    public MatrixInventoryModule(IInventoryWithDraggedSlot parent) {
        this.parentInventory = parent;
    }

    public void setInventoryPositionAndSize(float x, float y, float cellWidth, float cellHeight, float viewWidth, float viewHeight, float matrixHeight) {
        this.cellWidth = ScaleGui.get(cellWidth);
        this.cellHeight = ScaleGui.get(cellHeight);
        this.startX = ScaleGui.getCenterX(x);
        this.startY = ScaleGui.getCenterY(y);
        this.viewWidth = ScaleGui.get(viewWidth);
        this.viewHeight = ScaleGui.get(viewHeight);
        this.scrollViewHeight = this.viewHeight;
        this.scrollTotalHeight = this.cellHeight * matrixHeight;
    }

    public void setInventory(MatrixInventory matrixInventory) {
        this.matrixInventory = matrixInventory;
        matrixWidth = matrixInventory.getWidth();
        matrixHeight = matrixInventory.getHeight();
        slots.clear();
        GuiMatrixSlot guiMatrixSlot = parentInventory.getDraggedSlot();
        matrixInventory.getItems().forEach((integer, itemStack) -> {
            if(guiMatrixSlot != null && guiMatrixSlot.getItemStack() == itemStack) {
                return;
            }
            int[] xy = matrixInventory.getPosOfItem(integer);
            if (xy != null) {
                int w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
                int h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
                GuiMatrixSlot slot = new GuiMatrixSlot(startX + xy[0] * cellWidth, startY + xy[1] * cellHeight, cellWidth * w, cellHeight * h, itemStack, matrixInventory);
                slots.add(slot);
            }
        });
        this.scrollTotalHeight = this.cellHeight * matrixHeight;
    }

    private void drawScroll() {
        if(scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if(scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float x = startX + viewWidth + ScaleGui.get(10);
        float y = startY + ScaleGui.get(6);
        float iconWidth = ScaleGui.get(7);
        float iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(TextureRegister.textureInvArrowTop, x, y, iconWidth, iconHeight);
        y += iconHeight + ScaleGui.get(3);
        scrollTextureHeight = viewHeight - ScaleGui.get(24);
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if(scrollValue > 1) scrollValue = 1f;
        if(scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += scrollAnim * scrollYValue;
        scrollX = x;
        scrollY = y;
        scrollWidth = iconWidth;
        scrollHeight = iconHeight;
        GuiDrawUtils.drawRect(TextureRegister.textureInvScrollBody, x, y, iconWidth, iconHeight);
        y = startY + viewHeight - ScaleGui.get(8);
        iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(TextureRegister.textureInvArrowBot, x, y, iconWidth, iconHeight);
    }

    private void drawBackGroundElements() {
        float x = startX;
        float y = startY;
        GL11.glColor4f(12/255f, 12/255f, 12/255f, 0.6f);
        GL11.glDisable(GL_TEXTURE_2D);
        GuiDrawUtils.drawRect(x, y, viewWidth, viewHeight);
        glLineWidth(2f);
        glColor4f(1f, 1f, 1f, 0.1f);
        glBegin(GL_LINE_LOOP);
        glVertex2f(x, startY);
        glVertex2f(x + viewWidth, startY);
        glVertex2f(x + viewWidth, startY + viewHeight);
        glVertex2f(x, startY + viewHeight);
        glEnd();
        GL11.glEnable(GL_SCISSOR_TEST);
        glLineWidth(1f);
        float sx = startX;
        float sy = ScaleGui.screenHeight - (startY + viewHeight);
        float swidth = viewWidth;
        float sheight = viewHeight;
        glScissor((int)sx, (int)sy,(int)swidth , (int)sheight);
        y = startY - scrollAnim;
        for (int i = 0; i < matrixWidth; i++) {
            for (int j = 0; j < matrixHeight; j++) {
                glBegin(GL_LINE_LOOP);
                glVertex2f(x, y);
                glVertex2f(x + cellWidth, y);
                glVertex2f(x + cellWidth, y + cellHeight);
                glVertex2f(x, y + cellHeight);
                glEnd();
                y += cellHeight;
            }
            y = startY - scrollAnim;
            x += cellWidth;
        }
        glEnable(GL_TEXTURE_2D);
        GL11.glDisable(GL_SCISSOR_TEST);
        GL11.glColor4f(1f, 1f, 1f, 1f);
    }

    public void drawModule(int mouseX, int mouseY) {
        if(matrixInventory.isNeedSync()) {
            setInventory(matrixInventory);
            matrixInventory.setNeedSync(false);
        }
        drawBackGroundElements();
        drawScroll();
        GL11.glEnable(GL_SCISSOR_TEST);
        float x = startX;
        float y = ScaleGui.screenHeight - (startY + viewHeight);
        float width = viewWidth;
        float height = viewHeight;
        glScissor((int)x, (int)y,(int)width, (int)height);
        boolean hovered = mouseX > startX && mouseY > startY && mouseX < startX + viewWidth && mouseY < startY + viewHeight;
        for(GuiMatrixSlot slot : slots) {
            slot.addYPosition(-scrollAnim);
            slot.render(mouseX, mouseY, hovered);
        }
        GL11.glDisable(GL_SCISSOR_TEST);
        inventoryAABB = AxisAlignedBB.getBoundingBox(x, mc.displayHeight - y - height, -100, x + width, mc.displayHeight - y,100);
        GuiMatrixSlot draggedSlot = parentInventory.getDraggedSlot();
        if(draggedSlot != null && hovered) {
            if(draggedSlot.getItemStack() != null) {
                resizeDraggetSlotIfNeeded(draggedSlot);
                renderSlotPreview(mouseX, mouseY, draggedSlot);
            }
        }
    }

    private void resizeDraggetSlotIfNeeded(GuiMatrixSlot draggedSlot) {
        float width1 = draggedSlot.getWidth();
        float height1 = draggedSlot.getHeight();
        ItemStack itemStack = draggedSlot.getItemStack();
        int invMatrixWidth = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
        int invMatrixHeight = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
        float cellWidth = width1 / (float) invMatrixWidth;
        float cellHeight = height1 / (float) invMatrixHeight;
        if(cellWidth != this.cellWidth || cellHeight != this.cellHeight) {
            draggedSlot.setWidth(this.cellWidth * invMatrixWidth);
            draggedSlot.setHeight(this.cellHeight * invMatrixHeight);
        }
    }

    public boolean drawPopups(int mouseX, int mouseY) {
        if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            if (mouseX > startX && mouseY > startY && mouseX < startX + viewWidth && mouseY < startY + viewHeight) {
                AtomicBoolean popuped = new AtomicBoolean(false);
                slots.forEach(slot -> {
                    if (inventoryAABB.intersectsWith(AxisAlignedBB.getBoundingBox(slot.getX() + 1f, slot.getY() + 1f, -10, slot.getX() + slot.getWidth() - 1f, slot.getY() + slot.getHeight() - 1f, 10)))
                        if (slot.isHovered(mouseX, mouseY)) {
                            slot.renderPopup(mouseX, mouseY);
                            popuped.set(true);
                        }
                });

                return popuped.get();
            }
        }

        return false;
    }

    private final Vector2f helpVector = new Vector2f();

    private void renderSlotPreview(int mouseX, int mouseY, GuiMatrixSlot draggedSlot) {
        float x = mouseX;
        float y = mouseY;
        float invSizeX = startX + cellWidth * matrixWidth;
        float invSizeY = startY + cellHeight * matrixHeight;
        helpVector.x = -1;
        helpVector.y = -1;
        if(x > startX && y > startY && x <= invSizeX && y <= invSizeY) {
            glEnable(GL_SCISSOR_TEST);
            float sx = startX;
            float sy = ScaleGui.screenHeight - (startY + viewHeight);
            float swidth = viewWidth;
            float sheight = viewHeight;
            glScissor((int)sx, (int)sy,(int)swidth , (int)sheight);
            x = startX;
            y = startY - scrollAnim;
            while(x + cellWidth < mouseX) {
                x += cellWidth;
            }
            while(y + cellHeight < mouseY) {
                y += cellHeight;
            }
            if(x + draggedSlot.getWidth() > invSizeX) {
                x = invSizeX - draggedSlot.getWidth();
            }
            if(y + draggedSlot.getHeight() + scrollAnim > invSizeY) {
                y = invSizeY - draggedSlot.getHeight() - scrollAnim;
            }

            AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(x, y, -100, x + draggedSlot.getWidth(), y + draggedSlot.getHeight(), 100);
            boolean isCollided = false;
            for(GuiMatrixSlot slot : slots) {
                if(aabb.intersectsWith(AxisAlignedBB.getBoundingBox(slot.getX() + 1f, slot.getY() + 1f, -10, slot.getX() + slot.getWidth() - 1f, slot.getY() + slot.getHeight() - 1f, 10))) {
                    isCollided = true;
                    break;
                }
            }

            if(!isCollided) {
                helpVector.x = x;
                helpVector.y = y;
            }

            GL11.glDisable(GL_TEXTURE_2D);
            if(isCollided) GL11.glColor4f(0.5f, 0.2f, 0.2f, 0.5f);
            else GL11.glColor4f(0.2f, 0.5f, 0.2f, 0.5f);
            GuiDrawUtils.drawRect(x, y, draggedSlot.getWidth(), draggedSlot.getHeight());
            GL11.glEnable(GL_TEXTURE_2D);
            glDisable(GL_SCISSOR_TEST);
        }
    }

    public void mouseClicked(int mouseX, int mouseY, int mouseButton, GuiMatrixSlot draggedSlot) {
        if (mouseButton == 0) {
            if(draggedSlot == null) {
                handleDragItem(mouseX, mouseY);
            } else {
                handlePutItem(mouseX, mouseY, draggedSlot);
            }

            if(mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
                mouseStartY = mouseY;
                mouseScrolling = true;
                lastScroll = scroll;
            }
        }
    }

    public void mouseClickMove(int mouseY) {
        if(mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if(heightDiff > 0) {
                float diff = scrollTextureHeight / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }
            }
        }
    }

    public void mouseMovedOrUp() {
        mouseScrolling = false;
    }

    public void mouseScroll(int mouseX, int mouseY, int d) {
        if (d != 0) {
            if(mouseX >= startX && mouseY >= startY && mouseX < startX + viewWidth && mouseY < startY + viewHeight) {
                float diff = scrollTotalHeight - scrollViewHeight;
                if(diff > 0) {
                    scroll -= d / 2f;
                    if(scroll < 0) {
                        scroll = 0;
                    } else if(scroll > diff) {
                        scroll = diff;
                    }
                }
            }
        }
    }

    protected void handlePutItem(int mx, int my, GuiMatrixSlot draggedSlot) {
        if (putItem(mx, my, draggedSlot)) {
            int x = MathHelper.floor_float((helpVector.x - startX)) / (int)cellWidth;
            int y = MathHelper.floor_float((helpVector.y - startY + ScaleGui.get(scrollAnim))) / (int)cellHeight;
            parentInventory.putItem(x, y, matrixInventory);
            draggedSlot.setMatrixInventory(matrixInventory);
            parentInventory.setDraggedSlot(null);
            parentInventory.setDraggedSlotXY(-1, -1);
        }
    }

    protected void handleDragItem(int mx, int my) {
        GuiMatrixSlot slot = getDragItemSlot(mx, my);
        if(slot != null && slot.getItemStack() != null) {
            parentInventory.setDraggedSlot(slot);
            parentInventory.setDraggedSlotXY((mx - startX) / cellWidth, (my - startY + scrollAnim) / cellHeight);
        }
    }

    protected GuiMatrixSlot getDragItemSlot(int mx, int my) {
        mouseVector.xCoord = mx;
        mouseVector.yCoord = my;
        if(inventoryAABB.isVecInside(mouseVector)) {
            Iterator<GuiMatrixSlot> iterator = slots.iterator();
            while (iterator.hasNext()) {
                GuiMatrixSlot slot = iterator.next();
                if(slot.clickSlot(mx, my) && parentInventory.canTakeItem(matrixInventory, slot)) {
                    iterator.remove();
                    return slot;
                }
            }
        }
        return null;
    }

    protected boolean putItem(int mx, int my, GuiMatrixSlot slot) {
        if(slot.getItemStack() != null) {
            if(inventoryAABB.isVecInside(Vec3.createVectorHelper(mx, my, 0))) return helpVector.x != -1;
            else return false;
        }
        if(mx >= startX && mx <= startX + cellWidth * matrixWidth && my >= startY && my <= startY + cellHeight * matrixHeight && parentInventory.canPutItem(matrixInventory, slot)) {
            for (float i = startX; i < startX + cellWidth * matrixWidth; i += cellWidth) {
                if (mx >= i && mx < i + cellWidth) {
                    slot.setX(i);
                    break;
                }
            }

            for (float i = startY - scrollAnim; i < startY - scrollAnim + cellHeight * matrixHeight; i += cellHeight) {
                if (my >= i && my < i + cellHeight) {
                    slot.setY(i + scrollAnim);
                    break;
                }
            }

            ItemStack itemStack = slot.getItemStack();
            float w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
            float h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
            slot.setWidth(w * cellWidth);
            slot.setHeight(h * cellHeight);
            slots.add(slot);
            return true;
        }
        return false;
    }

    protected GuiMatrixSlot getClickedSlot(int mx, int my) {
        for (GuiMatrixSlot slot : slots) {
            if(slot.clickSlot(mx, my)) {
                return slot;
            }
        }
        return null;
    }

    public List<GuiMatrixSlot> getSlots() {
        return slots;
    }

    public MatrixInventory getMatrixInventory() {
        return matrixInventory;
    }
}
