package ru.krogenit.hud;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;
import net.smart.moving.SmartMovingContext;
import net.smart.moving.SmartMovingFactory;
import net.smart.moving.SmartMovingSelf;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Project;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.animator.GuiAnimation;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.GuiPopup;
import ru.krogenit.client.gui.api.IGuiWithPopup;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.guns.item.ItemAmmo;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.ItemGunClient;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.guns.render.AbstractMeleeWeaponRenderer;
import ru.krogenit.guns.render.AnimationNode;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.krogenit.skilltree.CoreSkillTree;
import ru.xlv.chat.XlvsChatMod;
import ru.xlv.chat.common.ChatChannelType;
import ru.xlv.chat.gui.GuiChannelChat;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.IItemUsable;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.gui.GameOverlayElement;
import ru.xlv.core.skill.Skill;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.mochar.XlvsMainMod;

import java.util.Collection;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class HudOverlayElement extends GameOverlayElement implements IGuiWithPopup {

	private static final Minecraft mc = Minecraft.getMinecraft();
	private GuiPopup popup;

	@Override
	public void init(ScaledResolution scaledResolution) {
		super.init(scaledResolution);
	}

	@Override
	public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
		if(mouseButton == 0) {
			if(popup != null) popup.mouseClicked(mouseX, mouseY, mouseButton);
		}
	}

	private void renderChat() {
		boolean isChatOpen = mc.currentScreen instanceof GuiChannelChat;
		ChatChannelType selectedChat = GuiChannelChat.chatType;
		GuiNewChat chat = XlvsChatMod.INSTANCE.getChannelChatManager().getChat(selectedChat);
		
		glPushMatrix();
		glTranslatef(-960f, -540f, 0f);
		glRotatef(4f, 0, 1, 0);

		int staticHeight = 1021;
		float elementWidth = 1686 / 2.75f;
		float elementHeight = 708 / 2.75f;
		float aspect = mc.displayWidth / (float) mc.displayHeight;
		float fullHdAspect = ScaleGui.FULL_HD;
		float aspectDiff = (fullHdAspect - aspect) * 450f;
		float elementX = aspect < fullHdAspect ? 90f + aspectDiff: 90f;
		float elementY = staticHeight / 2f - 30;
		GL11.glColor4f(1f, 1f, 1f, 1f);
		if(isChatOpen || GuiNewChat.renderMessage) 
			GuiDrawUtils.drawRect(TextureRegister.textureHUDChatBg, elementX, elementY, elementWidth, elementHeight);
		
		float x = elementX + 2;
		float y = elementY + 16;
		float scrollBGWidth = 130 / 2.75f;
		float scrollBGHeight = 622 / 2.75f;
		if(isChatOpen)
			GuiDrawUtils.drawRect(TextureRegister.textureHUDChatScrollBg, x, y, scrollBGWidth, scrollBGHeight);
		
		glDisable(GL_TEXTURE_2D);
		float viewY = 12;
		int chatHeight = chat.chatLines.size();
		int scroll = chat.getField_146250_j();
    	float diff = viewY / chatHeight;
    	if(diff > 1.0f) diff = 1.0f;
    	float scrollOffsetY = diff < 1 ? chatHeight : 0f;
    	if(scrollOffsetY > 180f)
    		scrollOffsetY = 180f;
		
		x = elementX + 20;
		y = elementY + (diff < 1 ? 16 : 26) + scrollOffsetY - scroll;
		float scrollWidth = 9.f;
		float scrollHeight = (scrollBGHeight - (diff < 1 ? 12 : 22)) - scrollOffsetY;
		
		if(isChatOpen)
			GuiDrawUtils.drawRect(x, y, scrollWidth, scrollHeight);
		glEnable(GL_TEXTURE_2D);

		x = elementX + 17;
		y = elementY + 14;
		float iconWidth = 39 / 2.75f;
		float iconHeight = 29 / 2.75f;
		if(isChatOpen) 
			GuiDrawUtils.drawRect(TextureRegister.textureHUDIconArrow, x, y, iconWidth, iconHeight);
		x = elementX + 17;
		y = elementY + 8 + scrollBGHeight;
		if(isChatOpen) 
			GuiDrawUtils.drawRect(TextureRegister.textureHUDIconArrowDown, x, y, iconWidth, iconHeight);
		
		iconWidth = 366 / 2.75f;
		iconHeight = 96 / 2.75f;
		x = elementX + 50;
		y = elementY;
		float sf = 1.8f;
		if(isChatOpen)
		for(int i=0;i<ChatChannelType.values().length;i++) {
			ChatChannelType type = ChatChannelType.values()[i];
			if(i == selectedChat.ordinal()) GuiDrawUtils.drawRect(TextureRegister.textureHUDChatButton, x, y, iconWidth, iconHeight, 0f, 0f, 0f, 1f);
			else GuiDrawUtils.drawRect(TextureRegister.textureHUDChatButton, x, y, iconWidth, iconHeight);
			FontContainer fontContainer = FontType.HATTEN.getFontContainer();
			GuiDrawUtils.drawStringNoScale(fontContainer, type.getDisplayString(), x + 70 - fontContainer.width(type.getDisplayString()), y + 10, sf, i == selectedChat.ordinal() ? 0xffffff : 0xA0A0A0);
			x += iconWidth - 33;
		}

		iconWidth = 1506 / 2.75f;
		iconHeight = 88 / 2.75f;
		x = elementX + 50;
		y = elementY + 30 + scrollBGHeight;
		if(isChatOpen) 
			GuiDrawUtils.drawRect(TextureRegister.textureHUDChatInput, x, y, iconWidth, iconHeight);
		
		iconWidth = 68 / 2.75f;
		iconHeight = 52 / 2.75f;
		x = elementX + 20;
		y = elementY + 36 + scrollBGHeight;
		glColor4f(1f, 1f, 1f, 1f);
		if(isChatOpen) 
			GuiDrawUtils.drawRect(TextureRegister.textureHUDChatSent, x, y, iconWidth, iconHeight);
		
		glPushMatrix();
		glTranslatef(133f + (aspect < fullHdAspect ? aspectDiff : 0), 698f, 0);
		glScaled(1.7f, 1.7f, 1f);
		chat.drawChatNew(mc.ingameGUI.updateCounter);
		glPopMatrix();

		if(mc.currentScreen instanceof GuiChat) {
			glPushMatrix();
			GuiChat guiChat = (GuiChat) mc.currentScreen;
			guiChat.renderChatInputField();
			glPopMatrix();
		}
		
		glPopMatrix();
	}
	
	private void renderManaElement() {
		glPushMatrix();
		glTranslatef(-960f, -540f, 0f);
		glRotatef(4f, 0, 1, 0);

		int staticHeight = 1021;
		float elementWidth = 1244 / 2.75f;
		float elementHeight = 276 / 2.75f;
		float aspect = mc.displayWidth / (float) mc.displayHeight;
		float fullHdAspect = 1920 / 1080f;
		float elementX = aspect < fullHdAspect ? 90f + (fullHdAspect - aspect) * 450f: 90f;
		float elementY = staticHeight - elementHeight - 30;
		GL11.glColor4f(1f, 1f, 1f, 1f);
		GuiDrawUtils.drawRect(TextureRegister.textureHUDElementLeft, elementX, elementY, elementWidth, elementHeight);

		float manaMaxValue = (float) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.MANA).getMaxValue();
		float manaValue = (float) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.MANA).getValue() / manaMaxValue;
		KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
		glColor4f(0.286f * 4f, 4f, 4f, 1f);
		elementWidth = 391;
		elementHeight = 20;
		GuiDrawUtils.drawRectWithMask(TextureRegister.textureHUDElementMana, elementX + 62, elementY + 0, elementWidth, 20, elementX + 62, elementY + 0, elementWidth * manaValue, elementHeight);

		float iconWidth = 250 / 6f;
		float iconHeight = 252 / 6f;
		GuiDrawUtils.drawRect(TextureRegister.textureHUDIconMana, elementX + 10, elementY - 10, iconWidth, iconHeight);
		KrogenitShaders.forwardPBRDirectionalShaderOld.disable();
		
		float sf = 3.5f;
		glColor4f(1f, 1f, 1f, 1f);
		String s = "" + (int) (manaValue * manaMaxValue);
		GuiDrawUtils.drawStringNoScale(FontType.Marske.getFontContainer(), s, elementX + 12, elementY + 47, sf, 0x222222);
		GuiDrawUtils.drawStringNoScale(FontType.Marske.getFontContainer(), s, elementX + 10, elementY + 45, sf, 0xffffff);
		
		float x = elementX + 110f;

		for(int i=0;i<4;i++) {
			Skill hotActiveSkill = XlvsMainMod.INSTANCE.getClientMainPlayer().getHotActiveSkill(i);
			if(hotActiveSkill != null) {
				String textureName = hotActiveSkill.getTextureName();
				ResourceLocation resLoc = new ResourceLocation(CoreSkillTree.MODID, "textures/hud/" + textureName + "_hud_left_256x256.png");
				float coolDownSeconds = hotActiveSkill.getCooldown() / 1000f;
				float maxCoolDownSeconds = hotActiveSkill.getMaxCooldown() / 1000f;
				float y = elementY + 40;
				iconWidth = 185 / 3.6f;
				iconHeight = 173 / 3.6f;
				if(coolDownSeconds > 0) {
					GuiDrawUtils.drawRect(resLoc, x, y, iconWidth, iconHeight, 0.4f, 0.4f, 0.4f, 0.75f);
					glDisable(GL_TEXTURE_2D);
					float value = coolDownSeconds / maxCoolDownSeconds;
					GuiDrawUtils.drawRect(x + 6 - value * 6f, y + 2 + value * 45f, x + 2, y + iconHeight - 2,
							x + iconWidth - 6, y + iconHeight - 2, x + iconWidth - 2 - value * 6f, y + 2 + value * 45f, 1f, 1f, 1f, 0.3f);
					glEnable(GL_TEXTURE_2D);
					sf = 3.5f;
					int timer = (int) (value * maxCoolDownSeconds);
					s = "" + timer;
					float sl = FontType.Marske.getFontContainer().width(s) * sf / 2f - 8f;
					GuiDrawUtils.drawStringNoScale(FontType.Marske.getFontContainer(), "" + timer, x + iconWidth / 3f + 2 - sl, y + iconHeight / 5f + 2, sf, 0x000000);
					GuiDrawUtils.drawStringNoScale(FontType.Marske.getFontContainer(), "" + timer, x + iconWidth / 3f - sl, y + iconHeight / 5f, sf, 0xffffff);
				} else {
					GuiDrawUtils.drawRect(resLoc, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 0.75f);
				}

				GuiDrawUtils.drawRect(TextureRegister.textureHUDSkillFrame, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 1f);
			}
			x+=iconWidth + 15;
		}
		
		glPopMatrix();
	}
	
	private void renderHPElement() {
		glPushMatrix();
		glTranslatef(960f, -540f, 0f);

		int staticHeight = 1021;
		float elementWidth = 1244 / 2.75f;
		float elementHeight = 276 / 2.75f;
		float aspect = mc.displayWidth / (float) mc.displayHeight;
		float fullHdAspect = 1920 / 1080f;
		float elementX = aspect < fullHdAspect ? -elementWidth - (fullHdAspect - aspect) * 450f : -elementWidth;
		float elementY = staticHeight - elementHeight - 30;
		
		glTranslatef(-90, 0f, 0f);
		glRotatef(-4f, 0, 1, 0);
		GuiDrawUtils.drawRect(TextureRegister.textureHUDElementRight, elementX, elementY, elementWidth, elementHeight);
		glColor4f(0.007f * 4f, 0.988f * 4f, 0.462f * 4f, 1f);

		float maxHealth = mc.thePlayer.getMaxHealth();
		float hpValue = mc.thePlayer.getHealth() / maxHealth;
		KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
		float hpWidth = 383;
		float hpHeight = 21;
		GuiDrawUtils.drawRectWithMask(TextureRegister.textureHUDElementHp, elementX, elementY, hpWidth, hpHeight, elementX + hpWidth * (1.0f - hpValue), elementY, hpWidth * hpValue, hpHeight);

		float iconWidth = 250 / 6f;
		float iconHeight = 252 / 6f;
		GuiDrawUtils.drawRect(TextureRegister.textureHUDIconHp, elementX - iconWidth + elementWidth - 10 , elementY - 10, iconWidth, iconHeight);
		KrogenitShaders.forwardPBRDirectionalShaderOld.disable();
		
//		iconWidth = 250 / 5f;
//		iconHeight = 252 / 5f;
//		glColor4f(1f, 1f, 1f, 1f);
//		float x = elementX - iconWidth + elementWidth - 70;
//		for(int i=0;i<1;i++) {
//			GuiDrawUtils.drawRect(new ResourceLocation(HudMainMod.MODID, "textures/gui/iconBuff.png"), x , elementY - iconHeight, iconWidth, iconHeight);
//			x -= iconWidth + 0f;
//		}
		
		float sf = 3.5f;
		glColor4f(1f, 1f, 1f, 1f);
		String s = "" + (int) (hpValue * maxHealth);
		FontContainer fontContainer = FontType.Marske.getFontContainer();
		float sl = fontContainer.width(s) * sf;
		GuiDrawUtils.drawStringNoScale(fontContainer, s, elementX + elementWidth - 10 - sl, elementY + 47, sf, 0x222222);
		GuiDrawUtils.drawStringNoScale(fontContainer, s, elementX + elementWidth - 12 - sl, elementY + 45, sf, 0xffffff);

		ItemStack itemStack = mc.thePlayer.getCurrentEquippedItem();
		ItemStack gunInHands = null;
		ItemStack meleeInHands = null;
		ItemAmmo gunAmmo;
		int totalAmmo = 0;
		int currentAmmo = 0;
		if(itemStack != null) {
			Item item = itemStack.getItem();
			if (item instanceof ItemGun) {
				ItemGun gun = (ItemGun) item;
				gunInHands = itemStack;
				gunAmmo = gun.getAmmo();
				currentAmmo = gun.getAmmo(itemStack);

				for (int i = 0; i < mc.thePlayer.inventory.mainInventory.length; i++) {
					if (mc.thePlayer.inventory.mainInventory[i] != null && mc.thePlayer.inventory.mainInventory[i].getItem() == gunAmmo) {
						ItemStack ammoStack = mc.thePlayer.inventory.mainInventory[i];
						totalAmmo += gunAmmo.getAmmo(ammoStack);
					}
				}

				MatrixInventory matrixInventory = XlvsMainMod.INSTANCE.getClientMainPlayer().getMatrixInventory();
				Collection<ItemStack> values = matrixInventory.getItems().values();
				for(ItemStack itemStack1 : values) {
					if(itemStack1 != null) {
						//matrix inventory contains null itemStacks
						if (itemStack1.getItem() == gunAmmo) {
							totalAmmo += gunAmmo.getAmmo(itemStack1);
						}
					}
				}
			} else if(item instanceof ItemMeleeWeaponClient) {
				meleeInHands = itemStack;
			}
		}

		if(gunInHands != null) {
			s = "" + currentAmmo;
			sl = fontContainer.width(s) * sf;
			GuiDrawUtils.drawStringNoScale(fontContainer, s, elementX + 12, elementY + 47, sf, 0x222222);
			GuiDrawUtils.drawStringNoScale(fontContainer, s, elementX + 10, elementY + 45, sf, 0xffffff);
			sf = sf / 1.1f;
			s = "|";
			GuiDrawUtils.drawStringNoScale(fontContainer, s, elementX + 16 + sl, elementY + 43, sf, 0x222222);
			GuiDrawUtils.drawStringNoScale(fontContainer, s, elementX + 14 + sl, elementY + 41, sf, 0xffffff);
			sl += fontContainer.width(s) * sf + 3;
			s = "" + totalAmmo;
			sf = 2.75f;
			GuiDrawUtils.drawStringNoScale(fontContainer, s, elementX + 20 + sl, elementY + 43, sf, 0x222222);
			GuiDrawUtils.drawStringNoScale(fontContainer, s, elementX + 18 + sl, elementY + 41, sf, 0xffffff);
			iconWidth = 39 / 3.5f;
			iconHeight = 56 / 3.5f;
			GuiDrawUtils.drawRect(TextureRegister.textureHUDIconBullets, elementX + sl + 35, elementY + 66, iconWidth, iconHeight);
			sl += fontContainer.width(s) * sf;
			renderGun(sl, gunInHands);
		} else if(meleeInHands != null) {
			renderMeleeWeapon(0, meleeInHands);
		}

		iconHeight = 125 / 3.25f;
		float x = elementX + sl + 40;
		float y = elementY + 42 + iconHeight;
		float lineWidth = 20f;
		float lineHeight = 3f;
		glDisable(GL_TEXTURE_2D);
		int selected = mc.thePlayer.inventory.currentItem;
		for(int i=0;i<4;i++) {
			if(i==selected) {
				glColor4f(1.0f, 0.211f, 0.211f, 1.0f);
			} else {
				glColor4f(1.0f, 1f, 1f, 1.0f);
			}
			GuiDrawUtils.drawRect(x , y, lineWidth, lineHeight);
			x += lineWidth + 2f;
		}
		glEnable(GL_TEXTURE_2D);
		glColor4f(1.0f, 1f, 1f, 1.0f);
		glPopMatrix();
	}

	private void renderMeleeWeapon(float xoffset, ItemStack itemStack) {
		glPushMatrix();
		glTranslatef(520 + xoffset, 1544f, 0f);
		AbstractMeleeWeaponRenderer gunRenderer = (AbstractMeleeWeaponRenderer) MinecraftForgeClient.getItemRenderer(itemStack, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON);
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		shader.setUseTexture(false);
		float colorGray = 0.1f;
		shader.setColor(colorGray, colorGray, colorGray, 0.2f);
		glDisable(GL_BLEND);
		if(gunRenderer != null) gunRenderer.renderGunInHud(shader);
		glTranslatef(-3f, -3f, 0f);
		KrogenitShaders.getCurrentPBRShader(false);
		shader.setColor(1f, 1f, 1f, 0.2f);
		if(gunRenderer != null) gunRenderer.renderGunInHud(shader);
		glEnable(GL_BLEND);
		KrogenitShaders.getCurrentPBRShader(false);
		shader.setColor(1f, 1f, 1f, 1f);
		shader.setUseTexture(true);
		KrogenitShaders.finishCurrentShader();
		glPopMatrix();
	}

	private void renderGun(float xoffset, ItemStack itemStack) {
		glPushMatrix();
		glTranslatef(520 + xoffset, 1544f, 0f);
		AbstractItemGunRenderer gunRenderer = (AbstractItemGunRenderer) MinecraftForgeClient.getItemRenderer(itemStack, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON);
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		shader.setUseTexture(false);
		float colorGray = 0.1f;
		shader.setColor(colorGray, colorGray, colorGray, 0.2f);
		glDisable(GL_BLEND);
		if(gunRenderer != null) gunRenderer.renderGunInHud(shader);
		glTranslatef(-3f, -3f, 0f);
		KrogenitShaders.getCurrentPBRShader(false);
		shader.setColor(1f, 1f, 1f, 0.2f);
		if(gunRenderer != null) gunRenderer.renderGunInHud(shader);
		glEnable(GL_BLEND);
		KrogenitShaders.getCurrentPBRShader(false);
		shader.setColor(1f, 1f, 1f, 1f);
		shader.setUseTexture(true);
		KrogenitShaders.finishCurrentShader();
		glPopMatrix();
	}
	
	private void renderCenterElement() {
		glPushMatrix();
		glTranslatef(0f, -540f, 0f);

		int staticHeight = 1021;
		float elementWidth = 1684 / 3f;
		float elementHeight = 226 / 3f;
		float elementX = -elementWidth / 2f;
		float elementY = staticHeight - elementHeight - 10;
		GuiDrawUtils.drawRect(TextureRegister.textureHUDSlotItems, elementX, elementY, elementWidth, elementHeight);
		
		float iconWidth = 213 / 3f;
		float iconHeight = 215 / 3f;
		for(int i=0;i<5;i++) {
			GuiDrawUtils.drawRect(TextureRegister.textureHUDSlotActive, elementX + 4 + i*120, elementY + 1, iconWidth, iconHeight);
		}

		GL11.glColor4f(1f,1f,1f,1f);
		GL11.glDisable(GL_BLEND);
		GL11.glEnable(GL_ALPHA_TEST);
		elementWidth = 1584 / 3f;
		elementHeight = 57 / 3f;
		GuiDrawUtils.drawRect(TextureRegister.textureHUDSlotNumbers, elementX + 32, elementY + 56, elementWidth, elementHeight);
		GL11.glEnable(GL_BLEND);
		GL11.glDisable(GL_ALPHA_TEST);
		float x = elementX + 51;
		float y = elementY + 61;
		float sf = 1.3f;
		for(int i=1;i<6;i++) {
			GuiDrawUtils.drawStringNoScale(FontType.HATTEN.getFontContainer(), "" + i, x, y, sf, 0x000000);
			x += 121;
		}
		glColor4f(4f, 0.905f * 4f, 0.215f * 4f, 1f);
		
		elementWidth = 305;
		elementHeight = 13;
		x = -elementWidth / 2f;
		y = elementY - 60;
		
		SmartMovingSelf moving = (SmartMovingSelf)SmartMovingFactory.getInstance(mc.thePlayer);
		float maxExhaustion = SmartMovingContext.Client.getMaximumExhaustion();
		float fitness = maxExhaustion - Math.min(moving.exhaustion, maxExhaustion);
		float stamina = fitness / 100f;
		KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
		GuiDrawUtils.drawRectWithMask(TextureRegister.textureHUDElementStamina,  x, y, elementWidth, elementHeight,  x, y, elementWidth * stamina, elementHeight);
		iconWidth = 55 / 2.5f;
		iconHeight = 75 / 2.5f;
		x = -elementWidth / 2f - 28;
		y = elementY - 52 - iconHeight / 2f;
		GuiDrawUtils.drawRect(TextureRegister.textureHUDIconStamina, x, y, iconWidth, iconHeight);
		KrogenitShaders.forwardPBRDirectionalShaderOld.disable();
		
		glColor4f(0.1f, 0.1f, 0.1f, 0.29f);
		iconWidth = 80;
		iconHeight = 30;
		x = -iconWidth / 2f;
		y = elementY - 45;
		GuiDrawUtils.drawRect(TextureRegister.textureHUDElementStaminaBg, x , y, iconWidth, iconHeight);
		
		sf = 3f;
		String s = "" + (int) (100f * stamina);
		float sl = FontType.Marske.getFontContainer().width(s) * sf / 2f;
		x = -sl - 0;
		y = elementY - 43;
		GuiDrawUtils.drawStringNoScale(FontType.Marske.getFontContainer(), s, x + 2, y + 2, sf, 0x222222);
		GuiDrawUtils.drawStringNoScale(FontType.Marske.getFontContainer(), s, x, y, sf, 0xffffff);

		ItemStack currentEquippedItem = mc.thePlayer.getCurrentEquippedItem();
		ItemStack currentUsableItem = XlvsMainMod.INSTANCE.getClientMainPlayer().getCurrentUsableItem();
		if(currentEquippedItem != null && currentEquippedItem.getItem() instanceof ItemGunClient) {
			AbstractItemGunRenderer itemRenderer = (AbstractItemGunRenderer) MinecraftForgeClient.getItemRenderer(currentEquippedItem, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON);
			boolean isPlayReloadAnim = itemRenderer.isPlayReloadAnim;
			if(isPlayReloadAnim) {
				x -= 160;
				y -= 80;
				float reloadBarWidth = 365;
				float reloadBarHeight = 25f;
				float reloadProgress = 0f;

				List<AnimationNode> reloadAnimationNodes = itemRenderer.getFullReloadAnimationNodes();
				for(AnimationNode animationNode : reloadAnimationNodes) {
					Vector3f animation = reloadAnimationNodes.get(animationNode.getId()).getAnimation();
					reloadProgress += (animation.x + animation.y + animation.z) / 3f;
				}

				GL11.glDisable(GL_TEXTURE_2D);
				GuiDrawUtils.drawRect(x, y, reloadBarWidth, reloadBarHeight, 0f, 0f, 0f, 0.4f);
				GL11.glEnable(GL_TEXTURE_2D);
				glColor4f(3f, 0.75f, 0.5f, 1f);
				KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
				GuiDrawUtils.drawRectWithMask(TextureRegister.textureHUDElementStamina,  x, y, reloadBarWidth, reloadBarHeight,  x, y, reloadBarWidth * reloadProgress, reloadBarHeight);
				KrogenitShaders.forwardPBRDirectionalShaderOld.disable();
				sf = 1.6f;
				y -= 20f;
				x += reloadBarWidth / 2f;
				s = "ПЕРЕЗАРЯДКА...";
				GuiDrawUtils.drawStringNoScale(FontType.FUTURA_PT_DEMI.getFontContainer(), s, x - FontType.FUTURA_PT_DEMI.getFontContainer().width(s) * sf / 2f + 2, y + 2, sf, 0x222222);
				GuiDrawUtils.drawStringNoScale(FontType.FUTURA_PT_DEMI.getFontContainer(), s, x - FontType.FUTURA_PT_DEMI.getFontContainer().width(s) * sf / 2f, y, sf, 0xffffff);
			}
		} else if(currentUsableItem != null) {
			Item item = currentUsableItem.getItem();
			if(item instanceof IItemUsable) {
				x -= 160;
				y -= 80;
				float progressBarWidth = 365;
				float progressBarHeight = 25f;
				float animation = ((IItemUsable) item).getAnimation(currentUsableItem);
				if(animation > 1f) animation = 1f;
				GL11.glDisable(GL_TEXTURE_2D);
				GuiDrawUtils.drawRect(x, y, progressBarWidth, progressBarHeight, 0f, 0f, 0f, 0.4f);
				GL11.glEnable(GL_TEXTURE_2D);
				glColor4f(0.5f, 3f, 0.75f, 1f);
				KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
				GuiDrawUtils.drawRectWithMask(TextureRegister.textureHUDElementStamina,  x, y, progressBarWidth, progressBarHeight,  x, y, progressBarWidth * animation, progressBarHeight);
				KrogenitShaders.forwardPBRDirectionalShaderOld.disable();
				sf = 1.6f;
				y -= 20f;
				x += progressBarWidth / 2f;
				s = "ПРИМЕНЕНИЕ...";
				GuiDrawUtils.drawStringNoScale(FontType.FUTURA_PT_DEMI.getFontContainer(), s, x - FontType.FUTURA_PT_DEMI.getFontContainer().width(s) * sf / 2f + 2, y + 2, sf, 0x222222);
				GuiDrawUtils.drawStringNoScale(FontType.FUTURA_PT_DEMI.getFontContainer(), s, x - FontType.FUTURA_PT_DEMI.getFontContainer().width(s) * sf / 2f, y, sf, 0xffffff);
			}
		}

		glPopMatrix();
	}
	
	@Override
	public void renderPre(Pre event) {
		if(event.type == ElementType.HOTBAR && (!mc.thePlayer.capabilities.isCreativeMode ||
				mc.currentScreen instanceof GuiAnimation)) {
			event.setCanceled(true);
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			glLoadIdentity();
			glScaled(1f, -1f, 1f);
			float aspect = mc.displayWidth / (float) mc.displayHeight;
			Project.gluPerspective(90.0f, aspect, 0f, 1000f);
			KrogenitShaders.setProjectionMatrix();
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glLoadIdentity();
			glTranslatef(0f, 0f, -500f);

			glEnable(GL_BLEND);
			glAlphaFunc(GL_GREATER, 0.0001f);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDepthMask(false);
			glDisable(GL_ALPHA_TEST);

			renderHPElement();
			if(!(mc.currentScreen instanceof GuiAnimation)) {
				renderManaElement();
				renderCenterElement();
				renderChat();
			}
			
	        glMatrixMode(GL_PROJECTION);
	        glLoadIdentity();
	        glOrtho(0.0D, mc.displayWidth, mc.displayHeight, 0.0D, 1000.0D, 3000.0D);
			KrogenitShaders.setProjectionMatrix();
	    	glMatrixMode(GL_MODELVIEW);
	    	glLoadIdentity();
	    	glTranslatef(0.0F, 0.0F, -2000.0F);

	    	if(popup != null) {
	    		if(popup.width != mc.displayWidth || popup.height != mc.displayHeight) popup.initGui();
	    		popup.drawScreen(event.mouseX, event.mouseY, event.partialTicks);
			}

			glDepthMask(true);
			glEnable(GL_ALPHA_TEST);
			glColor4f(1f, 1f, 1f, 1f);
			glDisable(GL_BLEND);

			float width = mc.displayWidth;
			float height = mc.displayHeight;
			float x = width / 2f - height / 4.0f;
			float y = height - height / 12.6f;
			IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
			for(int i = MatrixInventory.SlotType.HOT_SLOT0.ordinal(); i <= MatrixInventory.SlotType.HOT_SLOT4.ordinal(); i++) {
				MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[i];
				ItemStack itemStack = mc.thePlayer.inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
				if(itemStack != null) {		
					drawItemStack(itemStack, x, y);
				}
				x += height / 8.33f;
			}
			KrogenitShaders.finishCurrentShader();

//			width = ScaleGui.FULL_HD;
//			height = 1080f;
//			float aspectDiff = width - aspect;
//			float heightDiff = height - mc.displayHeight;
//			float addX =  (aspect < width ? aspectDiff * 460f : 0f);// + (mc.displayHeight < height ? heightDiff * 1f : 0f);
////			x = mc.displayWidth / 2f - 810;
//			glEnable(GL_BLEND);
//			glDisable(GL_TEXTURE_2D);
//
//			x = 95;
//			width = 125;
//			height = 33;
//			for(int i=0;i<5;i++) {
//				GuiDrawUtils.drawRect(ScaleGui.getCenterX(x) , ScaleGui.getCenterY(476), ScaleGui.get(width), ScaleGui.get(height), 1f, i / 5f, 0f, 0.2f);
//				x+=width+5;
//				width -= 10;
//			}
//
//			glEnable(GL_TEXTURE_2D);
//			glDisable(GL_BLEND);

			glPopMatrix();
			glMatrixMode(GL_PROJECTION);
			glPopMatrix();
			glMatrixMode(GL_MODELVIEW);
		} else if(event.type == ElementType.FOOD || event.type == ElementType.HEALTH || event.type == ElementType.EXPERIENCE ||
				event.type == ElementType.AIR ||
				(event.type == ElementType.CHAT && !mc.thePlayer.capabilities.isCreativeMode)) {
			event.setCanceled(true);
		}
	}
	
    private static void drawItemStack(ItemStack itemStack, float x, float y) {
		float sx = mc.displayHeight / 70f;
		float sy = mc.displayHeight / 70f;
		float w = mc.displayHeight / 55f;
		GuiDrawUtils.renderItem(itemStack, mc.displayHeight / 380f, -sx, -sy, x, y, w, w, 0f);
	}

	@Override
	public void popupAction(GuiButton button) {
		if(button.id == 0) {
			XlvsGroupMod.INSTANCE.getGroupHandler().acceptInvite();
			setPopup(null);
		} else {
			XlvsGroupMod.INSTANCE.getGroupHandler().declineInvite();
			setPopup(null);
		}
	}

	@Override
	public void setPopup(GuiPopup popup) {
		this.popup = popup;
	}
}
