package ru.krogenit.inventory;

import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StringUtils;
import net.smart.moving.SmartMovingContext;
import net.smart.moving.SmartMovingFactory;
import net.smart.moving.SmartMovingSelf;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.event.EventRenderArmorInventory;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.util.DecimalUtils;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.network.matrix.PacketMatrixPlayerInventorySync;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

public class GuiMatrixPlayerInventory extends GuiMatrixInventoryBase {

    public static CharacterAttributeType[] armorAttributeTypes =
            new CharacterAttributeType[] {CharacterAttributeType.ENERGY_PROTECTION, CharacterAttributeType.THERMAL_PROTECTION,
            CharacterAttributeType.FIRE_PROTECTION, CharacterAttributeType.ELECT_PROTECTION,
            CharacterAttributeType.TOXIC_PROTECTION, CharacterAttributeType.RADIATION_PROTECTION,
            CharacterAttributeType.EXPLOSION_PROTECTION};

    private final TObjectDoubleMap<CharacterAttributeType> armorAttributes = new TObjectDoubleHashMap<>();
    private final TObjectDoubleMap<CharacterAttributeType> armorAttributesAnimation = new TObjectDoubleHashMap<>();
    private final Vector3f lightPosition = new Vector3f(0f, -40f, 100f);
    private final Vector3f lightColor = new Vector3f(2f, 2f, 3f);

    public GuiMatrixPlayerInventory() {
        recalculateProtectionStats();
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixPlayerInventorySync());
    }

    @Override
    public void initGui() {
        super.initGui();
        MatrixInventory matrixInventory = XlvsMainMod.INSTANCE.getClientMainPlayer().getMatrixInventory();
        float startY = 146;
        float x = ScaleGui.getCenterX(1303, 0);
        float y = ScaleGui.getCenterY(startY, 0);
        float cellHeight = 57;
        setInventoryPositionAndSize(x, y, ScaleGui.get(cellHeight), ScaleGui.get(cellHeight));
        setInventory(matrixInventory);
        recalculateProtectionStats();
        scrollViewHeight = 734;
        scrollTotalHeight = cellHeight * matrixHeight;
    }

    @Override
    protected void setInventory(MatrixInventory matrixInventory) {
        super.setInventory(matrixInventory);
        recalculateProtectionStats();
    }

    private void recalculateProtectionStats() {
        armorAttributes.clear();
        for(CharacterAttributeType characterAttribute : armorAttributeTypes) {
            armorAttributes.put(characterAttribute, 0);
        }
        armorAttributes.put(CharacterAttributeType.BALLISTIC_PROTECTION, 0);
        armorAttributes.put(CharacterAttributeType.CUT_PROTECTION, 0);

        for (int i = MatrixInventory.SlotType.HEAD.ordinal(); i <= MatrixInventory.SlotType.FEET.ordinal(); i++) {
            MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[i];
            GuiMatrixSlot guiMatrixSlot = nonMatrixSlots.get(slotType);
            if(guiMatrixSlot != null) {
                ItemStack itemStack = guiMatrixSlot.getItemStack();
                if(itemStack != null && itemStack.getItem() instanceof ItemArmor) {
                    if(draggedSlot != null && draggedSlot.getSlotType() == slotType)
                        continue;
                    ItemArmor itemArmor = (ItemArmor) itemStack.getItem();
                    Map<CharacterAttributeType, CharacterAttributeMod> characterAttributeBoostMap = itemArmor.getCharacterAttributeBoostMap();
                    for(CharacterAttributeType characterAttribute : armorAttributeTypes) {
                        CharacterAttributeMod characterAttributeMod = characterAttributeBoostMap.get(characterAttribute);
                        double value = 0;
                        if(characterAttributeMod != null) value = characterAttributeMod.getValueAdd();
                        armorAttributes.put(characterAttribute, armorAttributes.get(characterAttribute) + value);
                    }
                    double value = itemArmor.getCharacterAttributeBoost(CharacterAttributeType.BALLISTIC_PROTECTION);
                    armorAttributes.put(CharacterAttributeType.BALLISTIC_PROTECTION, armorAttributes.get(CharacterAttributeType.BALLISTIC_PROTECTION) + value);
                    value = itemArmor.getCharacterAttributeBoost(CharacterAttributeType.CUT_PROTECTION);
                    armorAttributes.put(CharacterAttributeType.CUT_PROTECTION, armorAttributes.get(CharacterAttributeType.CUT_PROTECTION) + value);
                }
            }
        }
    }

    private void updateAnimation() {
        for(CharacterAttributeType type : armorAttributes.keySet()) {
            double value = armorAttributes.get(type);
            double animValue = armorAttributesAnimation.get(type);
            if(animValue < value) {
                armorAttributesAnimation.put(type, AnimationHelper.updateSlowEndAnim((float)animValue, (float)value, 0.2f, 0.1f));
            } else if(animValue > value) {
                armorAttributesAnimation.put(type, AnimationHelper.updateSlowEndAnim((float)animValue, (float)value, -0.2f, -0.1f));
            }
        }
    }

    private void renderScroll() {
        if(scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if(scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float x = 1886;
        float y = 154;
        float iconWidth = 7;
        float iconHeight = 4;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvArrowTop, x, y, iconWidth, iconHeight);
        y += iconHeight + 2;
        scrollTextureHeight = 711;
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if(scrollValue > 1) scrollValue = 1f;
        if(scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += iconHeight / 2f + scrollAnim * scrollYValue;
        scrollX = ScaleGui.getCenterX(x, iconWidth);
        scrollY = ScaleGui.getCenterY(y, iconHeight);
        scrollWidth = ScaleGui.get(iconWidth);
        scrollHeight = ScaleGui.get(iconHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvScrollBody, x, y, iconWidth, iconHeight);
        y = 875;
        iconHeight = 4;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvArrowBot, x, y, iconWidth, iconHeight);
    }

    private void renderPlayer() {
        float floorOffsetY = 50;
        float iconWidth = 1366;
        float iconHeight = 356;
        float x = 861;
        float y = 865f + floorOffsetY;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor, x, y, iconWidth, iconHeight);
        iconWidth = 1803;
        iconHeight = 493;
        x = 861;
        y = 815 + floorOffsetY;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor1, x, y, iconWidth, iconHeight);
        iconWidth = 742;
        iconHeight = 1109;
        x = 861;
        y = 350f + floorOffsetY;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerLight, x, y, iconWidth, iconHeight);
        iconWidth = 1385;
        iconHeight = 350;
        x = 861;
        y = 841 + floorOffsetY;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor2, x, y, iconWidth, iconHeight);

        glColor4f(1f, 1f, 1f, 1f);
        x = ScaleGui.getCenterX(861, 0);
        y = ScaleGui.getCenterY(385, 0);
        float scale = ScaleGui.get(315);
        renderPlayer(x, y, scale);
    }

    private void renderCenterElements() {
        renderPlayer();
        String playerName = mc.thePlayer.getDisplayName().toUpperCase();
        float x = 495;
        float y = 95;
        float fs = 60 / 32f;
        float sl = FontType.HelveticaNeueCyrLight.getFontContainer().width(playerName) * fs;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, playerName, x, y, fs, 0x626262);

        String lvl = "LVL " + XlvsMainMod.INSTANCE.getClientMainPlayer().getLvl();
        fs = 30 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, lvl, x + sl + 12.8f, y + 6.418f, fs, 0x626262);
        drawHpManaEnergy();
        drawMiscArmorStats();
    }

    private void drawHpManaEnergy() {
        float x = 515;
        float y = 151;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconHpGaus, x , y, 50, 50);
        y += 48;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconManaGaus, x , y, 55, 55);
        y += 51;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconStaminaGaus, x , y, 45, 65);

        x = 516;
        y = 151;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconHp, x, y, 33, 33);
        y += 48;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconMana, x, y, 40, 40);
        y += 51;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconStamina, x, y, 26, 41);

        x += 30.0f;
        y = 141;
        float fs = 60 / 22f;
        String shp = "" + (int) mc.thePlayer.getHealth();
        float sl = FontType.Marske.getFontContainer().width(shp) * fs;
        GuiDrawUtils.drawStringCenter(FontType.Marske, shp, x, y, fs, 0xffffff);
        fs = 22 / 22f;
        String hpRegen = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.HEALTH_REGEN).getValue();
        float regenOffsetX = 2;
        float regenOffsetY = 6;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, hpRegen, x + sl + regenOffsetX, y + regenOffsetY, fs, 0xffffff);
        fs = 60 / 22f;
        y+= 49;

        String smana = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.MANA).getValue();
        sl = FontType.Marske.getFontContainer().width(smana) * fs;
        GuiDrawUtils.drawStringCenter(FontType.Marske, smana, x, y, fs, 0xffffff);
        fs = 22 / 22f;
        String manaRegen = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.MANA_REGEN).getValue();
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, manaRegen, x + sl + regenOffsetX, y + regenOffsetY, fs, 0xffffff);
        y+= 49;
        fs = 60 / 22f;
        SmartMovingSelf moving = (SmartMovingSelf) SmartMovingFactory.getInstance(mc.thePlayer);
        float maxExhaustion = SmartMovingContext.Client.getMaximumExhaustion();
        float fitness = maxExhaustion - Math.min(moving.exhaustion, maxExhaustion);

        float stamina = fitness / 100f;
        String senergy = "" + (int) (stamina * 100);
        sl = FontType.Marske.getFontContainer().width(senergy) * fs;
        GuiDrawUtils.drawStringCenter(FontType.Marske, senergy, x, y, fs, 0xffffff);
        fs = 22 / 22f;
        String energyRegen = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.STAMINA_REGEN).getValue();
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, energyRegen, x + sl + regenOffsetX, y + regenOffsetY, fs, 0xffffff);
    }

    private void drawMiscArmorStats() {
        float iconWidth = 37;
        float iconHeight = 42;
        float x = 1179;
        float y = 152;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconArmor, x , y, iconWidth, iconHeight);
        y += 49;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCutDefence, x , y, iconWidth, iconHeight);

        y = 149;
        x = 1143;
        float fs = 40 / 22f;
        String armor = DecimalUtils.getFormattedStringWithOneDigit(armorAttributesAnimation.get(CharacterAttributeType.BALLISTIC_PROTECTION));
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBlack, armor, x, y, fs, 0xffffff);
        y += 49;
        String defence = DecimalUtils.getFormattedStringWithOneDigit(armorAttributesAnimation.get(CharacterAttributeType.CUT_PROTECTION));
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBlack, defence, x, y, fs, 0xffffff);

        iconWidth = 13;
        iconHeight = 20;
        x = 1093;
        y = 971;
        fs = 30 / 32f;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconEfficiency, x , y, iconWidth, iconHeight);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "0%", x + 15, y, fs, 0x504F4F);
        iconWidth = 19;
        iconHeight = 20;
        x += 75;
        GL11.glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconResilience, x , y, iconWidth, iconHeight);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "0%", x + 17, y, fs, 0x504F4F);
    }

    private void renderLeftElements() {
        float y = 95;
        float fs = 60 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "СОПРОТИВЛЕНИЕ", 51, y, fs, 0x626262);
//        y += 510;
//        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "СОСТОЯНИЕ", 51, y, fs, 0x626262);
        drawStats();
//        drawState();
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvElementBio, 144, 661, 163, 103);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvElementPower, 286, 673, 79, 79);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvElementLifeSystem, 378, 673, 79, 79);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvElementGrid, 234, 884, 368, 278);
    }

    private void drawState() {
        float x = 85;
        float y = 681;
        float iconWidth = 71;
        float iconHeight = 71;
        float fs = 30 / 32f;
        for(int i=0;i<4;i++) {
            if(i < 3) {
                glColor4f(0.4f, 1f, 0.4f, 1f);
                GuiDrawUtils.drawCenterCentered(new ResourceLocation("inventory", "bg_form.png"), x, y, iconWidth, iconHeight);
                glColor4f(1f, 1f, 1f, 1f);
                GuiDrawUtils.drawCenterCentered(new ResourceLocation("inventory", "icon_stat_energy_big.png"), x, y, iconWidth / 1.5f, iconHeight / 1.5f);
                String sTime = StringUtils.ticksToElapsedTime(100000);
                GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrMedium, sTime, x, y + iconHeight / 2f + 16, fs, 0xffffff);
            } else {
                glColor4f(1f, 0.4f, 0.4f, 1f);
                GuiDrawUtils.drawCenterCentered(new ResourceLocation("inventory", "bg_form.png"), x, y, iconWidth, iconHeight);
                glColor4f(1f, 1f, 1f, 1f);
                GuiDrawUtils.drawCenterCentered(new ResourceLocation("inventory", "icon_stat_fire_big.png"), x, y, iconWidth / 1.5f, iconHeight / 1.5f);
                String sTime = StringUtils.ticksToElapsedTime(50000);
                GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrMedium, sTime, x, y + iconHeight / 2f + 16, fs, 0xffffff);
            }
            x += 85;
            if(x > 292) {
                x = 85;
                y += 116;
            }
        }
    }

    private void drawStats() {
        float x = 65;
        float y = 141;
        float iconWidth = 31;
        float iconHeight = 31;
        float yOffset = 33;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconEnergy, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconHeat, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconFire, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconElectricity, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconToxins, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconRadiation, x, y, iconWidth, iconHeight);
        y += yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconExplosion, x, y, iconWidth, iconHeight);

        x = 96;
        y = 139;
        float fs = 30 / 32f;
        for(CharacterAttributeType characterAttributeType : armorAttributeTypes) {
            GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, characterAttributeType.getDisplayName().toUpperCase(), x, y, fs, 0xB4B4B4);
            y+=33;
        }

        x = 379;
        y = 139;
        fs = 30 / 32f;
        for(CharacterAttributeType characterAttributeType : armorAttributeTypes) {
            GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrMedium, DecimalUtils.getFormattedStringWithOneDigit(armorAttributesAnimation.get(characterAttributeType)), x, y, fs, 0xffffff);
            y+=33;
        }

//        y+=height/13f;
//        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "УСТОЙЧИВОСТЬ К РАДИАЦИИ", x, y, fsx, fsy, 0xB4B4B4);
//        y+=height/yOffset;
//        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "УСТОЙЧИВОСТЬ К ГОРЕНИЮ", x, y, fsx, fsy, 0xB4B4B4);
//        y+=height/yOffset;
//        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "УСТОЙЧИВОСТЬ К ОТРАВЛЕНИЮ", x, y, fsx, fsy, 0xB4B4B4);
//        y+=height/yOffset;
//        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "УСТОЙЧИВОСТЬ К КРОВОТЕЧЕНИЮ", x, y, fsx, fsy, 0xB4B4B4);

//        y+=height/13f;
//        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "23", x, y, fsx, fsy, 0xffffff);
//        y+=height/yOffset;
//        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "88", x, y, fsx, fsy, 0xffffff);
//        y+=height/yOffset;
//        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "20", x, y, fsx, fsy, 0xffffff);
//        y+=height/yOffset;
//        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "44", x, y, fsx, fsy, 0xA72910);

//        y += iconHeight * 2.75f;
//        GuiDrawUtils.drawRect(new ResourceLocation("inventory", "icon_stat_radiation1.png"), x, y, iconWidth, iconHeight);
//        y += iconHeight * 1.1f;
//        GuiDrawUtils.drawRect(new ResourceLocation("inventory", "icon_stat_burning.png"), x, y, iconWidth, iconHeight);
//        y += iconHeight * 1.1f;
//        GuiDrawUtils.drawRect(new ResourceLocation("inventory", "icon_stat_poisoned.png"), x, y, iconWidth, iconHeight);
//        y += iconHeight * 1.1f;
//        GuiDrawUtils.drawRect(new ResourceLocation("inventory", "icon_stat_bleeding.png"), x, y, iconWidth, iconHeight);
    }

    private void renderRightElements(int mouseX, int mouseY, float partialTick) {
        float y = 95;
        float fs = 60 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ИНВЕНТАРЬ", 1303, y, fs, 0x626262);
        drawMoney();
        drawInventory(mouseX, mouseY, partialTick);
    }

    private void drawInventory(int mouseX, int mouseY, float partialTick) {
        float x = startX;
        float y = startY;
        float invWidth = ScaleGui.get(571);
        float invHeight = ScaleGui.get(736);
        GL11.glColor4f(12/255f, 12/255f, 12/255f, 0.6f);
        GL11.glDisable(GL_TEXTURE_2D);
        GuiDrawUtils.drawRect(x, y, invWidth, invHeight);
        glLineWidth(2f);
        glColor4f(1f, 1f, 1f, 0.1f);
        glBegin(GL_LINE_LOOP);
        glVertex2f(x, startY);
        glVertex2f(x + invWidth, startY);
        glVertex2f(x + invWidth, startY + invHeight);
        glVertex2f(x, startY + invHeight);
        glEnd();
        GL11.glEnable(GL_SCISSOR_TEST);
        glLineWidth(1f);
        float sx = ScaleGui.getCenterX(1304, 0);
        float sy = ScaleGui.getCenterY(199, 0);
        float swidth = ScaleGui.get(1304);
        float sheight = ScaleGui.get(736);
        glScissor((int)sx, (int)sy,(int)swidth , (int)sheight);
        y = startY - ScaleGui.get(scrollAnim);
        for (int i = 0; i < matrixWidth; i++) {
            for (int j = 0; j < matrixHeight; j++) {
                glBegin(GL_LINE_LOOP);
                glVertex2f(x, y);
                glVertex2f(x + cellWidth, y);
                glVertex2f(x + cellWidth, y + cellHeight);
                glVertex2f(x, y + cellHeight);
                glEnd();
                y += cellHeight;
            }
            y = startY - ScaleGui.get(scrollAnim);
            x += cellWidth;
        }
        glEnable(GL_TEXTURE_2D);
        GL11.glDisable(GL_SCISSOR_TEST);
        GL11.glColor4f(1f, 1f, 1f, 1f);

        x = 1301;
        y = 916;
        float fs = 30 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ЯЧЕЙКИ БЫСТРОГО ДОСТУПА", x, y, fs, 0x626262);

        float iconWidth = 18;
        float iconHeight = 20;
        x = 1864;
        y = 915;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconWeight, x, y, iconWidth, iconHeight);
        x = 1845;
        fs = 30 / 32f;
        float currentWeight = 0;

        for(GuiMatrixSlot slot : slots) {
            ItemStack itemStack = slot.getItemStack();
            if(itemStack.getItem() instanceof ItemArmor) {
                ItemArmor itemArmor = (ItemArmor) itemStack.getItem();
                double value = itemArmor.getCharacterAttributeBoost(CharacterAttributeType.WEIGHT);
                currentWeight += (float) value;
            }
        }

        String weight = (int) currentWeight + " / " + (int)  XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.WEIGHT).getValue();
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrLight, weight, x, y, fs, 0x585858);

        iconHeight = 129;
        float yOffset = iconHeight + 9;
        iconWidth = 116;
        iconHeight = 3;
        x = 1027;
        y = 322;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);
        y+=yOffset;
        x = 1062;
        iconWidth = 46;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);
        y+=yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);
        iconWidth = 116;
        x = 1027;
        y+=yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);
        y+=yOffset;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvEquipmentLine, x, y, iconWidth, iconHeight);

        GuiDrawUtils.drawPreAlpha(55, 520, 360, 70, 1.2f);

        super.drawScreen(mouseX, mouseY, partialTick);
    }

    private void drawMoney() {
        String platina = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum();
        String credits = "" + XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits();
        float fs = 50 / 32f;
        float slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        float slplatina = FontType.Marske.getFontContainer().width(platina) * fs;

        float creditWidth = 22;
        float creditHeight = 22;
        float platinaWidth = 22;
        float platinaHeight = 22;
        float x = 1873;
        float y = 127;
        GL11.glEnable(GL_BLEND);
        GL11.glAlphaFunc(GL_GREATER, 0.001f);
        GL11.glDisable(GL_ALPHA_TEST);
        GL11.glDepthMask(false);
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, platina, x, y - 4, fs, 0xDFDEDE);
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, credits, x - platinaWidth * 2f - slplatina, y - 4, fs, 0xDFDEDE);
        y = 129;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x - slcredits - slplatina - platinaWidth * 3f, y, creditWidth, creditHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x - slplatina - platinaWidth, y, platinaWidth, platinaHeight);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();
        GL11.glDisable(GL_TEXTURE_2D);
        GL11.glColor4f(0f,0f,0f,0.925f);
        GuiDrawUtils.drawRect(0,0, width, height);
        GL11.glEnable(GL_TEXTURE_2D);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0.0D, width, height, 0.0D, 1000.0D, 3000.0D);
        KrogenitShaders.setProjectionMatrix();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0.0F, 0.0F, -2000.0F);

        glEnable(GL_BLEND);
        glAlphaFunc(GL_GREATER, 0.00001f);

        GL11.glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvBorderLeft, 1273, 540, 1f, 935);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvBorderLeft, 437, 540, 1, 935);
        renderCenterElements();
        renderLeftElements();
        renderScroll();
        renderRightElements(mouseX, mouseY, partialTick);
        if (itemDropdownMenu != null && itemDropdownMenu.isHovered()) {
            itemDropdownMenu.render(this, mouseX, mouseY, partialTick);
        }
//        glEnable(GL_BLEND);
//        if(mouseX > width / 37.647f && mouseY > height / 2.4713f && mouseX < width / 4.3935f && mouseY < height / 2.3126f) {
//            glColor4f(0.0f, 0.0f, 0.0f, 0.9f);
//            x = mouseX;
//            y = mouseY;
//            fsx = width / 1097f * 1.15f;
//            fsy = height / 617f * 1.15f;
//            renderToolTipSkill(x, y, x + width / 3.7f, y + height / 4.75f);
//            float textOffset = width / 40f;
//            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "УСТОЙЧИВОСТЬ К ГОРЕНИЮ", x + textOffset, y + height / 23f, fsx, fsy, 0xffffff);
//            iconWidth = width / 7.19f;
//            iconHeight = 1f;
//            GuiDrawUtils.drawRect(new ResourceLocation("inventory", "tooltip_border.png"), x + textOffset , y + height / 13f, iconWidth, iconHeight);
//
//            fsx = width / 1097f / 2.1f;
//            fsy = height / 617f / 2f;
//            String gor = EnumChatFormatting.GOLD + "горения " + EnumChatFormatting.WHITE;
//            String info = "Защитная характеристика персонажа, отвечающая за сопротивление накопительному эффекту " + gor + "на персонаже.\n\nПолучая урон от огня персонаж может загореться. " +
//                    "Эффекты "+ gor + "могут суммироваться друг с другом увеличивая получаемый от " + gor + "урон.\n\n\n" + EnumChatFormatting.GOLD + "Устойчивость к горению " + EnumChatFormatting.WHITE + "позволяет умньшить шанс игрока загореться.";
//            DrawUtils.drawScaledSplitStringWords(FontType.HelveticaNeueCyrLight, info, x + textOffset, y + height / 10f, fsx, fsy, 510f, -1, 0xffffff);
//        }
    }

    private void renderToolTipSkill(float posX, float posY, float posX2, float posY2) {
        float mouseOffsetX = width / 100f;
        posX += mouseOffsetX;
        posX2 += mouseOffsetX;
        float cutX = width / 60f;
        float cutY = height / 40f;
        glBegin(GL_POLYGON);
        glVertex2f(posX, posY);
        glVertex2f(posX2, posY2 - cutY);
        glVertex2f(posX2, posY);
        glVertex2f(posX, posY);
        glVertex2f(posX2 - cutX, posY2);
        glVertex2f(posX2, posY2 - cutY);
        glVertex2f(posX, posY);
        glVertex2f(posX, posY2);
        glVertex2f(posX2 - cutX, posY2);
        glEnd();
    }

    private void renderPlayer(float x, float y, float scale) {
        GuiDrawUtils.drawPlayer(mc.thePlayer, x, y, 0f, scale, 180f, 0.25f, 0.25f, 0.25f, lightPosition, lightColor,
                new EventRenderArmorInventory(mc.thePlayer, (RenderPlayer) RenderManager.instance.getEntityRenderObject(mc.thePlayer), draggedSlot != null && draggedSlot.getSlotType() != null ? draggedSlot.getSlotType().ordinal() : -1));
    }

    @Override
    protected boolean handlePutItem(int mx, int my) {
        if(super.handlePutItem(mx, my)) {
            recalculateProtectionStats();
            return true;
        }

        return false;
    }

    @Override
    protected void handleDragItem(int mx, int my) {
        super.handleDragItem(mx, my);
        if(draggedSlot != null) {
            recalculateProtectionStats();
        }
    }
}
