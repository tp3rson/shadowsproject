package ru.krogenit.inventory;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.inventory.popup.GuiPopupRenderer;
import ru.krogenit.util.ColorUtils;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryUtils;

import static org.lwjgl.opengl.GL11.*;

@Getter
@Setter
public class GuiMatrixSlot {

    protected float x, y, width, height, baseX, baseY;
    protected ItemStack itemStack;
    protected MatrixInventory.SlotType slotType;
    protected ResourceLocation slotTexture;
    protected static Minecraft mc = Minecraft.getMinecraft();
    protected MatrixInventory matrixInventory;

    public GuiMatrixSlot(float x, float y, float width, float height, ItemStack itemStack, MatrixInventory matrixInventory) {
        this.x = this.baseX = x;
        this.y = this.baseY = y;
        this.width = width;
        this.height = height;
        this.itemStack = itemStack;
        this.matrixInventory = matrixInventory;
    }

    public GuiMatrixSlot(float x, float y, float width, float height, ItemStack itemStack, MatrixInventory.SlotType slotType, MatrixInventory matrixInventory) {
        this.x = this.baseX = x;
        this.y = this.baseY = y;
        this.width = width;
        this.height = height;
        this.itemStack = itemStack;
        this.slotType = slotType;
        this.matrixInventory = matrixInventory;
    }

    public void render(int mx, int my, boolean invHovered) {
        if(itemStack != null) {
            glDisable(GL_TEXTURE_2D);
            glShadeModel(GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            Vector3f color = ColorUtils.getColorByRarity(itemStack.getItem());
            if(invHovered && isHovered(mx, my)) tessellator.setColorRGBA_F(color.x, color.y, color.z, 1f);
            else tessellator.setColorRGBA_F(color.x, color.y, color.z, 0.5f);
            tessellator.addVertex(x, y + height, 0);
            tessellator.addVertex(x + width, y + height, 0);
            tessellator.setColorRGBA_F(0f, 0f, 0f, 0f);
            tessellator.addVertex(x + width, y, 0);
            tessellator.addVertex(x, y, 0);
            tessellator.draw();
            glShadeModel(GL_FLAT);
            glLineWidth(1f);
            glColor4f(98/255f, 98/255f, 98/255f, 1.0f);
            glBegin(GL_LINE_STRIP);
            glVertex2f(x, y);
            glVertex2f(x + width, y);
            glVertex2f(x + width, y + height);
            glVertex2f(x, y + height);
            glVertex2f(x, y);
            glEnd();
            glEnable(GL_TEXTURE_2D);
            renderItem(mx, my);
        } else if(slotTexture != null) {
            GuiDrawUtils.drawRect(slotTexture, x, y, width, height);
        }
    }

    public void renderItem(int mx, int my) {
        float offset = 0f;
        GuiDrawUtils.renderItem(itemStack, 1.0f, offset, offset, x, y, width, height, MatrixInventoryUtils.getInvMatrixRotation(itemStack) * 90f);
    }

    public void renderPopup(int mx, int my) {
        if(itemStack != null) GuiPopupRenderer.drawPopup(mx, my, itemStack);
    }

    public boolean isHovered(int mx, int my) {
        return mx >= x && mx < x + width && my >= y && my < y + height;
    }

    public MatrixInventory.SlotType getSlotType() {
        return slotType;
    }

    public boolean clickSlot(int mx, int my) {
        return mx >= x && mx <= x + width && my >= y && my <= y + height;
    }

    public GuiMatrixSlot copyEmpty() {
        GuiMatrixSlot slot = new GuiMatrixSlot(x, y, width, height, null, matrixInventory);
        slot.setSlotTexture(slotTexture);
        return slot;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = this.baseX = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = this.baseY = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public void setSlotTexture(ResourceLocation slotTexture) {
        this.slotTexture = slotTexture;
    }

    public ResourceLocation getSlotTexture() {
        return slotTexture;
    }

    public void addYPosition(float y) {
        this.y = baseY + y;
    }
}
