package ru.krogenit.shop.pages;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shop.GuiShop;
import ru.krogenit.shop.item.GuiShopItem;
import ru.krogenit.shop.item.GuiShopPack;
import ru.krogenit.shop.item.ShopItem;
import ru.krogenit.shop.item.ShopPack;
import ru.krogenit.utils.AnimationHelper;

import java.util.ArrayList;
import java.util.List;

public class GuiPageShop extends AbstractGuiScreenAdvanced {

    private final GuiShop shop;
    private final ShopItem[] shopItems;
    private final ShopPack[] shopPacks;
    private final List<GuiShopItem> items = new ArrayList<>();
    private final List<GuiShopPack> packs = new ArrayList<>();
    private int selectedPack;

    private float scroll, scrollAnim, lastScroll;
    private float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    private float scrollX, scrollY, scrollWidth, scrollHeight;
    private boolean mouseScrolling;
    private float mouseStartY;
    private final Vector3f lightColor = new Vector3f(1f, 1f, 1f);
    private final Vector3f lightPosition = new Vector3f(0f, 0f, 15f);

    public GuiPageShop(GuiShop shop, ShopItem[] shopItems, ShopPack[] shopPacks) {
        super(ScaleGui.FULL_HD);
        this.shop = shop;
        this.selectedPack = 0;
        this.shopPacks = shopPacks;
        this.shopItems = shopItems;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        scrollViewHeight = ScaleGui.get(802);
        items.clear();
        packs.clear();
        float x = 1158;
        float y = 451;
        float width = 19;
        float height = 18;
        if(shopPacks.length > 0) {
            float buttonXSize = (shopPacks.length * width + (shopPacks.length - 1) * 4) / 2f;
            x -= buttonXSize;
            for (int i = 0; i < shopPacks.length; i++) {
                GuiButtonAdvanced b = new GuiButtonAdvanced(i, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), "");
                b.setTexture(i == selectedPack ? TextureRegister.textureShopMainSliderActive : TextureRegister.textureShopMainSlider);
                buttonList.add(b);
                x += 22;
            }
        }

        x = 610;
        y = 181;
        float startY = y;
        if(shopPacks.length > 0) {
            width = 1096;
            height = 299;
            for (ShopPack shopPack : shopPacks) {
                packs.add(new GuiShopPack(0, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), shopPack));
            }
            y += 331;
        }

        float startX = x;
        float offX = 279;
        float offY = 414;
        width = 261;
        height = 382;
        for (ShopItem shopItem : shopItems) {
            if (x >= 1705) {
                y += offY;
                x = startX;
            }
            items.add(new GuiShopItem(0, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), shopItem, this));
            x += offX;
        }
        y += offY + 35;
        scrollTotalHeight = ScaleGui.get(y - startY);
        lightPosition.x = ScaleGui.screenWidth / 250f;
        lightPosition.y = ScaleGui.screenHeight / 400f;
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        ((GuiButtonAdvanced)buttonList.get(selectedPack)).setTexture(TextureRegister.textureShopMainSlider);
        selectedPack = guiButton.id;
        ((GuiButtonAdvanced)buttonList.get(selectedPack)).setTexture(TextureRegister.textureShopMainSliderActive);
    }

    public void tryBuyItemCredits(ShopItem item) {
        shop.tryBuyItemCredits(item);
    }

    public void tryBuyItemPlatina(ShopItem item) {
        shop.tryBuyItemPlatina(item);
    }

    private void actionPerformed(GuiShopItem item) {

    }

    private void actionPerformed(GuiShopPack pack) {

    }

    private void drawScroll() {
        if(scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if(scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float x = ScaleGui.getCenterX(1720);
        float y = ScaleGui.getCenterY(181);
        float iconWidth = ScaleGui.get(7);
        float iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(TextureRegister.textureInvArrowTop, x, y, iconWidth, iconHeight);
        y += iconHeight + ScaleGui.get(3);
        scrollTextureHeight = ScaleGui.get(712);
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if(scrollValue > 1) scrollValue = 1f;
        if(scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += scrollAnim * scrollYValue;
        scrollX = x;
        scrollY = y;
        scrollWidth = iconWidth;
        scrollHeight = iconHeight;
        GuiDrawUtils.drawRect(TextureRegister.textureInvScrollBody, x, y, iconWidth, iconHeight);
        y = ScaleGui.getCenterY(903);
        iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(TextureRegister.textureInvArrowBot, x, y, iconWidth, iconHeight);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawScroll();

        if(packs.size() > 0) {
            for(GuiShopPack pack : packs) {
                pack.addYPosition(-scrollAnim);
            }
            packs.get(selectedPack).draw(mouseX, mouseY);
        }

        for(GuiButton guiButton : buttonList) {
            GuiButtonAdvanced b = (GuiButtonAdvanced) guiButton;
            b.addYPosition(-scrollAnim);
        }
        drawButtons(mouseX, mouseY, partialTick);

        KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        KrogenitShaders.forwardPBRDirectionalShaderOld.setModelView();
        GL11.glPopMatrix();
        KrogenitShaders.forwardPBRDirectionalShaderOld.setLightColor(lightColor.x, lightColor.y, lightColor.z);
        KrogenitShaders.forwardPBRDirectionalShaderOld.setLightPos(lightPosition.x, lightPosition.y, lightPosition.z);
        KrogenitShaders.forwardPBRDirectionalShaderOld.disable();
        for(GuiShopItem item : items) {
            item.addYPosition(-scrollAnim);
            if(shop.getPageAABB().intersectsWith(AxisAlignedBB.getBoundingBox(item.xPosition, item.yPosition, 0, item.xPosition + item.width, item.yPosition + item.height, 0)))
                item.draw(mouseX, mouseY);
        }

        if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            GL11.glDisable(GL11.GL_SCISSOR_TEST);
            for (GuiShopItem item : items) {
                if (shop.getPageAABB().intersectsWith(AxisAlignedBB.getBoundingBox(item.xPosition, item.yPosition, 0, item.xPosition + item.width, item.yPosition + item.height, 0))) {
                    item.drawPopup(mouseX, mouseY);
                }
            }
            GL11.glEnable(GL11.GL_SCISSOR_TEST);
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (mouseButton == 0) {
            for (GuiButton guibutton : this.buttonList) {
                if (guibutton.mousePressed(this.mc, mouseX, mouseY)) {
                    this.selectedButton = guibutton;
                    guibutton.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(guibutton);
                    return;
                }
            }

            for(GuiShopItem slot : items) {
                if(slot.mousePressed(mouseX, mouseY)) {
                    this.actionPerformed(slot);
                    return;
                }
            }

            if(packs.size() > 0) {
                GuiShopPack guiShopPack = packs.get(selectedPack);
                if (guiShopPack.mousePressed(mouseX, mouseY)) {
                    this.actionPerformed(guiShopPack);
                    return;
                }
            }

            if(mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
                mouseStartY = mouseY;
                mouseScrolling = true;
                lastScroll = scroll;
            }
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        if(mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if(heightDiff > 0) {
                float diff = scrollTextureHeight / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }
            }
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        mouseScrolling = false;
    }

    @Override
    public void scrollInput(int mouseX, int mouseY, int d) {
        if (d != 0) {
            float diff = scrollTotalHeight - scrollViewHeight;
            if(diff > 0) {
                scroll -= d / 2f;
                if(scroll < 0) {
                    scroll = 0;
                } else if(scroll > diff) {
                    scroll = diff;
                }
            }
        }
    }
}
