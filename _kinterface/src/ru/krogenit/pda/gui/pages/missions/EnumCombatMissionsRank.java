package ru.krogenit.pda.gui.pages.missions;

public enum EnumCombatMissionsRank {
    NOVICE("НОВОБРАНЕЦ"), MARINE("МОРПЕХ");

    String localized;

    EnumCombatMissionsRank(String localized) {
        this.localized = localized;
    }

    public String getLocalizedName() {
        return localized;
    }
}
