package ru.krogenit.pda.gui.pages.map;

import lombok.Getter;
import org.lwjgl.util.vector.Vector2f;

import java.util.List;

@Getter
public class NpcMapPin extends MapPin {
    private final List<String> description;

    public NpcMapPin(Vector2f position, String name, List<String> description) {
        super(name, EnumPinType.NPC_TRADER, position);
        this.description = description;
    }
}
