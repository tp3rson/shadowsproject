package ru.krogenit.pda.gui.pages.map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.lwjgl.util.vector.Vector2f;

@AllArgsConstructor
@Getter
public class MapPin {
    private final String pinName;
    private final EnumPinType pinType;
    private final Vector2f position;
}
