package ru.krogenit.pda.gui.pages.map;

import ru.krogenit.client.gui.api.GuiButtonAdvanced;

public abstract class GuiMapPin extends GuiButtonAdvanced {

    private final EnumPinType pinType;
    private final MapPin pin;

    public GuiMapPin(float x, float y, float widthIn, float heightIn, MapPin pin, String name) {
        super(-1, x, y, widthIn, heightIn, name);
        this.pin = pin;
        this.pinType = pin.getPinType();
    }

    abstract void drawPopup(int mouseX, int mouseY);

    public EnumPinType getPinType() {
        return pinType;
    }

    public MapPin getPin() {
        return pin;
    }
}
