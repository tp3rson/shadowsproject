package ru.krogenit.pda.gui.pages.map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

public class GuiMapPinNpc extends GuiMapPin {

    private final NpcMapPin npcPin;

    public GuiMapPinNpc(float x, float y, float widthIn, float heightIn, NpcMapPin pin) {
        super(x, y, widthIn, heightIn, pin, pin.getPinName());
        this.npcPin = pin;
        setTexture(TextureRegister.texturePDAMapPinNPC);
    }

    public void drawPopup(int mouseX, int mouseY) {
        boolean isHovered = isHovered(mouseX, mouseY);
        if(isHovered) {
            float descOffsetY = ScaleGui.get(13f);
            float x1 = xPosition + ScaleGui.get(45f);
            float yRightUp = yPosition - ScaleGui.get(13f);
            float totalDescOffset = npcPin.getDescription().size() * descOffsetY - (ScaleGui.get(3f));
            float yRightDown = yRightUp + totalDescOffset + (ScaleGui.get(49f));

            float textOffsetX = ScaleGui.get(14f);
            float textOffsetY = ScaleGui.get(19f);
            float fsMain = ScaleGui.get(30 / 32f);
            float fsDesc = ScaleGui.get(23 / 32f);
            float sl = FontType.HelveticaNeueCyrRoman.getFontContainer().width(displayString) * fsMain;

            for(String desc : npcPin.getDescription()) {
                float sl1 = FontType.HelveticaNeueCyrRoman.getFontContainer().width(desc) * fsDesc;
                if(sl1 > sl) {
                    sl = sl1;
                }
            }

            float x2 = x1 + sl + ScaleGui.get(27f);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(12/255f, 76/255f, 91/255f, 0.9f);
            tessellator.addVertex(x1, yRightDown,0f);
            tessellator.addVertex(x2, yRightDown, 0);
            tessellator.addVertex(x2, yRightUp, 0f);
            tessellator.addVertex(x1, yRightUp, 0f);
            tessellator.draw();

            GL11.glColor4f(55/255f, 140/255f, 187/255f, 1f);
            float lineWidth = ScaleGui.get(2f);
            GL11.glLineWidth(lineWidth);
            GL11.glBegin(GL11.GL_LINE_STRIP);
            GL11.glVertex2f(x1, yRightDown);
            GL11.glVertex2f(x2, yRightDown);
            GL11.glVertex2f(x2, yRightUp);
            GL11.glVertex2f(x1, yRightUp);
            GL11.glVertex2f(x1, yRightDown);
            GL11.glEnd();
            GL11.glEnable(GL11.GL_TEXTURE_2D);

            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman, displayString, x1 + textOffsetX, yRightUp + textOffsetY, 30 / 32f, 0xffffff);
            textOffsetY += ScaleGui.get(19f);

            for(String desc : npcPin.getDescription()) {
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman,  desc, x1 + textOffsetX, yRightUp + textOffsetY, 23 / 32f, 0xffffff);
                textOffsetY += descOffsetY;
            }
        }
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if(isHovered(mouseX, mouseY)) {
            float descOffsetY = ScaleGui.get(13f);
            float x = xPosition + width / 2f;
            float x1 = xPosition + ScaleGui.get(45f);
            float yLeftUp = yPosition + ScaleGui.get(2f);
            float yRightUp = yPosition - ScaleGui.get(13f);
            float totalDescOffset = npcPin.getDescription().size() * descOffsetY - (ScaleGui.get(3f));
            float yRightDown = yRightUp + totalDescOffset + (ScaleGui.get(49f));
            float yLeftDown = yPosition + (ScaleGui.get(36f));
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(0f, 180 / 255f, 219 / 255f, 0.5f);
            tessellator.addVertex(x, yLeftDown, 0f);
            tessellator.setColorRGBA_F(12 / 255f, 76 / 255f, 91 / 255f, 0.63f);
            tessellator.addVertex(x1, yRightDown, 0);
            tessellator.addVertex(x1, yRightUp, 0f);
            tessellator.setColorRGBA_F(0f, 180 / 255f, 219 / 255f, 0.5f);
            tessellator.addVertex(x, yLeftUp, 0f);
            tessellator.draw();
            GL11.glShadeModel(GL11.GL_FLAT);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
        }

        GL11.glColor4f(1f,1f,1f,1f);
        Utils.bindTexture(texture);
        GuiDrawUtils.drawRect(xPosition, yPosition, width, height);
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAMapIconMoney, xPosition + width / 2f,
                yPosition + ScaleGui.get(20f), ScaleGui.get(18f) , ScaleGui.get(18f));
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        return false;
    }
}
