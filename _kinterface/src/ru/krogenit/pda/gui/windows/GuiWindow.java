package ru.krogenit.pda.gui.windows;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

public abstract class GuiWindow extends AbstractGuiScreenAdvanced {

    protected EnumWindowType windowType;

    protected GuiPda pda;
    protected float firstPoint, secondPoint, fs1, fs2, collapseAnim;
    protected float windowCenterX, windowCenterY, windowWidth, scaledWidth, windowHeight, scaledHeight, halfWidth, halfHeight, topHeight;
    protected float windowCenterXAnim, windowCenterYAnim;
    protected float windowX, windowY, windowXAnim, windowYAnim;
    private ResourceLocation backgroundTexture;

    protected final GuiScroll guiScroll = new GuiScroll();

    protected int scissorX, scissorY, scissorWidth, scissorHeight;
    protected AxisAlignedBB scissorAABB;

    private final Vector2f mouseLastClick = new Vector2f(0, 0);
    private boolean movingByMouse;
    private boolean isCollapsed;
    private boolean isActive;

    private String header, leftCornerDesc;

    public GuiWindow(EnumWindowType windowType, GuiPda pda, float width, float height) {
        super(ScaleGui.FULL_HD);
        this.windowType = windowType;
        this.pda = pda;
        this.windowWidth = width;
        this.windowHeight = height;
        this.collapseAnim = 1f;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();
        this.scaledWidth = ScaleGui.get(windowWidth);
        this.scaledHeight = ScaleGui.get(windowHeight);
        this.windowCenterX = windowCenterXAnim = ScaleGui.screenCenterX;
        this.windowCenterY = windowCenterYAnim = ScaleGui.screenCenterY;
        this.halfWidth = scaledWidth / 2f;
        this.halfHeight = scaledHeight / 2f;
        this.windowX = windowXAnim = windowCenterX - halfWidth;
        this.windowY = windowYAnim = windowCenterY - halfHeight;
        this.topHeight = ScaleGui.get(52, 0);
        initControlButtons();
    }

    private void initControlButtons() {
        float buttonWidth = 29;
        float buttonHeight = 29;
        float x = scaledWidth - ScaleGui.get(87);
        float y = ScaleGui.get(17);
        GuiButtonAdvanced button = new GuiButtonAdvanced(0, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        button.setTexture(TextureRegister.texturePDAButtonCollapse);
        button.setTextureHover(TextureRegister.texturePDAButtonCollapseHover);
        buttonList.add(button);
        x += ScaleGui.get(36);
        button = new GuiButtonAdvanced(1, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        button.setTexture(TextureRegister.texturePDAButtonClose);
        button.setTextureHover(TextureRegister.texturePDAButtonCloseHover);
        buttonList.add(button);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if (b.id == 0) {
            collapseWindow();
        } else if (b.id == 1) {
            closeWindow();
        }
    }

    protected void collapseWindow() {
        pda.collapseWindow(this);
        isCollapsed = true;
    }

    protected void closeWindow() {
        pda.closeWindow(this);
    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if (firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
            if (fs1 > 0.9f) {
                secondPoint = AnimationHelper.updateSlowEndAnim(secondPoint, 1f, 0.1f, 0.0001f);
                if (secondPoint > 0.9f) {
                    fs2 = AnimationHelper.updateSlowEndAnim(fs2, 1f, 0.1f, 0.0001f);
                }
            }
        }

        float collapsedX = -mc.displayWidth / 2f * (1.5f - collapseAnim * 1.5f);
        float collapsedY = mc.displayHeight * (1.5f - collapseAnim * 1.5f);

        if (windowCenterXAnim < windowCenterX + collapsedX) {
            windowCenterXAnim = AnimationHelper.updateSlowEndAnim(windowCenterXAnim, windowCenterX + collapsedX, 0.25f, 0.1f);
            windowXAnim = AnimationHelper.updateSlowEndAnim(windowXAnim, windowX + collapsedX, 0.25f, 0.1f);
        } else if (windowCenterXAnim > windowCenterX + collapsedX) {
            windowCenterXAnim = AnimationHelper.updateSlowEndAnim(windowCenterXAnim, windowCenterX + collapsedX, -0.25f, -0.1f);
            windowXAnim = AnimationHelper.updateSlowEndAnim(windowXAnim, windowX + collapsedX, -0.25f, -0.1f);
        }

        if (windowCenterYAnim < windowCenterY + collapsedY) {
            windowCenterYAnim = AnimationHelper.updateSlowEndAnim(windowCenterYAnim, windowCenterY + collapsedY, 0.25f, 0.1f);
            windowYAnim = AnimationHelper.updateSlowEndAnim(windowYAnim, windowY + collapsedY, 0.25f, 0.1f);
        } else if (windowCenterYAnim > windowCenterY + collapsedY) {
            windowCenterYAnim = AnimationHelper.updateSlowEndAnim(windowCenterYAnim, windowCenterY + collapsedY, -0.25f, -0.1f);
            windowYAnim = AnimationHelper.updateSlowEndAnim(windowYAnim, windowY + collapsedY, -0.25f, -0.1f);
        }

        if (isCollapsed) {
            collapseAnim = AnimationHelper.updateSlowStartAnim(collapseAnim, 0f, -0.75f, -0.15f);
        } else {
            collapseAnim = AnimationHelper.updateSlowEndAnim(collapseAnim, 1.00f, 0.5f, 0.025f);
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();

        if (isCollapsed && collapseAnim <= 0f) {
            return;
        }

        for (GuiButton b : buttonList) {
            if (b instanceof GuiButtonAdvanced) {
                GuiButtonAdvanced b1 = (GuiButtonAdvanced) b;
                b1.addXPosition(windowXAnim);
                b1.addYPosition(windowYAnim);
                b1.scaleWidth(fs1 * collapseAnim);
            }
        }

        GL11.glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawRect(backgroundTexture, windowXAnim, windowYAnim, scaledWidth, scaledHeight * firstPoint * collapseAnim);

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        float x = windowXAnim + ScaleGui.get(23);
        float y = windowYAnim + ScaleGui.get(20);
        GuiDrawUtils.drawRect(x, y, ScaleGui.get(11) * collapseAnim, ScaleGui.get(11) * collapseAnim);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        float fs = 30 / 32f * fs2 * collapseAnim;
        x += ScaleGui.get(20f);
        y += ScaleGui.get(4f);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, header, x, y, fs, 0xffffff);

        x = windowXAnim + ScaleGui.get(40);
        y = windowCenterYAnim + halfHeight - ScaleGui.get(14) - scaledHeight * (1.0f - collapseAnim);
        fs = 18 / 32f * fs2 * collapseAnim;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, leftCornerDesc, x, y, fs, 0xffffff);

        drawButtons(mouseX, mouseY, partialTick);
    }

    public boolean tryActivateWindow(int mouseX, int mouseY, int mouseButton) {
        if (isCollapsed) return false;
        if (mouseButton == 0) {
            return mouseX > windowX && mouseY > windowY && mouseX < windowCenterX + halfWidth && mouseY < windowCenterY + halfHeight;
        }

        return false;
    }

    public boolean mouseClickedWindow(int mouseX, int mouseY, int mouseButton) {
        if (isCollapsed) return false;
        if (mouseButton == 0) {
            for (GuiButton guibutton : this.buttonList) {
                if (guibutton.mousePressed(this.mc, mouseX, mouseY)) {
                    this.selectedButton = guibutton;
                    guibutton.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(guibutton);
                    return true;
                }
            }

            if (guiScroll.mouseClicked(mouseX, mouseY, 0)) {
                return true;
            } else if (!movingByMouse && mouseX > windowX && mouseY > windowY && mouseX < windowCenterX + halfWidth && mouseY < windowCenterY - halfHeight + topHeight) {
                movingByMouse = true;
                mouseLastClick.x = mouseX;
                mouseLastClick.y = mouseY;
                return true;
            } else
                return mouseX > windowX && mouseY > windowY && mouseX < windowCenterX + halfWidth && mouseY < windowCenterY + halfHeight;
        }

        return false;
    }

    public boolean mouseClickMoveWindow(int mouseX, int mouseY) {
        if (isCollapsed) return false;
        if (movingByMouse) {
            float dx = mouseX - mouseLastClick.x;
            float dy = mouseY - mouseLastClick.y;
            windowCenterX += dx;
            windowCenterY += dy;
            windowX += dx;
            windowY += dy;
            mouseLastClick.x = mouseX;
            mouseLastClick.y = mouseY;
            return true;
        }

        return guiScroll.mouseClickMove(mouseX, mouseY);
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        if (isCollapsed) return;
        if (movingByMouse) movingByMouse = false;
        guiScroll.mouseMovedOrUp();
    }

    public boolean scrollInputWithReturn(int mouseX, int mouseY, int d) {
        if (isCollapsed) return false;

        if (d != 0) {
            if (mouseX > windowX && mouseY > windowY && mouseX < windowCenterX + halfWidth && mouseY < windowCenterY + halfHeight) {
                guiScroll.mouseScroll(d);
                return true;
            }
        }

        return false;
    }

    public void onWindowClosed() {

    }

    public void onWindowCollapsed() {
        isActive = false;
    }

    protected void setBackgroundTexture(ResourceLocation backgroundTexture) {
        this.backgroundTexture = backgroundTexture;
    }

    public void setCollapsed(boolean collapsed) {
        isCollapsed = collapsed;
    }

    public boolean isCollapsed() {
        return isCollapsed;
    }

    public EnumWindowType getWindowType() {
        return windowType;
    }

    public void setLeftCornerDesc(String leftCornerDesc) {
        this.leftCornerDesc = leftCornerDesc;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isActive() {
        return isActive;
    }
}
