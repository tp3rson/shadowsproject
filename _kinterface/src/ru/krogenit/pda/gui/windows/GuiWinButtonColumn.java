package ru.krogenit.pda.gui.windows;

import net.minecraft.client.Minecraft;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.xlv.customfont.FontType;

public class GuiWinButtonColumn extends GuiButtonAdvanced {

    private EnumSortType sortType;
    private final IGuiSortable guiSortable;

    public GuiWinButtonColumn(int id, float x, float y, float width, float height, String text, IGuiSortable guiSortable) {
        super(id, x, y, width, height, text);
        sortType = EnumSortType.NONE;
        this.guiSortable = guiSortable;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        boolean isHovering = isHovered(mouseX, mouseY);
        float fs = 28 / 32f;
        float textOffset;
        int textColor = 0x666666;

        if(sortType != EnumSortType.NONE) {
            textOffset = ScaleGui.get(2);
            textColor = 0xffffff;
        } else if(isHovering) {
            textColor = 0xffffff;
            textOffset = ScaleGui.get(9);
        } else textOffset = ScaleGui.get(9);

        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, displayString, xPosition + textOffset, yPosition + height / 2f, fs, textColor);

        if(sortType != EnumSortType.NONE) {
            float width = ScaleGui.get(8);
            float height = ScaleGui.get(6);
            float x = xPosition + ScaleGui.get(8) + FontType.HelveticaNeueCyrLight.getFontContainer().width(displayString) * ScaleGui.get(fs);
            float y = yPosition + ScaleGui.get(14.5f);
            if(sortType == EnumSortType.ASCENDING) {
                GuiDrawUtils.drawRect(TextureRegister.textureInvArrowTop, x, y, width, height);
            } else if(sortType == EnumSortType.DESCENDING) {
                GuiDrawUtils.drawRect(TextureRegister.textureInvArrowBot, x, y, width, height);
            }
        }
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        if(super.mousePressed(mc, mouseX, mouseY)) {
            getNextSort();
            guiSortable.sort(id, sortType);
            return true;
        }

        return false;
    }

    private void getNextSort() {
        if(sortType == EnumSortType.NONE) {
            sortType = EnumSortType.DESCENDING;
        } else if(sortType == EnumSortType.DESCENDING) {
            sortType = EnumSortType.ASCENDING;
        } else {
            sortType = EnumSortType.NONE;
        }
    }

    public void resetSort() {
        sortType = EnumSortType.NONE;
    }
}
