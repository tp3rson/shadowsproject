package ru.krogenit.pda.gui.windows.achievements;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.customfont.FontType;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.glBlendFuncSeparate;

public class GuiSlotAchieve extends GuiButtonAdvanced {

    private final Achievement achievement;
    private Vector3f color;
    private final String stage;

    public GuiSlotAchieve(Achievement achievement, float x, float y, float width, float height) {
        super(-1, x, y, width, height, achievement.getAchievementName());
        this.achievement = achievement;
        setTexture(achievement.getTexture());
        switch (achievement.getType()) {
            case BRONZE:
                color = new Vector3f(235/255f, 129/255f, 5/255f);
                break;
            case SILVER:
                color = new Vector3f(1f, 1f, 1f);
                break;
            case GOLD:
                color = new Vector3f(255/255f, 216/255f, 0f);
                break;
        }
        stage = "Этап " + (achievement.getType().ordinal() + 1);
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        boolean isHovered = isHovered(mouseX, mouseY);
        if(achievement.isAchieved()) {
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GuiDrawUtils.drawRect(xPosition, yPosition, width, height, 61/255f, 61/255f, 61/255f, 0.9f);
            GuiDrawUtils.drawGradient(xPosition, yPosition, width, height, color.x, color.y, color.z, isHovered ? 0.8f : 0.6f, 0,0,0,0.1f);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
        } else {
            if(isHovered) {
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                GuiDrawUtils.drawRect(xPosition, yPosition, width, height, 97/255f, 97/255f, 97/255f, 0.9f);
                GL11.glEnable(GL11.GL_TEXTURE_2D);
            } else {
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                GuiDrawUtils.drawRect(xPosition, yPosition, width, height, 61/255f, 61/255f, 61/255f, 0.9f);
                GL11.glEnable(GL11.GL_TEXTURE_2D);
            }
        }

        float iconWidth = ScaleGui.get(90f);
        float iconHeight = ScaleGui.get(86f);
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawRectCentered(texture, xPosition + width / 2.0f, yPosition + ScaleGui.get(73f), iconWidth, iconHeight);

        float textOffsetY = 0f;
        if(!achievement.isAchieved() && isHovered) {
            float x = xPosition + ScaleGui.get(20f);
            float y = yPosition + ScaleGui.get(167f);
            float width = ScaleGui.get(144f);
            float height = ScaleGui.get(15f);
            GL11.glColor4f(173/255f, 173/255f, 173/255f, 0.9f);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glBegin(GL11.GL_LINE_STRIP);
            GL11.glVertex2f(x, y);
            GL11.glVertex2f(x + width, y);
            GL11.glVertex2f(x + width, y + height);
            GL11.glVertex2f(x, y + height);
            GL11.glVertex2f(x, y);
            GL11.glEnd();
            glColor4f(1f,1f,1f,1f);
            glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_ZERO, GL_ZERO);
            GuiDrawUtils.drawRect(xPosition, yPosition, this.width, this.height);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glEnable(GL_TEXTURE_2D);

            float load = achievement.getCurrentProgress() / (float) achievement.getMaxProgress();
            x = xPosition + ScaleGui.get(21f);
            y = yPosition + ScaleGui.get(167.5f);
            float maskWidth = ScaleGui.get(140f);
            float maskHeight = ScaleGui.get(14f);

            glColor4f(1f,1f,1f, 1f);
            float loadMaskWidth = 0f;
            float totalMaskWidth = (load - 0.05f) * maskWidth + ScaleGui.get(1f);
            float step = ScaleGui.get(4.5f);
            if(load <= 0.005f) {
                loadMaskWidth = 0;
            } else if(load > 0.99f) {
                loadMaskWidth = maskWidth;
            } else {
                while(loadMaskWidth < totalMaskWidth) {
                    loadMaskWidth += step;
                }
            }

            glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_SRC_COLOR, GL_ZERO);
            GuiDrawUtils.drawRect(TextureRegister.texturePDAAchieveBarMask, x, y, maskWidth, maskHeight);
            glDisable(GL_TEXTURE_2D);
            glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);
            GuiDrawUtils.drawRect(x, y, loadMaskWidth, maskHeight);
            glEnable(GL_TEXTURE_2D);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            float fs = 24 / 32f;
            x = xPosition + this.width / 2f;
            y += ScaleGui.get(25f);
            GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrBold, achievement.getCurrentProgress() + " / " + achievement.getMaxProgress(), x, y, fs, 0xffffff);
            textOffsetY -= ScaleGui.get(16f);
        }

        float fs = 26 / 32f;
        float x = xPosition + width / 2f;
        float y = yPosition + ScaleGui.get(150f) + textOffsetY;
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, displayString, x, y, fs, 0xffffff);
        fs = 22 / 32f;
        y += ScaleGui.get(18f);
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, stage, x, y, fs, 0xffffff);
    }

    public Achievement getAchievement() {
        return achievement;
    }
}
