package ru.krogenit.pda.gui.windows.achievements;

public enum EnumAchievementSection {
    FIRST("СЕЗОН ЗАРАЖЕНИЕ"), SECOND("ФАРШИРОВЩИК"), THIRD("ЗАПАСЛИВЫЙ");

    private final String localized;

    EnumAchievementSection(String localized) {
        this.localized = localized;
    }

    public String getName() {
        return localized;
    }
}
