package ru.xlv.trades.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketTradeCancel implements IPacketOut {
    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {}
}
