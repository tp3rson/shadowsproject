package ru.xlv.trades.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.trades.XlvsTradeMod;
import ru.xlv.trades.handle.result.TradeInviteResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketTradeInvite implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    private TradeInviteResult tradeInviteResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if (REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
            String targetName = bbis.readUTF();
            tradeInviteResult = XlvsTradeMod.INSTANCE.getTradeHandler().handleInvite(entityPlayer, targetName);
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(tradeInviteResult != null);
        if (tradeInviteResult != null) {
            bbos.writeUTF(tradeInviteResult.getResponseMessage());
        } else {
            bbos.writeUTF(XlvsCore.INSTANCE.getLocalization().getResponseTooManyRequests());
        }
    }
}
