package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public abstract class AbstractClientMessageHandler<T extends IMessage> extends AbstractMessageHandler<T> {
	public final IMessage handleMessageOnServerSide(T msg, MessageContext ctx) {
		return null;
	}
}
