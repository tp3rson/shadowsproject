package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class PacketUpdateBlockLight implements IMessage {

	private float power;
	private float r, g, b;
	private int x, y, z;
	
	public PacketUpdateBlockLight(float power, float r, float g, float b, int x, int y, int z) {
		this.power = power;
		this.r = r;
		this.g = g;
		this.b = b;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public void fromBytes(ByteBuf data) {
		power = data.readFloat();
		r = data.readFloat();
		g = data.readFloat();
		b = data.readFloat();
		x = data.readInt();
		y = data.readInt();
		z = data.readInt();
	}

	@Override
	public void toBytes(ByteBuf data) {
		data.writeFloat(power);
		data.writeFloat(r);
		data.writeFloat(g);
		data.writeFloat(b);
		data.writeInt(x);
		data.writeInt(y);
		data.writeInt(z);
	}
}
