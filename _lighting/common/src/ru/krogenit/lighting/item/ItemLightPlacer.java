package ru.krogenit.lighting.item;

import net.minecraft.item.Item;

public class ItemLightPlacer extends Item {

	public ItemLightPlacer() {
		setMaxStackSize(1);
		setUnlocalizedName("itemLightPlacer");
	}
}
