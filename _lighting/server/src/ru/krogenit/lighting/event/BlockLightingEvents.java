package ru.krogenit.lighting.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.world.BlockEvent;
import ru.krogenit.lighting.EnumLightEvent;
import ru.krogenit.lighting.LightManagerServer;
import ru.krogenit.lighting.network.PacketDispatcherLighting;
import ru.krogenit.lighting.network.PacketUpdateLight;

public class BlockLightingEvents {
	
	@SubscribeEvent
	public void onBreakBlock(BlockEvent.BreakEvent event) {
		if (LightManagerServer.isLightBlock(event.block)) PacketDispatcherLighting.sendToAllAround(new PacketUpdateLight(event.x, event.y, event.z, 0, EnumLightEvent.Remove), event.getPlayer(), 128);
	}
}
