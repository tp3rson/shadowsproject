package ru.krogenit.lighting.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ChatComponentText;
import org.lwjgl.opengl.GL11;
import ru.krogenit.lighting.EnumLightEvent;
import ru.krogenit.lighting.PointLight;
import ru.krogenit.lighting.network.PacketDispatcherLighting;
import ru.krogenit.lighting.network.PacketUpdateBlockLight;
import ru.krogenit.lighting.network.PacketUpdateLight;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class GuiBlockLight extends GuiScreen {
	
	private List<GuiTextField> fields;
	private NumberFormat formatter = new DecimalFormat("#0.000");
	private int x, y, z;
	private PointLight p;
	
	public GuiBlockLight(int x, int y, int z, PointLight p) {
		this.fields = new ArrayList<>();
		this.x = x;
		this.y = y;
		this.z = z;
		this.p = p;
	}
	
	@Override
	public void initGui() {
		buttonList.clear();
		fields.clear();
		
		int baseX = width / 2;
		int baseY = height / 2;
		int width = 150;
		int height = 20;
		int offsetHeight = 24;
		
		GuiOptionSliderLighting power = new GuiOptionSliderLighting(0, baseX - 75, baseY - 170, "Distance: ", p.power / 50f);
		buttonList.add(power);
		
		baseY += offsetHeight;
		GuiOptionSliderLighting colorR = new GuiOptionSliderLighting(0, baseX - 75, baseY - 170, "R: ", p.color.x / 25f);
		buttonList.add(colorR);
		baseY += offsetHeight;
		GuiOptionSliderLighting colorG = new GuiOptionSliderLighting(0, baseX - 75, baseY - 170, "G: ", p.color.y / 25f);
		buttonList.add(colorG);
		baseY += offsetHeight;
		GuiOptionSliderLighting colorB = new GuiOptionSliderLighting(0, baseX - 75, baseY - 170, "B: ", p.color.z / 25f);
		buttonList.add(colorB);
		
		baseX += 40;
		width = 110;
		offsetHeight = 40;
		
		baseY += offsetHeight;
		GuiTextField powerField = new GuiTextField(fontRendererObj, baseX - 75, baseY - 170, width, height);
		fields.add(powerField);
		
		offsetHeight = 24;
		
		baseY += offsetHeight;
		GuiTextField colorRField = new GuiTextField(fontRendererObj, baseX - 75, baseY - 170, width, height);
		fields.add(colorRField);
		baseY += offsetHeight;
		GuiTextField colorGField = new GuiTextField(fontRendererObj, baseX - 75, baseY - 170, width, height);
		fields.add(colorGField);
		baseY += offsetHeight;
		GuiTextField colorBField = new GuiTextField(fontRendererObj, baseX - 75, baseY - 170, width, height);
		fields.add(colorBField);
		updateFields(p);
		
		offsetHeight = 36;
		baseY += offsetHeight;
		baseX -= 40;
		buttonList.add(new GuiButton(1, baseX - 100, baseY - 170, "Удалить"));
	}
	
	@Override
	protected void actionPerformed(GuiButton guiButton) {
		if(guiButton.id == 1) {
			this.mc.thePlayer.closeScreen();
			PacketDispatcherLighting.sendToServer(new PacketUpdateLight(x, y, z, 0, EnumLightEvent.Remove));
		}
	}
	
	private void updateFields(PointLight p) {
		fields.get(0).setText(formatter.format(p.power / 50f));
		fields.get(1).setText(formatter.format(p.color.x / 25f));
		fields.get(2).setText(formatter.format(p.color.y / 25f));
		fields.get(3).setText(formatter.format(p.color.z / 25f));
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTick) {
		drawDefaultBackground();
		
		int textWidth = 248;
		int textHight = 400;

		int cenX = width / 2;
		int cenY = height / 2;

		float x = cenX - textWidth / 2f;
		float y = cenY - textHight / 2f;
		float k = x + textWidth;
		float l = y + textHight;
		GL11.glColor4f(0.75F, 0.75F, 0.75F, 0.25F);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		Tessellator t = new Tessellator();
		t.startDrawingQuads();
		t.addVertexWithUV(x, l, 0, 0, 1.0F);
		t.addVertexWithUV(k, l, 0, 1, 1.0F);
		t.addVertexWithUV(k, y, 0, 1, 0.0F);
		t.addVertexWithUV(x, y, 0, 0, 0.0F);
		t.draw();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		
		super.drawScreen(mouseX, mouseY, partialTick);
		
		for (GuiTextField field : fields)
			field.drawTextBox();
		
		int posX = cenX - 80;
		int posY = cenY - 52;
		String s = "Дистанция: ";
		fontRendererObj.drawString(s, posX, posY, Integer.MAX_VALUE, false);
		 posX = cenX - 50;
		posY += 24;
		s = "R: ";
		fontRendererObj.drawString(s, posX, posY, Integer.MAX_VALUE, false);
		posY += 24;
		s = "G: ";
		fontRendererObj.drawString(s, posX, posY, Integer.MAX_VALUE, false);
		posY += 24;
		s = "B: ";
		fontRendererObj.drawString(s, posX, posY, Integer.MAX_VALUE, false);
		
		
		if(System.currentTimeMillis() % 2 == 0) {
			p.power = ((GuiOptionSliderLighting)buttonList.get(0)).value * 50f;
			p.color.x = ((GuiOptionSliderLighting)buttonList.get(1)).value * 25f;
			p.color.y = ((GuiOptionSliderLighting)buttonList.get(2)).value * 25f;
			p.color.z = ((GuiOptionSliderLighting)buttonList.get(3)).value * 25f;
			
			PacketDispatcherLighting.sendToServer(new PacketUpdateBlockLight(p.power, p.color.x, p.color.y, p.color.z, this.x, this.y, this.z));
		}
	}
	
	@Override
	public void onGuiClosed() {
		p.power = ((GuiOptionSliderLighting)buttonList.get(0)).value * 50f;
		p.color.x = ((GuiOptionSliderLighting)buttonList.get(1)).value * 25f;
		p.color.y = ((GuiOptionSliderLighting)buttonList.get(2)).value * 25f;
		p.color.z = ((GuiOptionSliderLighting)buttonList.get(3)).value * 25f;
		
		PacketDispatcherLighting.sendToServer(new PacketUpdateBlockLight(p.power, p.color.x, p.color.y, p.color.z, this.x, this.y, this.z));
	}
	
	@Override
	public void keyTyped(char character, int key) {
		if (key == 1 || key == 18) {
			this.mc.thePlayer.closeScreen();
		}

		for (GuiTextField field : fields)
			field.textboxKeyTyped(character, key);
		
		try {
			float value = Float.parseFloat(fields.get(0).getText());
			if(value > 50f) value = 50f;
			else if(value < 0) value = 0f;
			((GuiOptionSliderLighting)buttonList.get(0)).value = value;
			((GuiOptionSliderLighting)buttonList.get(0)).displayString = "Distance: " + value;

			value = Float.parseFloat(fields.get(1).getText());
			if(value > 25f) value = 25f;
			else if(value < 0) value = 0f;
			((GuiOptionSliderLighting)buttonList.get(1)).value = value;
			((GuiOptionSliderLighting)buttonList.get(1)).displayString = "R: " + value;
			
			value = Float.parseFloat(fields.get(2).getText());
			if(value > 25f) value = 25f;
			else if(value < 0) value = 0f;
			((GuiOptionSliderLighting)buttonList.get(2)).value = value;
			((GuiOptionSliderLighting)buttonList.get(2)).displayString = "G: " + value;
			
			value = Float.parseFloat(fields.get(3).getText());
			if(value > 25f) value = 25f;
			else if(value < 0) value = 0f;
			((GuiOptionSliderLighting)buttonList.get(3)).value = value;
			((GuiOptionSliderLighting)buttonList.get(3)).displayString = "B: " + value;
		} catch(Exception e) {
			mc.thePlayer.addChatMessage(new ChatComponentText("Неверное значение в поле"));
		}
	}
	
	@Override
	public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
		super.mouseClicked(mouseX, mouseY, mouseButton);
		updateFields(p);
		for (GuiTextField field : fields)
			field.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
		super.mouseMovedOrUp(mouseX, mouseY, which);
		updateFields(p);
	}
}
