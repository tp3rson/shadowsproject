package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import ru.xlv.gun.network.packets.PacketUnreload;

public class PacketUnreloadHandler implements IMessageHandler<PacketUnreload, IMessage> {

	@Override
	public IMessage onMessage(PacketUnreload message, MessageContext ctx) {
		return null;
	}
}
