package ru.xlv.gun.render;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.xlv.gun.item.Items;

import java.util.Random;

public class RenderWeaponThings {

	private ModelSmoke model_smoke = new ModelSmoke();
	private ModelHand hands = new ModelHand();
	public static float renderTicks = 0;
	public static boolean livingdown = true;
	public static float run4Progress = 0F, lastRun4Progress = 0F;
	public static float runProgress = 0F, lastRunProgress = 0F;
	public static float run1Progress = 0F, lastRun1Progress = 0F;
	public static float run2Progress = 0F, lastRun2Progress = 0F;
	public static float run3Progress = 0F, lastRun3Progress = 0F;
	public static float run5Progress = 0F, lastRun5Progress = 0F;
	private static final ResourceLocation TEXTURE_SMOKE_FIRE = new ResourceLocation("batmod:textures/effects/smoke.png");
	private static final ResourceLocation TEXTURE_SMOKE = new ResourceLocation("batmod:textures/effects/smoke_without_fire.png");

	public void renderLeftArm(float handX, float handY, float handZ, float rotateX, float rotateY, float rotateZ, float scaleHand) {
		GL11.glPushMatrix();
		GL11.glRotatef(rotateX, 1, 0, 0);
		GL11.glRotatef(rotateY, 0, 1, 0);
		GL11.glRotatef(rotateZ, 0, 0, 1);
		GL11.glScalef(scaleHand, 3, scaleHand);
		GL11.glTranslatef(handX + 0.32F, handY + 0.12F, handZ);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glRotatef(rotateX - 1, 1, 0, 0);
		GL11.glRotatef(rotateY, 0, 1, 0);
		GL11.glRotatef(rotateZ, 0, 0, 1);
		GL11.glScalef(scaleHand, 3, scaleHand);
		GL11.glTranslatef(handX, handY, handZ);
		hands.render_left(Minecraft.getMinecraft().thePlayer);
		GL11.glPopMatrix();

	}

	public void renderRightArm(float handX, float handY, float handZ, float rotateX, float rotateY, float rotateZ, float scaleHand) {
		GL11.glPushMatrix();
		GL11.glRotatef(rotateX, 1, 0, 0);
		GL11.glRotatef(rotateY, 0, 1, 0);
		GL11.glRotatef(rotateZ, 0, 0, 1);
		GL11.glTranslatef(handX - 0.72F * scaleHand / 1.65F, handY + 0.2F, handZ);
		GL11.glScalef(scaleHand, 1.6F, scaleHand);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glRotatef(rotateX, 1, 0, 0);
		GL11.glRotatef(rotateY, 0, 1, 0);
		GL11.glRotatef(rotateZ, 0, 0, 1);
		GL11.glTranslatef(handX, handY, handZ);
		GL11.glScalef(scaleHand, 1.6F, scaleHand);
		hands.render_right(Minecraft.getMinecraft().thePlayer);
		GL11.glPopMatrix();
	}

	public void flash() {
		ItemStack stack = Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem();
		if (stack != null) {
			Random rand = new Random();
			float muzzle_flash = 0.5F;
			float muzzle_flash2 = (float) rand.nextInt(360);
			muzzle_flash -= 0.05F;
			GL11.glPushMatrix();
			float f = 0.0F;
			if (stack.getTagCompound() != null) {
				if (stack.getTagCompound().getString("stvol").equals("OTAN Silencer")
						|| stack.getTagCompound().getString("stvol").equals("ПБС-4") || stack.getItem() == Items.val
						|| stack.getItem() == Items.vss || stack.getTagCompound().getString("stvol").equals("Osprey")
						|| stack.getTagCompound().getString("stvol").equals("7x62 AAC SDN")) {
					ru.xlv.core.util.Utils.bindTexture(TEXTURE_SMOKE_FIRE);
				} else {
					ru.xlv.core.util.Utils.bindTexture(TEXTURE_SMOKE);
				}
			}
			GL11.glTranslated(-0.8D, -1D, 0D);
			GL11.glTranslated(0.0D, 0.0D, (double) f + f);
			GL11.glRotatef(muzzle_flash2, 0.0F, 0.0F, 1F);
			GL11.glScalef(muzzle_flash / muzzle_flash / 10.0F, muzzle_flash / muzzle_flash / 10.0F, muzzle_flash / muzzle_flash / 10.0F);
			GL11.glEnable(3042);
			model_smoke.render(0.0625F);
			GL11.glDisable(3042);
			GL11.glPopMatrix();
		}
	}

	public void doAnimations() {}
}
    

