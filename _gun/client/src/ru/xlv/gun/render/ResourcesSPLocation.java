package ru.xlv.gun.render;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import ru.xlv.core.model.ModelLoader;

public class ResourcesSPLocation {

   public static final IModelCustom vip = ModelLoader.loadModel(new ResourceLocation("sp", "models/armor/vip.sp"));
   
   public static final IModelCustom pulemet = ModelLoader.loadModel(new ResourceLocation("sp", "models/guns/pulemet/pulemetmodel.sp"));
   public static final IModelCustom smg = ModelLoader.loadModel(new ResourceLocation("sp", "models/guns/smg/smgmodel.sp"));
   public static final IModelCustom aspeed = ModelLoader.loadModel(new ResourceLocation("sp", "models/guns/aspeed/aspeedmodel.sp"));
   public static final IModelCustom aspeedmag = ModelLoader.loadModel(new ResourceLocation("sp", "models/guns/aspeed/magazine.sp"));
   
   public static final ResourceLocation leg = new ResourceLocation("sp", "textures/gui/legendary.png");
}
