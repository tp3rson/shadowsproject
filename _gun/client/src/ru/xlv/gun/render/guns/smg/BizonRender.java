package ru.xlv.gun.render.guns.smg;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class BizonRender implements IItemRenderer {

	// public static final IModelCustom model = spproject.loaderModel.Modell.loadModel(new
	// ResourceLocation("batmod", "models/guns/ak74.sp"));
	public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/bizon/texture.png");
	public static final ResourceLocation meshtex = new ResourceLocation("batmod", "models/guns/bizon/mesh.png");
	
	public static final ResourceLocation tex_new = new ResourceLocation("batmod", "models/guns/bizon/texture.dds");
	public static final ResourceLocation meshtex_new = new ResourceLocation("batmod", "models/guns/bizon/mesh.dds");
	
	public static final ResourceLocation tex_n = new ResourceLocation("batmod", "models/guns/bizon/texture_n.dds");
	public static final ResourceLocation meshtex_n = new ResourceLocation("batmod", "models/guns/bizon/mesh_n.dds");
	
	public static final ResourceLocation tex_s = new ResourceLocation("batmod", "models/guns/bizon/texture_s.dds");

	private static RenderAttachments renderAtt;
	RenderWeaponThings rWT = new RenderWeaponThings();

	Minecraft mc = Minecraft.getMinecraft();
//	IModelCustom model;
	Model model;

	public BizonRender() {

		renderAtt = new RenderAttachments();
//		this.model = spproject.loaderModel.Modell.loadModel(new ResourceLocation("batmod:models/guns/bizon/model.sp"));
//        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
		model = new Model(new ResourceLocation("batmod:models/guns/bizon/model_new.sp"));
	}

	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}

	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type) {
		case EQUIPPED_FIRST_PERSON:
			return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}

	}

	private static boolean loaded;

	public void renderItem(ItemRenderType type, ItemStack is, Object... data) {
//		if(!loaded) {
//			loaded = true;
//			System.out.println("bizon");
//			TextureLoaderDDS.loadTexture(tex_new);
//			TextureLoaderDDS.loadTexture(tex_n);
//			TextureLoaderDDS.loadTexture(tex_s);
//			TextureLoaderDDS.loadTexture(meshtex_new);
//			TextureLoaderDDS.loadTexture(meshtex_n);
//		}
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;

		ItemWeapon weapon = (ItemWeapon) is.getItem();
		int shootTimer = (int) weapon.otdachaTimer;
		// анимация прицеливания
		// анимация бега
		// анимация выстрела
		// анимация перезарядки
		// анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;

		float runAnim = rWT.lastRun1Progress
				+ (rWT.run1Progress - rWT.lastRun1Progress) * RenderWeaponThings.renderTicks;

		float shootAnim = rWT.lastRun2Progress
				+ (rWT.run2Progress - rWT.lastRun2Progress) * RenderWeaponThings.renderTicks;

		float reloadAnim = rWT.lastRun3Progress
				+ (rWT.run3Progress - rWT.lastRun3Progress) * RenderWeaponThings.renderTicks;

		float livingAnim = rWT.lastRun4Progress
				+ (rWT.run4Progress - rWT.lastRun4Progress) * RenderWeaponThings.renderTicks;
		switch (type) {
			case EQUIPPED_FIRST_PERSON: {
				rWT.doAnimations();

				float x = 0;
				float y = 0;
				float z = 0;
				GL11.glPushMatrix();
				RenderHelper.disableStandardItemLighting();
				GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
				if (AttachmentType.checkAttachment(is, "scope", Items.pkas)) {
					// gameSettings.fovSetting = 90 - 20 * aimAnim;
					x = -0.275F;
					y = -0.235F;
					z = 0.2438F;

				}
				if (AttachmentType.checkAttachment(is, "scope", Items.pso)) {
					// gameSettings.fovSetting = 90 - 60 * aimAnim;
					x = -0.046F;
					y = -0.196F;
					z = 0.08F;

				}
				if (AttachmentType.checkAttachment(is, "scope", Items.kobra)) {
					// gameSettings.fovSetting = 90 - 10 * aimAnim;
					x = -0.19F;
					y = -0.194F;
					z = 0.143F;

				}
				float dSWS = 1F;
				float f1 = 1F;
				if (Utils.isPlayerAiming()) {
					dSWS = 0.2F;
					f1 = 1F;
				}
				float fov = gameSettings.fovSetting / 10000;
				if (!Utils.isPlayerAiming())
					GL11.glTranslatef(0, 0.05F * livingAnim - aimAnim / 30, 0);
				else
					GL11.glTranslatef(0, -0.6F * livingAnim * fov, 0);
				GL11.glRotatef(65 * reloadAnim, 0, 0, 1);
				GL11.glRotatef(-25 * runAnim, 1, 0, 0);
				GL11.glRotatef(-15 * runAnim, 0, 0, 1);
				GL11.glTranslatef(-0.4F * runAnim, -0.8F * runAnim, 0.5F * runAnim);
				GL11.glRotated(-1.5F * aimAnim, 1.0, 0.0, 1.0);
				GL11.glTranslatef(-0.468F * aimAnim + x * aimAnim, 0.055F * aimAnim + y * aimAnim,
						0.0315F * aimAnim + z * aimAnim);
				GL11.glRotated(4.0F * shootAnim * dSWS, 1.0, 0.0, 1.0);
				GL11.glRotated(-1.0F * shootAnim * dSWS, 0.0, 1.0, 1.0);
				GL11.glTranslatef(-0.085F * shootAnim * dSWS, 0.1F * shootAnim * dSWS, 0.084F * shootAnim * dSWS);

				GL11.glPushMatrix();
				GL11.glScalef(1.6F, 1.6F, 1.6F);

				GL11.glRotated(0.0, 1.0, 0.0, 0.0);
				GL11.glRotated(45.0, 0.0, 1.0, 0.0);
				GL11.glRotated(1.0, 0.0, 0.0, 1.0);
				// GL11.glTranslatef(-0.4F * aimAnim, 0.203F * aimAnim, -0.570F * aimAnim);
				GL11.glTranslatef(-0.26F, 0.39F, -0.245F);
				renderAttachments(type, is);
//			ru.xlv.core.util.Utils.bindTexture(tex);
				KrogenitShaders.forwardDirect.enable();
				KrogenitShaders.forwardDirect.setOldRender(false);
				KrogenitShaders.forwardDirect.setNormalMapping(true);
				KrogenitShaders.forwardDirect.setSpecularMapping(true);
				TextureLoaderDDS.bindTexture(tex_new);
				TextureLoaderDDS.bindNormalMap(tex_n);
				TextureLoaderDDS.bindSpecularMap(tex_s);
				model.renderPart("gun");
//			ru.xlv.core.util.Utils.bindTexture(meshtex);
				KrogenitShaders.forwardDirect.setSpecularMapping(false);
				TextureLoaderDDS.bindTexture(meshtex_new);
				TextureLoaderDDS.bindNormalMap(meshtex_n);
				model.renderPart("mesh");
				KrogenitShaders.forwardDirect.setNormalMapping(false);
				KrogenitShaders.forwardDirect.disable();
				TextureLoaderDDS.unbind();
				if (shootTimer == weapon.shootSpeed - 1) {
					GL11.glPushMatrix();
					GL11.glScalef(0.13F, 0.13F, 0.13F);
					GL11.glRotatef(90, 0, 1, 0);
					GL11.glTranslatef(-1.5F, 2.0F, 34F);
					GL11.glTranslatef(1.8F * aimAnim, 0, 0);
					rWT.flash();
					GL11.glPopMatrix();
				}
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);

				GL11.glPopMatrix();
				KrogenitShaders.forwardDirect.enable();
				KrogenitShaders.forwardDirect.setOldRender(true);
				GL11.glPushMatrix();
				rWT.renderRightArm(0.29F, -1.65F, 0.92F, -90, -5, -25, 2.1F);
				GL11.glPopMatrix();
				GL11.glPushMatrix();
				rWT.renderLeftArm(-0.62F, -0.51F, -0.36F, 20, 25, -80, 2.4F);
				GL11.glPopMatrix();
				KrogenitShaders.forwardDirect.disable();

				GL11.glPopMatrix();
			}
			break;
			case EQUIPPED: {
				GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
				GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
				GL11.glTranslatef(0.2F, -0.05F, -0.5F);
				GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
				renderEntityAndEquipped(type, is);
			}
			break;
			case ENTITY: {
				renderEntityAndEquipped(type, is);
			}
			break;
			default:
				break;
		}

	}

	public void renderAttachments(ItemRenderType type, ItemStack is) {
		if (AttachmentType.checkAttachment(is, "scope", Items.pkas)) {
			GL11.glPushMatrix();
			GL11.glScalef(0.95F, 0.95F, 0.95F);
			GL11.glTranslatef(0.1F, 0.03F, 0.02F);
			renderAtt.renderPKAS(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if (AttachmentType.checkAttachment(is, "scope", Items.kobra)) {
			GL11.glPushMatrix();
			GL11.glScalef(0.95F, 0.95F, 0.95F);
			GL11.glTranslatef(0.1F, 0.03F, 0.02F);
			renderAtt.renderKobra(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if (AttachmentType.checkAttachment(is, "scope", Items.pso)) {
			GL11.glPushMatrix();
			GL11.glScalef(0.95F, 0.95F, 0.95F);
			GL11.glTranslatef(0.2F, -0.01F, -0.019F);
			// GL11.glScalef(0.9F, 0.9F, 0.9F);
			renderAtt.renderPSO(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if (AttachmentType.checkAttachment(is, "stvol", Items.osprey)) {
			GL11.glPushMatrix();
			GL11.glScalef(0.75F, 0.75F, 0.75F);
			GL11.glTranslatef(-0.38F, 0.01F, -0.04F);
			renderAtt.renderOsprey();
			GL11.glPopMatrix();

		}
		if (AttachmentType.checkAttachment(is, "grip", Items.laser)) {
			GL11.glPushMatrix();
			GL11.glScalef(0.05F, 0.05F, 0.05F);
			GL11.glTranslatef(13.7F, -11.8F, -1.57F);
			renderAtt.renderLaser(type);
			GL11.glPopMatrix();
		}
	}

	public void renderEntityAndEquipped(ItemRenderType type, ItemStack is) {
		GL11.glPushMatrix();

		GL11.glScalef(0.5F, 0.5F, 0.5F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		NBTTagCompound tagz = is.getTagCompound();
		if (tagz != null) {
			if (tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0
					|| tagz.getString("laser").length() > 0) {
				renderAttachments(type, is);
			}
		}
//		ru.xlv.core.util.Utils.bindTexture(tex);
		KrogenitShaders.forwardDirect.enable();
		KrogenitShaders.forwardDirect.setOldRender(false);
		KrogenitShaders.forwardDirect.setNormalMapping(true);
		KrogenitShaders.forwardDirect.setSpecularMapping(true);
		TextureLoaderDDS.bindTexture(tex_new);
		TextureLoaderDDS.bindNormalMap(tex_n);
		TextureLoaderDDS.bindSpecularMap(tex_s);
		model.render();
		TextureLoaderDDS.unbind();
		KrogenitShaders.forwardDirect.setNormalMapping(false);
		KrogenitShaders.forwardDirect.setSpecularMapping(false);
		KrogenitShaders.forwardDirect.setOldRender(true);
//		Shaders.forwardDirect.disable();

		GL11.glPopMatrix();
	}
}
