package ru.xlv.gun.render.guns.sniper;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class L96Render implements IItemRenderer {
	

    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/l96/texture.png");
    public static final ResourceLocation sighttex = new ResourceLocation("batmod", "models/guns/l96/sight.png");
    
    RenderWeaponThings rWT = new RenderWeaponThings();
    private static RenderAttachments renderAtt;
   
	Minecraft mc = Minecraft.getMinecraft();
	
	private static float handBolt = 0F;
	private static float handBolt1 = 0F;
	private static float handBolt2 = 0F;
	IModelCustom model;
	
	
	public L96Render() {
		
		renderAtt = new RenderAttachments();
		this.model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/l96/model.sp"));
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;

		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;
	      //анимация перезарядки болта
	    float boltAnim = rWT.lastRun5Progress + (rWT.run5Progress - rWT.lastRun5Progress)* RenderWeaponThings.renderTicks;
		switch (type) {
		case EQUIPPED_FIRST_PERSON: {
			rWT.doAnimations();
 			if(shootTimer == weapon.shootSpeed-1){
			GL11.glPushMatrix();
			GL11.glRotated(1.0F*shootAnim, 1.0, 0.0, 1.0); 
			GL11.glTranslatef(0.1F * aimAnim, 0.203F * aimAnim, -1.570F * aimAnim);
			GL11.glScalef(0.25F, 0.25F, 0.25F);
			GL11.glRotatef(135, 0, 1, 0);
			GL11.glTranslatef(-2.5F, 3.7F, 85F);
			rWT.flash();
			GL11.glPopMatrix();
			}		
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(AttachmentType.checkAttachment(is,"scope", Items.leupold)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = -0.024F;
				y = -0.044F;
				z = 0.041F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.acog)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = 0.028F;
				y = -0.044F;
				z = -0.011F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.susat)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = -0.13F;
				y = -0.1325F;
				z = 0.143F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.barska)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = -0.20F;
				y = -0.055F;
				z = 0.206F;
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.trAc)){
				//gameSettings.fovSetting = 90 - 40 * aimAnim;
				x = 0.106F;
				y = -0.0174F;
				z = -0.1F;
			}
			float dSWS = 1F;
			float f1 = 1F;
			if((is.getTagCompound().getString("scope") != null || is.getTagCompound().getString("planka") != null) && Utils.isPlayerAiming()){
			   dSWS = 0.4F;
			   f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
			GL11.glRotated(-0.15F*aimAnim, 1.0, 0.0, 1.0);
			GL11.glTranslatef(-0.696F * aimAnim+ x*aimAnim, 0.048F * aimAnim + y*aimAnim, 0.1415F * aimAnim + z*aimAnim);
			GL11.glRotated(4.0F*shootAnim, 1.0, 0.0, 1.0);
			GL11.glTranslatef(-0.05F * shootAnim*dSWS, 0.115F*shootAnim, 0.064F * shootAnim*dSWS);
			GL11.glTranslatef(-0.12F * boltAnim, 0F, 0F);
			GL11.glRotated(-8.0 * boltAnim, 1.0, 0.0, 0.0);
			GL11.glPushMatrix();
			GL11.glScalef(1.15F, 1.15F, 1.15F);
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			
			GL11.glTranslatef(-0.22F, 0.6F, -0.345F);
			renderAttachments(type, is);
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderPart("gun");
			
			
			//GL11.glCallList(bulletL);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			
			GL11.glPopMatrix();
			
			GL11.glPushMatrix();
			
			//анимация болта здесь
			
			
			
			GL11.glPushMatrix();
			GL11.glScalef(1.25F, 1.25F, 1.25F);
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			GL11.glRotated(0.0, 0.0, 0.0, 1.0);
			
			GL11.glTranslatef(-0.15F, 0.495F, -0.322F);
			if(shootTimer < 29 && shootTimer > 16 && handBolt2 < 0.24F){
				handBolt2 += 0.03F;
			}
			else if(handBolt2 > 0 && shootTimer < 16){
				handBolt2 -= 0.03F;
			}
			
			GL11.glTranslatef(-handBolt2*1.3F, 0F, 0F);
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderPart("bolt");
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			GL11.glPopMatrix();
			if(shootTimer < 37 && shootTimer > 16 && handBolt < 0.24F){
				handBolt += 0.03F;
			}
			else if(handBolt > 0 && shootTimer < 10){
				handBolt -= 0.03F;
			}
			if(shootTimer < 29 && shootTimer > 20 && handBolt1 < 0.24F){
				handBolt1 += 0.03F;
			}
			else if(handBolt1 > 0 && shootTimer < 16){
				handBolt1 -= 0.03F;
			}
			//анимация руки назад
			GL11.glTranslatef(-handBolt1/1.2F, handBolt1/2, handBolt1/1.3F);
			//анимация руки вбок
			GL11.glTranslatef(handBolt/2, handBolt/1.1F, handBolt*1.6F);
			rWT.renderRightArm(0.42F, -1.75F, 0.97F, -90, -5, -35, 1.9F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.7F, -0.4F, -0.31F, 10, 25, -75, 2.2F);
			GL11.glPopMatrix();
		
			
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.2F, -0.05F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        GL11.glScalef(1.2F, 1.2F, 1.2F);
	        renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	public void renderAttachments(ItemRenderType type, ItemStack is){
		    if(is.getTagCompound().getString("scope").length() < 3){
		    ru.xlv.core.util.Utils.bindTexture(sighttex);
			model.renderPart("sight");
		    }
			else if(AttachmentType.checkAttachment(is,"scope", Items.leupold)){
			GL11.glPushMatrix();
			GL11.glScalef(1F, 1F, 1F);
			GL11.glTranslatef(-0.05F, 0.095F, 0.072F);
			renderAtt.renderLeupold(Utils.isPlayerAiming());
			GL11.glPopMatrix();
			}
			else if(AttachmentType.checkAttachment(is,"scope", Items.acog)){
				GL11.glPushMatrix();
				GL11.glScalef(1F, 1F, 1F);
				GL11.glTranslatef(-0.05F, 0.095F, 0.072F);
				renderAtt.renderACOG(Utils.isPlayerAiming());
				GL11.glPopMatrix();
				}
			else if(AttachmentType.checkAttachment(is,"scope", Items.susat)){
				GL11.glPushMatrix();
				GL11.glScalef(1F, 1F, 1F);
				GL11.glTranslatef(-0.05F, 0.1F, 0.078F);
				renderAtt.renderSUSAT(Utils.isPlayerAiming());
				GL11.glPopMatrix();
			}
			else if(AttachmentType.checkAttachment(is,"scope", Items.trAc)){
				GL11.glPushMatrix();
				GL11.glScalef(1F, 1F, 1F);
				GL11.glTranslatef(-0.05F, 0.1F, 0.078F);
				renderAtt.renderTRAC(Utils.isPlayerAiming());
				GL11.glPopMatrix();
			}
			else if(AttachmentType.checkAttachment(is,"scope", Items.barska)){
				GL11.glPushMatrix();
				GL11.glScalef(1F, 1F, 1F);
				GL11.glTranslatef(-0.05F, 0.09F, 0.081F);
				renderAtt.renderBarska(Utils.isPlayerAiming());
				GL11.glPopMatrix();
			}
	
		    if(AttachmentType.checkAttachment(is,"stvol", Items.aac762sdn)){
				 GL11.glPushMatrix();
				 GL11.glTranslatef(1.6F, 0.02F, 0.05F);
				 renderAtt.renderAAC762SDN();
				 GL11.glPopMatrix();
				
			}
		    if(AttachmentType.checkAttachment(is,"grip", Items.laser)){
				 GL11.glPushMatrix();
				 GL11.glScalef(0.05F, 0.05F, 0.05F);
				 GL11.glTranslatef(23.7F, -8.8F, -1.47F);
	            renderAtt.renderLaser(type);
	            GL11.glPopMatrix();
			 }
	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		GL11.glScalef(0.35F, 0.35F, 0.35F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0 || tagz.getString("laser").length() > 0) {
				renderAttachments(type, is);
			}
		}
		ru.xlv.core.util.Utils.bindTexture(tex);
		model.renderPart("gun");
		GL11.glPopMatrix();
    }
   
  
}
