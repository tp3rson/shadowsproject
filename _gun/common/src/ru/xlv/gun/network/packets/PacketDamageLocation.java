package ru.xlv.gun.network.packets;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;

public class PacketDamageLocation implements IMessage {

	public double attackerPosX;
	public double attackerPosZ;

	public PacketDamageLocation() {
	}

	public PacketDamageLocation(double posX, double posZ) {
		this.attackerPosX = posX;
		this.attackerPosZ = posZ;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeDouble(attackerPosX);
		buf.writeDouble(attackerPosZ);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		attackerPosX = buf.readDouble();
		attackerPosZ = buf.readDouble();
	}
}