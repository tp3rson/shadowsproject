package ru.xlv.gun.entity;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.item.EntityPainting;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.world.World;

import java.util.List;


public class EntityBullet extends EntityArrow implements IProjectile {

	private int xTile = -1;
	private int yTile = -1;
	private int zTile = -1;
	private int inTile = 0;
	private int inData = 0;
	private boolean inGround = false;
	public EntityPlayer player;
	private int ticksInAir = 0;
	private float damage;
	private int liveTime = 600;

	public int ticks = 0;

	public EntityBullet(World world) {
		super(world);
		setSize(0.2F, 0.2F);
	}

	public EntityBullet(World par1World, EntityPlayer par2EntityPlayer, float damage, float accuracy) {
		super(par1World);
		this.renderDistanceWeight = 0.0D;
		this.player = par2EntityPlayer;
		setSize(0.2F, 0.2F);
		setLocationAndAngles(par2EntityPlayer.posX, par2EntityPlayer.posY + par2EntityPlayer.getEyeHeight(), par2EntityPlayer.posZ, par2EntityPlayer.rotationYaw, par2EntityPlayer.rotationPitch);
		this.posX -= MathHelper.cos(this.rotationYaw / 180.0F * 3.141593F) * 0.16F;
		this.posY -= 0.0D;
		this.posZ -= MathHelper.sin(this.rotationYaw / 180.0F * 3.141593F) * 0.16F;
		setPosition(par2EntityPlayer.posX, par2EntityPlayer.posY + par2EntityPlayer.getEyeHeight(), par2EntityPlayer.posZ);
		this.yOffset = 0.0F;
		this.motionX = (-MathHelper.sin(this.rotationYaw / 180.0F * 3.141593F) * MathHelper.cos(this.rotationPitch / 180.0F * 3.141593F));
		this.motionZ = (MathHelper.cos(this.rotationYaw / 180.0F * 3.141593F) * MathHelper.cos(this.rotationPitch / 180.0F * 3.141593F));
		this.motionY = (-MathHelper.sin(this.rotationPitch / 180.0F * 3.141593F));
		this.damage = damage;
		setThrowableHeading(this.motionX, this.motionY, this.motionZ, 300F, accuracy);
	}

	public void onUpdate() {
		super.onEntityUpdate();
		this.ticks += 1;
		this.liveTime -= 1;
		if ((this.posY > 300.0D) || (this.liveTime <= 0)) {
			setDead();
		}
		if ((this.prevRotationPitch == 0.0F) && (this.prevRotationYaw == 0.0F)) {
			float f = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
			this.prevRotationYaw = (this.rotationYaw = (float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / 3.141592653589793D));
			this.prevRotationPitch = (this.rotationPitch = (float) (Math.atan2(this.motionY, f) * 180.0D / 3.141592653589793D));
		}
		Block block = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);
		if (block.getMaterial() != Material.air && block.getMaterial() != Material.glass) {
			block.setBlockBoundsBasedOnState(this.worldObj, this.xTile, this.yTile, this.zTile);
			AxisAlignedBB axisalignedbb = block.getCollisionBoundingBoxFromPool(this.worldObj, this.xTile, this.yTile, this.zTile);

			if (axisalignedbb != null && axisalignedbb.isVecInside(Vec3.createVectorHelper(this.posX, this.posY, this.posZ))) {
				this.inGround = true;
			}
		}

		if (this.inGround) {
			setDead();
		} else {
			++this.ticksInAir;
			Vec3 vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
			Vec3 vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
			MovingObjectPosition movingobjectposition = this.worldObj.rayTraceBlocks_do_do(vec3, vec31, false, true, false);
			vec3 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
			vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);

			if (movingobjectposition != null) {
				vec31 = Vec3.createVectorHelper(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
			}

			Entity entity = null;
			List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ)
					.expand(1.0D, 1.0D, 1.0D));
			double d0 = 0.0D;

			for (Object o : list) {
				Entity entity1 = (Entity) o;

				if ((entity1.canBeCollidedWith()) && ((entity1 != this.player) || (this.ticksInAir >= 5))) {
					float f1 = 0.3F;
					AxisAlignedBB axisalignedbb1 = entity1.boundingBox.expand(f1, f1, f1);
					MovingObjectPosition movingobjectposition1 = axisalignedbb1.calculateIntercept(vec3, vec31);

					if (movingobjectposition1 != null) {
						double d1 = vec3.distanceTo(movingobjectposition1.hitVec);
						if ((d1 < d0) || (d0 == 0.0D)) {
							entity = entity1;
							d0 = d1;
						}
					}
				}
			}

			if (entity != null) {
				movingobjectposition = new MovingObjectPosition(entity);
			}
			if ((movingobjectposition != null) && ((movingobjectposition.entityHit instanceof EntityPlayer))) {
				EntityPlayer entityplayer = (EntityPlayer) movingobjectposition.entityHit;
				if (entityplayer.capabilities.disableDamage || entityplayer.capabilities.isCreativeMode || this.player != null && !(this.player).canAttackPlayer(entityplayer)) {
					movingobjectposition = null;
				}
			}

			if (movingobjectposition != null) {
				if (movingobjectposition.entityHit instanceof EntityPainting) return;

				if ((movingobjectposition.entityHit != null) && (this.ticks >= 1)) {
					this.inGround = true;
					DamageSource damagesource;
					if (this.player != null) {
						damagesource = DamageSource.causeArrowDamage(this, player);
					} else {
						damagesource = new DamageSource("bullet");
					}
					if (movingobjectposition.entityHit.attackEntityFrom(damagesource, damage)) ;{
						if (((movingobjectposition.entityHit instanceof EntityLivingBase)) && (!movingobjectposition.entityHit.isDead)) {
							EntityLivingBase entityliving = (EntityLivingBase) movingobjectposition.entityHit;
							entityliving.hurtResistantTime = 0;
						}
					}
				} else {
					this.xTile = movingobjectposition.blockX;
					this.yTile = movingobjectposition.blockY;
					this.zTile = movingobjectposition.blockZ;
					this.inTile = this.worldObj.getBlockMetadata(this.xTile, this.yTile, this.zTile);
					if (this.inTile != 0) {
						this.inData = this.worldObj.getBlockMetadata(this.xTile, this.yTile, this.zTile);
						this.motionX = ((float) (movingobjectposition.hitVec.xCoord - this.posX));
						this.motionY = ((float) (movingobjectposition.hitVec.yCoord - this.posY));
						this.motionZ = ((float) (movingobjectposition.hitVec.zCoord - this.posZ));
						float f2 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
						this.posX -= this.motionX / f2 * 0.0500000007450581D;
						this.posY -= this.motionY / f2 * 0.0500000007450581D;
						this.posZ -= this.motionZ / f2 * 0.0500000007450581D;
						setIsCritical(false);
						this.inGround = true;
						Block block1 = block;
						if (block1.getMaterial() != Material.air && block1.getMaterial() != Material.glass) {
							block1.onEntityCollidedWithBlock(this.worldObj, this.xTile, this.yTile, this.zTile, this);
						}
					}
					Block block1 = this.worldObj.getBlock(movingobjectposition.blockX, movingobjectposition.blockY, movingobjectposition.blockZ);
					if (block1 != null && block1.getMaterial() != (Material.water)) {
						for (int d1 = 0; d1 < 5; d1++) {
							this.worldObj.spawnParticle("blockcrack_" + Block.getIdFromBlock(block1) + "_0", this.posX, this.posY, this.posZ, 0, 0, 0);
						}
						if (block1.getMaterial() != (Material.glass)) {
							this.inGround = true;
						}
					}
				}
			}

			this.posX += this.motionX;
			this.posY += this.motionY;
			this.posZ += this.motionZ;

			float f2 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
			this.rotationYaw = ((float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / 3.141592653589793D));

			for (this.rotationPitch = ((float) (Math.atan2(this.motionY, f2) * 180.0D / 3.141592653589793D)); this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F);

			while (this.rotationPitch - this.prevRotationPitch >= 180.0F) {
				this.prevRotationPitch += 360.0F;
			}

			while (this.rotationYaw - this.prevRotationYaw < -180.0F) {
				this.prevRotationYaw -= 360.0F;
			}

			while (this.rotationYaw - this.prevRotationYaw >= 180.0F) {
				this.prevRotationYaw += 360.0F;
			}

			this.rotationPitch = (this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F);
			this.rotationYaw = (this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F);

			float f4 = 1F;
			if (isInWater()) {
				for (int j1 = 0; j1 < 4; j1++) {
					float f3 = 0.25F;
					float f5 = 0.75F;
					this.worldObj.spawnParticle("bubble",
							this.posX - this.motionX * f3, this.posY - 0.25 + this.motionY * f5, this.posZ - this.motionZ * f3,
							this.motionX, this.motionY, this.motionZ);
				}
				f4 = 0.8F;
			}

			this.motionX *= f4;
			this.motionY *= f4;
			this.motionZ *= f4;
			setPosition(this.posX, this.posY, this.posZ);
			if (this.inGround) {
				setDead();
			}
		}

	}

	public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound) {
		par1NBTTagCompound.setShort("xTile", (short) this.xTile);
		par1NBTTagCompound.setShort("yTile", (short) this.yTile);
		par1NBTTagCompound.setShort("zTile", (short) this.zTile);
		par1NBTTagCompound.setDouble("motX", this.motionX);
		par1NBTTagCompound.setDouble("motY", this.motionY);
		par1NBTTagCompound.setDouble("motZ", this.motionZ);
		par1NBTTagCompound.setByte("inTile", (byte) this.inTile);
		par1NBTTagCompound.setByte("inData", (byte) this.inData);
		par1NBTTagCompound.setByte("inGround", (byte) (this.inGround ? 1 : 0));
		par1NBTTagCompound.setFloat("damage", this.damage);
	}

	public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound) {
		this.xTile = par1NBTTagCompound.getShort("xTile");
		this.yTile = par1NBTTagCompound.getShort("yTile");
		this.zTile = par1NBTTagCompound.getShort("zTile");
		this.motionX = par1NBTTagCompound.getDouble("motX");
		this.motionY = par1NBTTagCompound.getDouble("motY");
		this.motionZ = par1NBTTagCompound.getDouble("motZ");
		this.inTile = (par1NBTTagCompound.getByte("inTile") & 0xFF);
		this.inData = (par1NBTTagCompound.getByte("inData") & 0xFF);
		this.inGround = (par1NBTTagCompound.getByte("inGround") == 1);
		if (par1NBTTagCompound.hasKey("damage")) {
			this.damage = par1NBTTagCompound.getFloat("damage");
		}
	}
}