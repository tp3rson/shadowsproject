package ru.xlv.gun.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.*;
import net.minecraft.world.World;

import java.util.List;

public class EntityBulletCasing extends EntityThrowable {

	public float xRotation = 0.0F;
	public float yRotation = 0.0F;
	public float zRotation = 0.0F;
	public float xRotationSpeed = 0.0F;
	public float yRotationSpeed = 0.0F;
	public float zRotationSpeed = 0.0F;
	private double prevY;
	public boolean renderOnGround = true;
	public String model;
	public ResourceLocation texture;

	public EntityBulletCasing(World world) {
		super(world);
	}

	public EntityBulletCasing(World par1World, EntityLivingBase shooter) {
		super(par1World);
		this.setLocationAndAngles(shooter.posX, shooter.posY + (double) shooter.getEyeHeight() - 0.3D, shooter.posZ, shooter.rotationYawHead, shooter.rotationPitch);
		double lookX = (double) (-MathHelper.sin(super.rotationYaw / 180.0F * 3.1415927F) * MathHelper.cos(super.rotationPitch / 180.0F * 3.1415927F));
		double lookY = (double) (-MathHelper.sin(super.rotationPitch / 180.0F * 3.1415927F));
		double lookZ = (double) (MathHelper.cos(super.rotationYaw / 180.0F * 3.1415927F) * MathHelper.cos(super.rotationPitch / 180.0F * 3.1415927F));
		double ySpeed = (double) MathHelper.sin((super.rotationPitch + 90.0F) / 180.0F * 3.1415927F);
		Vec3 look = Vec3.createVectorHelper(lookX, lookY, lookZ).normalize();
		super.posX += look.xCoord * 0.01D;
		super.posY += look.yCoord * 0.01D;
		super.posZ += look.zCoord * 0.01D;

		super.posX += look.xCoord * 0.3D;
		super.posY += look.yCoord * 0.3D;
		super.posZ += look.zCoord * 0.3D;

		look.rotateAroundY((float) Math.toRadians(-90.0D));
		super.motionY = ySpeed * 0.25D;
		super.motionX = look.xCoord * 0.15D;
		super.motionZ = look.zCoord * 0.15D;
		super.motionY *= 1.0D + (Math.random() - 0.5D) / 2.0D;
		super.motionX *= 1.0D + (Math.random() - 0.5D) / 2.0D;
		super.motionZ *= 1.0D + (Math.random() - 0.5D) / 2.0D;
		super.posX += super.motionX / 2.0D;
		super.posZ += super.motionZ / 2.0D;
		this.xRotationSpeed = ((float) Math.random() - 0.5F) * 10.0F;
		this.yRotationSpeed = (float) Math.random() * 10.0F + 10.0F;
		this.zRotationSpeed = ((float) Math.random() - 0.5F) * 10.0F;
		this.setSize(0.05F, 0.05F);
		super.height = 0.05F;
		this.model = "batmod:models/bullet.sp";
		this.texture = new ResourceLocation("batmod:models/bullettex.png");
	}

	public EntityBulletCasing(World par1World, double posX, double posY, double posZ, float yaw, float pitch, float distanceFactor, String sleeve) {
		super(par1World);
		this.setLocationAndAngles(posX, posY - 0.3D, posZ, yaw, pitch);
		yaw += 90.0F;
		double ySpeed = (double) MathHelper.sin((super.rotationPitch + 90.0F) / 180.0F * 3.1415927F);
		super.motionY = ySpeed * 0.25D;
		super.motionX = (double) (-MathHelper.sin(yaw / 180.0F * 3.1415927F) * MathHelper.cos(pitch / 180.0F * 3.1415927F)) * 0.15D;
		super.motionZ = (double) (MathHelper.cos(yaw / 180.0F * 3.1415927F) * MathHelper.cos(pitch / 180.0F * 3.1415927F)) * 0.15D;
		super.posX += (double) (-MathHelper.sin(super.rotationYaw / 180.0F * 3.1415927F) * MathHelper.cos(super.rotationPitch / 180.0F * 3.1415927F)) * (double) distanceFactor;
		super.posZ += (double) (MathHelper.cos(super.rotationYaw / 180.0F * 3.1415927F) * MathHelper.cos(super.rotationPitch / 180.0F * 3.1415927F)) * (double) distanceFactor;
		super.posY += (double) (-MathHelper.sin(super.rotationPitch / 180.0F * 3.1415927F)) * (double) distanceFactor + 0.1D;
		super.posX += super.motionX / 2.0D;
		super.posY += super.motionY / 2.0D;
		super.posZ += super.motionZ / 2.0D;
		super.motionY *= 1.0D + (Math.random() - 0.5D) / 2.0D;
		super.motionX *= 1.0D + (Math.random() - 0.5D) / 2.0D;
		super.motionZ *= 1.0D + (Math.random() - 0.5D) / 2.0D;
		this.xRotationSpeed = ((float) Math.random() - 0.5F) * 10.0F;
		this.yRotationSpeed = (float) Math.random() * 10.0F + 10.0F;
		this.zRotationSpeed = ((float) Math.random() - 0.5F) * 10.0F;
		this.setSize(0.05F, 0.05F);
		super.height = 0.05F;
		this.model = sleeve;
		this.texture = new ResourceLocation("stalker", "models/sleeves/" + sleeve + ".png");
	}

	protected float getGravityVelocity() {
		return 0.07F;
	}

	public void onUpdate() {
		super.onUpdate();
		float rotationFactor = super.posY == this.prevY ? 0.94905F : 0.999F;
		this.xRotationSpeed *= rotationFactor;
		this.yRotationSpeed *= rotationFactor;
		this.zRotationSpeed *= rotationFactor;
		this.xRotation = (this.xRotation + this.xRotationSpeed) % 360.0F;
		this.yRotation = (this.yRotation + this.yRotationSpeed) % 360.0F;
		this.zRotation = (this.zRotation + this.zRotationSpeed) % 360.0F;
		this.prevY = super.posY;
		if (super.ticksExisted > 250) {
			this.setDead();
		}
	}

	protected void onImpact(MovingObjectPosition mop) {
		if (mop.entityHit != null) {
			if (mop.typeOfHit.ordinal() == 0) {
				this.pushOff(mop.blockX, mop.blockY, mop.blockZ, mop.sideHit);
			} else {
				this.hitEntity(mop.entityHit);
			}
		}
	}

	private void hitEntity(Entity entity) {
		super.motionX *= -0.5D;
		super.motionY *= -0.5D;
		super.motionZ *= -0.5D;
		this.xRotationSpeed *= 0.5F;
		this.yRotationSpeed *= 0.5F;
		this.zRotationSpeed *= 0.5F;
		this.calculateNewImpact();
	}

	private void pushOff(int blockX, int blockY, int blockZ, int side) {
		boolean setY = Math.abs(super.motionY) > 0.20000000298023224D || side == 0;
		if (side != 0 && side != 1) {
			if (side != 2 && side != 3) {
				super.motionX *= -0.5D;
				super.motionY *= 0.800000011920929D;
				super.motionZ *= 0.5D;
			} else {
				super.motionX *= 0.5D;
				super.motionY *= 0.800000011920929D;
				super.motionZ *= -0.5D;
			}
		} else if (setY) {
			super.motionX *= 0.800000011920929D;
			super.motionY *= -0.5D;
			super.motionZ *= 0.800000011920929D;
			this.renderOnGround = false;
		} else {
			super.motionX *= 0.800000011920929D;
			super.motionY = 0.0D;
			super.motionZ *= 0.800000011920929D;
			this.xRotationSpeed = 0.0F;
			this.zRotationSpeed = 0.0F;
			this.xRotation = 0.0F;
			this.zRotation = 0.0F;
			this.renderOnGround = true;
		}

		if (!setY) {
			this.xRotationSpeed *= 0.5F;
			this.yRotationSpeed *= 0.5F;
			this.zRotationSpeed *= 0.5F;
		} else {
			this.xRotationSpeed = ((float) Math.random() - 0.5F) * 10.0F;
			this.yRotationSpeed = (float) Math.random() * 10.0F + 10.0F;
			this.zRotationSpeed = ((float) Math.random() - 0.5F) * 10.0F;
		}
		this.calculateNewImpact();
	}

	private void calculateNewImpact() {
		Vec3 vec3 = Vec3.createVectorHelper(super.posX, super.posY, super.posZ);
		Vec3 vec31 = Vec3.createVectorHelper(super.posX + super.motionX, super.posY + super.motionY, super.posZ + super.motionZ);
		MovingObjectPosition movingobjectposition = super.worldObj.rayTraceBlocks(vec3, vec31);
		vec3 = Vec3.createVectorHelper(super.posX, super.posY, super.posZ);
		vec31 = Vec3.createVectorHelper(super.posX + super.motionX, super.posY + super.motionY, super.posZ + super.motionZ);

		if (movingobjectposition != null) {
			vec31 = Vec3.createVectorHelper(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
		}

		if (!super.worldObj.isRemote) {
			Entity entity = null;
			List list = super.worldObj.getEntitiesWithinAABBExcludingEntity(this, super.boundingBox.addCoord(super.motionX, super.motionY, super.motionZ).expand(1.0D, 1.0D, 1.0D));
			double d0 = 0.0D;
			EntityLivingBase entityliving = this.getThrower();
			for (Object o : list) {
				Entity entity1 = (Entity) o;
				if (entity1.canBeCollidedWith() && (entity1 != entityliving || super.ticksExisted >= 5)) {
					float f = 0.3F;
					AxisAlignedBB axisalignedbb = entity1.boundingBox.expand((double) f, (double) f, (double) f);
					MovingObjectPosition movingobjectposition1 = axisalignedbb.calculateIntercept(vec3, vec31);
					if (movingobjectposition1 != null) {
						double d1 = vec3.distanceTo(movingobjectposition1.hitVec);
						if (d1 < d0 || d0 == 0.0D) {
							entity = entity1;
							d0 = d1;
						}
					}
				}
			}

			if (entity != null) {
				movingobjectposition = new MovingObjectPosition(entity);
			}
		}

		if (movingobjectposition != null) {
			this.onImpact(movingobjectposition);
		}
	}

	@SideOnly(Side.CLIENT)
	public void setPositionAndRotation2(double par1, double par3, double par5, float par7, float par8, int par9) {}
}
