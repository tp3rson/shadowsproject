package ru.xlv.gun.container;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemAttachment;
import ru.xlv.gun.item.ItemWeapon;

public class SlotGun extends Slot {

    private int slotID;
    private SlotGun gunSlot;

    public SlotGun(IInventory inventory, int i, int x, int y, SlotGun s) {
        super(inventory, i, x, y);
        this.slotID = i;
        this.gunSlot = s;
    }

    public boolean isItemValid(ItemStack stack) {
        switch (this.slotID) {
            case 0: return stack == null || stack.getItem() instanceof ItemWeapon;
            case 1: return stack == null || stack.getItem() instanceof ItemAttachment && ((ItemAttachment) stack.getItem()).etype == AttachmentType.stvol && canAttachToCurrentGun(stack);
            case 2: return stack == null || stack.getItem() instanceof ItemAttachment && ((ItemAttachment) stack.getItem()).etype == AttachmentType.scope && canAttachToCurrentGun(stack);
            case 4: return stack == null || stack.getItem() instanceof ItemAttachment && (((ItemAttachment) stack.getItem()).etype == AttachmentType.grip) && canAttachToCurrentGun(stack);
            default: return false;
        }
    }

    private boolean canAttachToCurrentGun(ItemStack stack) {
        if (stack != null && stack.getItem() instanceof ItemAttachment && this.gunSlot.getHasStack() && this.gunSlot.getStack().getItem() instanceof ItemWeapon) {
            for (int i = 0; i < ((ItemWeapon) this.gunSlot.getStack().getItem()).attachments.length; i++) {
                if ((((ItemWeapon) this.gunSlot.getStack().getItem()).attachments[i].equals(stack.getItem()))) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }
}
