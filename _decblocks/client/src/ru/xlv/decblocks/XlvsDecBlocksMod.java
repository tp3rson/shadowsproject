package ru.xlv.decblocks;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;

import static ru.xlv.decblocks.XlvsDecBlocksMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
public class XlvsDecBlocksMod {

    static final String MODID = "xlvsdecblocks";

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        BlockRegistry.init();
    }
}
