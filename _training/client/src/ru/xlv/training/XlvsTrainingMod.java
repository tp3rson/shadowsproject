package ru.xlv.training;

import lombok.Getter;
import lombok.Setter;
import net.minecraftforge.common.MinecraftForge;
import ru.krogenit.render.HudTrainingElement;
import ru.krogenit.render.TrainerRenderer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.module.Construct;
import ru.xlv.core.common.module.InstanceHolder;
import ru.xlv.core.common.module.Module;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.training.common.trainer.ITrainer;
import ru.xlv.training.event.EventListener;
import ru.xlv.training.network.PacketTrainingKeyPress;
import ru.xlv.training.network.PacketTrainingStop;
import ru.xlv.training.network.PacketTrainingSync;

@Setter
@Getter
public class XlvsTrainingMod extends Module<XlvsTrainingMod> {

    @InstanceHolder
    public static XlvsTrainingMod INSTANCE;

    private ITrainer activeTrainer;

    @Construct
    public void init() {
        CommonUtils.registerFMLEvents(new EventListener());
        MinecraftForge.EVENT_BUS.register(new TrainerRenderer());
        XlvsCore.INSTANCE.getGameOverlayManager().registerElement(new HudTrainingElement());
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(getModuleId(),
                new PacketTrainingKeyPress(),
                new PacketTrainingStop(),
                new PacketTrainingSync()
        );
    }

    @Override
    public String getModuleId() {
        return "xlvstraining";
    }
}
