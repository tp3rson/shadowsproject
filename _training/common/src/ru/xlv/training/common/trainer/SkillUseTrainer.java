package ru.xlv.training.common.trainer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SkillUseTrainer implements ITrainer {

    private final int skillId;
    private final String description;

    @Override
    public ITrainer clone() {
        return new SkillUseTrainer(skillId, description);
    }
}
