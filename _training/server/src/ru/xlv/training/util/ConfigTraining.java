package ru.xlv.training.util;

import lombok.Getter;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Configurable
public class ConfigTraining implements IConfigGson {

    private final WorldPosition trainingSpawnPoint = new WorldPosition(0, 0, 0, 0);
    private final WorldPosition serverSpawnPoint = new WorldPosition(0, 0, 0, 0);
    private final List<ConfigTrainerData> trainerList = new ArrayList<>();

    @Override
    public File getConfigFile() {
        return new File("config/training/config.json");
    }
}
