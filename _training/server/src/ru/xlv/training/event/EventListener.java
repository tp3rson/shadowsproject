package ru.xlv.training.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import noppes.npcs.entity.EntityCustomNpc;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.event.*;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.mochar.player.character.skill.result.SkillExecuteResult;
import ru.xlv.mochar.util.Utils;
import ru.xlv.training.common.trainer.ITrainer;
import ru.xlv.training.common.trainer.MarkerTrainer;
import ru.xlv.training.handle.TrainingHandler;
import ru.xlv.training.handle.TrainingManager;

@RequiredArgsConstructor
@SuppressWarnings("unused")
public class EventListener {

    private static final long CHECK_COLLIDES_PERIOD = 200L;

    private final TrainingManager trainingManager;
    private final TrainingHandler trainingHandler;

    private long lastCheckCollidesTimeMills;

    @SubscribeEvent
    public void event(ServerPlayerLoginEvent event) {
        trainingManager.startTraining(FlexPlayer.of(event.getServerPlayer()));
    }

    @SubscribeEvent
    public void event(ServerPlayerLogoutEvent event) {
        trainingManager.resetTraining(FlexPlayer.of(event.getServerPlayer()));
    }

    @SubscribeEvent
    public void event(LivingEvent.LivingUpdateEvent event) {
        if(event.entityLiving instanceof EntityPlayer && System.currentTimeMillis() - lastCheckCollidesTimeMills >= CHECK_COLLIDES_PERIOD) {
            ITrainer activeTrainer = trainingManager.getActiveTrainer(event.entityLiving.getCommandSenderName());
            if (activeTrainer instanceof MarkerTrainer && Utils.isPlayerInside((EntityPlayer) event.entityLiving, ((MarkerTrainer) activeTrainer).getWorldPosition(), ((MarkerTrainer) activeTrainer).getRadius())) {
                WorldPosition worldPosition = ((MarkerTrainer) activeTrainer).getWorldPosition();
                trainingHandler.interactMarker((EntityPlayer) event.entityLiving, event.entityLiving.dimension, worldPosition.getX(), worldPosition.getY(), worldPosition.getZ(), ((MarkerTrainer) activeTrainer).getRadius());
            }
        }
    }

    @SubscribeEvent
    public void event(EntityInteractEvent event) {
        if(event.entity instanceof EntityCustomNpc) {
            trainingHandler.interactNpc(event.entityPlayer.getCommandSenderName(), ((EntityCustomNpc) event.entity).getCustomNameTag());
        }
    }

    @SubscribeEvent
    public void event(PlayerUsedSkillEvent event) {
        if(event.getSkillExecuteResult() == SkillExecuteResult.SUCCESS) {
            trainingHandler.skillUse(event.getServerPlayer().getPlayerName(), event.getSkill().getSkillType().getSkillId());
        }
    }

    @SubscribeEvent
    public void event(PlayerLearnedSkillEvent event) {
        trainingHandler.skillLearn(event.getServerPlayer().getPlayerName(), event.getSkill().getSkillType().getSkillId());
    }

    @SubscribeEvent
    public void event(PlayerSelectSkillEvent event) {
        trainingHandler.skillSelect(event.getServerPlayer().getPlayerName(), event.getSkill().getSkillType().getSkillId());
    }

    @SubscribeEvent
    public void event(PlayerEquipItemEvent event) {
        trainingHandler.equipItem(event.getServerPlayer().getPlayerName(), event.getItemStack().getItem());
    }
}
