package ru.xlv.core.network;

import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.event.PlayerKilledClientEvent;

import java.io.IOException;

@NoArgsConstructor
public class PacketPlayerDeathEvent implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        int entityId = bbis.readInt();
        Entity entityByID = Minecraft.getMinecraft().theWorld.getEntityByID(entityId);
        if(entityByID instanceof EntityPlayer) {
            XlvsCoreCommon.EVENT_BUS.post(new PlayerKilledClientEvent((EntityPlayer) entityByID));
        }
    }
}
