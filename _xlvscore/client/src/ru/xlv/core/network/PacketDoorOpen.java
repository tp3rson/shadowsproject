package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketDoorOpen implements IPacketOut {

    private int x, y, z;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(x);
        bbos.writeInt(y);
        bbos.writeInt(z);
    }
}
