package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionOpenClose implements IPacketOut {

    private boolean open;
    private int entityId = -1;
    private int x, y, z;

    public PacketMatrixInventoryTransactionOpenClose(int entityId, boolean open) {
        this.entityId = entityId;
        this.open = open;
    }

    public PacketMatrixInventoryTransactionOpenClose(int x, int y, int z, boolean open) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.open = open;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(entityId);
        bbos.writeInt(x);
        bbos.writeInt(y);
        bbos.writeInt(z);
        bbos.writeBoolean(open);
    }
}
