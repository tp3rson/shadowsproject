package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Collection;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionItemMultiMove implements IPacketCallbackEffective<Boolean> {

    private Collection<int []> itemPositions;
    private boolean fromMyInventory, toMyInventory;

    private boolean success;

    public PacketMatrixInventoryTransactionItemMultiMove(boolean fromMyInventory, boolean toMyInventory, Collection<int []> itemPositions) {
        this.itemPositions = itemPositions;
        this.fromMyInventory = fromMyInventory;
        this.toMyInventory = toMyInventory;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(fromMyInventory);
        bbos.writeBoolean(toMyInventory);
        bbos.writeInt(itemPositions.size());
        for (int[] itemPosition : itemPositions) {
            bbos.writeInt(itemPosition[0]);
            bbos.writeInt(itemPosition[1]);
        }
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        success = bbis.readBoolean();
    }

    @Nullable
    @Override
    public Boolean getResult() {
        return success;
    }
}
