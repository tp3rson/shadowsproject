package ru.xlv.core.network.matrix;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.event.ContainerMatrixInventoryOpenEvent;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionGuiOpen implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        int x = bbis.readInt();
        int y = bbis.readInt();
        int z = bbis.readInt();
        XlvsCoreCommon.EVENT_BUS.post(new ContainerMatrixInventoryOpenEvent(x, y, z));
    }
}
