package ru.xlv.core.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallback;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillUse implements IPacketCallback {

    private int id;

    public PacketSkillUse(int id) {
        this.id = id;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(id);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        PacketSkillBuildSync.readSkillBuilds(bbis);
    }
}
