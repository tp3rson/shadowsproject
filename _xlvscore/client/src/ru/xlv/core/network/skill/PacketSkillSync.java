package ru.xlv.core.network.skill;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.skill.SkillIO;
import ru.xlv.mochar.XlvsMainMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        SkillIO.readSkillList(bbis).forEach(skill -> XlvsMainMod.INSTANCE.getClientMainPlayer().updateCharacterSkill(skill));
    }
}
