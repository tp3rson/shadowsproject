package ru.xlv.core.network.skill;

import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mochar.XlvsMainMod;

import java.io.IOException;

public class PacketSkillBuildSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        XlvsMainMod.INSTANCE.getClientMainPlayer().setCurrentSkillBuildIndex(bbis.readInt());
        readSkillBuilds(bbis);
    }

    public static void readSkillBuilds(ByteBufInputStream bbis) throws IOException {
        int skillBuildCount = bbis.readInt();
        for (int j = 0; j < skillBuildCount; j++) {
            int skillBuildIndex = bbis.readInt();
            int activeSkills = bbis.readInt();
            int[] skillIds = new int[activeSkills];
            for (int i = 0; i < activeSkills; i++) {
                skillIds[i] = bbis.readInt();
            }
            XlvsMainMod.INSTANCE.getClientMainPlayer().setActiveSkill(skillBuildIndex, skillIds);
            int passiveSkills = bbis.readInt();
            skillIds = new int[passiveSkills];
            for (int i = 0; i < passiveSkills; i++) {
                skillIds[i] = bbis.readInt();
            }
            XlvsMainMod.INSTANCE.getClientMainPlayer().setPassiveSkill(skillBuildIndex, skillIds);
        }
        XlvsMainMod.INSTANCE.getClientMainPlayer().setAvailableActiveSkillSlotAmount(bbis.readInt());
        XlvsMainMod.INSTANCE.getClientMainPlayer().setAvailablePassiveSkillSlotAmount(bbis.readInt());
    }
}
