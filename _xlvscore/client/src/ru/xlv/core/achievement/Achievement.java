package ru.xlv.core.achievement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Achievement {

    private String name;
    private String description;
    private String imageURL;
    private int progress, maxProgress;
}
