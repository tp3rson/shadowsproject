package ru.xlv.core.player;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.player.character.CharacterType;

@Setter
@Getter
@RequiredArgsConstructor
public class ClientPlayer {
    private final String playerName;
    private boolean isOnline;
    private CharacterType characterType;
    private int level;
    private long lastOnlinePeriodTimeMills;
    private ItemStack currentBracers;
}
