package ru.xlv.core.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiModule extends GuiExtended {

    private int widthTemp, heightTemp;

    private boolean isActive;

    public GuiModule(GuiScreen parent, int width, int height) {
        super(parent);
        this.widthTemp = width;
        this.heightTemp = height;
    }

    public GuiModule(int width, int height) {
        this.widthTemp = width;
        this.heightTemp = height;
    }

    @Override
    public void initGui() {
        super.initGui();
        width = widthTemp;
        height = heightTemp;
    }

    @Override
    public void actionPerformed(GuiButton guiButton) {
        super.actionPerformed(guiButton);
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
