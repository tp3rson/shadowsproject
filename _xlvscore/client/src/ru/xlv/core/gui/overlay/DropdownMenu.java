package ru.xlv.core.gui.overlay;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
public class DropdownMenu {

    private final float x, y;
    private final float width, height;
    private final String name;
    private final List<DropdownElement> elements = new ArrayList<>();

    public DropdownMenu(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.name = null;
    }

    private boolean hovered;

    public void addElement(DropdownElement dropdown) {
        elements.add(dropdown);
    }

    public void render(GuiScreen parent, int mx, int my, float partialTicks) {
        if(isMouseOver(mx, my, 0)) {
            if(!hovered) {
                hovered = true;
            } else {
                boolean flag = false;
                for (int i = 0; i < elements.size(); i++) {
                    if(isMouseOver(mx, my, i)) {
                        flag = true;
                        break;
                    }
                }
                hovered = flag;
            }
        }
        for (int i = 0; i < elements.size(); i++) {
            int color = Integer.MIN_VALUE;
            if (isMouseOver(mx, my, i)) {
                color >>= 16;
            }
            Gui.drawRect(x, y + height * i, x + width, y + height * (i + 1), color);
            GL11.glColor4f(1, 1, 1, 1);
//            GuiDrawUtils.drawString(FontType.HelveticaNeueCyrLight, elements.get(i).getName(), Mouse.getX(), parent.height - Mouse.getY() - 1, 1F, 0xffffff);
            GL11.glPushMatrix();
            GL11.glTranslatef(x + parent.mc.fontRenderer.getStringWidth(elements.get(i).getName()) / 2F - 5, y, 0);
            GL11.glScalef(2, 2, 2);
            parent.mc.fontRenderer.drawString(elements.get(i).getName(), 0, 0, 0xffffff);
            GL11.glPopMatrix();
            GL11.glColor4f(1, 1, 1, 1);
        }
    }

    public boolean mouseClick(int mx, int my) {
        for (int i = 0; i < elements.size(); i++) {
            if (isMouseOver(mx, my, i)) {
                elements.get(i).getRunnable().run();
                hovered = false;
                return true;
            }
        }
        return false;
    }

    private boolean isMouseOver(int mx, int my, int index) {
        return mx >= x && mx < x + width && my >= y + height * index && my < y + height * (index + 1);
    }
}
