package ru.xlv.core.gui.button;

import net.minecraft.util.ResourceLocation;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.utils.Utils;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.gui.GuiCharacterMainMenu;
import ru.xlv.customfont.FontType;

public class ButtonCharacterItem extends GuiButtonAdvanced {

    private final CharacterType characterType;
    private final int level;
    private boolean isActive;
    private ResourceLocation classTexture;

    public ButtonCharacterItem(int buttonId, float x, float y, float widthIn, float heightIn, CharacterType characterType, int level) {
        super(buttonId, x, y, widthIn, heightIn, "");
        this.characterType = characterType;
        this.level = level;
        if(characterType == CharacterType.TANK) {
            classTexture = GuiCharacterMainMenu.textureCharacterTank;
        } else if(characterType == CharacterType.ASSASSIN) {
            classTexture = GuiCharacterMainMenu.textureCharacterStormTrooper;
        } else if(characterType == CharacterType.MEDIC) {
            classTexture = GuiCharacterMainMenu.textureCharacterMedic;
        }
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isActive() {
        return isActive;
    }

    public CharacterType getCharacterType() {
        return characterType;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            Utils.bindTexture(GuiCharacterMainMenu.resLocCharacterItemBg);
            if(isHovered(mouseX, mouseY) && textureHover != null) {
                Utils.bindTexture(textureHover);
            }
            GuiDrawUtils.drawRect(xPosition, yPosition, width, height);
            if(characterType != null) {
                GuiDrawUtils.drawRect(classTexture, xPosition, yPosition, height, height);
            }
            
        	float rectSize = height / 1.9f;
        	float x = xPosition + width / 1.155f;
        	float y = yPosition + height / 4.2f;
            GuiDrawUtils.drawRect(GuiCharacterMainMenu.resLocEllipse, x, y, rectSize, rectSize);
            drawText();
        }
    }

    @Override
    protected void drawText() {
        int j = 14737632;

        if (packedFGColour != 0) {
            j = packedFGColour;
        } else if (!this.enabled) {
            j = 10526880;
        } else if (this.field_146123_n) {
            j = 16777120;
        }
        if(isActive)
            j = 16777120;

        float textScale = 2.4f;
        GuiDrawUtils.drawStringNoXYScale(FontType.FUTURA_PT_DEMI, "Класс: " + characterType.getDisplayName(), this.xPosition + height * 1.2f, this.yPosition + height / 2.7f, textScale, j);

//        float textScale = height / 38f;
//        String charName = "Никитосик";
//        DrawUtils.drawString(FontType.FUTURA_PT_DEMI, charName, this.xPosition + height * 1.15f, this.yPosition + height / 3.8f, textScale, textScale, j);
//        textScale = height / 70f;
//        String charString = "Герой: " + characterType.getDisplayName();
//        DrawUtils.drawString(FontType.OpenSans_Italic, charString, this.xPosition + height * 1.2f, this.yPosition + height / 1.7f, textScale, textScale, 0x6d6d6d);
        String levelString = "" + level;
        textScale = 2f;
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.FUTURA_PT_BOLD, levelString, this.xPosition + this.width - width / 10.5f, this.yPosition + (this.height - height / 5f) / 2f, textScale, 0xe79839);
    }
}
