package ru.xlv.core.gui.button;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;
import ru.krogenit.client.gui.api.GuiDrawUtils;

public class Button extends GuiButton {

    private boolean drawBorder;
    private int borderColor;
    private float textScale = 1;
    private int textColor = -1;
    private boolean checkHover = true;

    public Button(int buttonId, float x, float y, float widthIn, float heightIn, String buttonText) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
    }

    public void setCheckHover(boolean checkHover) {
        this.checkHover = checkHover;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public void setTextScale(float textScale) {
        this.textScale = textScale;
    }

    public void enableBorder() {
        this.drawBorder = true;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        if (this.visible) {
            drawButtonBody(mc, mouseX, mouseY);
            if(drawBorder) {
                drawBorder(xPosition, yPosition, width, height, borderColor);
            }
            drawText(mc, 0, 0);
        }
    }

    private void drawBorder(float x, float y, float w, float h, int color) {
        Gui.drawRect(x, y, x + w, y + 1, color);
        Gui.drawRect(x, y + h - 1, x + w, y + h, color);
        Gui.drawRect(x, y + 1, x + 1, y + h - 1, color);
        Gui.drawRect(x + w - 1, y + 1, x + w, y + h - 1, color);
    }

    public void drawButtonBody(Minecraft mc, int mouseX, int mouseY) {
        GL11.glColor4f(1, 1, 1, 1);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
        this.mouseDragged(mc, mouseX, mouseY);
    }

    protected void drawText(Minecraft mc, int xOffset, int yOffset) {
        FontRenderer fontrenderer = mc.fontRenderer;
        int j = 14737632;

        if (packedFGColour != 0)
        {
            j = packedFGColour;
        }
        else
        if (!this.enabled)
        {
            j = 10526880;
        }
        else if (this.field_146123_n)
        {
            j = 16777120;
        }
        if(textColor != -1)
            j = textColor;
        GL11.glPushMatrix();
        GL11.glTranslatef(this.xPosition + this.width / 2 + xOffset, this.yPosition + (this.height - 8 * textScale) / 2 + yOffset, 0);
        GL11.glScalef(textScale, textScale, textScale);
        this.drawCenteredString(fontrenderer, this.displayString, 0, 0, j);
        GL11.glPopMatrix();
    }

    protected boolean isHovered(int mouseX, int mouseY) {
        return checkHover && (this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height);
    }
}
