package ru.xlv.core.renderer.texture;

import net.minecraft.client.renderer.texture.AbstractTexture;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.src.Config;
import net.minecraft.src.GlStateManager;
import shadersmod.client.MultiTexID;
import shadersmod.client.ShadersTex;

import java.awt.image.BufferedImage;
import java.util.Arrays;

public class LightTexture extends AbstractTexture {

    private final int[] dynamicTextureData;
    public final int width;
    public final int height;
    private boolean shadersInitialized;

    public LightTexture(int[] bytes, int width, int height) {
        this.shadersInitialized = false;
        this.dynamicTextureData = bytes;
        this.width = width;
        this.height = height;
        if (Config.isShaders()) {
            initDynamicTexture(this.width, this.height, this);
            this.shadersInitialized = true;
        } else {
            TextureUtil.allocateTexture(this.getGlTextureId(), width, height);
        }
        this.updateDynamicTexture();
    }

    public LightTexture(BufferedImage par1BufferedImage) {
        this(par1BufferedImage.getWidth(), par1BufferedImage.getHeight());
        par1BufferedImage.getRGB(0, 0, par1BufferedImage.getWidth(), par1BufferedImage.getHeight(), this.dynamicTextureData, 0, par1BufferedImage.getWidth());
        this.updateDynamicTexture();
    }

    public LightTexture(int par1, int par2) {
        this.shadersInitialized = false;
        this.width = par1;
        this.height = par2;
        this.dynamicTextureData = new int[par1 * par2 * 3];

        if (Config.isShaders()) {
            initDynamicTexture(this.width, this.height, this);
            this.shadersInitialized = true;
        } else {
            TextureUtil.allocateTexture(this.getGlTextureId(), par1, par2);
        }
    }

    public void loadTexture(IResourceManager par1ResourceManager) {}

    public void updateDynamicTexture() {
        if (Config.isShaders()) {
            if (!this.shadersInitialized) {
                initDynamicTexture(this.width, this.height, this);
            }
            MultiTexID multiTex = getMultiTexID();
            GlStateManager.bindTexture(multiTex.base);
            ShadersTex.updateDynTexSubImage1(dynamicTextureData, width, height, 0, 0, 0);
            GlStateManager.bindTexture(multiTex.norm);
            ShadersTex.updateDynTexSubImage1(dynamicTextureData, width, height, 0, 0, 1);
            GlStateManager.bindTexture(multiTex.spec);
            ShadersTex.updateDynTexSubImage1(dynamicTextureData, width, height, 0, 0, 2);
            GlStateManager.bindTexture(multiTex.base);
        } else {
            TextureUtil.uploadTexture(this.getGlTextureId(), this.dynamicTextureData, this.width, this.height);
        }
    }

    public int[] getTextureData() {
        return this.dynamicTextureData;
    }

    private static void initDynamicTexture(int width, int height, LightTexture tex) {
        MultiTexID multiTex = tex.getMultiTexID();
        int[] aint = tex.getTextureData();
        int size = width * height;
        Arrays.fill(aint, size, size * 2, -8421377);
        Arrays.fill(aint, size * 2, size * 3, 0);
        TextureUtil.allocateTexture(multiTex.base, width, height);
        TextureUtil.setTextureFiltering(false, false);
        TextureUtil.setTextureClamped(false);
        TextureUtil.allocateTexture(multiTex.norm, width, height);
        TextureUtil.setTextureFiltering(false, false);
        TextureUtil.setTextureClamped(false);
        TextureUtil.allocateTexture(multiTex.spec, width, height);
        TextureUtil.setTextureFiltering(false, false);
        TextureUtil.setTextureClamped(false);
        GlStateManager.bindTexture(multiTex.base);
    }
}
