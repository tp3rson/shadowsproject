package ru.xlv.core.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import ru.xlv.core.common.item.tag.EnumItemTag;

import java.util.List;

public class KeyBindingDrop extends KeyBinding {

    public KeyBindingDrop(String p_i45001_1_, int p_i45001_2_, String p_i45001_3_) {
        super(p_i45001_1_, p_i45001_2_, p_i45001_3_);
    }

    @Override
    public boolean isPressed() {
        if(Minecraft.getMinecraft().thePlayer != null) {
            List<EnumItemTag> list = EnumItemTag.getItemTags(Minecraft.getMinecraft().thePlayer.getHeldItem());
            if(list != null) {
                for (EnumItemTag itemTag : list) {
                    if(!EnumItemTag.isTagCanBeDropped(itemTag)) {
                        return false;
                    }
                }
            }
        }
        return super.isPressed();
    }
}
