package ru.xlv.core.util.data;

import net.minecraft.client.Minecraft;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.util.data.json.CharacterLevelParser;
import ru.xlv.core.util.data.json.NewsParser;
import ru.xlv.parser.IJsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RequestHandler {

    private ExecutorService executorService;

    private final String BASE_ADDRESS = "https://shadowsproject.ru/api/";

    public RequestHandler() {
        executorService = Executors.newSingleThreadExecutor();
    }

    public CompletableFuture<Boolean> loadCharacterLevels(OnDataAvailable<Map<CharacterType, Integer>> onDataAvailable) {
        return CommonUtils.supplyAsync(() -> {
            String PAGE_CHARACTER_LEVELS = String.format("users/%s/level", Minecraft.getMinecraft().getSession().getUsername());
//        String PAGE_CHARACTER_LEVELS = String.format("users/%s/level", "Erriour");
            doParsedRequest(BASE_ADDRESS + PAGE_CHARACTER_LEVELS, CharacterLevelParser.class, onDataAvailable);
            return true;
        });
    }

    public CompletableFuture<Boolean> loadNews(OnDataAvailable<NewsList> onDataAvailable) {
        return CommonUtils.supplyAsync(() -> {
            String PAGE_NEWS = "articles/";
            doParsedRequest(BASE_ADDRESS + PAGE_NEWS, NewsParser.class, onDataAvailable);
            return true;
        });
    }

    private <T> void doParsedRequest(String address, Class<? extends IJsonParser<T>> jsonParserClass, OnDataAvailable<T> onDataAvailable) {
        String request = null;
        try {
            request = doRequest(address);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(request == null) throw new NullPointerException("No response from server!");
        IJsonParser<T> jsonParser = null;
        try {
            jsonParser = jsonParserClass.getConstructor(String.class).newInstance(request);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        if(jsonParser != null) {
            jsonParser.parse();
            onDataAvailable.onDataAvailable(jsonParser.getResult());
        }
    }

    private String doRequest(String request) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(request).openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        connection.setUseCaches(false);

        String redirect = connection.getHeaderField("Location");
        if (redirect != null) {
            connection = (HttpURLConnection) new URL(redirect).openConnection();
        }

        connection.connect();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }

    public interface OnDataAvailable<T> {
        void onDataAvailable(T data);
    }
}
