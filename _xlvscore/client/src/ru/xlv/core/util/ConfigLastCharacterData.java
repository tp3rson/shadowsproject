package ru.xlv.core.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@AllArgsConstructor
public class ConfigLastCharacterData implements IConfigGson {

    @ToString
    @Getter
    @RequiredArgsConstructor
    public static class CharacterData {
        @IgnoreObf
        @Configurable
        private final int characterTypeOrdinal;
        @IgnoreObf
        @Configurable
        private final String headItemName;
        @IgnoreObf
        @Configurable
        private final String bodyItemName;
        @IgnoreObf
        @Configurable
        private final String bracersItemName;
        @IgnoreObf
        @Configurable
        private final String pantsItemName;
        @IgnoreObf
        @Configurable
        private final String botsItemName;
        @IgnoreObf
        @Configurable
        private final int characterLevel;

        public CharacterType getCharacterType() {
            return CharacterType.values()[characterTypeOrdinal];
        }
    }

    @IgnoreObf
    @Configurable
    private final List<CharacterData> characterDataList = new ArrayList<>();

    public CharacterData getCharacterData(CharacterType characterType) {
        for (CharacterData characterData : characterDataList) {
            if(characterData.getCharacterType() == characterType) {
                return characterData;
            }
        }
        return null;
    }

    public void setCharacterData(CharacterData characterData) {
        characterDataList.removeIf(characterData1 -> characterData1.getCharacterType() == characterData.getCharacterType());
        characterDataList.add(characterData);
    }

    @Override
    public File getConfigFile() {
        return new File("config/last_character_data.out");
    }
}
