package ru.xlv.core.common.entity;

import cpw.mods.fml.common.registry.EntityRegistry;
import net.minecraft.entity.Entity;

public class EntityRegisterHelper {

    public static void register(Class<? extends Entity> clazz) {
        EntityRegistry.registerGlobalEntityID(clazz, clazz.getSimpleName(), EntityRegistry.findGlobalUniqueEntityId());
    }

//    @SideOnly(Side.CLIENT)
//    public static void registerRender(Class<? extends Entity> clazz, Render render) {
//        RenderingRegistry.registerEntityRenderingHandler(clazz, render);
//    }
//
//    @SideOnly(Side.CLIENT)
//    public static void registerRender(Class<? extends Entity> clazz, SimpleRender simpleRender) {
//        RenderingRegistry.registerEntityRenderingHandler(clazz, new Render() {
//            @Override
//            public void doRender(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {
//                simpleRender.render(entity, x, y, z, rotationYaw, tickTime);
//            }
//
//            @Override
//            public void doRenderPost(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {}
//
//            @Override
//            protected ResourceLocation getEntityTexture(Entity entity) {
//                return null;
//            }
//        });
//    }
//
//    @SideOnly(Side.CLIENT)
//    public interface SimpleRender {
//        void render(Entity entity, double x, double y, double z, float rotationYaw, float tickTime);
//    }
}
