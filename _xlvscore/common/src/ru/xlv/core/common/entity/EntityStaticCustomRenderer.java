package ru.xlv.core.common.entity;

import net.minecraft.world.World;

public class EntityStaticCustomRenderer extends EntityStatic {

    public static final int UNIQUE_ENTITY_NAME_DATA_ID = 32;

    private String uniqueEntityName;

    public EntityStaticCustomRenderer(World p_i1681_1_) {
        super(p_i1681_1_);
    }

    public EntityStaticCustomRenderer(World p_i1681_1_, String uniqueEntityName) {
        super(p_i1681_1_);
        this.uniqueEntityName = uniqueEntityName;
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        if(uniqueEntityName != null) {
            getDataWatcher().addObject(UNIQUE_ENTITY_NAME_DATA_ID, uniqueEntityName);
        }
    }

    public void setUniqueEntityName(String uniqueEntityName) {
        getDataWatcher().updateObject(UNIQUE_ENTITY_NAME_DATA_ID, uniqueEntityName);
    }

    public String getUniqueEntityName() {
        return getDataWatcher().getWatchableObjectString(UNIQUE_ENTITY_NAME_DATA_ID);
    }
}
