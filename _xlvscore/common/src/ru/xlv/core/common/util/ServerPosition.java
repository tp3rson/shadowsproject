package ru.xlv.core.common.util;

import lombok.Getter;

@Getter
public class ServerPosition extends WorldPosition {

    private final String serverAddress;

    public ServerPosition(String serverAddress, int dimension, double x, double y, double z) {
        super(dimension, x, y, z);
        this.serverAddress = serverAddress;
    }
}
