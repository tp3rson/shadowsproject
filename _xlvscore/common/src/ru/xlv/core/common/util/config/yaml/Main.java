package ru.xlv.core.common.util.config.yaml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

//    public static void main(String... ss) {
//        IConfigYaml configYaml = () -> "1/test.yml";
//        try {
//            YamlModel model1 = new YamlModel("a", "", 1, 1);
//            model1.setModels(Arrays.asList(new YamlModel("jopa", "", 1, 1), new YamlModel("jopa1", "", 1, 1)));
//            model1.setStrings(Arrays.asList("s", "ss", "sss"));
//            configYaml.saveAsMap(model1);
////            Map<String, Map<String, ?>> map = configYaml.loadAsMap(new YamlModel());
////            YamlModel model = configYaml.getObjectFromMap(YamlModel.class, map.get(YamlModel.class.getSimpleName()));
//            YamlModel model = configYaml.load1(YamlModel.class);
//            System.out.println(model.name);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public static class YamlModel implements IMappedYamlModel {

        String name, description;
        int id;
        long time;
        List<YamlModel> models;
        List<String> strings;

        public YamlModel() {
        }

        public YamlModel(String name, String description, int id, long time) {
            this.name = name;
            this.description = description;
            this.id = id;
            this.time = time;
        }

        public List<YamlModel> getModels() {
            return models;
        }

        public List<String> getStrings() {
            return strings;
        }

        public void setModels(List<YamlModel> models) {
            this.models = models;
        }

        public void setStrings(List<String> strings) {
            this.strings = strings;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public int getId() {
            return id;
        }

        public long getTime() {
            return time;
        }

        @Override
        public Map<Class<?>, String> getTags() {
            Map<Class<?>, String> map = new HashMap<>();
//            map.put(getClass(), "!ModelTag");
            return map;
        }
    }
}
