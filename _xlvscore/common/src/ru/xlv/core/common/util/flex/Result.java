package ru.xlv.core.common.util.flex;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Getter
@RequiredArgsConstructor
public abstract class Result<T extends Result<T, V>, V extends Enum<?>> {

    private final V type;

    public T handleSuccess(Runnable runnable) {
        if (isSuccess()) {
            runnable.run();
        }
        //noinspection unchecked
        return (T) this;
    }

    public <U> U handleSuccess(Supplier<U> supplier) {
        return supplier.get();
    }

    public T handleFailure(Consumer<T> consumer) {
        if (isSuccess()) {
            //noinspection unchecked
            consumer.accept((T) this);
        }
        //noinspection unchecked
        return (T) this;
    }

    public <U> U handleFailure(Function<T, U> function) {
        //noinspection unchecked
        return function.apply((T) this);
    }

    public abstract boolean isSuccess();
}
