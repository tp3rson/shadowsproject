package ru.xlv.core.common.util;

public interface IResult {

    boolean isSuccess();

    default boolean isFailure() {
        return !isSuccess();
    }
}
