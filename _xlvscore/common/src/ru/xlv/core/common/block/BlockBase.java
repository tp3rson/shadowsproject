package ru.xlv.core.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockBase extends Block {

    public BlockBase(String name) {
        this(name, Material.rock);
    }

    public BlockBase(String name, Material p_i45394_1_) {
        super(p_i45394_1_);
        setBlockName(name);
        setBlockTextureName(name);
    }
}
