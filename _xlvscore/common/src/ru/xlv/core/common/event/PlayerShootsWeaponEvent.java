package ru.xlv.core.common.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

@Getter
@RequiredArgsConstructor
@Cancelable
public class PlayerShootsWeaponEvent extends Event {

    private final EntityPlayer entityPlayer;
    private final ItemStack itemStack;
}
