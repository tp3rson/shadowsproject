package ru.xlv.core.common.item;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.item.quality.EnumItemQuality;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public class ItemIOUtils {

    public static void write(ItemBase itemBase, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeUTF(itemBase.getUnlocalizedName());
        byteBufOutputStream.writeInt(itemBase.getInvMatrixWidth());
        byteBufOutputStream.writeInt(itemBase.getInvMatrixHeight());
        byteBufOutputStream.writeInt(itemBase.getItemRarity().ordinal());
        byteBufOutputStream.writeInt(itemBase.getItemQuality().ordinal());
        byteBufOutputStream.writeUTF(itemBase.getDescription());
        byteBufOutputStream.writeUTF(itemBase.getSlogan());
        byteBufOutputStream.writeUTF(itemBase.getDisplayName());
        byteBufOutputStream.writeUTF(itemBase.getSecondName());
        byteBufOutputStream.writeInt(itemBase.getItemTags().size());
        for (EnumItemTag itemTag : itemBase.getItemTags()) {
            byteBufOutputStream.writeInt(itemTag.ordinal());
        }
    }

    public static void read(ByteBufInputStream byteBufInputStream) throws IOException {
        String name = byteBufInputStream.readUTF();
        for (ItemBase itemBase : ItemRegistry.REGISTRY) {
            if(itemBase.getUnlocalizedName().equals(name)) {
                itemBase.setInvMatrixSize(byteBufInputStream.readInt(), byteBufInputStream.readInt());
                itemBase.setItemRarity(EnumItemRarity.values()[byteBufInputStream.readInt()]);
                itemBase.setItemQuality(EnumItemQuality.values()[byteBufInputStream.readInt()]);
                itemBase.setDescription(byteBufInputStream.readUTF());
                itemBase.setSlogan(byteBufInputStream.readUTF());
                itemBase.setDisplayName(byteBufInputStream.readUTF());
                itemBase.setSecondName(byteBufInputStream.readUTF());
                String[] ss = new String[byteBufInputStream.readInt()];
                for (int i = 0; i < ss.length; i++) {
                    ss[i] = EnumItemTag.values()[byteBufInputStream.readInt()].name();
                }
                ItemRegistry.addItemTags(itemBase, ss);
                break;
            }
        }
    }
}
