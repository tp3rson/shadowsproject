package ru.xlv.core.common.item;

import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ItemArmor extends ItemBase implements IAttributeProvider, ISlotSpecified {

    public enum SetFamily {
        SCORPION,
        PTN,
        BKU_0992,
        AZUR_ES,
        CENSOR,
        NIHIL,
        MURUS_KL,
        ENIGMA_B,
        SPEC_RIOT
    }

    protected final Map<CharacterAttributeType, CharacterAttributeMod> characterAttributeBoostMap = new HashMap<>();

    protected SetFamily setFamily;
    private final List<MatrixInventory.SlotType> slotTypes = new ArrayList<>();

    public ItemArmor(String name) {
        super(name);
    }

    public void addSlotType(MatrixInventory.SlotType slotType) {
        this.slotTypes.add(slotType);
    }
}
