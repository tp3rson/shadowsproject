package ru.xlv.core.common.item;

import ru.xlv.core.common.inventory.MatrixInventory;

import java.util.List;

public interface ISlotSpecified {

    List<MatrixInventory.SlotType> getSlotTypes();
}
