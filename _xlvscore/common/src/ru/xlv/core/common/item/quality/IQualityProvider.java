package ru.xlv.core.common.item.quality;

public interface IQualityProvider {

    EnumItemQuality getItemQuality();
}
