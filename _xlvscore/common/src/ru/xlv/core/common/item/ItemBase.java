package ru.xlv.core.common.item;

import lombok.*;
import lombok.experimental.Accessors;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import ru.xlv.core.common.inventory.MatrixInventoryUtils;
import ru.xlv.core.common.item.quality.EnumItemQuality;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.item.tag.EnumItemTag;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Builder
@AllArgsConstructor
public class ItemBase extends Item implements IItemAppliedInstantly {

    @Singular("itemTag")
    private List<EnumItemTag> itemTags = new ArrayList<>();

    @Singular("appliedInstantly")
    private List<AppliedInstantly> appliedInstantlyList = new ArrayList<>();

    @Accessors(fluent = true)
    private boolean removeAfterApplied;

    private int invMatrixWidth = MatrixInventoryUtils.DEFAULT_INV_MATRIX_ITEM_WIDTH;
    private int invMatrixHeight = MatrixInventoryUtils.DEFAULT_INV_MATRIX_ITEM_HEIGHT;

    private EnumItemQuality itemQuality = EnumItemQuality.BASIC;
    private EnumItemRarity itemRarity = EnumItemRarity.COMMON;

    private String description, slogan, displayName, secondName;
    private final String name;

    public ItemBase(String name) {
        this.name = name;
        super.setUnlocalizedName(name);
        super.setTextureName(name);
        super.setCreativeTab(CreativeTabs.tabMisc);
    }

    protected void init() {
        if(getUnlocalizedName().equals("item.null")) {
            setUnlocalizedName(getName());
            setTextureName("xlvscore:" + getName());
            setCreativeTab(CreativeTabs.tabMisc);
        }
    }

    public void setInvMatrixSize(int width, int height) {
        invMatrixWidth = width;
        invMatrixHeight = height;
    }
}
