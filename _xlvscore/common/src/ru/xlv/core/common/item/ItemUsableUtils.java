package ru.xlv.core.common.item;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

@Deprecated
public class ItemUsableUtils {

    public static void setUsable(ItemStack itemStack, int uses, int maxUses) {
        if(!isUsable(itemStack)) return;
        setUses(itemStack, uses);
        itemStack.getTagCompound().setInteger("maxUses", maxUses);
    }

    public static void setUses(ItemStack itemStack, int uses) {
        if(!isUsable(itemStack)) return;
        if(itemStack.getTagCompound() == null) {
            itemStack.setTagCompound(new NBTTagCompound());
        }
        itemStack.getTagCompound().setInteger("currentUses", uses);
    }

    public static boolean isUsable(ItemStack itemStack) {
        return itemStack != null && itemStack.getTagCompound() != null && itemStack.getTagCompound().hasKey("currentUses");
    }

    public static int getUses(ItemStack itemStack) {
        return isUsable(itemStack) ? itemStack.getTagCompound().getInteger("currentUses") : 0;
    }

    public static int getMaxUses(ItemStack itemStack) {
        return isUsable(itemStack) ? itemStack.getTagCompound().getInteger("maxUses") : 0;
    }
}
