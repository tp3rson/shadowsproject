package ru.xlv.core.common.player.character;

import lombok.Getter;
import ru.xlv.core.common.util.ConfigAttributes;

@Getter
public enum CharacterAttributeType {

    MAX_HEALTH("Здоровье", ConfigAttributes.INSTANCE.DEFAULT_VALUE_HEALTH_ATTRIBUTE, CharacterAttributeCategory.BASE),
    STAMINA("Стамина", ConfigAttributes.INSTANCE.DEFAULT_VALUE_STAMINA_ATTRIBUTE, CharacterAttributeCategory.BASE),
    MANA("Мана", ConfigAttributes.INSTANCE.DEFAULT_VALUE_MANA_ATTRIBUTE, ConfigAttributes.INSTANCE.DEFAULT_VALUE_MANA_ATTRIBUTE, CharacterAttributeCategory.BASE),
    MOVE_SPEED("Скорость передвижения", ConfigAttributes.INSTANCE.DEFAULT_VALUE_MOVE_SPEED_ATTRIBUTE, CharacterAttributeCategory.BASE),
//    SPRINT("Ускорение", 0, CharacterAttributeCategory.BASE),
    WEIGHT("Переносимый вес", ConfigAttributes.INSTANCE.DEFAULT_VALUE_WEIGHT_ATTRIBUTE, CharacterAttributeCategory.BASE),
    HEALTH_REGEN("Регенерация здоровья", ConfigAttributes.INSTANCE.DEFAULT_VALUE_HEALTH_REGEN_ATTRIBUTE, CharacterAttributeCategory.REGEN),
    STAMINA_REGEN("Восстановление сил", ConfigAttributes.INSTANCE.DEFAULT_VALUE_STAMINA_REGEN_ATTRIBUTE, CharacterAttributeCategory.REGEN),
    MANA_REGEN("Восстановление маны", ConfigAttributes.INSTANCE.DEFAULT_VALUE_MANA_REGEN_ATTRIBUTE, CharacterAttributeCategory.REGEN),
    MANA_MOD("Энергоэффективность", ConfigAttributes.INSTANCE.DEFAULT_VALUE_MANA_MODIF_ATTRIBUTE),
    DAMAGE_BASE_PROTECTION("Устойчивость", 1D, CharacterAttributeCategory.BASE),
    COOLDOWN_MOD("Модификатор перезарядки способностей", 1D, CharacterAttributeCategory.BASE),
    SKILL_REGEN_MOD("Модификатор лечения способностями", 1D, CharacterAttributeCategory.BASE),
    WEAPON_RELOAD_MOD("Модификатор перезарядки оружия", 1D, CharacterAttributeCategory.BASE),
    WEAPON_RECOIL_MOD("Модификатор отдачи оружия", 1D, CharacterAttributeCategory.BASE),

    BALLISTIC_PROTECTION("Баллистическая защита", 0, CharacterAttributeCategory.PROTECTION),
    CUT_PROTECTION("Режущая защита", 0, CharacterAttributeCategory.PROTECTION),
    ENERGY_PROTECTION("Энерго-устойчивость", 0, CharacterAttributeCategory.PROTECTION),
    THERMAL_PROTECTION("Теплостойкость", 0, CharacterAttributeCategory.PROTECTION),
    FIRE_PROTECTION("Огнестойкость", 0, CharacterAttributeCategory.PROTECTION),
    ELECT_PROTECTION("Электростойкость", 0, CharacterAttributeCategory.PROTECTION),
    TOXIC_PROTECTION("Устойчивость к токсинам", 0, CharacterAttributeCategory.PROTECTION),
    RADIATION_PROTECTION("Устойчивость к радиации", 0, CharacterAttributeCategory.PROTECTION),
    EXPLOSION_PROTECTION("Взрывоустойчивость", 0, CharacterAttributeCategory.PROTECTION),

    PENETRATION("Проникновение", 1D, CharacterAttributeCategory.BASE),
    DAMAGE_MOD("Модификатор урона", 1D, CharacterAttributeCategory.DAMAGE),
    BALLISTIC_DAMAGE("Баллистический урон", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.BALLISTIC_PROTECTION),
    CUT_DAMAGE("Режущий урон", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.CUT_PROTECTION),
    ENERGY_DAMAGE("Энергетический урон", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.ENERGY_PROTECTION),
    THERMAL_DAMAGE("Термальный урон", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.THERMAL_PROTECTION),
    FIRE_DAMAGE("Огненный урон", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.FIRE_PROTECTION),
    ELECTRIC_DAMAGE("Электрический урон", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.ELECT_PROTECTION),
    TOXIC_DAMAGE("Токсический урон", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.TOXIC_PROTECTION),
    RADIATION_DAMAGE("Радиационный урон", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.RADIATION_PROTECTION),
    EXPLOSION_DAMAGE("Урон взрывом", 0, CharacterAttributeCategory.DAMAGE, CharacterAttributeType.EXPLOSION_PROTECTION),
    FROZEN_DAMAGE("Урон холодом", 0, CharacterAttributeCategory.DAMAGE),
    FRACTAL_DAMAGE("Дробящий урон", 0, CharacterAttributeCategory.DAMAGE),
    PRICKLE_DAMAGE("Колющий урон", 0, CharacterAttributeCategory.DAMAGE);

    private CharacterAttributeCategory category;
    private CharacterAttributeType oppositeAttribute;
    private final String displayName;
    private String description;
    private final double defaultValue;
    private double maxValue;

    CharacterAttributeType(String displayName, double defaultValue) {
        this.displayName = displayName;
        this.defaultValue = this.maxValue = defaultValue;
    }

    CharacterAttributeType(String displayName, double defaultValue, double maxValue) {
        this(displayName, defaultValue);
        this.maxValue = maxValue;
    }

    CharacterAttributeType(String displayName, double defaultValue, CharacterAttributeCategory characterAttributeCategory) {
        this(displayName, defaultValue);
        this.category = characterAttributeCategory;
    }

    CharacterAttributeType(String displayName, double defaultValue, double maxValue, CharacterAttributeCategory characterAttributeCategory) {
        this(displayName, defaultValue, maxValue);
        this.category = characterAttributeCategory;
    }

    CharacterAttributeType(String displayName, double defaultValue, CharacterAttributeCategory characterAttributeCategory, CharacterAttributeType oppositeAttributeType) {
        this(displayName, defaultValue, characterAttributeCategory);
        this.oppositeAttribute = oppositeAttributeType;
    }

    CharacterAttributeType(String displayName, String description, double defaultValue) {
        this(displayName, defaultValue);
        this.description = description;
    }
}
