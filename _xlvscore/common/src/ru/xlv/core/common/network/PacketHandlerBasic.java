package ru.xlv.core.common.network;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLEventChannel;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import lombok.Getter;
import lombok.SneakyThrows;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@Getter
public abstract class PacketHandlerBasic  {

    private final FMLEventChannel channel;

    private final PacketRegistry packetRegistry;

    private final String channelName;

    public PacketHandlerBasic(PacketRegistry packetRegistry, String channelName) {
        this.packetRegistry = packetRegistry;
        this.channelName = channelName;
        channel = NetworkRegistry.INSTANCE.newEventDrivenChannel(getChannelName());
        channel.register(this);
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SuppressWarnings("unused")
    @SneakyThrows
    @SubscribeEvent
    public void onClientPacketReceived(FMLNetworkEvent.ClientCustomPacketEvent event) {
        if(event.packet.channel().equals(getChannelName())) {
            ByteBufInputStream byteBufInputStream = new ByteBufInputStream(event.packet.payload());
            onClientPacketReceived(byteBufInputStream);
            byteBufInputStream.close();
        }
    }

    @SuppressWarnings("unused")
    @SneakyThrows
    @SubscribeEvent
    public void onServerPacketReceived(FMLNetworkEvent.ServerCustomPacketEvent event) {
        if(event.packet.channel().equals(getChannelName())) {
            ByteBufInputStream byteBufInputStream = new ByteBufInputStream(event.packet.payload());
            onServerPacketReceived(((NetHandlerPlayServer) event.handler).playerEntity, byteBufInputStream);
            byteBufInputStream.close();
        }
    }

    protected void onClientPacketReceived(ByteBufInputStream bbis) throws IOException {}

    protected void onServerPacketReceived(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {}

    @SneakyThrows
    public void sendPacketToServer(IPacketOut packet) {
        ByteBufOutputStream byteBufOutputStream = new ByteBufOutputStream(Unpooled.buffer());
        byteBufOutputStream.writeInt(getPacketId(packet));
        packet.write(byteBufOutputStream);
        sendPacketToServer(byteBufOutputStream);
    }

    @SneakyThrows
    public void sendPacketToPlayer(EntityPlayer entityPlayer, IPacketOut packet) {
        ByteBufOutputStream byteBufOutputStream = new ByteBufOutputStream(Unpooled.buffer());
        byteBufOutputStream.writeInt(getPacketId(packet));
        packet.write(byteBufOutputStream);
        sendPacketToPlayer(entityPlayer, byteBufOutputStream);
    }

    @SneakyThrows
    public void sendPacketToPlayer(EntityPlayer entityPlayer, IPacketOutServer packet) {
        ByteBufOutputStream byteBufOutputStream = new ByteBufOutputStream(Unpooled.buffer());
        byteBufOutputStream.writeInt(getPacketId(packet));
        packet.write(entityPlayer, byteBufOutputStream);
        sendPacketToPlayer(entityPlayer, byteBufOutputStream);
    }

    @SneakyThrows
    public void sendPacketsToPlayer(EntityPlayer entityPlayer, IPacketOutServer... packets) {
        for (IPacketOutServer packet : packets) {
            sendPacketToPlayer(entityPlayer, packet);
        }
    }

    public void sendPacketToServer(ByteBufOutputStream bbos) throws IOException {
        FMLProxyPacket proxyPacket = new FMLProxyPacket(new PacketBuffer(bbos.buffer()), getChannelName());
        getChannel().sendToServer(proxyPacket);
        bbos.close();
    }

    public void sendPacketToPlayer(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        if(entityPlayer == null) return;
        FMLProxyPacket proxyPacket = new FMLProxyPacket(new PacketBuffer(bbos.buffer()), getChannelName());
        getChannel().sendTo(proxyPacket, (EntityPlayerMP) entityPlayer);
        bbos.close();
    }

    protected int getPacketId(IPacket packet) {
        if(!packetRegistry.getClassRegistry().containsKey(packet.getClass())) {
            throw new RuntimeException("Id of packet " + packet + " not found!");
        }
        return packetRegistry.getClassRegistry().get(packet.getClass());
    }

    protected IPacket getPacketById(int pid) {
        return packetRegistry.getRegistry().get(pid);
    }
}
