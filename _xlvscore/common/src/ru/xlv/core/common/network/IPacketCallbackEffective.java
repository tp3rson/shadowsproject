package ru.xlv.core.common.network;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

/**
 * @param <T> - тип ответа от сервера
 * */
public interface IPacketCallbackEffective<T> extends IPacketCallback {

    @Override
    void write(ByteBufOutputStream bbos) throws IOException;
    @Override
    void read(ByteBufInputStream bbis) throws IOException;

    /**
     * @return сконструированный ответ от сервера. Конструкцию ответа следует проводить в {@link IPacketCallbackEffective#read(ByteBufInputStream)}
     * */
    @Nullable
    T getResult();
}
