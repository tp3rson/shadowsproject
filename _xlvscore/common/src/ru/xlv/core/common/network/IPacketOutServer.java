package ru.xlv.core.common.network;

import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.entity.player.EntityPlayer;

import java.io.IOException;

public interface IPacketOutServer extends IPacket {

    /**
     * Calls when the packet is sending out.
     * */
    void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException;
}
