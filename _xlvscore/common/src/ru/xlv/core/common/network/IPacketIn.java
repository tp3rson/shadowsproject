package ru.xlv.core.common.network;

import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public interface IPacketIn extends IPacket {

    /**
     * Calls when the packet came from other side.
     * */
    void read(ByteBufInputStream bbis) throws IOException;

}
