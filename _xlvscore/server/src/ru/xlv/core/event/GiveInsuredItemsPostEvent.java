package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

@Getter
@RequiredArgsConstructor
@Cancelable
public class GiveInsuredItemsPostEvent extends Event {

    private final EntityPlayer entityPlayer;
    private final List<ItemStack> itemStackList;
}
