package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import ru.xlv.core.player.ServerPlayer;

public class ServerPlayerLoginEvent extends Event {

    private final ServerPlayer serverPlayer;

    public ServerPlayerLoginEvent(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
    }

    public ServerPlayer getServerPlayer() {
        return serverPlayer;
    }
}
