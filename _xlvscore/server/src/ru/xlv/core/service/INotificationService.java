package ru.xlv.core.service;

import ru.xlv.core.player.ServerPlayer;

public interface INotificationService {

    void sendNotification(ServerPlayer serverPlayer, String message);
}
