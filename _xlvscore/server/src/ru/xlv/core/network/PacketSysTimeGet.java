package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.PlayerSysTimeTracker;

import java.io.IOException;

@NoArgsConstructor
public class PacketSysTimeGet implements IPacketOutServer, IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        long time = bbis.readLong();
        PlayerSysTimeTracker.setSySTime(entityPlayer.getCommandSenderName(), time);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {}
}
