package ru.xlv.core.network;

import lombok.NoArgsConstructor;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@ControllablePacket(period = 200L)
@NoArgsConstructor
public class PacketDoorOpen implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int x = bbis.readInt();
        int y = bbis.readInt();
        int z = bbis.readInt();
        Block block = entityPlayer.worldObj.getBlock(x, y, z);
        if(block == Blocks.wooden_door) {
            block.onBlockActivated(entityPlayer.worldObj, x, y, z, entityPlayer, 0, 0, 0, 0);
            entityPlayer.worldObj.playAuxSFXAtEntity(null, 1003, x, y, z, 0);
        }
    }
}
