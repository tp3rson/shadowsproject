package ru.xlv.core.util;

import lombok.RequiredArgsConstructor;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class ScheduledConsumableTask<T> {

    private final long period;
    private final Consumer<T> consumer;
    private long lastTimeMills;

    public void update(T t) {
        if(System.currentTimeMillis() >= lastTimeMills) {
            lastTimeMills = System.currentTimeMillis() + period;
            consumer.accept(t);
        }
    }
}
