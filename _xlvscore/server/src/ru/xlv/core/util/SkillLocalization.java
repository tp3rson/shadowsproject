package ru.xlv.core.util;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;

import java.io.File;

@Getter
@Configurable
public class SkillLocalization extends Localization {

    private final String responseSkillLearnResultSuccessMessage = "responseSkillLearnResultSuccessMessage";
    private final String responseSkillLearnResultNotFoundMessage = "responseSkillLearnResultNotFoundMessage";
    private final String responseSkillLearnResultConditionsNotFulfilledMessage = "responseSkillLearnResultConditionsNotFulfilledMessage";
    private final String responseSkillLearnResultAlreadyLearnedMessage = "responseSkillLearnResultAlreadyLearnedMessage";

    private final String responseSkillBuildCreateSuccessMessage = "responseSkillBuildCreateSuccessMessage";
    private final String responseSkillBuildCreateNotAvailableSlotMessage = "responseSkillBuildCreateNotAvailableSlotMessage";
    private final String responseSkillBuildCreateUnknownsMessage = "responseSkillBuildCreateUnknownsMessage";

    private final String responseSkillSelectSuccessMessage = "responseSkillSelectSuccessMessage";
    private final String responseSkillSelectNotLearnedMessage = "responseSkillSelectNotLearnedMessage";
    private final String responseSkillSelectWrongSlotIndexMessage = "responseSkillSelectWrongSlotIndexMessage";
    private final String responseSkillSelectSameFamilyMessage = "responseSkillSelectSameFamilyMessage";
    private final String responseSkillSelectDoesNotExistMessage = "responseSkillSelectDoesNotExistMessage";

    @Override
    public File getConfigFile() {
        return new File("config/skill/localization.json");
    }
}
