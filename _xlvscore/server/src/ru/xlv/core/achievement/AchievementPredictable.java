package ru.xlv.core.achievement;

import ru.xlv.core.player.ServerPlayer;

import java.util.function.Predicate;

public class AchievementPredictable extends AchievementBase {

    private final Predicate<ServerPlayer> predicate;

    public AchievementPredictable(AchievementType achievementType, Predicate<ServerPlayer> predicate) {
        super(achievementType);
        this.predicate = predicate;
    }

    public void achieve(ServerPlayer serverPlayer) {
        if (!isAchieved && predicate.test(serverPlayer)) {
            isAchieved = true;
        }
    }

    @Override
    @Deprecated
    public void achieve() {
        isAchieved = true;
    }

    @Override
    public boolean isAchieved() {
        return isAchieved;
    }

    @Override
    public AchievementType getAchievementType() {
        return achievementType;
    }

    @Override
    public Achievement clone() {
        AchievementPredictable achievementPredictable = new AchievementPredictable(achievementType, predicate);
        achievementPredictable.isAchieved = isAchieved;
        return achievementPredictable;
    }
}
