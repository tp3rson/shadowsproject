package ru.xlv.core.achievement;

import lombok.Getter;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.storage.ISavableNBT;
import ru.xlv.core.common.storage.NBTLoader;
import ru.xlv.core.util.IConverter;

import java.util.ArrayList;
import java.util.List;

public class AchievementManager implements ISavableNBT {

    private static final IConverter<Achievement, String> CONVERTER = new IConverter<Achievement, String>() {
        @Override
        public String convertTo(Achievement achievement) {
            String s = "";
            s += achievement.getAchievementType().name() + NBTLoader.ARRAY_DELIMITER;
            if(achievement instanceof AchievementBase) {
                s += achievement.isAchieved();
            } else if(achievement instanceof AchievementProgressive) {
                s += ((AchievementProgressive) achievement).getProgress() + NBTLoader.ARRAY_DELIMITER;
                s += ((AchievementProgressive) achievement).getMaxProgress();
            }
            return s;
        }

        @Override
        public Achievement convertFrom(String s) {
            String[] split = s.split(NBTLoader.ARRAY_DELIMITER);
            if(split.length > 0) {
                String typeName = split[0];
                if(!typeName.isEmpty()) {
                    Achievement achievement = AchievementFactory.getAchievement(AchievementType.valueOf(typeName));
                    if(achievement instanceof AchievementBase) {
                        ((AchievementBase) achievement).isAchieved = Boolean.parseBoolean(split[1]);
                    } else if(achievement instanceof AchievementProgressive) {
                        ((AchievementProgressive) achievement).progress = Integer.parseInt(split[1]);
                        ((AchievementProgressive) achievement).maxProgress = Integer.parseInt(split[2]);
                    }
                    return achievement;
                }
            }
            return null;
        }
    };

    private static final String ACHIEVEMENT_LIST_KEY = "achievements";

    @Getter
    @DatabaseValue
    private final List<Achievement> achievements = new ArrayList<>();

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        nbtLoader.writeListToNBT(nbtTagCompound, ACHIEVEMENT_LIST_KEY, achievements, CONVERTER::convertTo);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        nbtLoader.readListFromNBT(nbtTagCompound, ACHIEVEMENT_LIST_KEY, achievements, CONVERTER::convertFrom);
    }
}
