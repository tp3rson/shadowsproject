package ru.xlv.core.achievement;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.XlvsCore;

@RequiredArgsConstructor
public enum AchievementType {

    SEND_POST(
            XlvsCore.INSTANCE.getAchievementLocalization().getSendPostAchievementName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getSendPostAchievementDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getSendPostAchievementImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    FIRST_FRIENDS(
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstFriendsName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstFriendsDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstFriendsImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{1, 5, 10, 20}}
    ),
    LEVEL_UP(
            XlvsCore.INSTANCE.getAchievementLocalization().getLevelUpName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getLevelUpDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getLevelUpImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{5, 10, 20, 30}}
    ),
    BE_REGISTER(
            XlvsCore.INSTANCE.getAchievementLocalization().getBeRegisteredName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getBeRegisteredDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getBeRegisteredImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{3, 6, 12, 24}}
    ),
    FIRST_GROUP(
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstGroupName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstGroupDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstGroupImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    BLOCK_PLAYER(
            XlvsCore.INSTANCE.getAchievementLocalization().getBlockPlayerName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getBlockPlayerDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getBlockPlayerImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    FIRST_TRADE(
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstTradeName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstTradeDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getFirstTradeImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    BUY_AUCTION_ITEM(
            XlvsCore.INSTANCE.getAchievementLocalization().getBuyAuctionItemName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getBuyAuctionItemDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getBuyAuctionItemImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    REVENGE(
            XlvsCore.INSTANCE.getAchievementLocalization().getRevengeName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getRevengeDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getRevengeImageUrl(),
            AchievementBase.class, new Object[0]
    ),

    KILL_ON_LOC_BOLOTA(
            XlvsCore.INSTANCE.getAchievementLocalization().getKillOnLocBolotaName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getKillOnLocBolotaDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getKillOnLocBolotaImageUrl(),
            AchievementProgressive.class, new Object[]{ 50 }
    ),

    //todo дописать заюзав LambdaMetaFactory
//    GET_CRITICAL_RADIATION(
//            XlvsCore.INSTANCE.getAchievementLocalization().getGetCriticalRadiationName(),
//            XlvsCore.INSTANCE.getAchievementLocalization().getGetCriticalRadiationDescription(),
//            XlvsCore.INSTANCE.getAchievementLocalization().getGetCriticalRadiationImageUrl(),
//            AchievementPredictable.class, new Object[]{AchievementPredicates.GET_CRITICAL_RADIATION_ACHIEVEMENT_TYPE_PREDICATE}
//    ),
    DEAD_BY_CHEMICAL_DAMAGE(
            XlvsCore.INSTANCE.getAchievementLocalization().getDeadByChemicalDamageName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDeadByChemicalDamageDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDeadByChemicalDamageImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    DEAD_BY_FIRE(
            XlvsCore.INSTANCE.getAchievementLocalization().getDeadByFireName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDeadByFireDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDeadByFireImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    COLD_METHOD(
            XlvsCore.INSTANCE.getAchievementLocalization().getColdMethodName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getColdMethodDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getColdMethodImageUrl(),
            AchievementProgressive.class, new Object[]{10}
    ),
    SHOCK_AND_TREMBLING(
            XlvsCore.INSTANCE.getAchievementLocalization().getShockAndTremblingName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getShockAndTremblingDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getShockAndTremblingImageUrl(),
            AchievementProgressive.class, new Object[]{10}
    ),
    INBORN_SNIPER(
            XlvsCore.INSTANCE.getAchievementLocalization().getInbornSniperName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getInbornSniperDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getInbornSniperImageUrl(),
            AchievementProgressive.class, new Object[]{10}
    ),

    EXPERT_OF_INCARN_09(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfIncarn09Name(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfIncarn09Description(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfIncarn09ImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_SEEKER(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfSeekerName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfSeekerDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfSeekerImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_CONTRIBUTOR(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfContributorName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfContributorDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfContributorImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_ISAAC(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfIsaacName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfIsaacDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfIsaacImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_PROWLER(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfProwlerName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfProwlerDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfProwlerImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_SPITTER(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfSpitterName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfSplitterDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfSplitterImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_KEEPER(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfKeeperName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfKeeperDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfKeeperImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_WASP(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfWaspName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfWaspDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfWaspImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_ARKONISTA(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfArkonistaName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfArkonistaDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfArkonistaImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_ASR_37(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfAsr37Name(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfAsr37Description(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfAsr37ImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_RAD_ZINGER(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfRadZingerName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfRadZingerDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfRadZingerImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_BONE_CRUSHER(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfBoneCrusherName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfBoneCrusherDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfBoneCrusherImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_VERTMUNT(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfVermuntName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfVermuntDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfVermuntImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_DEVOURER(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfDevourerName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfDevourerDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfDevourerImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_OSIRIS(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfOsirisName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfOsirisDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfOsirisImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_BARRACUDA(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfBarracudaName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfBarracudaDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfBarracudaImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),
    EXPERT_OF_AZALLET(
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfAzalletName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfAzalletDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getExpertOfAzalletImageUrl(),
            AchievementPhased.class, new Object[]{new int[]{25, 50, 100, 200, 500}}
    ),

    EQUIP_SCORPION(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipMaradeurName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipScorpionDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipScorpionImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    EQUIP_PALADIN(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipPtnName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipPaladinDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipPaladinImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    EQUIP_BKU_0992(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipBku0992Name(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipBku0992Description(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipBku0992ImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    EQUIP_RADIANT(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipAzurESName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipRadiantDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipRadiantImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    EQUIP_CENSOR(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipCensorName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipCensorDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipCensorImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    EQUIP_MISFORTUNE(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipNihilName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipMisfortuneDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipMisfortuneImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    EQUIP_BANK(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipMurusKLName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipBankDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipBankImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    EQUIP_SILENCE(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipEnigmaBName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipSilenceDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipSilenceImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    EQUIP_SPEC_RIOT(
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipSpecRiotName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipSpecRiotDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getEquipSpecRiotImageUrl(),
            AchievementBase.class, new Object[0]
    ),

    DISCOVER_PHOENIX_OUTPOST_ON_TERRA(
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixOutpostOnTerraName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixOutpostOnTerraDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixOutpostOnTerraImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    DISCOVER_PHOENIX_FARMS_ON_TERRA(
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixFarmsOnTerraName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixFarmsOnTerraDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixFarmsOnTerraImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    DISCOVER_PHOENIX_LABORATORY_ON_TERRA(
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixLaboratoryOnTerraName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixLaboratoryOnTerraDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixLaboratoryOnTerraImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    DISCOVER_PHOENIX_BASE_ON_TERRA(
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixBaseOnTerraName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixBaseOnTerraDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixBaseOnTerraImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    DISCOVER_PHOENIX_SPACEPORT_ON_TERRA(
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixSpaceportOnTerraName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixSpaceportOnTerraDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixSpaceportOnTerraImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    DISCOVER_PHOENIX_MINES_ON_TERRA(
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixMinesOnTerraName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixMinesOnTerraDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixMinesOnTerraImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    DISCOVER_PHOENIX_PURITY_CAMP_ON_TERRA(
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixPurityCampOnTerraName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixPurityCampOnTerraDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverPhoenixPurityCampOnTerraImageUrl(),
            AchievementBase.class, new Object[0]
    ),
    DISCOVER_TERRA(
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverTerraName(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverTerraDescription(),
            XlvsCore.INSTANCE.getAchievementLocalization().getDiscoverTerraImageUrl(),
            AchievementBase.class, new Object[0]
    );

    @Getter
    final String name, description, imageURL;
    final Class<? extends Achievement> achievementClass;
    final Object[] params;
}
