package ru.xlv.core.database.redis;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.pubsub.api.reactive.RedisPubSubReactiveCommands;
import lombok.Getter;
import ru.xlv.core.database.AbstractProviderDB;
import ru.xlv.core.util.JsonUtils;

@Getter
public class RedisProvider extends AbstractProviderDB<ConfigRedis> {

    private RedisClient redisClient;
    private StatefulRedisConnection<String, String> connection;
    private RedisCommands<String, String> sync;

    private StatefulRedisPubSubConnection<String, String> pubSubConnection;
    private RedisPubSubReactiveCommands<String, String> reactivePubSubCommands;

    public RedisProvider(ConfigRedis config) {
        super(config);
    }

    @Override
    public void init() {
//        Config config = new Config();
//        config.setCodec(new JsonJacksonCodec())
//                .setThreads(8)
//                .setNettyThreads(8)
//                .useSingleServer()
//                .setAddress("redis://" + this.config.getHost() + ":" + this.config.getPort())
//                .setPassword(this.config.getPassword())
////                .setDatabase(3)
//                .setClientName("XlvsCore");
//        redissonClient = Redisson.create(config);

        RedisURI redisURI = new RedisURI();
        redisURI.setHost(config.getHost());
        redisURI.setPort(config.getPort());
        redisURI.setPassword(config.getPassword());
        redisClient = RedisClient.create(redisURI);
        connection = redisClient.connect();
        sync = connection.sync();
        pubSubConnection = redisClient.connectPubSub();
        reactivePubSubCommands = pubSubConnection.reactive();
//        reactivePubSubCommands.subscribe("channel").subscribe();
//        reactivePubSubCommands.observeChannels().doOnNext(stringStringChannelMessage -> {
//            System.out.println(stringStringChannelMessage.getMessage());
//        }).subscribe();
    }

    @Override
    public void shutdown() {
        redisClient.shutdown();
    }

    public void putObject(String key, Object value) {
        sync.set(key, JsonUtils.SIMPLE_GSON.toJson(value));
    }

    public <T> T getObject(String key, Class<T> tClass) {
        return JsonUtils.SIMPLE_GSON.fromJson(sync.get(key), tClass);
    }
}
