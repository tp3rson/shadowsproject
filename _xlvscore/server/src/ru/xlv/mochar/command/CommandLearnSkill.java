package ru.xlv.mochar.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.network.skill.PacketSkillList;
import ru.xlv.mochar.player.character.skill.result.SkillLearnResult;
import ru.xlv.mochar.util.Utils;

public class CommandLearnSkill extends CommandBase {
    @Override
    public String getCommandName() {
        return "skilllearn";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer((EntityPlayer) p_71515_1_);
        SkillLearnResult skillLearnResult = serverPlayer.getSelectedCharacter().getSkillManager().learn(serverPlayer, Integer.parseInt(p_71515_2_[0]));
        switch (skillLearnResult) {
            case SUCCESS:
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer((EntityPlayer) p_71515_1_, new PacketSkillList(serverPlayer));
                Utils.sendMessage(serverPlayer, "Способность изучена.");
                break;
            case NOT_FOUND:
                Utils.sendMessage(serverPlayer, "Способность не найдена.");
                break;
            case CONDITIONS_NOT_FULFILLED:
                Utils.sendMessage(serverPlayer, "Условия не выполнены.");
                break;
            case ALREADY_LEARNED:
                Utils.sendMessage(serverPlayer, "Способность уже изучена.");
        }
    }
}
