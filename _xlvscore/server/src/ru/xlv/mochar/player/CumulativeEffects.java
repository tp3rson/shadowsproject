package ru.xlv.mochar.player;

import ru.xlv.core.player.ServerPlayer;

public class CumulativeEffects {

    /*
    * once per second decrease or increase
    * */
    public static final CumulativeEffect RADIATION = new CumulativeEffect(1D, 1D, 1000L, 1000L) {
        @Override
        public boolean mustBeAmplified(ServerPlayer serverPlayer) {
            return false;
        }

        @Override
        public void onAmplified(ServerPlayer serverPlayer) {

        }
    };

    public static final CumulativeEffect FIRE = new CumulativeEffect(1D, 1D, 1000L, 1000L) {
        @Override
        public boolean mustBeAmplified(ServerPlayer serverPlayer) {
            return serverPlayer.getEntityPlayer().isBurning();
        }

        @Override
        public void onAmplified(ServerPlayer serverPlayer) {

        }
    };
}
