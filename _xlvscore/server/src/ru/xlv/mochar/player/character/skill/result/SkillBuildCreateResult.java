package ru.xlv.mochar.player.character.skill.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.XlvsCore;

@Getter
@RequiredArgsConstructor
public enum SkillBuildCreateResult {

    SUCCESS(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillBuildCreateSuccessMessage()),
    NOT_AVAILABLE_SLOT(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillBuildCreateNotAvailableSlotMessage()),
    UNKNOWN(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillBuildCreateUnknownsMessage());

    private final String responseMessage;
}
