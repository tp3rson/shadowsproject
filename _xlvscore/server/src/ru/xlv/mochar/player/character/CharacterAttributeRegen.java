package ru.xlv.mochar.player.character;

import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;

public class CharacterAttributeRegen extends CharacterAttributeUpdated {

    public CharacterAttributeRegen(CharacterAttributeType type, long period) {
        super(type, period);
    }

    public CharacterAttributeRegen(CharacterAttributeType type, double value, long period) {
        super(type, value, period);
    }

    @Override
    public void updatePeriodically(ServerPlayer player) {
        super.updatePeriodically(player);
        switch (getType()) {
            case HEALTH_REGEN:
                if (player.isOnline()) {
                    player.getEntityPlayer().heal((float) getValue());
                }
                break;
            case MANA_REGEN:
                player.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA).addValue(getValue());
                break;
            case STAMINA_REGEN:
                player.getSelectedCharacter().getAttribute(CharacterAttributeType.STAMINA).addValue(getValue());
        }
    }
}
