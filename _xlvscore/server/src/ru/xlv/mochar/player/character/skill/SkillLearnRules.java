package ru.xlv.mochar.player.character.skill;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.network.IPacketComposable;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.util.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class SkillLearnRules {

    public static abstract class SkillLearnRule implements IPacketComposable {

        private final Predicate<ServerPlayer> predicatePlayer;
        private Predicate<Skill> predicateSkill;
        private Consumer<ServerPlayer> learnConsumer;

        private SkillLearnRule(Predicate<ServerPlayer> predicate) {
            this.predicatePlayer = predicate;
        }

        private SkillLearnRule(Predicate<ServerPlayer> predicatePlayer, Predicate<Skill> predicateSkill) {
            this.predicatePlayer = predicatePlayer;
            this.predicateSkill = predicateSkill;
        }

        public void onLearn(ServerPlayer serverPlayer) {
            if(learnConsumer != null) {
                learnConsumer.accept(serverPlayer);
            }
        }

        protected void setLearnConsumer(Consumer<ServerPlayer> learnConsumer) {
            this.learnConsumer = learnConsumer;
        }

        public boolean checkCondition(Skill skill, ServerPlayer serverPlayer) {
            return (predicatePlayer == null || predicatePlayer.test(serverPlayer)) && (predicateSkill == null || predicateSkill.test(skill));
        }
    }

    public static class SkillLearnRuleExp extends SkillLearnRule {

        private final ExperienceType experienceType;
        private final double amount;

        public SkillLearnRuleExp(ExperienceType experienceType, double amount) {
            super(serverPlayer -> serverPlayer.getSelectedCharacter() != null && serverPlayer.getSelectedCharacter().getExperienceManager().getExperience(experienceType).getValue() >= amount);
            this.amount = amount;
            this.experienceType = experienceType;
            setLearnConsumer(serverPlayer -> serverPlayer.getSelectedCharacter().getExperienceManager().consumeExp(experienceType, amount));
        }

        public double getAmount() {
            return amount;
        }

        public ExperienceType getExperienceType() {
            return experienceType;
        }

        @Override
        public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
            writableList.add(0);
            writableList.add(experienceType.ordinal());
            writableList.add(amount);
        }
    }

    public static class SkillLearnRuleItem extends SkillLearnRule {

        private final Item item;
        private final int amount;

        public SkillLearnRuleItem(Item item, int amount) {
            super(serverPlayer -> {
                int a = 0;
                for (ItemStack value : serverPlayer.getSelectedCharacter().getMatrixInventory().getItems().values()) {
                    if(value.getItem() == item) {
                        if(++a >= amount) {
                            return true;
                        }
                    }
                }
                return false;
            });
            this.item = item;
            this.amount = amount;
            setLearnConsumer(serverPlayer -> {
                for (int i = 0; i < amount; i++) {
                    serverPlayer.getSelectedCharacter().getMatrixInventory().removeItem(item);
                }
            });
        }

        public int getAmount() {
            return amount;
        }

        public Item getItem() {
            return item;
        }

        @Override
        public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
            writableList.add(1);
            writableList.add(Item.getIdFromItem(item));
            writableList.add(amount);
        }
    }

    public static class SkillLearnRuleAnother extends SkillLearnRule {

        @Getter
        private final TIntList skillIds;

        public SkillLearnRuleAnother(int... skillIds) {
            super(serverPlayer -> {
                if (serverPlayer.getSelectedCharacter() != null) {
                    for (Skill learnedSkill : serverPlayer.getSelectedCharacter().getSkillManager().getLearnedSkills()) {
                        for (int skillId : skillIds) {
                            if(learnedSkill.getSkillType().getSkillId() == skillId) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            });
            this.skillIds = new TIntArrayList(skillIds);
        }

        public SkillLearnRuleAnother(SkillType... skillTypes) {
            this(Utils.integerArrayToIntArray(Arrays.stream(skillTypes).map(SkillType::getSkillId).toArray(Integer[]::new)));
        }

        public int getSkillId() {
            return skillIds.get(0);
        }

        @Override
        public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
            writableList.add(2);
            writeArray(skillIds, writableList);
        }
    }
}
