package ru.xlv.mochar.player.character.skill.cost;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.event.PlayerConsumeManaEvent;
import ru.xlv.core.player.ServerPlayer;

import java.util.List;

public class SkillCostMana implements ISkillCost {

    private final int amount;

    public SkillCostMana(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean consume(ServerPlayer serverPlayer) {
        if (serverPlayer.getSelectedCharacter() != null) {
            CharacterAttribute characterAttribute = serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA);
            PlayerConsumeManaEvent event = new PlayerConsumeManaEvent(serverPlayer, this.amount);
            XlvsCoreCommon.EVENT_BUS.post(event);
            double amount = event.getAmount();
            return characterAttribute.consume(amount);
        }
        return false;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
        writableList.add(0);
        writableList.add(amount);
    }
}
