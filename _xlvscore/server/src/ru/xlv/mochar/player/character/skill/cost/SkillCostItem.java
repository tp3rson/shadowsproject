package ru.xlv.mochar.player.character.skill.cost;

import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.item.Item;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.player.ServerPlayer;

import java.util.List;

public class SkillCostItem implements ISkillCost {

    private final Item item;
    private final int amount;

    public SkillCostItem(Item item, int amount) {
        this.item = item;
        this.amount = amount;
    }

    @Override
    public boolean consume(ServerPlayer serverPlayer) {
        if (CommonUtils.countItems(item, serverPlayer.getEntityPlayer().inventory) >= amount) {
            for (int i = 0; i < amount; i++) {
                serverPlayer.getEntityPlayer().inventory.consumeInventoryItem(item);
            }
            return true;
        }
        return false;
    }

    public Item getItem() {
        return item;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
        writableList.add(1);
        writableList.add(amount);
        writableList.add(Item.getIdFromItem(item));
    }
}
