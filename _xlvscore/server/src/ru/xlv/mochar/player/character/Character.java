package ru.xlv.mochar.player.character;

import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import org.apache.commons.lang3.text.StrBuilder;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.achievement.AchievementManager;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryFactory;
import ru.xlv.core.common.item.AppliedInstantly;
import ru.xlv.core.common.item.IAttributeProvider;
import ru.xlv.core.common.item.IItemAppliedInstantly;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.player.character.*;
import ru.xlv.core.common.storage.ISavableNBT;
import ru.xlv.core.common.storage.NBTLoader;
import ru.xlv.core.event.ServerPlayerFirstLoginEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Maps;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.mochar.network.PacketPlayerMainSync;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.SkillManager;
import ru.xlv.mochar.player.character.skill.experience.ExperienceManager;
import ru.xlv.mochar.player.stat.StatManager;

import javax.annotation.Nonnull;
import java.util.*;

public class Character implements ISavableNBT, IMatrixInventoryProvider {

    public static final int MATRIX_INV_WIDTH = 10, MATRIX_INV_HEIGHT = 30;

    @Getter
    @DatabaseValue
    private boolean firstJoin;
    @Getter
    @DatabaseValue
    private CharacterType characterType;
    @Getter
    @DatabaseValue
    private final ExperienceManager experienceManager = new ExperienceManager();
    @Getter
    @DatabaseValue
    private final SkillManager skillManager = new SkillManager();
    @Getter
    @DatabaseValue
    private MatrixInventory matrixInventory;
    @Getter
    @DatabaseValue
    private final Wallet wallet = new Wallet();
    @Getter
    @DatabaseValue
    private final StatManager statManager = new StatManager();
    @Getter
    @DatabaseValue
    private final AchievementManager achievementManager = new AchievementManager();
    @Getter
    @DatabaseValue
    private final List<String> discoveredLocationNames = new ArrayList<>();
    @DatabaseValue
    private final Map<CharacterAttributeType, CharacterAttribute> characterAttributes = Collections.synchronizedMap(new HashMap<>());
    @Getter
    @DatabaseValue
    private final Map<CharacterDebuffType, CharacterDebuff> characterDebuffMap = Collections.synchronizedMap(new HashMap<>());

    private final List<CharacterAttributeMod> characterAttributeMods = new ArrayList<>();
    private final Map<MatrixInventory.SlotType, List<CharacterAttributeMod>> armorAttributeMods = new HashMap<>();

    private ServerPlayer serverPlayer;

    private final ScheduledConsumableTask<ServerPlayer> regenScheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
        double value = getAttribute(CharacterAttributeType.HEALTH_REGEN).getValue();
        if(value > 0) {
            serverPlayer.getEntityPlayer().heal((float) value);
        }
        double value1 = getAttribute(CharacterAttributeType.MANA_REGEN).getValue();
        if(value1 > 0) {
            getAttribute(CharacterAttributeType.MANA).addValue(value1);
        }
    });

    public void init(@Nonnull ServerPlayer serverPlayer, @Nonnull CharacterType characterType) {
        this.characterType = characterType;
        this.serverPlayer = serverPlayer;
        //init attributes
        if(characterAttributes.isEmpty()) {
            for (CharacterAttributeType value : CharacterAttributeType.values()) {
                if (value.getCategory() == CharacterAttributeCategory.REGEN) {
                    characterAttributes.put(value, new CharacterAttributeRegen(value, value.getDefaultValue(), 1000L));
                } else {
                    characterAttributes.put(value, new CharacterAttribute(value, value.getDefaultValue(), value.getMaxValue()));
                }
            }
        }
        skillManager.init();
        if(matrixInventory == null) {
            matrixInventory = MatrixInventoryFactory.create(MATRIX_INV_WIDTH, MATRIX_INV_HEIGHT);
        }
        matrixInventory.verify();
        calcInvItemMods(serverPlayer);
        for (CharacterAttributeType value : CharacterAttributeType.values()) {
            if(!characterAttributes.containsKey(value)) {
                characterAttributes.put(value, new CharacterAttribute(value, value.getDefaultValue(), value.getMaxValue()));
            }
        }
        if(getAttribute(CharacterAttributeType.MANA).getMaxValue() == 0) {
            getAttribute(CharacterAttributeType.MANA).setValue(CharacterAttributeType.MANA.getDefaultValue());
        }
        characterAttributes.put(CharacterAttributeType.HEALTH_REGEN, new CharacterAttribute(CharacterAttributeType.HEALTH_REGEN, CharacterAttributeType.HEALTH_REGEN.getDefaultValue()));
        characterAttributes.put(CharacterAttributeType.MANA_REGEN, new CharacterAttribute(CharacterAttributeType.MANA_REGEN, CharacterAttributeType.MANA_REGEN.getDefaultValue()));
        characterAttributes.put(CharacterAttributeType.DAMAGE_BASE_PROTECTION, new CharacterAttribute(CharacterAttributeType.DAMAGE_BASE_PROTECTION, 1D));
        characterAttributes.remove(null);
        if(!firstJoin) {
            firstJoin = true;
            XlvsCoreCommon.EVENT_BUS.post(new ServerPlayerFirstLoginEvent(serverPlayer, this));
        }
    }

    public void update(@Nonnull ServerPlayer serverPlayer) {
        getSkillManager().getSelectedActiveSkills().forEach(activableSkill -> activableSkill.update(serverPlayer));
        getSkillManager().getSelectedPassiveSkills().forEach(skill -> {
            if(skill instanceof ISkillUpdatable) {
                ((ISkillUpdatable) skill).update(serverPlayer);
            }
        });
        synchronized (characterDebuffMap) {
            characterDebuffMap.values().removeIf(CharacterDebuff::shouldBeRemoved);
        }
        regenScheduledConsumableTask.update(serverPlayer);
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketPlayerMainSync(serverPlayer));
    }

    public void calcInvItemMods(ServerPlayer serverPlayer) {
        for (int i = 0; i < serverPlayer.getEntityPlayer().inventory.getSizeInventory(); i++) {
            ItemStack stackInSlot = serverPlayer.getEntityPlayer().inventory.getStackInSlot(i);
            if (stackInSlot != null && stackInSlot.getItem() instanceof ItemArmor) {
                onInvItemAddedToSpecSlot(stackInSlot);
            }
        }
    }

    public void healBySkill(@Nonnull ServerPlayer serverPlayer, float amount) {
        double attributeCompleteValue = getAttributeCompleteValue(CharacterAttributeType.SKILL_REGEN_MOD);
        serverPlayer.getEntityPlayer().heal((float) (amount * attributeCompleteValue));
    }

    public double getAttributeCompleteValue(@Nonnull CharacterAttributeType characterAttributeType) {
        double value = getAttribute(characterAttributeType).getValue();
        value = applyMods(characterAttributeType, value);
        return value;
    }

    public double getAttributeCompleteValueWithArmor(@Nonnull CharacterAttributeType characterAttributeType) {
        double value = getAttribute(characterAttributeType).getValue();
        value += getArmorAttributeMod(characterAttributeType);
        value = applyMods(characterAttributeType, value);
        return value;
    }

    private double applyMods(CharacterAttributeType characterAttributeType, double value) {
        double attributeMods = 0D;
        double attributeAdds = 0D;
        for (CharacterAttributeMod characterAttributeMod : characterAttributeMods) {
            if(characterAttributeMod.getAttributeType() == characterAttributeType) {
                attributeMods += characterAttributeMod.getValueMod() - 1D;
                attributeAdds += characterAttributeMod.getValueAdd();
            }
        }
        if (attributeAdds != 0) {
            value += attributeAdds;
        }
        if (attributeMods != 0) {
            value *= 1D + attributeMods;
        }
        return value;
    }

    @Nonnull
    public CharacterAttribute getAttribute(@Nonnull CharacterAttributeType characterAttributeType) {
        return characterAttributes.get(characterAttributeType);
    }

    public void handleAttributeMods(ServerPlayer serverPlayer) {
        if(serverPlayer.isOnline()) {
            synchronized (characterAttributeMods) {
                characterAttributeMods.removeIf(CharacterAttributeMod::shouldBeRemoved);
            }
        }
    }

    public void addAttributeMod(@Nonnull CharacterAttributeMod characterAttributeMod) {
        synchronized (characterAttributeMods) {
            characterAttributeMod.setCreationTimeMills(System.currentTimeMillis());
            characterAttributeMods.add(characterAttributeMod);
        }
    }

    public void addAttributeMod(@Nonnull CharacterAttributeMod characterAttributeMod, boolean removeSame) {
        if(removeSame) {
            removeAttributeMod(characterAttributeMod);
        }
        addAttributeMod(characterAttributeMod);
    }

    public void removeAttributeMod(@Nonnull CharacterAttributeMod characterAttributeMod) {
        synchronized (characterAttributeMods) {
            characterAttributeMods.removeIf(characterAttributeBoost1 -> characterAttributeBoost1.equals(characterAttributeMod) || (characterAttributeMod.getUuid() != null && characterAttributeMod.getUuid().equals(characterAttributeBoost1.getUuid())));
        }
    }

    public boolean containsAttributeMod(@Nonnull CharacterAttributeMod characterAttributeMod) {
        synchronized (characterAttributeMods) {
            return characterAttributeMods.contains(characterAttributeMod);
        }
    }

    public void addDebuff(@Nonnull CharacterDebuffType characterDebuffType, long period) {
        synchronized (characterDebuffMap) {
            characterDebuffMap.put(characterDebuffType, new CharacterDebuff(characterDebuffType, period));
        }
    }

    public boolean isDebuffActive(@Nonnull CharacterDebuffType characterDebuffType) {
        synchronized (characterDebuffMap) {
            return characterDebuffMap.containsKey(characterDebuffType);
        }
    }

    private double getArmorAttributeMod(@Nonnull CharacterAttributeType characterAttributeType) {
        double d = 0D;
        synchronized (armorAttributeMods) {
            for (List<CharacterAttributeMod> list : armorAttributeMods.values()) {
                for (CharacterAttributeMod characterAttributeMod : list) {
                    if(characterAttributeMod.getAttributeType() == characterAttributeType) {
                        d += characterAttributeMod.getValueAdd();
                    }
                }
            }
        }
        return d;
    }

    @Nonnull
    public Collection<CharacterAttribute> getCharacterAttributes() {
        return characterAttributes.values();
    }

    private static final String DISCOVERED_LOCATION_NAMES_KEY = "discoveredLocationNames";

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        nbtLoader.writeToNBT(nbtTagCompound,
                getMatrixInventory(),
                getWallet(),
                getExperienceManager(),
                getSkillManager(),
                getStatManager(),
                getAchievementManager()
        );
        nbtLoader.writeListToNBT(nbtTagCompound, DISCOVERED_LOCATION_NAMES_KEY, discoveredLocationNames, s -> s);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        nbtLoader.readFromNBT(nbtTagCompound,
                getMatrixInventory(),
                getWallet(),
                getExperienceManager(),
                getSkillManager(),
                getStatManager(),
                getAchievementManager()
        );
        nbtLoader.readListFromNBT(nbtTagCompound, DISCOVERED_LOCATION_NAMES_KEY, discoveredLocationNames, s -> s);
    }

    @Override
    public boolean canInteractWith(@Nonnull EntityPlayer entityPlayer) {
        return true;
    }

    @Override
    public void onMatrixInvCleaned() {
        synchronized (armorAttributeMods) {
            armorAttributeMods.clear();
        }
    }

    @Override
    public void onInvItemAddedToSpecSlot(@Nonnull ItemStack itemStack) {
        Item item = itemStack.getItem();
        if (item instanceof ItemArmor) {
            synchronized (armorAttributeMods) {
                ((ItemArmor) item).getSlotTypes().forEach(slotType -> {
                    if (armorAttributeMods.get(slotType) != null) {
                        armorAttributeMods.get(slotType).clear();
                    }
                    ((IAttributeProvider) item).getCharacterAttributeBoostMap().values().forEach(characterAttributeMod -> Maps.addElemToMappedList(armorAttributeMods, slotType, characterAttributeMod));
                });
            }
        }
    }

    @Override
    public void onInvItemRemovedFromSpecSlot(@Nonnull ItemStack itemStack) {
        Item item = itemStack.getItem();
        if(item instanceof ItemArmor) {
            synchronized (armorAttributeMods) {
                ((ItemArmor) item).getSlotTypes().forEach(slotType -> {
                    if (armorAttributeMods.get(slotType) != null) {
                        armorAttributeMods.get(slotType).clear();
                    }
                });
            }
        }
    }

    @Override
    public void onMatrixInvItemAdded(@Nonnull ItemStack itemStack) {
        Item item = itemStack.getItem();
        if(item instanceof IItemAppliedInstantly) {
            List<AppliedInstantly> appliedInstantlyList = ((IItemAppliedInstantly) item).getAppliedInstantlyList();
            if(appliedInstantlyList != null && !appliedInstantlyList.isEmpty()) {
                int credits = 0;
                int exp = 0;
                for (AppliedInstantly appliedInstantly : appliedInstantlyList) {
                    switch (appliedInstantly.getType()) {
                        case CREDITS:
                            getWallet().addCredits(appliedInstantly.getValue());
                            credits += appliedInstantly.getValue();
                            break;
                        case EXP:
                            getExperienceManager().addExp(ExperienceType.BATTLE, appliedInstantly.getValue());
                            exp += appliedInstantly.getValue();
                    }
                }
                StrBuilder strBuilder = new StrBuilder("Вам зачислено ");
                if(credits != 0) {
                    strBuilder.append(String.valueOf(credits)).append(" кредитов");
                }
                if(exp != 0) {
                    if(credits != 0) {
                        strBuilder.append(" и ");
                    }
                    strBuilder.append(String.valueOf(exp)).append(" боевого опыта");
                }
                FlexPlayer.of(serverPlayer)
                        .notNull()
                        .sendNotification(strBuilder.toString());
                if(((IItemAppliedInstantly) item).removeAfterApplied()) {
                    getMatrixInventory().removeItem(itemStack);
                }
            }
        }
    }
}
