package ru.xlv.mochar.player.character.skill.result;

import ru.xlv.core.XlvsCore;

public enum SkillSelectResult {

    DOES_NOT_EXIST(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillSelectDoesNotExistMessage()),
    NOT_LEARNED(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillSelectNotLearnedMessage()),
    WRONG_SLOT_INDEX(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillSelectWrongSlotIndexMessage()),
    SUCCESS(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillSelectSuccessMessage()),
    SAME_FAMILY(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillSelectSameFamilyMessage());

    private final String displayMessage;

    SkillSelectResult(String displayMessage) {
        this.displayMessage = displayMessage;
    }

    public String getDisplayMessage() {
        return displayMessage;
    }
}
