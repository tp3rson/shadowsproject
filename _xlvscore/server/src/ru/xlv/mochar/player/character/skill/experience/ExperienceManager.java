package ru.xlv.mochar.player.character.skill.experience;

import lombok.Getter;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.core.common.storage.ISavableNBT;
import ru.xlv.core.common.storage.NBTLoader;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.util.Lang;
import ru.xlv.mochar.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public class ExperienceManager implements ISavableNBT {

    @DatabaseValue
    private final List<Experience> experiences = Collections.synchronizedList(new ArrayList<>());

    public ExperienceManager() {
        for (ExperienceType value : ExperienceType.values()) {
            experiences.add(new Experience(value, 0D));
        }
    }

    public void addExp(ExperienceType experienceType, double value) {
        Experience experience = getExperience(experienceType);
        experience.setValue(experience.getValue() + value);
    }

    public void addExp(ServerPlayer serverPlayer, ExperienceType experienceType, double value) {
        addExp(experienceType, value);
        Utils.sendMessage(serverPlayer, Lang.EXP_ADD_MESSAGE, value, experienceType.name());
    }

    public boolean consumeExp(ExperienceType experienceType, double value) {
        Experience experience = getExperience(experienceType);
        if(experience.getValue() >= value) {
            experience.setValue(experience.getValue() - value);
            return true;
        }
        return false;
    }

    public boolean consumeExp(ServerPlayer serverPlayer, ExperienceType experienceType, double value) {
        boolean flag = consumeExp(experienceType, value);
        if(flag) {
            Utils.sendMessage(serverPlayer, Lang.EXP_CONSUME_MESSAGE, value, experienceType);
        }
        return flag;
    }

    public Experience getExperience(ExperienceType experienceType) {
        for (Experience experience : experiences) {
            if(experience.getType() == experienceType) {
                return experience;
            }
        }
        return null;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        for (Experience experience : experiences) {
            nbtTagCompound.setDouble(experience.getType().name(), experience.getValue());
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        for (Experience experience : experiences) {
            experience.setValue(nbtTagCompound.getDouble(experience.getType().name()));
        }
    }
}
