package ru.xlv.mochar.player.character.skill;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import lombok.Getter;
import ru.xlv.core.common.database.DatabaseValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class SkillListProvider {

    private final List<Skill> learnedSkills = new ArrayList<>();
    @DatabaseValue
    private final TIntList learnedSkillIds = new TIntArrayList();
    @DatabaseValue
    private final Map<Integer, int[]> skillBuildActivesMap = new HashMap<>();
    @DatabaseValue
    private final Map<Integer, int[]> skillBuildPassivesMap = new HashMap<>();
}
