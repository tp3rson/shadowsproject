package ru.xlv.mochar.player;

import ru.xlv.core.player.ServerPlayer;

import java.io.Serializable;

public abstract class CumulativeEffect implements Serializable {

    private double level;
    private double levelAmpValue, levelDecreaseValue;
    private final long ampPeriod, decreasePeriod;
    private long lastTimeMills;

    public CumulativeEffect(double levelAmpValue, double levelDecreaseValue, long ampPeriod, long decreasePeriod) {
        this.levelAmpValue = levelAmpValue;
        this.levelDecreaseValue = levelDecreaseValue;
        this.ampPeriod = ampPeriod;
        this.decreasePeriod = decreasePeriod;
    }

    public void update(ServerPlayer serverPlayer) {
        if(mustBeAmplified(serverPlayer) && System.currentTimeMillis() - lastTimeMills >= ampPeriod) {
            lastTimeMills = System.currentTimeMillis();
            level += levelAmpValue;
            onAmplified(serverPlayer);
        }
    }

    public abstract boolean mustBeAmplified(ServerPlayer serverPlayer);

    public abstract void onAmplified(ServerPlayer serverPlayer);

    public double getLevel() {
        return level;
    }
}
