package ru.xlv.mochar.location;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.PlayerEnterLocationEvent;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.XlvsMainMod;
import ru.xlv.mochar.network.PacketEnterLocation;
import ru.xlv.mochar.util.Lang;
import ru.xlv.mochar.util.Utils;

public class LocationHandler {

    private final ScheduledConsumableTask<EntityPlayer> checkScheduledTask = new ScheduledConsumableTask<>(500L, entityPlayer -> {
        Location playerLocationIn = XlvsMainMod.INSTANCE.getLocationManager().getPlayerLocationIn(entityPlayer.getCommandSenderName());
        boolean wasReseted = false;
        for (Location location : XlvsMainMod.INSTANCE.getLocationManager().getLocationList()) {
            if(playerLocationIn == null || playerLocationIn != location) {
                if(location.isInside((int) entityPlayer.posX, (int) entityPlayer.posZ)) {
                    XlvsMainMod.INSTANCE.getLocationManager().setPlayerLocationIn(entityPlayer.getCommandSenderName(), location);
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketEnterLocation(Utils.format(Lang.AREA_ENTER_MESSAGE, location.getName())));
                    XlvsCoreCommon.EVENT_BUS.post(new PlayerEnterLocationEvent(XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer.getCommandSenderName()), location));
                    wasReseted = true;
                    break;
                }
            }
        }
        if(playerLocationIn != null && !wasReseted && !playerLocationIn.isInside((int) entityPlayer.posX, (int) entityPlayer.posZ)) {
            XlvsMainMod.INSTANCE.getLocationManager().setPlayerLocationIn(entityPlayer.getCommandSenderName(), null);
        }
    });

//    @SubscribeEvent(priority = EventPriority.HIGHEST)
//    public void event(LivingHurtEvent event) {
//        if(event.entityLiving instanceof EntityPlayer) {
//            if(event.source.getEntity() instanceof EntityPlayer) {
//                Location playerLocationIn = XlvsMainMod.INSTANCE.getLocationManager().getPlayerLocationIn(event.entityLiving.getCommandSenderName());
//                if (playerLocationIn != null && playerLocationIn.getLocationFlags().contains(LocationFlag.NO_PVP)) {
//                    Utils.sendMessage((EntityPlayer) event.source.getEntity(), "Not a pvp area!");
//                    event.setCanceled(true);
//                }
//            }
//        }
//    }

    @SubscribeEvent
    public void event(TickEvent.PlayerTickEvent event) {
        checkScheduledTask.update(event.player);
    }
}
