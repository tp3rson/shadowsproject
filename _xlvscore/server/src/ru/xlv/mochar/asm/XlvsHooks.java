package ru.xlv.mochar.asm;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockTallGrass;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.login.INetHandlerLoginServer;
import net.minecraft.network.login.client.C00PacketLoginStart;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.util.StepSoundRegistry;
import ru.xlv.core.network.PacketPlaySound;
import ru.xlv.core.player.PlayerNBTLogInProcessor;
import ru.xlv.core.util.SoundType;
import ua.agravaine.hooklib.asm.Hook;
import ua.agravaine.hooklib.asm.ReturnCondition;

import java.util.List;

public class XlvsHooks {

    @Hook
    public static void processPacket(C00PacketLoginStart c00PacketLoginStart, INetHandlerLoginServer p_148833_1_) {
        PlayerNBTLogInProcessor playerLogInProcessor = XlvsCore.INSTANCE.getPlayerManager().getPlayerLogInProcessor(PlayerNBTLogInProcessor.class);
        if (playerLogInProcessor != null) {
            playerLogInProcessor.userAboutToLogIn(c00PacketLoginStart.func_149304_c());
        }
    }

    @Hook(injectOnExit = true)
    public static void readPacketData(C00PacketLoginStart c00PacketLoginStart, PacketBuffer packetBuffer) {
        int selectedCharacterIndex = packetBuffer.readInt();
        PlayerNBTLogInProcessor playerLogInProcessor = XlvsCore.INSTANCE.getPlayerManager().getPlayerLogInProcessor(PlayerNBTLogInProcessor.class);
        if (playerLogInProcessor != null) {
            playerLogInProcessor.putSelectedCharacter(c00PacketLoginStart.func_149304_c().getName(), selectedCharacterIndex);
        }
    }

    public static boolean canDropItem(EntityPlayerMP entityPlayer, C07PacketPlayerDigging c07PacketPlayerDigging) {
        if(c07PacketPlayerDigging.func_149506_g() == 3 || c07PacketPlayerDigging.func_149506_g() == 4) {
            List<EnumItemTag> list = EnumItemTag.getItemTags(entityPlayer.getHeldItem());
            if (list != null) {
                for (EnumItemTag itemTag : list) {
                    if (!EnumItemTag.isTagCanBeDropped(itemTag)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Hook(returnCondition = ReturnCondition.ALWAYS, createMethod = true)
    public static AxisAlignedBB getCollisionBoundingBoxFromPool(BlockLeaves blockLeaves, World world, int x, int y, int z) {
        return null;
    }

    @Hook(returnCondition = ReturnCondition.ALWAYS, createMethod = true)
    public static AxisAlignedBB getCollisionBoundingBoxFromPool(BlockTallGrass blockTallGrass, World world, int x, int y, int z) {
        return null;
    }

    @Hook(returnCondition = ReturnCondition.ALWAYS)
    public static boolean isOpaqueCube(BlockLeaves blockLeaves) {
        return false;
    }

    @Hook
    public static void func_145780_a(Entity entity, int x, int y, int z, Block block) {
        SoundType soundType = StepSoundRegistry.getSoundType(block, entity.worldObj.getBlockMetadata(x, y, z));
        if (soundType != null) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToAllAroundExcept(entity, 16, new PacketPlaySound(entity, soundType, 0.15F + entity.worldObj.rand.nextFloat() / 10, 1F));
        }
    }

//    @Hook
//    public static void displayGUIChest(EntityPlayerMP entityPlayerMP, IInventory p_71007_1_) {
//        MatrixInventoryController.INSTANCE.openMatrixInventory(entityPlayerMP, p_71007_1_, true);
//        System.out.println(entityPlayerMP + " opened a ghost inventory " + p_71007_1_.getInventoryName());
//    }

//    @Hook
//    public static void processPlayerDigging(NetHandlerPlayServer netHandlerPlayServer, C07PacketPlayerDigging c07PacketPlayerDigging) {
//        if(c07PacketPlayerDigging.func_149506_g() == 3 || c07PacketPlayerDigging.func_149506_g() == 4) {
//            List<EnumItemTag> list = EnumItemTag.getItemTags(netHandlerPlayServer.playerEntity.getHeldItem());
//            if(list != null) {
//                for (EnumItemTag itemTag : list) {
//                    if(!EnumItemTag.isTagCanBeDropped(itemTag)) {
//                        try {
//                            Field field = c07PacketPlayerDigging.getClass().getDeclaredField("field_149508_e");
//                            field.setAccessible(true);
//                            field.set(c07PacketPlayerDigging, );
//                        } catch (NoSuchFieldException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//        }
//    }
}
