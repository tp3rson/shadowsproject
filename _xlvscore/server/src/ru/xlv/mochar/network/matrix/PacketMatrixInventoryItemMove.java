package ru.xlv.mochar.network.matrix;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.inventory.MatrixInventoryTransaction;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryItemMove implements IPacketInOnServer {

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int fromX = bbis.readInt();
        int fromY = bbis.readInt();
        int toX = bbis.readInt();
        int toY = bbis.readInt();
        int rotation = bbis.readInt();
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if(serverPlayer != null) {
            MatrixInventoryTransaction matrixInventoryTransaction = new MatrixInventoryTransaction(serverPlayer.getSelectedCharacter());
            matrixInventoryTransaction.moveItem(fromX, fromY, toX, toY, rotation);
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
        }
    }
}
