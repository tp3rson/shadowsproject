package ru.xlv.mochar.network.matrix;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixPlayerInventorySync implements IPacketOutServer, IPacketInOnServer {

    private ServerPlayer serverPlayer;

    public PacketMatrixPlayerInventorySync(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
    }

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer != null) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(serverPlayer.getSelectedCharacter().getMatrixInventory().getWidth());
        bbos.writeInt(serverPlayer.getSelectedCharacter().getMatrixInventory().getHeight());
        bbos.writeInt(serverPlayer.getSelectedCharacter().getMatrixInventory().getItems().size());
        for (Integer id : serverPlayer.getSelectedCharacter().getMatrixInventory().getItems().keySet()) {
            int[] posOfItem = serverPlayer.getSelectedCharacter().getMatrixInventory().getPosOfItem(id);
            if(posOfItem == null) throw new IOException("An error has occurred during composing an open matrix inv packet.");
            bbos.writeInt(posOfItem[0]);
            bbos.writeInt(posOfItem[1]);
            ByteBufUtils.writeItemStack(bbos.buffer(), serverPlayer.getSelectedCharacter().getMatrixInventory().getItems().get(id));
        }
    }
}
