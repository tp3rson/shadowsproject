package ru.xlv.mochar.network.matrix;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.inventory.MatrixInventoryController;
import ru.xlv.mochar.inventory.MatrixInventoryTransaction;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionItemMove implements IPacketInOnServer {

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int fromX = bbis.readInt();
        int fromY = bbis.readInt();
        int toX = bbis.readInt();
        int toY = bbis.readInt();
        int rotation = bbis.readInt();
        boolean fromMyInventory = bbis.readBoolean();
        boolean toMyInventory = bbis.readBoolean();
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if(serverPlayer != null) {
            IMatrixInventoryProvider transactionMatrixInventoryProvider = MatrixInventoryController.INSTANCE.getTransactionMatrixInventoryProvider(entityPlayer);
            if (transactionMatrixInventoryProvider == null) {
                transactionMatrixInventoryProvider = serverPlayer.getSelectedCharacter();
            }
            IMatrixInventoryProvider inventoryFrom = fromMyInventory ? serverPlayer.getSelectedCharacter() : transactionMatrixInventoryProvider;
            IMatrixInventoryProvider inventoryTo = toMyInventory ? serverPlayer.getSelectedCharacter() : transactionMatrixInventoryProvider;
            MatrixInventoryTransaction matrixInventoryTransaction = new MatrixInventoryTransaction(inventoryTo, inventoryFrom);
            matrixInventoryTransaction.moveItem(fromX, fromY, toX, toY, rotation);
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixInventoryTransactionSync(transactionMatrixInventoryProvider.getMatrixInventory()));
        }
    }
}
