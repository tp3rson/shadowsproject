package ru.xlv.mochar.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.core.player.ServerPlayer;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillBuildChange implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(500L);

    private ServerPlayer serverPlayer;
    private boolean success;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if(REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
            int skillBuildIndex = bbis.readInt();
            serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
            success = serverPlayer.getSelectedCharacter().getSkillManager().changeSkillBuild(skillBuildIndex);
            packetCallbackSender.send();
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        if(success) {
            new PacketSkillBuildSync(serverPlayer).write(entityPlayer, bbos);
        }
        bbos.writeBoolean(success);
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
