package ru.xlv.mochar.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.mochar.player.character.skill.result.SkillLearnResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillLearn implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(500L);

    private SkillLearnResult skillLearnResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if(REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
            int skillId = bbis.readInt();
            FlexPlayer.of(entityPlayer)
                    .notNull()
                    .apply(serverPlayer -> serverPlayer.getSelectedCharacter().getSkillManager().learn(serverPlayer, skillId))
                    .test((flexPlayer, skillLearnResult1) -> skillLearnResult1.isSuccess())
                    .accept((flexPlayer, skillLearnResult1) -> flexPlayer.sendPacket(new PacketSkillList(flexPlayer.getPlayer())))
                    .accept((flexPlayer, skillLearnResult1) -> skillLearnResult = skillLearnResult1)
                    .accept((flexPlayer, skillLearnResult1) -> packetCallbackSender.send());
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(skillLearnResult == SkillLearnResult.SUCCESS);
        bbos.writeUTF(skillLearnResult.getResponseMessage());
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
