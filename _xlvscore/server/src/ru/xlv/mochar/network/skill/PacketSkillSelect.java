package ru.xlv.mochar.network.skill;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.result.SkillSelectResult;
import ru.xlv.mochar.util.Utils;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillSelect implements IPacketInOnServer {

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if(serverPlayer != null) {
            int c = bbis.readInt();
            for (int i = 0; i < c; i++) {
                boolean isSelectedSkillActive = bbis.readBoolean();
                int selectedSkillId = bbis.readInt();
                int hotSlotIndex = bbis.readInt();
                SkillSelectResult skillSelectResult = serverPlayer.getSelectedCharacter().getSkillManager().selectSkill(serverPlayer, isSelectedSkillActive, selectedSkillId, hotSlotIndex);
                Utils.sendMessage(serverPlayer, skillSelectResult.getDisplayMessage() + " " + hotSlotIndex + "=" + selectedSkillId);
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketSkillList(serverPlayer));
            }
        }
    }
}
