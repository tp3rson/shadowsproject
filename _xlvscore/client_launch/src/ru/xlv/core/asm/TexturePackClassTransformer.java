package ru.xlv.core.asm;

import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

public class TexturePackClassTransformer implements IClassTransformer {

    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        if (transformedName.equals("net.minecraft.client.Minecraft")) {
            return patch(basicClass);
        }
        return basicClass;
    }

    private byte[] patch(byte[] basicClass) {
        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(basicClass);
        classReader.accept(classNode, 0);

        String refreshResources = "c";
        for (MethodNode mn : classNode.methods) {
            if (mn.name.equals(refreshResources) && mn.desc.equals("()V")) {
                for (int i = 0; i < mn.instructions.size(); i++) {
                    AbstractInsnNode ain = mn.instructions.get(i);
                    if (ain instanceof MethodInsnNode) {
                        MethodInsnNode min = (MethodInsnNode) ain;
                        if (min.name.equals("newArrayList")) {
                            AbstractInsnNode target = mn.instructions.get(i + 1);
                            InsnList toInsert = new InsnList();
                            toInsert.add(new VarInsnNode(Opcodes.ALOAD, 1));
                            toInsert.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "ru/xlv/core/launch/Utils", "insertCustomPack", "(Ljava/util/List;)V", false));
                            mn.instructions.insert(target, toInsert);
                            break;
                        }
                    }
                }
                break;
            }
        }

        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        classNode.accept(writer);
        return writer.toByteArray();
    }
}