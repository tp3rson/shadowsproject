package ru.xlv.container.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.inventory.GuiMatrixSlot;
import ru.krogenit.inventory.popup.GuiPopupRenderer;
import ru.krogenit.npc.IInventoryWithDraggedSlot;
import ru.krogenit.npc.MatrixInventoryModule;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.container.network.PacketContainerInteract;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryUtils;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.network.matrix.PacketMatrixInventoryTransactionItemMove;
import ru.xlv.core.network.matrix.PacketMatrixInventoryTransactionTakeAll;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiContainer extends AbstractGuiScreenAdvanced implements IInventoryWithDraggedSlot {

    protected GuiMatrixSlot draggedSlot;
    protected float draggedSlotX, draggedSlotY;
    private final List<MatrixInventoryModule> modules = new ArrayList<>();
    private final EntityContainer entityContainer;
    private String playerNameUpperCase, playerClass;
    private int playerLvl;

    public GuiContainer(EntityContainer entityContainer) {
        super(ScaleGui.FULL_HD);
        this.entityContainer = entityContainer;
        for(int i=0;i<2;i++) {
            modules.add(new MatrixInventoryModule(this));
        }

        this.playerNameUpperCase = entityContainer.getPlayerName().toUpperCase();
        this.playerLvl = entityContainer.getPlayerLevel();
        this.playerClass = entityContainer.getCharacterType().getDisplayName().toUpperCase();
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        float buttonWidth = 170;
        float buttonHeight = 39;
        float x = 760;
        float y = 756;
        GuiButtonAnimated button = new GuiButtonAnimated(0, ScaleGui.getCenterX(x),  ScaleGui.getCenterY(y),
                ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВЗЯТЬ ВСЁ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
        initInventories();
    }

    private void initInventories() {
        ClientMainPlayer clientPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
        MatrixInventory matrixInventory = clientPlayer.getMatrixInventory();
        float x = 1237;
        float y = 161;
        float cellHeight = 57;
        float cellWidth = 57;
        MatrixInventoryModule module = modules.get(0);
        module.setInventoryPositionAndSize(x, y, cellWidth, cellHeight, 571, 736, matrixInventory.getHeight());
        module.setInventory(matrixInventory);
        x = 562;
        y = 181;
        MatrixInventory matrixInventory1 = clientPlayer.getTransactionMatrixInventory();
        module = modules.get(1);
        module.setInventoryPositionAndSize(x, y, cellWidth, cellHeight, 571, 514, matrixInventory1.getHeight());
        module.setInventory(matrixInventory1);
    }

    public void sync() {
        this.playerNameUpperCase = entityContainer.getPlayerName().toUpperCase();
        this.playerLvl = entityContainer.getPlayerLevel();
        this.playerClass = entityContainer.getCharacterType().getDisplayName().toUpperCase();
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 0) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventoryTransactionTakeAll());
        }
    }

    private void drawCorpseMoney() {
        String platina = "0";//DecimalUtils.getFormattedStringWithSpaces(XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum());
        String credits = "0";//DecimalUtils.getFormattedStringWithSpaces(XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits());
        float fs = 70 / 32f;
        float slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        float slplatina = FontType.Marske.getFontContainer().width(platina) * fs;
        float creditWidth = 28;
        float creditHeight = 28;
        float platinaWidth = 28;
        float platinaHeight = 28;
        float halfLineWidth = (creditWidth + platinaWidth + 15 + slcredits + slplatina) / 2f;
        float x = 847 - halfLineWidth;
        float y = 722;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x, y, creditWidth, creditHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x + creditWidth + 30 + slcredits, y, platinaWidth, platinaHeight);
        GuiDrawUtils.drawStringCenter(FontType.Marske, platina, x + creditWidth + 25 + platinaWidth + slcredits, y - 7, fs, 0xDFDEDE);
        GuiDrawUtils.drawStringCenter(FontType.Marske, credits, x + creditWidth - 2, y - 7, fs, 0xDFDEDE);
    }

    private void drawMoney() {
        String platina = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum();
        String credits = "" + XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits();
        float fs = 50 / 32f;
        float slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        float slplatina = FontType.Marske.getFontContainer().width(platina) * fs;

        float creditWidth = 22;
        float creditHeight = 22;
        float platinaWidth = 22;
        float platinaHeight = 22;
        float x = 1806;
        float y = 142;
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, platina, x, y - 4, fs, 0xDFDEDE);
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, credits, x - platinaWidth * 2f - slplatina, y - 4, fs, 0xDFDEDE);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x - slcredits - slplatina - platinaWidth * 3f, y, creditWidth, creditHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x - slplatina - platinaWidth, y, platinaWidth, platinaHeight);
    }

    private void drawWeight() {
        float iconWidth = 18;
        float iconHeight = 20;
        float x = 1798;
        float y = 932;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconWeight, x, y, iconWidth, iconHeight);
        x = 1778;
        float fs = 30 / 32f;
        float currentWeight = 0;

        for(GuiMatrixSlot slot : modules.get(0).getSlots()) {
            ItemStack itemStack = slot.getItemStack();
            if(itemStack.getItem() instanceof ItemArmor) {
                ItemArmor itemArmor = (ItemArmor) itemStack.getItem();
                double value = itemArmor.getCharacterAttributeBoost(CharacterAttributeType.WEIGHT);
                currentWeight += (float) value;
            }
        }

        String weight = (int) currentWeight + " / " + (int)  XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.WEIGHT).getValue();
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrLight, weight, x, y, fs, 0x585858);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        if(entityContainer.isDead) {
            mc.displayGuiScreen(null);
        }

        super.drawScreen(mouseX, mouseY, partialTick);
        drawDefaultBackground();
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GuiDrawUtils.drawCenter(153, 161, 389, 414, 0f, 0f, 0f, 0.5f);
        GuiDrawUtils.drawCenter(542, 161, 626, 667, 0f, 0f, 0f, 0.5f);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GuiDrawUtils.drawCenter(TextureRegister.textureInvBorderBot, 153, 769, 1676, 199);
        GuiDrawUtils.drawCenter(TextureRegister.textureInvBorderTop, 153, 59, 1677, 2);
        GuiDrawUtils.drawCenter(TextureRegister.textureShopDevider, 175, 521, 347, 2);
        GuiDrawUtils.drawCenter(TextureRegister.textureInvCorpseAva, 173, 181, 349, 258);
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCTradeArrowsRight, 883, 89, 285, 52);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GuiDrawUtils.drawGradient(ScaleGui.getCenterX(173), ScaleGui.getCenterY(181), ScaleGui.get(349), ScaleGui.get(258), 226/255f, 0f, 26/255f, 0.2f, 0f, 0f, 0f, 0f);
        GL11.glEnable(GL11.GL_TEXTURE_2D);

        float fs = 60 / 32f;
        float fl = FontType.HelveticaNeueCyrLight.getFontContainer().width(playerNameUpperCase) * fs;
        float y = 468;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, playerNameUpperCase, 173, y, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ЛВЛ " + playerLvl, 180 + fl, y + 9, 26 / 32f, 0x666666);

        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, playerClass, 174, 501, 30 / 32f, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "СТАТУС: " + EnumChatFormatting.RED + "МЕРТВ", 174, 545, 30 / 32f, 0xffffff);

        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ДОБЫЧА", 151, 115, fs, 0x626262);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ВАШ ИНВЕНТАРЬ", 1237, 115, fs, 0x626262);
        drawMoney();
        drawCorpseMoney();
        drawWeight();
        drawButtons(mouseX, mouseY, partialTick);

        GuiDrawUtils.drawSplittedStringCenter(FontType.DeadSpace, "LoLorem ipsumLorem ipsumLorem ipsumLorem ipsumrem ipsum\ndolor sit aLorem ipsumLorem ipsummet, consec\n" +
                "tetur adipisicing\n" +
                "LoLorem ipsumLorem ipsumLorrem ipsumrem ipsum\n" +
                "dolor sit aLorem ipsumLor", 152, 702, 20 / 32f, this.width, -1, 0x626262, EnumStringRenderType.DEFAULT);
        GuiDrawUtils.drawSplittedStringCenter(FontType.DeadSpace, "LoLorem ipsumLorem ipsumLorem ipsumLorem ipsumrem ipsum\n" +
                "dolor sit aLorem ipsumLorem ipsummet, consec\n" +
                "tetur adipisicing", 1828, 997, 20 / 32f, this.width, -1, 0x626262, EnumStringRenderType.RIGHT);

        for(MatrixInventoryModule module : modules) {
            module.drawModule(mouseX, mouseY);
        }

        boolean popuped = false;
        for(MatrixInventoryModule module : modules) {
            if(module.drawPopups(mouseX, mouseY))
                popuped = true;
        }

        if(!popuped) {
            GuiPopupRenderer.updateBackAnimation();
        }

        if(draggedSlot != null) {
            if(draggedSlot.getItemStack() != null) {
                int w = MatrixInventoryUtils.getInvMatrixWidth(draggedSlot.getItemStack()) + 1;
                int h = MatrixInventoryUtils.getInvMatrixHeight(draggedSlot.getItemStack()) + 1;
                float offX = draggedSlot.getWidth() / (float)w;
                float offY = draggedSlot.getHeight() / (float)h;
                glPushMatrix();
                glTranslatef(-draggedSlot.getX() + mouseX - offX, -draggedSlot.getY() + mouseY - offY, 0);
                draggedSlot.renderItem(mouseX, mouseY);
                glPopMatrix();
            }
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);
        for(MatrixInventoryModule module : modules) {
            module.mouseClickMove(mouseY);
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        super.mouseMovedOrUp(mouseX, mouseY, which);
        for(MatrixInventoryModule module : modules) {
            module.mouseMovedOrUp();
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        for(MatrixInventoryModule module : modules) {
            module.mouseClicked(mouseX, mouseY, mouseButton, draggedSlot);
        }
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int d = Mouse.getEventDWheel();
        int mouseX = Mouse.getX();
        int mouseY = this.height - Mouse.getY() - 1;
        if (d != 0) {
            for(MatrixInventoryModule module : modules) {
                module.mouseScroll(mouseX, mouseY, d);
            }
        }
    }

    @Override
    public void keyTyped(char character, int key) {
        if(key == Keyboard.KEY_R && draggedSlot != null && draggedSlot.getItemStack() != null) {
            MatrixInventoryUtils.setInvMatrixRotation(draggedSlot.getItemStack(), MatrixInventoryUtils.getInvMatrixRotation(draggedSlot.getItemStack()) + 1);
            int w = MatrixInventoryUtils.getInvMatrixWidth(draggedSlot.getItemStack());
            int h = MatrixInventoryUtils.getInvMatrixHeight(draggedSlot.getItemStack());
            draggedSlot.setWidth(w * draggedSlot.getWidth() / (float)h);
            draggedSlot.setHeight(h * draggedSlot.getHeight() / (float)w);
            return;
        }
        super.keyTyped(character, key);
    }

    @Override
    public GuiMatrixSlot getDraggedSlot() {
        return draggedSlot;
    }

    @Override
    public void setDraggedSlotXY(float x, float y) {
        this.draggedSlotX = x;
        this.draggedSlotY = y;
    }

    @Override
    public void setDraggedSlot(GuiMatrixSlot draggedSlot) {
        this.draggedSlot = draggedSlot;
    }

    @Override
    public float getDraggedSlotX() {
        return draggedSlotX;
    }

    @Override
    public float getDraggedSlotY() {
        return draggedSlotY;
    }

    @Override
    public boolean canTakeItem(MatrixInventory matrixInventory, GuiMatrixSlot slot) {
        return true;
    }

    @Override
    public boolean canPutItem(MatrixInventory matrixInventory, GuiMatrixSlot slot) {
        return true;
    }

    @Override
    public boolean isPlayerInventory(MatrixInventory matrixInventory) {
        return matrixInventory == modules.get(0).getMatrixInventory();
    }

    @Override
    public void putItem(int x, int y, MatrixInventory toInventory) {
        MatrixInventory fromMatrix = draggedSlot.getMatrixInventory();
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventoryTransactionItemMove(
                (int)draggedSlotX,
                (int)draggedSlotY,
                x,
                y,
                MatrixInventoryUtils.getInvMatrixRotation(draggedSlot.getItemStack()),
                isPlayerInventory(fromMatrix),
                isPlayerInventory(toInventory)
        ));
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketContainerInteract(entityContainer.getEntityId(), false));
    }
}
