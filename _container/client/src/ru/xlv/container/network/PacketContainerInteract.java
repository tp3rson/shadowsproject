package ru.xlv.container.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketContainerInteract implements IPacketOut {

    private int entityId;
    private boolean startInteract;

    public PacketContainerInteract(int entityId, boolean startInteract) {
        this.entityId = entityId;
        this.startInteract = startInteract;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(entityId);
        bbos.writeBoolean(startInteract);
    }
}
