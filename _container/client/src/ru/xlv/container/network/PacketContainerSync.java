package ru.xlv.container.network;

import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketContainerSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        int entityId = bbis.readInt();
        Entity entityByID = Minecraft.getMinecraft().thePlayer.worldObj.getEntityByID(entityId);
        if (entityByID instanceof EntityContainer) {
            ((EntityContainer) entityByID).setGrowing(bbis.readBoolean());
            ((EntityContainer) entityByID).setRotation(bbis.readFloat());
            ((EntityContainer) entityByID).setRarity(EnumItemRarity.values()[bbis.readInt()]);
        }
    }
}
