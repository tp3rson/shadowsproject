package ru.xlv.container.network;

import lombok.NoArgsConstructor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.container.entity.EntityContainerServer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.inventory.MatrixInventoryController;
import ru.xlv.mochar.network.matrix.PacketMatrixInventoryTransactionSync;

import java.io.IOException;

@NoArgsConstructor
public class PacketContainerInteract implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int entityId = bbis.readInt();
        boolean startInteract = bbis.readBoolean();
        Entity entityByID = entityPlayer.worldObj.getEntityByID(entityId);
        if (entityByID instanceof EntityContainerServer) {
            ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
            if (serverPlayer != null) {
                if(startInteract) {
                    ((EntityContainerServer) entityByID).startInteract(serverPlayer);
                    if (MatrixInventoryController.INSTANCE.openMatrixInventory(entityPlayer, (IMatrixInventoryProvider) entityByID)) {
                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixInventoryTransactionSync(((EntityContainerServer) entityByID).getMatrixInventory()));
                    }
                } else {
                    ((EntityContainerServer) entityByID).endInteract(serverPlayer);
                    MatrixInventoryController.INSTANCE.closeCurrentMatrixInventory(entityPlayer);
                }
            }
        }
    }
}
