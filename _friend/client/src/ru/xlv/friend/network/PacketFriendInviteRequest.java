package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.PacketClientPlayerSync;
import ru.xlv.core.player.ClientPlayer;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketFriendInviteRequest implements IPacketCallbackEffective<PacketFriendInviteRequest.Result> {

    @Getter
    @RequiredArgsConstructor
    public static class Result {
        private boolean success;
        private ClientPlayer clientPlayer;
        private String responseMessage;
    }

    private final Result result = new Result();

    private String targetName;

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result.success = bbis.readBoolean();
        boolean hasPlayer = bbis.readBoolean();
        if(hasPlayer) {
            result.clientPlayer = PacketClientPlayerSync.readClientPlayer(bbis);
        }
        result.responseMessage = bbis.readUTF();
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(targetName);
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }
}
