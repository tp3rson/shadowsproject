package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.core.util.Flex;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.common.FriendRelation;
import ru.xlv.mochar.network.PacketClientPlayerSync;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@NoArgsConstructor
public class PacketFriendPlayerListGet implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(500L);

    private final List<String> friendRelations = new ArrayList<>();

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        int filter1 = bbis.readInt();
        int offset = bbis.readInt();
        REQUEST_CONTROLLER.doCompletedRequestAsync(entityPlayer, () -> {
            if (Flex.inEnumRange(FilterType.class, filter1)) {
                FilterType filterType = FilterType.values()[filter1];
                Stream<String> relations = null;
                switch (filterType) {
                    case FRIENDS:
                        relations = XlvsFriendMod.INSTANCE.getFriendHandler().getAllFriendNames(entityPlayer.getCommandSenderName()).stream();
                        break;
                    case INCOMING_INVITE:
                        relations = XlvsFriendMod.INSTANCE.getFriendHandler().getAllRelations(entityPlayer.getCommandSenderName())
                                .stream()
                                .filter(friendRelation -> !friendRelation.getInitiator().equals(entityPlayer.getCommandSenderName()))
                                .map(FriendRelation::getInitiator);
                        break;
                    case OUTGOING_INVITE:
                        relations = XlvsFriendMod.INSTANCE.getFriendHandler().getAllRelations(entityPlayer.getCommandSenderName())
                                .stream()
                                .filter(friendRelation -> friendRelation.getInitiator().equals(entityPlayer.getCommandSenderName()))
                                .map(FriendRelation::getTarget);
                }
                relations.skip(offset).limit(20).forEach(friendRelations::add);
            }
        });
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        writeCollection(bbos, friendRelations, (byteBufOutputStream, s) -> FlexPlayer.of(s)
                .notNull()
                .accept(serverPlayer -> {
                    try {
                        PacketClientPlayerSync.writeClientPlayer(serverPlayer, bbos);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                })
        );
    }

    public enum FilterType {
        FRIENDS,
        OUTGOING_INVITE,
        INCOMING_INVITE
    }
}
