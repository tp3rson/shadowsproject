package ru.xlv.friend.common;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FriendIO {

    public static List<FriendRelation> readFriendRelationList(ByteBufInputStream byteBufInputStream) throws IOException {
        List<FriendRelation> list = new ArrayList<>();
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            list.add(readFriendRelation(byteBufInputStream));
        }
        return list;
    }

    public static void writeFriendRelationList(List<FriendRelation> list, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(list.size());
        for (FriendRelation friendRelation : list) {
            writeFriendRelation(friendRelation, byteBufOutputStream);
        }
    }

    public static void writeFriendRelation(FriendRelation friendRelation, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeUTF(friendRelation.getInitiator());
        byteBufOutputStream.writeUTF(friendRelation.getTarget());
        byteBufOutputStream.writeInt(friendRelation.getState().ordinal());
    }

    public static FriendRelation readFriendRelation(ByteBufInputStream byteBufInputStream) throws IOException {
        String initiator = byteBufInputStream.readUTF();
        String target = byteBufInputStream.readUTF();
        FriendRelation friendRelation = new FriendRelation(initiator, target);
        int i = byteBufInputStream.readInt();
        if(i < 0 || i >= FriendRelation.State.values().length) throw new IOException();
        FriendRelation.State state = FriendRelation.State.values()[i];
        friendRelation.setState(state);
        return friendRelation;
    }
}
