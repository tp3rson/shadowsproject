package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.common.player.character.CharacterDebuffType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.Set;

public class SkillZarnitsa extends ActivableSkill {

    private final double radius;

    public SkillZarnitsa(SkillType skillType) {
        super(skillType);
        radius = skillType.getCustomParam("stun_enemy_all_range", double.class);
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        Set<ServerPlayer> enemyServerPlayersAround = SkillUtils.getEnemyServerPlayersAround(serverPlayer, radius);
        enemyServerPlayersAround.remove(serverPlayer);
        enemyServerPlayersAround.forEach(serverPlayer1 -> serverPlayer1.getSelectedCharacter().addDebuff(CharacterDebuffType.STUN, getSkillType().getDuration()));
    }

    @Nonnull
    @Override
    public SkillZarnitsa clone() {
        return new SkillZarnitsa(getSkillType());
    }
}
