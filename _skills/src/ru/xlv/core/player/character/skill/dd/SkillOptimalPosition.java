package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.player.character.CharacterDebuffType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillOptimalPosition extends ActivableSkill {

    private static final UUID MOD_UUID = UUID.randomUUID();
    private static final UUID MOD_UUID1 = UUID.randomUUID();
    private static final UUID MOD_UUID2 = UUID.randomUUID();

    private final CharacterAttributeMod characterAttributeMod, characterAttributeMod1, characterAttributeMod2;

    public SkillOptimalPosition(SkillType skillType) {
        super(skillType);
        double reloadMod = 1D + skillType.getCustomParam("reloadspeed_add_value", double.class) / 100D;
        double recoilMod = 1D + skillType.getCustomParam("shotrecoil_add_value", double.class) / 100D;
        double protectionMod = 1D + skillType.getCustomParam("protect_add_self_all_value", double.class) / 100D;
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.WEAPON_RELOAD_MOD)
                .valueMod(reloadMod)
                .period(getSkillType().getDuration())
                .uuid(MOD_UUID)
                .build();
        characterAttributeMod1 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.WEAPON_RECOIL_MOD)
                .valueMod(recoilMod)
                .period(getSkillType().getDuration())
                .uuid(MOD_UUID1)
                .build();
        characterAttributeMod2 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionMod)
                .period(getSkillType().getDuration())
                .uuid(MOD_UUID2)
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addDebuff(CharacterDebuffType.BLOCK_MOVE, getSkillType().getDuration());
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true);
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod2, true);
    }

    @Nonnull
    @Override
    public SkillOptimalPosition clone() {
        return new SkillOptimalPosition(getSkillType());
    }
}
