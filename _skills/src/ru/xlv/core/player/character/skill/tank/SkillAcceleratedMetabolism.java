package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillAcceleratedMetabolism extends Skill implements ISkillUpdatable {

    private final CharacterAttributeMod characterAttributeMod;

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillAcceleratedMetabolism(SkillType skillType) {
        super(skillType);
        double healthRegenAdd = skillType.getCustomParam("hp_regen_self_value", double.class);
        double healthRegenMod = 1D + skillType.getCustomParam("income_hp_add_value", double.class) / 100D;
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.SKILL_REGEN_MOD)
                .valueMod(healthRegenMod)
                .period(skillType.getDuration())
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
            serverPlayer.getEntityPlayer().heal((float) healthRegenAdd);
        });
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Nonnull
    @Override
    public SkillAcceleratedMetabolism clone() {
        return new SkillAcceleratedMetabolism(getSkillType());
    }
}
