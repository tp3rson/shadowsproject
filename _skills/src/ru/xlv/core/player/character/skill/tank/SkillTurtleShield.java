package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillTurtleShield extends ActivableSkill {

    private final CharacterAttributeMod characterAttributeMod, characterAttributeMod1;

    public SkillTurtleShield(SkillType skillType) {
        super(skillType);
        double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
        double protectionMod = 1D + skillType.getCustomParam("income_damage_value", double.class) / 100D;
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(skillType.getDuration())
                .build();
        characterAttributeMod1 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionMod)
                .period(skillType.getDuration())
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true);
    }

    @Nonnull
    @Override
    public SkillTurtleShield clone() {
        return new SkillTurtleShield(getSkillType());
    }
}
