package ru.xlv.core.player.character.skill;

import lombok.experimental.UtilityClass;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Flex;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@UtilityClass
public class SkillUtils {

    @Nonnull
    public Set<EntityPlayer> getEnemyPlayersAround(@Nonnull EntityPlayer entityPlayer, double radius) {
        List<?> list0 = entityPlayer.worldObj.getEntitiesWithinAABBExcludingEntity(entityPlayer, CommonUtils.getAxisAlignedBBOf(entityPlayer, radius));
        if (list0.isEmpty()) {
            return new HashSet<>();
        }
        Stream<EntityPlayer> entityPlayerStream = list0.stream()
                .filter(object -> object instanceof EntityPlayer)
                .map(object -> ((EntityPlayer) object));
        GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(entityPlayer.getCommandSenderName());
        if (playerGroup != null) {
            entityPlayerStream = entityPlayerStream.filter(entityPlayer1 -> Flex.forEach(playerGroup.getPlayers(), serverPlayer1 -> serverPlayer1.getEntityPlayer() != entityPlayer1));
        }
        return entityPlayerStream.filter(entityPlayer1 -> !entityPlayer1.getCommandSenderName().equals(entityPlayer.getCommandSenderName())).collect(Collectors.toSet());
    }

    @Nonnull
    public Set<ServerPlayer> getEnemyServerPlayersAround(@Nonnull ServerPlayer serverPlayer, double radius) {
        List<ServerPlayer> list0 = XlvsCore.INSTANCE.getPlayerManager().getPlayersAround(serverPlayer, radius);
        if (list0 == null || list0.isEmpty()) {
            return new HashSet<>();
        }
        Stream<ServerPlayer> entityPlayerStream = list0.stream();
        GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
        if (playerGroup != null) {
            entityPlayerStream = entityPlayerStream.filter(serverPlayer1 -> Flex.forEach(playerGroup.getPlayers(), serverPlayer2 -> serverPlayer2 != serverPlayer1));
        }
        return entityPlayerStream.filter(serverPlayer1 -> serverPlayer1 != serverPlayer).collect(Collectors.toSet());
    }

    @Nonnull
    public Set<ServerPlayer> getAllyPlayersAround(@Nonnull ServerPlayer serverPlayer, double radius) {
        GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
        Set<ServerPlayer> set = new HashSet<>();
        if (playerGroup != null) {
            set.addAll(playerGroup.getPlayers());
        } else {
            set.add(serverPlayer);
            return set;
        }
        return set.stream()
                .filter(serverPlayer1 -> serverPlayer1.isOnline() && serverPlayer1.getEntityPlayer().getDistanceToEntity(serverPlayer.getEntityPlayer()) <= radius)
                .collect(Collectors.toSet());
    }
}
