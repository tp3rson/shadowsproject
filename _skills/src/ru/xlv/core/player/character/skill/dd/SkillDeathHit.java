package ru.xlv.core.player.character.skill.dd;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.PlayerDamageEntityEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillDeathHit extends ActivableSkill {

    private final double damageMod;

    private ServerPlayer serverPlayer;

    public SkillDeathHit(SkillType skillType) {
        super(skillType);
        damageMod = 1D + skillType.getCustomParam("outcome_melee_damage_value", double.class) / 100D;
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerDamageEntityEvent.Post event) {
        if(event.getServerPlayer() == serverPlayer) {
            event.setAmount((float) (event.getAmount() * damageMod));
            XlvsCoreCommon.EVENT_BUS.unregister(this);
        }
    }

    @Nonnull
    @Override
    public SkillDeathHit clone() {
        return new SkillDeathHit(getSkillType());
    }
}
