package ru.xlv.core.player.character.skill.dd;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillAdrenalineRush extends ActivableSkill {

    private final long reduceCooldown;

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    private ServerPlayer serverPlayer;

    public SkillAdrenalineRush(SkillType skillType) {
        super(skillType);
        double damageMod = 1D + skillType.getCustomParam("outcome_damage_value", double.class) / 100D;
        double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
        reduceCooldown = ((long) (skillType.getCustomParam("additional_duration_per_kill", double.class) * 1000));
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_MOD)
                .valueMod(damageMod)
                .period(1000L)
                .build();
        CharacterAttributeMod characterAttributeMod1 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(1000L)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(500L, serverPlayer -> {
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true);
        });
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        super.update(serverPlayer);
        if(isActive()) {
            scheduledConsumableTask.update(serverPlayer);
        }
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
        MinecraftForge.EVENT_BUS.register(this);
    }

    @Override
    protected void deactivateSkill(ServerPlayer serverPlayer) {
        super.deactivateSkill(serverPlayer);
        MinecraftForge.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(LivingDeathEvent event) {
        if(event.source.getSourceOfDamage() instanceof EntityPlayer && event.source.getSourceOfDamage() == serverPlayer.getEntityPlayer()) {
            reduceCooldown(reduceCooldown);
        }
    }

    @Nonnull
    @Override
    public SkillAdrenalineRush clone() {
        return new SkillAdrenalineRush(getSkillType());
    }
}
