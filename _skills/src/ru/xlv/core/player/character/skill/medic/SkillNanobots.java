package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillNanobots extends ActivableSkill {

    private static final UUID CHARACTER_ATTRIBUTE_BOOTS_DAMAGE_UUID = UUID.randomUUID();

    private final double healAmount;
    private final double manaAmount;

    private final ScheduledConsumableTask<ServerPlayer> scheduledTask;

    private double x, y, z;

    public SkillNanobots(SkillType skillType) {
        super(skillType);
        this.healAmount = skillType.getCustomParam("hp_regen_self_value", double.class);
        this.manaAmount = skillType.getCustomParam("mana_regen_self_value", double.class);
        final double damageMod = 1D - skillType.getCustomParam("income_damage_value", double.class) / 100D;
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(damageMod)
                .uuid(CHARACTER_ATTRIBUTE_BOOTS_DAMAGE_UUID)
                .period(1000L)
                .build();
        scheduledTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            serverPlayer.getEntityPlayer().heal((float) healAmount);
            serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA).addValue(manaAmount);
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        });
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        x = serverPlayer.getEntityPlayer().posX;
        y = serverPlayer.getEntityPlayer().posY;
        z = serverPlayer.getEntityPlayer().posZ;
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        super.update(serverPlayer);
        if(isActive()) {
            if(x != serverPlayer.getEntityPlayer().posX || y != serverPlayer.getEntityPlayer().posY || z != serverPlayer.getEntityPlayer().posZ) {
                deactivateSkill(serverPlayer);
                return;
            }
            scheduledTask.update(serverPlayer);
        }
    }

    @Nonnull
    @Override
    public SkillNanobots clone() {
        return new SkillNanobots(getSkillType());
    }
}
