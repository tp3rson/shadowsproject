package ru.xlv.core.player.character.skill;

import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillEmptyPassive extends Skill {

    public SkillEmptyPassive(SkillType skillType) {
        super(skillType);
    }

    @Nonnull
    @Override
    public SkillEmptyPassive clone() {
        return new SkillEmptyPassive(getSkillType());
    }
}
