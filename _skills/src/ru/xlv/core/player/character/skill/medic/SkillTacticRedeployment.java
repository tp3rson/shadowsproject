package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillTacticRedeployment extends ActivableSkill {

    private final CharacterAttributeMod characterAttributeMod, characterAttributeMod1;

    public SkillTacticRedeployment(SkillType skillType) {
        super(skillType);
        final double protectionMod = 1D + skillType.getCustomParam("shield_add_self_value", double.class) / 100D;
        final double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
        characterAttributeMod = CharacterAttributeMod.builder()
                .period(skillType.getDuration())
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionMod)
                .build();
        characterAttributeMod1 = CharacterAttributeMod.builder()
                .period(skillType.getDuration())
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod);
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod1);
    }

    @Nonnull
    @Override
    public SkillTacticRedeployment clone() {
        return new SkillTacticRedeployment(getSkillType());
    }
}
