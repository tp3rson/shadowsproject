package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Flex;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SkillShieldProjection extends ActivableSkill {

    private static final UUID CHARACTER_ATTRIBUTE_BOOST_UUID = UUID.randomUUID();

    private final double radius;

    private final CharacterAttributeMod characterAttributeMod;

    public SkillShieldProjection(SkillType skillType) {
        super(skillType);
        final double protectionMod = 1D + skillType.getCustomParam("shield_add_ally_low_hp_value", double.class) / 100D;
        radius = skillType.getCustomParam("shield_add_ally_low_hp_range", double.class);
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionMod)
                .uuid(CHARACTER_ATTRIBUTE_BOOST_UUID)
                .period(skillType.getDuration())
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
        if (playerGroup != null) {
            List<ServerPlayer> list = playerGroup.getPlayers()
                    .stream()
                    .filter(serverPlayer1 -> serverPlayer != serverPlayer1)
                    .filter(ServerPlayer::isOnline)
                    .filter(serverPlayer1 -> serverPlayer.getEntityPlayer().getDistanceToEntity(serverPlayer1.getEntityPlayer()) < radius)
                    .collect(Collectors.toList());
            if(!list.isEmpty()) {
                ServerPlayer uniqueElement = Flex.getUniqueElement(
                        list,
                        serverPlayer1 -> serverPlayer1.getEntityPlayer().getDistanceToEntity(serverPlayer.getEntityPlayer()),
                        (aFloat, aFloat2) -> aFloat > aFloat2
                );
                if (uniqueElement != null) {
                    uniqueElement.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
                }
            }
        } else {
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        }
    }

    @Nonnull
    @Override
    public SkillShieldProjection clone() {
        return new SkillShieldProjection(getSkillType());
    }
}
