package ru.xlv.core.api;

import lombok.SneakyThrows;
import ru.xlv.core.crypt.FakeDecryptor;
import ru.xlv.core.crypt.IDecryptor;
import ru.xlv.core.crypt.MainDecryptor;
import ru.xlv.core.util.CoreUtils;
import ru.xlv.core.util.FolderResourceLoader;
import ru.xlv.core.util.ZipResourceLoader;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CoreAPI {

    private static final List<IResourceLoader> resourceLoaderList = Collections.synchronizedList(new ArrayList<>());
    private static IDecryptor decryptor;
    private static boolean isInitialized;

    @SneakyThrows
    public static void init(String token, String primaryKey, String url, String username) {
        if(isInitialized) return;
        isInitialized = true;
        if(CoreUtils.isStartedFromGradle()) {
            registerResourceLoader(new FolderResourceLoader());
        } else {
            registerResourceLoader(new ZipResourceLoader());
        }
        CoreAPI.registerResourceLoader((IResourceLoader) Class.forName("ru.xlv.core.util.MinecraftResourceLoader").newInstance());

        if(CoreUtils.isDebugEnabled || CoreUtils.isStartedFromGradle() || token == null) {
            setDecryptor(new FakeDecryptor());
//            CoreAPI.setDecryptor(new TestDecryptor());
        } else {
//            setDecryptor(new FakeDecryptor());
            setDecryptor(new MainDecryptor(token, primaryKey, url, username));
        }
    }

    @SneakyThrows
    public static void connect(String token, String username) {
        Socket socket = new Socket("localhost", 1204);
        socket.setSoTimeout(5000);
        PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
        printWriter.println(token);
        printWriter.flush();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String url = bufferedReader.readLine();
        String pk = bufferedReader.readLine();
        printWriter.close();
        bufferedReader.close();
        socket.close();
//        System.out.println(url);
//        System.out.println(pk);
        init(token, pk, url, username);
    }

//    @SneakyThrows
//    public static void main(String[] args) {
//        connect("asd", "");
////        HttpURLConnection connection = (HttpURLConnection) new URL("http://s1.shadowsproject.ru:8080/kz/sosi/moihui?a=5bd263c32663619bd83db2672a1d9533&b=Kashmir").openConnection();
////        connection.setRequestMethod("GET");
////        connection.connect();
////        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
////        while() {
////
////        }
////        MainDecryptor mainDecryptor = new MainDecryptor("5bd263c32663619bd83db2672a1d9533", "yds2zg6dJ7x1iyidVloecQ==", "http://s1.shadowsproject.ru:8080/kz/sosi/moihui?a=%s&b=%s", "Kashmir");
//    }

    private static byte[] readArray(DataInputStream dataInputStream, int length) throws IOException {
        byte[] bytes = new byte[length];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = dataInputStream.readByte();
        }
        return bytes;
    }

    public static void setDecryptor(IDecryptor decryptor) {
        CoreAPI.decryptor = decryptor;
    }

    public static IDecryptor getDecryptor() {
        return decryptor;
    }

    public static void registerResourceLoader(IResourceLoader resourceLoader) {
        resourceLoaderList.add(resourceLoader);
    }

    public static List<IResourceLoader> getResourceLoaders() {
        return resourceLoaderList;
    }

    public static InputStream getResourceInputStream(String loc, boolean decryptBefore) {
        try {
            for (IResourceLoader resourceLoader : getResourceLoaders()) {
                InputStream inputStream;
                if ((inputStream = resourceLoader.getResourceInputStream(loc)) != null) {
                    return decryptBefore ? getDecryptor().getDecryptedInputStream(inputStream) : inputStream;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        if(CoreUtils.isStartedFromGradle())
            return null;
        else
            throw new RuntimeException("An error has occurred during loading resource " + loc);
    }
}
