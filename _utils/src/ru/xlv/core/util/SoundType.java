package ru.xlv.core.util;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public enum SoundType {

    OPEN_CHEST("block.chest.chest_open"),
    DOOR_LOCKED("block.door.door_locked"),
    BUTTON_ACCEPT("interface.button.button_accept"),
    BUTTON_BACKWARD("interface.button.button_backward"),
    BUTTON_CLICK("interface.button.button_click"),
    BUTTON_DECLINE("interface.button.button_decline"),
    BUTTON_HOVER("interface.button.button_hover"),
    BUTTON_NOT_AVAILABLE("interface.button.button_not_available"),
    INV_ARMOR_PICK_DOWN("interface.inventory.armor_pick_down"),
    INV_ARMOR_PICK_UP("interface.inventory.armor_pick_up"),
    INV_ARMOR_PUT_ON("interface.inventory.armor_put_on"),
    INV_ARMOR_PUT_DOWN("interface.inventory.armor_put_down"),
    INV_HEALING_ITEM_PICK_DOWN("interface.inventory.healing_item_pick_down"),
    INV_HEALING_ITEM_PICK_UP("interface.inventory.healing_item_pick_up"),
    INV_HEAVY_WEAPON_PICK_DOWN("interface.inventory.heavy_weapon_pick_down"),
    INV_HEAVY_WEAPON_PICK_UP("interface.inventory.heavy_weapon_pick_up"),
    INV_ITEM_PICK_DOWN("interface.inventory.item_pick_down"),
    INV_ITEM_PICK_UP("interface.inventory.item_pick_up"),
    INV_LIGHT_WEAPON_PICK_DOWN("interface.inventory.light_weapon_pick_down"),
    INV_LIGHT_WEAPON_PICK_UP("interface.inventory.light_weapon_pick_up"),
    INV_MEDIUM_WEAPON_PICK_DOWN("interface.inventory.medium_weapon_pick_down"),
    INV_MEDIUM_WEAPON_PICK_UP("interface.inventory.medium_weapon_pick_up"),
    INV_MELEE_PICK_DOWN("interface.inventory.melee_pick_down"),
    INV_MELEE_PICK_UP("interface.inventory.melee_pick_up"),
    MODIFICATION_ADD("interface.modification.modification_add"),
    MODIFICATION_REMOVE("interface.modification.modification_remove"),
    LETTER_RECEIVED("interface.notification.letter_recieved"),
    LETTER_SENT("interface.notification.letter_sended"),
    NEW_ACHIEVEMENT("interface.notification.new_achievement"),
    SKILL_LEARNED("interface.notification.skill_researched"),
    BLOCK_CONCRETE_STEP("block.concrete.concrete_footstep"),
    BLOCK_DIRT_STEP("block.dirt.dirt_footstep"),
    BLOCK_GLASS_STEP("block.glass.glass_footstep"),
    BLOCK_GRASS_STEP("block.grass.grass_footstep"),
    BLOCK_GRAVEL_STEP("block.gravel.gravel_footstep"),
    BLOCK_HARDWOOD_STEP("block.hardwood.hardwood_footstep"),
    BLOCK_ICE_STEP("block.ice.ice_footstep"),
    BLOCK_METAL_STEP("block.metal.metal_footstep"),
    BLOCK_MUD_STEP("block.mud.mud_footstep"),
    BLOCK_SAND_STEP("block.sand.sand_footstep"),
    BLOCK_SNOW_STEP("block.snow.snow_footstep"),
    BLOCK_STONE_STEP("block.stone.stone_footstep"),
    BLOCK_WATER_STEP("block.water.water_footstep"),
    BLOCK_WOOD_STEP("block.wood.wood_footstep"),
    RAIN("ambient.rain.rain"),
    PLAYER_DEATH("damage.death.slight_scream")
    ;

    public static final String DEFAULT_DOMAIN = "xlvscore:";

    private String soundKey;

    SoundType(String soundKey) {
        this.soundKey = soundKey.contains(":") ? soundKey : DEFAULT_DOMAIN + soundKey;
    }

    public String getSoundKey() {
        return (soundKey == null ? name().toLowerCase().replaceAll("_", ".") : soundKey);
    }
}
