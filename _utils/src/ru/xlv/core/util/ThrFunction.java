package ru.xlv.core.util;

import java.io.IOException;

public interface ThrFunction<T, R> {

    R apply(T t) throws IOException;
}
