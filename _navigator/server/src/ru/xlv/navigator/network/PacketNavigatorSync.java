package ru.xlv.navigator.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.network.IPacketOutServer;
import ru.xlv.navigator.XlvsNavigatorMod;
import ru.xlv.navigator.common.NavigatorUtils;

import java.io.IOException;

@NoArgsConstructor
public class PacketNavigatorSync implements IPacketOutServer {
    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        NavigatorUtils.writePortalList(XlvsNavigatorMod.INSTANCE.getPortalManager().getPortals(), bbos);
        NavigatorUtils.writeMarkerList(XlvsNavigatorMod.INSTANCE.getMarkerManager().getMarkers(), bbos);
    }
}
