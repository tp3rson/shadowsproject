package ru.xlv.navigator.handle;

import lombok.Getter;
import ru.xlv.navigator.common.marker.Marker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MarkerManager {

    @Getter
    private final List<Marker> markers = Collections.synchronizedList(new ArrayList<>());


}
