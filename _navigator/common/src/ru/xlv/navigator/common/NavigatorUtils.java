package ru.xlv.navigator.common;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.util.ByteBufInputStream;
import ru.xlv.navigator.common.marker.Marker;
import ru.xlv.navigator.common.portal.Portal;

import java.io.IOException;
import java.util.List;

public class NavigatorUtils {

    public static void writeMarkerList(List<Marker> list, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(list.size());
        for (Marker marker : list) {
            writeMarker(marker, byteBufOutputStream);
        }
    }

    public static void writePortalList(List<Portal> list, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(list.size());
        for (Portal portal : list) {
            writePortal(portal, byteBufOutputStream);
        }
    }

    public static void writePortal(Portal portal, ByteBufOutputStream byteBufOutputStream) {

    }

    public static void readPortal(Portal portal, ByteBufInputStream byteBufInputStream) {

    }

    public static void writeMarker(Marker marker, ByteBufOutputStream byteBufOutputStream) {

    }

    public static void readMarker(ByteBufInputStream byteBufInputStream) {

    }
}
