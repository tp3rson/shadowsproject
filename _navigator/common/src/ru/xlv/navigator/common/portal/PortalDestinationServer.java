package ru.xlv.navigator.common.portal;

import lombok.Getter;

@Getter
public class PortalDestinationServer extends PortalDestinationXYZ {

    private final String ip;
    private final int port;

    public PortalDestinationServer(String ip, int port, double x, double y, double z) {
        super(x, y, z);
        this.ip = ip;
        this.port = port;
    }
}
