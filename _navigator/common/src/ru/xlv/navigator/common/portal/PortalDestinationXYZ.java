package ru.xlv.navigator.common.portal;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PortalDestinationXYZ extends PortalDestination {

    private final double x, y, z;
}
