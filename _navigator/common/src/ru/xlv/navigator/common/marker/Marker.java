package ru.xlv.navigator.common.marker;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Marker {

    private boolean isInteractive;
    private double x, y, z;
}
