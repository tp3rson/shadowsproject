#version 120

varying vec2 textureCoords;
uniform sampler2D downsampleTexture;
uniform sampler2D gaussianTexture;

uniform vec2 texelSize;

float w0(float a)
{
    return (1.0/6.0)*(a*(a*(-a + 3.0) - 3.0) + 1.0);
}

float w1(float a)
{
    return (1.0/6.0)*(a*a*(3.0*a - 6.0) + 4.0);
}

float w2(float a)
{
    return (1.0/6.0)*(a*(a*(-3.0*a + 3.0) + 3.0) + 1.0);
}

float w3(float a)
{
    return (1.0/6.0)*(a*a*a);
}

float g0(float a)
{
    return w0(a) + w1(a);
}

float g1(float a)
{
    return w2(a) + w3(a);
}

float h0(float a)
{
    return -1.0 + w1(a) / (w0(a) + w1(a));
}

float h1(float a)
{
    return 1.0 + w3(a) / (w2(a) + w3(a));
}

vec4 texture2D_bicubic(sampler2D tex, vec2 uv)
{
	vec4 texelSize = vec4(texelSize,1.0/texelSize);
	
	uv = uv*texelSize.zw;
	vec2 iuv = floor( uv );
	vec2 fuv = fract( uv );

    float g0x = g0(fuv.x);
    float g1x = g1(fuv.x);
    float h0x = h0(fuv.x);
    float h1x = h1(fuv.x);
    float h0y = h0(fuv.y);
    float h1y = h1(fuv.y);

	vec2 p0 = (vec2(iuv.x + h0x, iuv.y + h0y) - 0.5) * texelSize.xy;
	vec2 p1 = (vec2(iuv.x + h1x, iuv.y + h0y) - 0.5) * texelSize.xy;
	vec2 p2 = (vec2(iuv.x + h0x, iuv.y + h1y) - 0.5) * texelSize.xy;
	vec2 p3 = (vec2(iuv.x + h1x, iuv.y + h1y) - 0.5) * texelSize.xy;
	
    return g0(fuv.y) * (g0x * texture2D(tex, p0)  +
                        g1x * texture2D(tex, p1)) +
           g1(fuv.y) * (g0x * texture2D(tex, p2)  +
                        g1x * texture2D(tex, p3));
}

void main() {

vec3 bloom = texture2D(downsampleTexture,textureCoords/2.0).rgb;	//1/4 res
bloom += texture2D_bicubic(gaussianTexture,textureCoords/2.).rgb; //1/8 res
bloom += texture2D_bicubic(gaussianTexture,textureCoords/4.+vec2(0.25+2.*texelSize.x,.0)).rgb;  //1/16 res
bloom += texture2D_bicubic(gaussianTexture,textureCoords/8.+vec2(0.375+4.0*texelSize.x,.0)).rgb*1.0; //1/32 res
bloom += texture2D_bicubic(gaussianTexture,textureCoords/16.+vec2(0.4375+6.0*texelSize.x,.0)).rgb*1.0; //1/64 res
bloom += texture2D_bicubic(gaussianTexture,textureCoords/32.+vec2(0.46875+8.0*texelSize.x,.0)).rgb*1.25; //1/128 res
bloom += texture2D_bicubic(gaussianTexture,textureCoords/64.+vec2(0.484375+10.0*texelSize.x,.0)).rgb*2.5; //1/256 res

gl_FragData[0].rgb = bloom/100.0;
gl_FragData[0].rgb = clamp(gl_FragData[0].rgb,0.0,65000.);
}
