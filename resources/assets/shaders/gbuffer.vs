//gBuffer shader
//Krogenit 
#version 330 core

layout (location = 4) in vec3 in_position;
layout (location = 5) in vec2 in_textureCoords;
layout (location = 6) in vec3 in_normal;
layout (location = 11) in vec3 in_tangent;

out vec2 texCoords;
out vec2 lightMapCoords;
out vec3 normal;
out vec3 position;
out vec4 color;
out vec3 tangent;

uniform mat4 projMat, viewMat;
uniform mat4 lightMapMatrix;
uniform vec4 in_color;
uniform vec2 in_lightMapCoord;

void main() {
	vec4 pos = viewMat * vec4(in_position, 1.0);
	normal = (viewMat * vec4(in_normal, 0.0)).xyz;
	texCoords = in_textureCoords;
	
	color = in_color;
	tangent = (viewMat * vec4(in_tangent, 0.0)).xyz;	
	position = pos.xyz;
	gl_Position = projMat * pos;
	
	lightMapCoords = (lightMapMatrix * vec4(in_lightMapCoord, 0.0, 1.0)).xy;
}