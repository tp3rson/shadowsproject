//Krogenit
#version 330
out vec4 outputColor;

uniform sampler2D gColor;
uniform sampler2D gLightMap;
uniform sampler2D gPosition;
uniform sampler2D gNormal;

uniform sampler2D lightmap;

uniform bool usePointLights;

in vec2 texCoords;

void main() {
	//vec4 gLightMapCoord = texture(gLightMap, texCoords);
	vec4 colorAlbedo = texture(gColor, texCoords);

	//vec3 colorLightmap = texture(lightmap, gLightMapCoord.xy).xyz;
	
	outputColor.rgb = colorAlbedo.rgb;
	outputColor.w = colorAlbedo.w;
}