//Fragment Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330
out vec4 outputColor;

#define POINT_LIGHTS 1

in vec2 texCoords;
in vec2 lightMapCoords;
in vec4 color;
in vec3 position;
in vec3 normal;
in vec3 tangent;
in vec3 lightPos;

uniform bool useTexture;
uniform bool useLightMap;
uniform sampler2D diffuse;
uniform sampler2D normalMap;
uniform sampler2D specularMap;
uniform sampler2D glossMap;
uniform sampler2D lightMap;

uniform bool useNormalMapping;
uniform bool useSpecularMapping;
uniform bool useGlossMapping;

uniform struct PointLight {
   vec3 position;
   float attenuation;
   vec3 color;
   float specular;
} pointLights[POINT_LIGHTS];

vec3 calculatePointLight(in PointLight light, in vec3 E, 
in vec3 delta, in float dist, in vec3 normal, in float specularUnit, in float shininess) {
	vec3 L = normalize(delta);
	vec3 R = normalize(-reflect(L,normal));
	float nDotL = max(dot(normal,L), 0.0);
	
	float att = 1.0 - dist / light.attenuation;
	
	if(att > 0) {
		vec3 colorL = light.color;
		float rDotE = max(dot(R,E),0.0);
		float power = pow(rDotE, 128.0 * shininess);
		vec3 amb = colorL * att / 2.0;		
		vec3 dif = colorL * nDotL * att * 2.0; 
		vec3 spec = 6 * colorL * power * att * specularUnit * light.specular;

		vec3 final = amb + dif + spec;
			
		return final;
	} else {
		return vec3(0,0,0);
	}
}

void main() {
	vec3 delta = (lightPos - position.xyz);
	float dist = length(delta);
	if(dist <= pointLights[0].attenuation) {
		vec4 diffuseColor = vec4(1.0,1.0,1.0,1.0);
		
		if(useTexture) {
			diffuseColor = texture(diffuse, texCoords) * color;
		} else {
			diffuseColor = color;
		}
		
		vec3 lightMapColor = vec3(1.0, 1.0, 1.0);
	
		if(useLightMap) {
			vec2 newLightMapCoords = lightMapCoords;
			newLightMapCoords.x = 0.05;
			lightMapColor = texture(lightMap, newLightMapCoords).xyz;
		}
		
		outputColor.w = diffuseColor.w;

		vec3 unitNormal = normal;
		float specularUnit = 1.0;
		float shininess = 0.25; 
		
		if(useNormalMapping) {
			vec3 normalTex = texture(normalMap, texCoords).rgb;
			vec3 normalMapValue = 2.0 * normalTex - 1.0;
			mat3 toTangentSpace = mat3(
				1, 0, 0,
				0, 1, 0,
				0, 0, 1
			);
			vec3 bitang = cross(normal, tangent);
			unitNormal = normalize(normalMapValue);
			
			toTangentSpace = mat3(
				tangent, bitang, normal
			);
				
			unitNormal = normalize(toTangentSpace * unitNormal);
		}
			
		if(useSpecularMapping) {
			specularUnit = texture(specularMap, texCoords).r;
		}
		
		if(useGlossMapping) {
			shininess = texture(glossMap, texCoords).r;
		}

		vec3 lightValue = lightMapColor * calculatePointLight(pointLights[0], normalize(-position.xyz), delta, dist, unitNormal, specularUnit, shininess);

		outputColor.rgb = diffuseColor.rgb * lightValue;
	} else {
		outputColor = vec4(0.0, 0.0, 0.0, 0.0);
	}
}