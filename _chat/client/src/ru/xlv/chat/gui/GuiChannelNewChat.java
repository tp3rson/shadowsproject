package ru.xlv.chat.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiNewChat;

public class GuiChannelNewChat extends GuiNewChat {
    public GuiChannelNewChat(Minecraft p_i1022_1_) {
        super(p_i1022_1_);
    }

    public void scroll(int p_146229_1_) {
        this.field_146250_j += p_146229_1_;
        int chatSize = 12;
        int j = this.field_146253_i.size();
        if (this.field_146250_j > j - chatSize) {
            this.field_146250_j = j - chatSize;
        }

        if (this.field_146250_j <= 0) {
            this.field_146250_j = 0;
            this.field_146251_k = false;
        }
    }
}
