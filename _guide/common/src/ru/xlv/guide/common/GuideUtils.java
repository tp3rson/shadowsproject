package ru.xlv.guide.common;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GuideUtils {

    public static void writeGuideList(List<Guide> list, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(list.size());
        for (Guide guide : list) {
            writeGuide(guide, byteBufOutputStream);
        }
    }

    public static List<Guide> readGuideList(ByteBufInputStream byteBufInputStream) throws IOException {
        List<Guide> list = new ArrayList<>();
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            list.add(readGuide(byteBufInputStream));
        }
        return list;
    }

    public static void writeGuide(Guide guide, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(guide.getCategory().ordinal());
        byteBufOutputStream.writeUTF(guide.getArticle());
        byteBufOutputStream.writeUTF(guide.getText());
        byteBufOutputStream.writeUTF(guide.getImageURL());
    }

    public static Guide readGuide(ByteBufInputStream byteBufInputStream) throws IOException {
        String article = byteBufInputStream.readUTF();
        String text = byteBufInputStream.readUTF();
        String imageURL = byteBufInputStream.readUTF();
        int c = byteBufInputStream.readInt();
        if(c < 0 || c >= GuideCategory.values().length) throw new IOException();
        GuideCategory category = GuideCategory.values()[c];
        return new Guide(article, text, imageURL, category);
    }
}
