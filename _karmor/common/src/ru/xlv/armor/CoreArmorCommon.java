package ru.xlv.armor;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import ru.krogenit.armor.item.ItemArmorByPart;
import ru.xlv.core.common.item.ItemArmor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CoreArmorCommon {

    public static final String MODID = "armor";

    public static final List<ItemArmor> registeredArmorList = new ArrayList<>();

    public static ItemArmorByPart murusKLHead, murusKLBody, murusKLBracers, murusKLLegs, murusKLFeet;
    public static ItemArmorByPart censorHead, censorBody, censorBracers, censorLegs, censorFeet;
    public static ItemArmorByPart bku0992Head, bku0992Body, bku0992Bracers, bku0992Legs, bku0992Feet;
    public static ItemArmorByPart ptnHead, ptnBody, ptnBracers, ptnLegs, ptnFeet;
    public static ItemArmorByPart enigmaBHead, enigmaBBody, enigmaBBracers, enigmaBLegs, enigmaBFeet;
    public static ItemArmorByPart azurESHead, azurESBody, azurESBracers, azurESLegs, azurESFeet;
    public static ItemArmorByPart specriotHead, specriotBody, specriotBracers, specriotLegs, specriotFeet;
    public static ItemArmorByPart conditor07Head, conditor07Body, conditor07Bracers, conditor07Legs, conditor07Feet;
    public static ItemArmorByPart nihilHead, nihilBody, nihilBracers, nihilLegs, nihilFeet;
    public static ItemArmorByPart hex6Head, hex6Body, hex6Bracers, hex6Legs, hex6Feet;
    public static ItemArmorByPart steelSunHead, steelSunBody, steelSunBracers, steelSunLegs, steelSunFeet;
    public static ItemArmorByPart paxiaXHead, paxiaXBody, paxiaXBracers, paxiaXLegs, paxiaXFeet;
    public static ItemArmorByPart omniTAUHead, omniTAUBody, omniTAUBracers, omniTAULegs, omniTAUFeet;
    public static ItemArmorByPart razmusMHead, razmusMBody, razmusMBracers, razmusMLegs, razmusMFeet;
    public static ItemArmorByPart maradeurHead, maradeurBody, maradeurBracers, maradeurLegs, maradeurFeet;

    public static void registerItems() {
        for (Field field : CoreArmorCommon.class.getFields()) {
            if(field.getType().isAssignableFrom(ItemArmorByPart.class)) {
                try {
                    ItemArmorByPart item = (ItemArmorByPart) field.get(null);
                    registerItem(item);
                    registeredArmorList.add(item);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void registerItem(Item item) {
        GameRegistry.registerItem(item, item.getUnlocalizedName());
    }
}
