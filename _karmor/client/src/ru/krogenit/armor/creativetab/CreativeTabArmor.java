package ru.krogenit.armor.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import ru.xlv.armor.CoreArmorCommon;

public class CreativeTabArmor extends CreativeTabs {

    public CreativeTabArmor() {
        super("Armor");
    }

    @Override
    public Item getTabIconItem() {
        return CoreArmorCommon.steelSunBody;
    }
}
