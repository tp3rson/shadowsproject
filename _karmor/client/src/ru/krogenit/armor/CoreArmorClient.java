package ru.krogenit.armor;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import ru.krogenit.armor.client.*;
import ru.krogenit.armor.creativetab.CreativeTabArmor;
import ru.krogenit.armor.event.EventRenderFirstPersonHand;
import ru.krogenit.armor.item.ItemArmorByPart;
import ru.krogenit.armor.item.ItemArmorByPartClient;
import ru.krogenit.armor.network.PacketArmorSync;
import ru.krogenit.event.EventNPCArmor;
import ru.xlv.armor.CoreArmorCommon;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;

import java.util.HashMap;

import static ru.xlv.armor.CoreArmorCommon.MODID;

@Mod(modid = CoreArmorCommon.MODID, name = "Armor By Parts", version = "1.0.0")
public class CoreArmorClient {

	@Instance(CoreArmorCommon.MODID)
	public static CoreArmorClient instance;

	public static final CreativeTabArmor armorTab = new CreativeTabArmor();
    private static final HashMap<String, IArmorRenderer> armors = new HashMap<>();
	
	@EventHandler
	@SuppressWarnings("unused")
	public void event(FMLInitializationEvent e) {
		CoreArmorCommon.murusKLHead = new ItemArmorByPartClient("murusKLHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.MURUS_KL);
		CoreArmorCommon.murusKLBody = new ItemArmorByPartClient("murusKLBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.MURUS_KL);
		CoreArmorCommon.murusKLBracers = new ItemArmorByPartClient("murusKLBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.MURUS_KL);
		CoreArmorCommon.murusKLLegs = new ItemArmorByPartClient("murusKLLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.MURUS_KL);
		CoreArmorCommon.murusKLFeet = new ItemArmorByPartClient("murusKLFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.MURUS_KL);

		CoreArmorCommon.censorHead = new ItemArmorByPartClient("censorHead", MatrixInventory.SlotType.HEAD);
		CoreArmorCommon.censorBody = new ItemArmorByPartClient("censorBody", MatrixInventory.SlotType.BODY);
		CoreArmorCommon.censorBracers = new ItemArmorByPartClient("censorBracers", MatrixInventory.SlotType.BRACERS);
		CoreArmorCommon.censorLegs = new ItemArmorByPartClient("censorLegs", MatrixInventory.SlotType.LEGS);
		CoreArmorCommon.censorFeet = new ItemArmorByPartClient("censorFeet", MatrixInventory.SlotType.FEET);

		CoreArmorCommon.bku0992Head = new ItemArmorByPartClient("bku0992Head", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.BKU_0992);
		CoreArmorCommon.bku0992Body = new ItemArmorByPartClient("bku0992Body", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.BKU_0992);
		CoreArmorCommon.bku0992Bracers = new ItemArmorByPartClient("bku0992Bracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.BKU_0992);
		CoreArmorCommon.bku0992Legs = new ItemArmorByPartClient("bku0992Legs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.BKU_0992);
		CoreArmorCommon.bku0992Feet = new ItemArmorByPartClient("bku0992Feet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.BKU_0992);

		CoreArmorCommon.ptnHead = new ItemArmorByPartClient("ptnHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.PTN);
		CoreArmorCommon.ptnBody = new ItemArmorByPartClient("ptnBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.PTN);
		CoreArmorCommon.ptnBracers = new ItemArmorByPartClient("ptnBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.PTN);
		CoreArmorCommon.ptnLegs = new ItemArmorByPartClient("ptnLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.PTN);
		CoreArmorCommon.ptnFeet = new ItemArmorByPartClient("ptnFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.PTN);

		CoreArmorCommon.enigmaBHead = new ItemArmorByPartClient("enigmaBHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.ENIGMA_B);
		CoreArmorCommon.enigmaBBody = new ItemArmorByPartClient("enigmaBBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.ENIGMA_B);
		CoreArmorCommon.enigmaBBracers = new ItemArmorByPartClient("enigmaBBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.ENIGMA_B);
		CoreArmorCommon.enigmaBLegs = new ItemArmorByPartClient("enigmaBLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.ENIGMA_B);
		CoreArmorCommon.enigmaBFeet = new ItemArmorByPartClient("enigmaBFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.ENIGMA_B);

		CoreArmorCommon.azurESHead = new ItemArmorByPartClient("azurESHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.AZUR_ES);
		CoreArmorCommon.azurESBody = new ItemArmorByPartClient("azurESBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.AZUR_ES);
		CoreArmorCommon.azurESBracers = new ItemArmorByPartClient("azurESBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.AZUR_ES);
		CoreArmorCommon.azurESLegs = new ItemArmorByPartClient("azurESLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.AZUR_ES);
		CoreArmorCommon.azurESFeet = new ItemArmorByPartClient("azurESFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.AZUR_ES);

		CoreArmorCommon.specriotHead = new ItemArmorByPartClient("specriotHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.SPEC_RIOT);
		CoreArmorCommon.specriotBody = new ItemArmorByPartClient("specriotBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.SPEC_RIOT);
		CoreArmorCommon.specriotBracers = new ItemArmorByPartClient("specriotBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.SPEC_RIOT);
		CoreArmorCommon.specriotLegs = new ItemArmorByPartClient("specriotLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.SPEC_RIOT);
		CoreArmorCommon.specriotFeet = new ItemArmorByPartClient("specriotFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.SPEC_RIOT);

		CoreArmorCommon.conditor07Head = new ItemArmorByPartClient("conditor07Head", MatrixInventory.SlotType.HEAD);
		CoreArmorCommon.conditor07Body = new ItemArmorByPartClient("conditor07Body", MatrixInventory.SlotType.BODY);
		CoreArmorCommon.conditor07Bracers = new ItemArmorByPartClient("conditor07Bracers", MatrixInventory.SlotType.BRACERS);
		CoreArmorCommon.conditor07Legs = new ItemArmorByPartClient("conditor07Legs", MatrixInventory.SlotType.LEGS);
		CoreArmorCommon.conditor07Feet = new ItemArmorByPartClient("conditor07Feet", MatrixInventory.SlotType.FEET);

		CoreArmorCommon.nihilHead = new ItemArmorByPartClient("nihilHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.NIHIL);
		CoreArmorCommon.nihilBody = new ItemArmorByPartClient("nihilBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.NIHIL);
		CoreArmorCommon.nihilBracers = new ItemArmorByPartClient("nihilBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.NIHIL);
		CoreArmorCommon.nihilLegs = new ItemArmorByPartClient("nihilLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.NIHIL);
		CoreArmorCommon.nihilFeet = new ItemArmorByPartClient("nihilFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.NIHIL);

		CoreArmorCommon.hex6Head = new ItemArmorByPartClient("hex6Head", MatrixInventory.SlotType.HEAD);
		CoreArmorCommon.hex6Body = new ItemArmorByPartClient("hex6Body", MatrixInventory.SlotType.BODY);
		CoreArmorCommon.hex6Bracers = new ItemArmorByPartClient("hex6Bracers", MatrixInventory.SlotType.BRACERS);
		CoreArmorCommon.hex6Legs = new ItemArmorByPartClient("hex6Legs", MatrixInventory.SlotType.LEGS);
		CoreArmorCommon.hex6Feet = new ItemArmorByPartClient("hex6Feet", MatrixInventory.SlotType.FEET);

		CoreArmorCommon.steelSunHead = new ItemArmorByPartClient("steelSunHead", MatrixInventory.SlotType.HEAD);
		CoreArmorCommon.steelSunBody = new ItemArmorByPartClient("steelSunBody", MatrixInventory.SlotType.BODY);
		CoreArmorCommon.steelSunBracers = new ItemArmorByPartClient("steelSunBracers", MatrixInventory.SlotType.BRACERS);
		CoreArmorCommon.steelSunLegs = new ItemArmorByPartClient("steelSunLegs", MatrixInventory.SlotType.LEGS);
		CoreArmorCommon.steelSunFeet = new ItemArmorByPartClient("steelSunFeet", MatrixInventory.SlotType.FEET);

		CoreArmorCommon.paxiaXHead = new ItemArmorByPartClient("paxiaXHead", MatrixInventory.SlotType.HEAD);
		CoreArmorCommon.paxiaXBody = new ItemArmorByPartClient("paxiaXBody", MatrixInventory.SlotType.BODY);
		CoreArmorCommon.paxiaXBracers = new ItemArmorByPartClient("paxiaXBracers", MatrixInventory.SlotType.BRACERS);
		CoreArmorCommon.paxiaXLegs = new ItemArmorByPartClient("paxiaXLegs", MatrixInventory.SlotType.LEGS);
		CoreArmorCommon.paxiaXFeet = new ItemArmorByPartClient("paxiaXFeet", MatrixInventory.SlotType.FEET);

		CoreArmorCommon.omniTAUHead = new ItemArmorByPartClient("omniTAUHead", MatrixInventory.SlotType.HEAD);
		CoreArmorCommon.omniTAUBody = new ItemArmorByPartClient("omniTAUBody", MatrixInventory.SlotType.BODY);
		CoreArmorCommon.omniTAUBracers = new ItemArmorByPartClient("omniTAUBracers", MatrixInventory.SlotType.BRACERS);
		CoreArmorCommon.omniTAULegs = new ItemArmorByPartClient("omniTAULegs", MatrixInventory.SlotType.LEGS);
		CoreArmorCommon.omniTAUFeet = new ItemArmorByPartClient("omniTAUFeet", MatrixInventory.SlotType.FEET);

		CoreArmorCommon.razmusMHead = new ItemArmorByPartClient("razmusMHead", MatrixInventory.SlotType.HEAD);
		CoreArmorCommon.razmusMBody = new ItemArmorByPartClient("razmusMBody", MatrixInventory.SlotType.BODY);
		CoreArmorCommon.razmusMBracers = new ItemArmorByPartClient("razmusMBracers", MatrixInventory.SlotType.BRACERS);
		CoreArmorCommon.razmusMLegs = new ItemArmorByPartClient("razmusMLegs", MatrixInventory.SlotType.LEGS);
		CoreArmorCommon.razmusMFeet = new ItemArmorByPartClient("razmusMFeet", MatrixInventory.SlotType.FEET);

		CoreArmorCommon.maradeurHead = new ItemArmorByPartClient("maradeurHead", MatrixInventory.SlotType.HEAD);
		CoreArmorCommon.maradeurBody = new ItemArmorByPartClient("maradeurBody", MatrixInventory.SlotType.BODY);
		CoreArmorCommon.maradeurBracers = new ItemArmorByPartClient("maradeurBracers", MatrixInventory.SlotType.BRACERS);
		CoreArmorCommon.maradeurLegs = new ItemArmorByPartClient("maradeurLegs", MatrixInventory.SlotType.LEGS);
		CoreArmorCommon.maradeurFeet = new ItemArmorByPartClient("maradeurFeet", MatrixInventory.SlotType.FEET);

		CoreArmorCommon.registerItems();

		registerItemArmorWithModel(CoreArmorCommon.murusKLHead, ArmorRenderRegistry.armorEngineer);
		registerItemArmorWithModel(CoreArmorCommon.murusKLBody, ArmorRenderRegistry.armorEngineer);
		registerItemArmorWithModel(CoreArmorCommon.murusKLBracers, ArmorRenderRegistry.armorEngineer);
		registerItemArmorWithModel(CoreArmorCommon.murusKLLegs, ArmorRenderRegistry.armorEngineer);
		registerItemArmorWithModel(CoreArmorCommon.murusKLFeet, ArmorRenderRegistry.armorEngineer);

		registerItemArmorWithModel(CoreArmorCommon.censorHead, ArmorRenderRegistry.armorLight);
		registerItemArmorWithModel(CoreArmorCommon.censorBody, ArmorRenderRegistry.armorLight);
		registerItemArmorWithModel(CoreArmorCommon.censorBracers, ArmorRenderRegistry.armorLight);
		registerItemArmorWithModel(CoreArmorCommon.censorLegs, ArmorRenderRegistry.armorLight);
		registerItemArmorWithModel(CoreArmorCommon.censorFeet, ArmorRenderRegistry.armorLight);

		registerItemArmorWithModel(CoreArmorCommon.bku0992Head, ArmorRenderRegistry.armorMilitary);
		registerItemArmorWithModel(CoreArmorCommon.bku0992Body, ArmorRenderRegistry.armorMilitary);
		registerItemArmorWithModel(CoreArmorCommon.bku0992Bracers, ArmorRenderRegistry.armorMilitary);
		registerItemArmorWithModel(CoreArmorCommon.bku0992Legs, ArmorRenderRegistry.armorMilitary);
		registerItemArmorWithModel(CoreArmorCommon.bku0992Feet, ArmorRenderRegistry.armorMilitary);

		registerItemArmorWithModel(CoreArmorCommon.ptnHead, ArmorRenderRegistry.armorPolice);
		registerItemArmorWithModel(CoreArmorCommon.ptnBody, ArmorRenderRegistry.armorPolice);
		registerItemArmorWithModel(CoreArmorCommon.ptnBracers, ArmorRenderRegistry.armorPolice);
		registerItemArmorWithModel(CoreArmorCommon.ptnLegs, ArmorRenderRegistry.armorPolice);
		registerItemArmorWithModel(CoreArmorCommon.ptnFeet, ArmorRenderRegistry.armorPolice);

		registerItemArmorWithModel(CoreArmorCommon.enigmaBHead, ArmorRenderRegistry.armorPredator);
		registerItemArmorWithModel(CoreArmorCommon.enigmaBBody, ArmorRenderRegistry.armorPredator);
		registerItemArmorWithModel(CoreArmorCommon.enigmaBBracers, ArmorRenderRegistry.armorPredator);
		registerItemArmorWithModel(CoreArmorCommon.enigmaBLegs, ArmorRenderRegistry.armorPredator);
		registerItemArmorWithModel(CoreArmorCommon.enigmaBFeet, ArmorRenderRegistry.armorPredator);

		registerItemArmorWithModel(CoreArmorCommon.azurESHead, ArmorRenderRegistry.armorScientific);
		registerItemArmorWithModel(CoreArmorCommon.azurESBody, ArmorRenderRegistry.armorScientific);
		registerItemArmorWithModel(CoreArmorCommon.azurESBracers, ArmorRenderRegistry.armorScientific);
		registerItemArmorWithModel(CoreArmorCommon.azurESLegs, ArmorRenderRegistry.armorScientific);
		registerItemArmorWithModel(CoreArmorCommon.azurESFeet, ArmorRenderRegistry.armorScientific);

		registerItemArmorWithModel(CoreArmorCommon.specriotHead, ArmorRenderRegistry.armorSpider);
		registerItemArmorWithModel(CoreArmorCommon.specriotBody, ArmorRenderRegistry.armorSpider);
		registerItemArmorWithModel(CoreArmorCommon.specriotBracers, ArmorRenderRegistry.armorSpider);
		registerItemArmorWithModel(CoreArmorCommon.specriotLegs, ArmorRenderRegistry.armorSpider);
		registerItemArmorWithModel(CoreArmorCommon.specriotFeet, ArmorRenderRegistry.armorSpider);

		registerItemArmorWithModel(CoreArmorCommon.conditor07Head, ArmorRenderRegistry.armorStealth);
		registerItemArmorWithModel(CoreArmorCommon.conditor07Body, ArmorRenderRegistry.armorStealth);
		registerItemArmorWithModel(CoreArmorCommon.conditor07Bracers, ArmorRenderRegistry.armorStealth);
		registerItemArmorWithModel(CoreArmorCommon.conditor07Legs, ArmorRenderRegistry.armorStealth);
		registerItemArmorWithModel(CoreArmorCommon.conditor07Feet, ArmorRenderRegistry.armorStealth);

		registerItemArmorWithModel(CoreArmorCommon.nihilHead, ArmorRenderRegistry.armorVlad);
		registerItemArmorWithModel(CoreArmorCommon.nihilBody, ArmorRenderRegistry.armorVlad);
		registerItemArmorWithModel(CoreArmorCommon.nihilBracers, ArmorRenderRegistry.armorVlad);
		registerItemArmorWithModel(CoreArmorCommon.nihilLegs, ArmorRenderRegistry.armorVlad);
		registerItemArmorWithModel(CoreArmorCommon.nihilFeet, ArmorRenderRegistry.armorVlad);

		registerItemArmorWithModel(CoreArmorCommon.hex6Head, ArmorRenderRegistry.armorJager);
		registerItemArmorWithModel(CoreArmorCommon.hex6Body, ArmorRenderRegistry.armorJager);
		registerItemArmorWithModel(CoreArmorCommon.hex6Bracers, ArmorRenderRegistry.armorJager);
		registerItemArmorWithModel(CoreArmorCommon.hex6Legs, ArmorRenderRegistry.armorJager);
		registerItemArmorWithModel(CoreArmorCommon.hex6Feet, ArmorRenderRegistry.armorJager);

		registerItemArmorWithModel(CoreArmorCommon.steelSunHead, ArmorRenderRegistry.armorMedic);
		registerItemArmorWithModel(CoreArmorCommon.steelSunBody, ArmorRenderRegistry.armorMedic);
		registerItemArmorWithModel(CoreArmorCommon.steelSunBracers, ArmorRenderRegistry.armorMedic);
		registerItemArmorWithModel(CoreArmorCommon.steelSunLegs, ArmorRenderRegistry.armorMedic);
		registerItemArmorWithModel(CoreArmorCommon.steelSunFeet, ArmorRenderRegistry.armorMedic);

		registerItemArmorWithModel(CoreArmorCommon.paxiaXHead, ArmorRenderRegistry.armorColonist);
		registerItemArmorWithModel(CoreArmorCommon.paxiaXBody, ArmorRenderRegistry.armorColonist);
		registerItemArmorWithModel(CoreArmorCommon.paxiaXBracers, ArmorRenderRegistry.armorColonist);
		registerItemArmorWithModel(CoreArmorCommon.paxiaXLegs, ArmorRenderRegistry.armorColonist);
		registerItemArmorWithModel(CoreArmorCommon.paxiaXFeet, ArmorRenderRegistry.armorColonist);

		registerItemArmorWithModel(CoreArmorCommon.omniTAUHead, ArmorRenderRegistry.armorEye);
		registerItemArmorWithModel(CoreArmorCommon.omniTAUBody, ArmorRenderRegistry.armorEye);
		registerItemArmorWithModel(CoreArmorCommon.omniTAUBracers, ArmorRenderRegistry.armorEye);
		registerItemArmorWithModel(CoreArmorCommon.omniTAULegs, ArmorRenderRegistry.armorEye);
		registerItemArmorWithModel(CoreArmorCommon.omniTAUFeet, ArmorRenderRegistry.armorEye);

		registerItemArmorWithModel(CoreArmorCommon.razmusMHead, ArmorRenderRegistry.armorHeavy);
		registerItemArmorWithModel(CoreArmorCommon.razmusMBody, ArmorRenderRegistry.armorHeavy);
		registerItemArmorWithModel(CoreArmorCommon.razmusMBracers, ArmorRenderRegistry.armorHeavy);
		registerItemArmorWithModel(CoreArmorCommon.razmusMLegs, ArmorRenderRegistry.armorHeavy);
		registerItemArmorWithModel(CoreArmorCommon.razmusMFeet, ArmorRenderRegistry.armorHeavy);

		registerItemArmorWithModel(CoreArmorCommon.maradeurHead, ArmorRenderRegistry.armorWarrior);
		registerItemArmorWithModel(CoreArmorCommon.maradeurBody, ArmorRenderRegistry.armorWarrior);
		registerItemArmorWithModel(CoreArmorCommon.maradeurBracers, ArmorRenderRegistry.armorWarrior);
		registerItemArmorWithModel(CoreArmorCommon.maradeurLegs, ArmorRenderRegistry.armorWarrior);
		registerItemArmorWithModel(CoreArmorCommon.maradeurFeet, ArmorRenderRegistry.armorWarrior);

		XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID, new PacketArmorSync());
		ArmorRenderPlayerEvent armorRenderPlayerEvent = new ArmorRenderPlayerEvent();
		XlvsCoreCommon.EVENT_BUS.register(armorRenderPlayerEvent);
		MinecraftForge.EVENT_BUS.register(armorRenderPlayerEvent);
		MinecraftForge.EVENT_BUS.register(new EventRenderFirstPersonHand());
		XlvsCoreCommon.EVENT_BUS.register(new EventNPCArmor());
		XlvsCoreCommon.EVENT_BUS.register(new EventRenderNPCArmor());
	}

	private void registerItemArmorWithModel(ItemArmorByPart ItemArmorByPart, IArmorRenderer armorRenderer) {
		registerArmorModel(ItemArmorByPart.getUnlocalizedName(), armorRenderer);
		MinecraftForgeClient.registerItemRenderer(ItemArmorByPart, new RenderItemArmor(armorRenderer));
	}

    public static IArmorRenderer getModelForArmor(String name) {
    	return armors.get(name); 	
    }
    
    public static void registerArmorModel(String name, IArmorRenderer renderer) {
    	armors.put(name, renderer);
    }

}
