package ru.krogenit.armor.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import ru.krogenit.client.renderer.IReloadableRenderer;
import ru.krogenit.client.renderer.ReloadableRendererManager;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.xlv.armor.CoreArmorCommon;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.resource.ResourceLocationStateful;

import static org.lwjgl.opengl.GL11.*;

public class ArmorRenderer implements IArmorRenderer, IReloadableRenderer {

	private static final Minecraft mc = Minecraft.getMinecraft();
	private final Model model;
	private TextureDDS resourceLocationStatefulDiffuse;
	private TextureDDS resourceLocationStatefulEmission;
	private TextureDDS resourceLocationStatefulNormal;
	private TextureDDS resourceLocationStatefulSpecular;
	private TextureDDS resourceLocationStatefulGloss;

	private final boolean hasEmission;
	private final float emissionPower;
	private final String name;

	public ArmorRenderer(String name, boolean hasEmission, float emissionPower) {
		this.model = new Model(new ResourceLocationStateful(new ResourceLocation(CoreArmorCommon.MODID, "models/Armor-" + name + ".obj")).getResourceLocation());
		this.hasEmission = hasEmission;
		this.emissionPower = emissionPower;
		this.name = name.toLowerCase();
		reload();
		ReloadableRendererManager.registerRenderer(this);
	}

	@Override
	public void reload() {
		this.resourceLocationStatefulDiffuse = new TextureDDS(new ResourceLocation("advancedblocks", "textures/armor/" + name + "_d."+mc.gameSettings.textureSize+".dds"));
		this.resourceLocationStatefulNormal = new TextureDDS(new ResourceLocation("advancedblocks","textures/armor/"+ name + "_n."+mc.gameSettings.textureSize+".dds"));
		this.resourceLocationStatefulSpecular = new TextureDDS(new ResourceLocation("advancedblocks","textures/armor/" + name + "_s."+mc.gameSettings.textureSize+".dds"));
		if(hasEmission) this.resourceLocationStatefulEmission = new TextureDDS(new ResourceLocation("advancedblocks","textures/armor/" + name + "_e."+mc.gameSettings.textureSize+".dds"));
		this.resourceLocationStatefulGloss = new TextureDDS(new ResourceLocation("advancedblocks","textures/armor/" + name + "_g."+mc.gameSettings.textureSize+".dds"));
	}

	private void setupShaderStates(IPBR shader) {
		TextureLoaderDDS.bindTexture(resourceLocationStatefulDiffuse);
		TextureLoaderDDS.bindNormalMap(resourceLocationStatefulNormal, shader);

		if(hasEmission) {
			TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulEmission, shader, emissionPower * 2f);
		}

		TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulSpecular, shader);
		TextureLoaderDDS.bindGlossMap(resourceLocationStatefulGloss, shader);
	}

	private void endShaderStates(IPBR shader) {
		if(hasEmission) shader.setEmissionMapping(false);
		shader.setGlossMapping(false);
		shader.setNormalMapping(false);
		shader.setSpecularMapping(false);
		KrogenitShaders.finishCurrentShader();
		TextureLoaderDDS.unbind();
	}

	@Override
	public void render(ModelBiped modelBipedMain, MatrixInventory.SlotType type, EntityLivingBase p, ItemStack itemStack) {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		switch (type) {
			case HEAD:
				glPushMatrix();
				modelBipedMain.bipedHead.postRender(0.0625f);
				glScalef(1.1f, 1.1f, 1.1f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(0f, -1.5f, 0f);
				model.renderPart("Helmet", shader);
				glPopMatrix();
				break;
			case BODY:
				glPushMatrix();
				modelBipedMain.bipedBody.postRender(0.0625f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(0f, -1.5f, 0f);
				model.renderPart("Torso", shader);
				glPopMatrix();

				glPushMatrix();
				modelBipedMain.bipedLeftArm.postRender(0.0625f);
				glScalef(1.075f, 1.01f, 1.075f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(-0.31f, -1.375f, 0f);
				model.renderPart("LArm", shader);
				glPopMatrix();

				glPushMatrix();
				modelBipedMain.bipedRightArm.postRender(0.0625f);
				glScalef(1.075f, 1.01f, 1.075f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(0.31f, -1.375f, 0f);
				model.renderPart("RArm", shader);
				glPopMatrix();

				break;
			case BRACERS:
				glPushMatrix();
				modelBipedMain.bipedLeftArm.postRender(0.0625f);
				glScalef(1.075f, 1.01f, 1.075f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(-0.31f, -1.375f, 0f);
				model.renderPart("LGlove", shader);
				glPopMatrix();

				glPushMatrix();
				modelBipedMain.bipedRightArm.postRender(0.0625f);
				glScalef(1.075f, 1.01f, 1.075f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(0.31f, -1.375f, 0f);
				model.renderPart("RGlove", shader);
				glPopMatrix();
				break;
			case LEGS:
				glPushMatrix();
				modelBipedMain.bipedBody.postRender(0.0625f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(0f, -1.5f, 0f);
				model.renderPart("Belt", shader);
				glPopMatrix();

				glPushMatrix();
				modelBipedMain.bipedLeftLeg.postRender(0.0625f);
				glScalef(1.075f, 1.0f, 1.0f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(-0.125f, -0.75f, 0f);
				model.renderPart("LLeg", shader);
				glPopMatrix();

				glPushMatrix();
				modelBipedMain.bipedRightLeg.postRender(0.0625f);
				glScalef(1.075f, 1, 1.0f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(0.125f, -0.75f, 0f);
				model.renderPart("RLeg", shader);
				glPopMatrix();
				break;
			case FEET:
				glPushMatrix();
				modelBipedMain.bipedLeftLeg.postRender(0.0625f);
				glScalef(1.075f, 1, 1.075f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(-0.125f, -0.75f, 0f);
				model.renderPart("LBoot", shader);
				glPopMatrix();

				glPushMatrix();
				modelBipedMain.bipedRightLeg.postRender(0.0625f);
				glScalef(1.075f, 1, 1.075f);
				glRotatef(180f, 1, 0, 0);
				glTranslatef(0.125f, -0.75f, 0f);
				model.renderPart("RBoot", shader);
				glPopMatrix();
				break;
		}
		endShaderStates(shader);
	}

	@Override
	public void renderBodyInv(ItemRenderType type) {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		glTranslatef(0.05f, -0.15f, -0.0f);
		model.renderPart("Torso", shader);
		model.renderPart("LArm", shader);
		model.renderPart("RArm", shader);

		endShaderStates(shader);
	}

	@Override
	public void renderLegsInv(ItemRenderType type) {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		float scale = 1.45f;
		glScalef(scale, scale, scale);
		glTranslatef(0.025f, 0.14f, -0.0f);
		model.renderPart("LLeg", shader);
		model.renderPart("RLeg", shader);

		endShaderStates(shader);
	}

	@Override
	public void renderBootsInv(ItemRenderType type) {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);
		float scale = 1.5f;
		glScalef(scale, scale, scale);
		glTranslatef(0.025f, 0.5f, -0.0f);
		model.renderPart("LBoot", shader);
		model.renderPart("RBoot", shader);

		endShaderStates(shader);
	}

	@Override
	public void renderHeadInv(ItemRenderType type) {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		float scale = 1.4f;
		glScalef(scale, scale, scale);
		glTranslatef(0.025f, -1.05f, -0.0f);
		model.renderPart("Helmet", shader);

		endShaderStates(shader);
	}

	@Override
	public void renderBracersInv(ItemRenderType type) {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		glTranslatef(0.025f, 0.05f, 0.0f);
		model.renderPart("LGlove", shader);
		model.renderPart("RGlove", shader);

		endShaderStates(shader);
	}

	@Override
	public void renderLeftBodyArmFirstPerson() {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		glPushMatrix();
		glRotatef(180f, 1, 0, 0);
		glTranslatef(-0.28f, -1.13f, -0.125f);
		glScalef(1.1f, 1, 1.1f);
		model.renderPart("LArm", shader);
		glPopMatrix();

		endShaderStates(shader);
	}

	@Override
	public void renderRightBodyArmFirstPerson() {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		glPushMatrix();
		glRotatef(180f, 1, 0, 0);
		glTranslatef(0.53f, -1.13f, -0.125f);
		glScalef(1.1f, 1, 1.1f);
		model.renderPart("RArm", shader);
		glPopMatrix();

		endShaderStates(shader);
	}

	@Override
	public void renderLeftBracerFirstPerson() {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		glPushMatrix();
		glRotatef(180f, 1, 0, 0);
		glTranslatef(-0.28f, -1.13f, -0.125f);
		glScalef(1.1f, 1, 1.1f);
		model.renderPart("LGlove", shader);
		glPopMatrix();

		endShaderStates(shader);
	}

	@Override
	public void renderRightBracerFirstPerson() {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		setupShaderStates(shader);

		glPushMatrix();
		glRotatef(180f, 1, 0, 0);
		glTranslatef(0.53f, -1.13f, -0.125f);
		glScalef(1.1f, 1, 1.1f);
		model.renderPart("RGlove", shader);
		glPopMatrix();

		endShaderStates(shader);
	}

	@Override
	public void renderBody() {

	}

	@Override
	public void renderHead() {

	}

	@Override
	public void renderLeftHand() {

	}

	@Override
	public void renderRightHand() {

	}

	@Override
	public void renderLeftLeg() {

	}

	@Override
	public void renderRightLeg() {

	}

	@Override
	public void renderLeftBoots() {

	}

	@Override
	public void renderRightBoots() {

	}

	@Override
	public void renderLeftGlove() {

	}

	@Override
	public void renderRightGlove() {

	}

	@Override
	public void renderLegs() {

	}

	@Override
	public void renderBoots() {

	}

}
