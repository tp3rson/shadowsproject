package ru.krogenit.armor.network;

import lombok.NoArgsConstructor;
import ru.xlv.armor.util.ArmorUtil;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketArmorSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        ArmorUtil.readArmor(bbis);
    }
}
