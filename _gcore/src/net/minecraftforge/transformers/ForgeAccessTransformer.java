package net.minecraftforge.transformers;

import cpw.mods.fml.common.asm.transformers.AccessTransformer;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.IOException;

@IgnoreObf
public class ForgeAccessTransformer extends AccessTransformer
{
    public ForgeAccessTransformer() throws IOException
    {
        super("forge_at.cfg");
    }
}