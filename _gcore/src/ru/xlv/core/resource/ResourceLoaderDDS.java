package ru.xlv.core.resource;

import net.minecraft.client.renderer.texture.SimpleTexture;
import ru.krogenit.dds_loader.DDSFile;
import ru.krogenit.dds_loader.TextureLoaderDDS;

public class ResourceLoaderDDS extends ResourceLoader<DDSFile> {

    public ResourceLoaderDDS() {
        super("dds");
    }

    @Override
    public DDSFile loadAsync(ResourceLocationStateful resourceLocationStateful) {
        return TextureLoaderDDS.loadDDSFile(resourceLocationStateful.getResourceLocation());
    }

    @Override
    public void loadSync0(ResourceLocationStateful resourceLocationStateful, DDSFile ddsFile) {
        TextureLoaderDDS.loadTexture(ddsFile, resourceLocationStateful, new SimpleTexture(resourceLocationStateful.getResourceLocation()));
    }

    @Override
    public boolean isLoaded(ResourceLocationStateful resourceLocationStateful) {
        return TextureLoaderDDS.getTexture(resourceLocationStateful.getResourceLocation()) != null;
    }

    @Override
    public void deleteResource(ResourceLocationStateful resourceLocationStateful) {
        TextureLoaderDDS.deleteTexture(resourceLocationStateful.getResourceLocation());
    }

    @Override
    public void deleteAll() {
        TextureLoaderDDS.deleteAllTextures();
    }
}
