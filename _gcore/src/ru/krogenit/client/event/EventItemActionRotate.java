package ru.krogenit.client.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;

@AllArgsConstructor
@Getter
public class EventItemActionRotate extends Event {

    private final ItemStack itemStack;
    private final EntityPlayer entityPlayer;
    private final EnumAction action;

    @Override
    public boolean isCancelable() {
        return true;
    }
}
