package ru.krogenit.client.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.item.ItemStack;

@AllArgsConstructor
@Getter
public class EventSwingAnimationFirstPerson extends Event {
    private final ItemStack itemStack;
    private final float tickTime;

    @Override
    public boolean isCancelable() {
        return true;
    }
}
