package ru.krogenit.client.gui.api;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.utils.AnimationHelper;
import ru.krogenit.utils.Utils;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.customfont.FontType;

import java.awt.*;

@Getter
@Setter
public class GuiButtonAdvanced extends GuiButton {

    protected static final Minecraft mc = Minecraft.getMinecraft();

    protected ResourceLocation texture, textureHover;

    public float xBase, yBase;
    public float widthBase, heightBase;
    protected float textBlending;
    protected float textScale;
    protected boolean hovered;

    public GuiButtonAdvanced(int id, float x, float y, float width, float height, String text) {
        super(id, x, y, width, height, text);
        this.xBase = x;
        this.yBase = y;
        this.widthBase = width;
        this.heightBase = height;
        this.textScale = height / 36f;
    }

    public GuiButtonAdvanced(int id, float x, float y, float width, float height, String text, float textScale) {
        super(id, x, y, width, height, text);
        this.xBase = x;
        this.yBase = y;
        this.widthBase = width;
        this.heightBase = height;
        this.textScale = textScale;
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        this.drawButton(mouseX, mouseY);
    }

    protected void updateAnimation(boolean hovered) {
        if (hovered) {
            textBlending += AnimationHelper.getAnimationSpeed() * 0.1f;
            if (textBlending > 1) textBlending = 1;
        } else {
            textBlending -= AnimationHelper.getAnimationSpeed() * 0.1f;
            if (textBlending < 0) textBlending = 0f;
        }
    }

    public void drawButton(int mouseX, int mouseY) {
        if (enabled && visible) {
            boolean hovered = isHovered(mouseX, mouseY);
            if(hovered && !this.hovered) {
                SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
            }
            this.hovered = hovered;
            updateAnimation(hovered);
            if(texture != null) Utils.bindTexture(texture);
            else GL11.glDisable(GL11.GL_TEXTURE_2D);
            if(isHovered(mouseX, mouseY) && textureHover != null) {
                Utils.bindTexture(textureHover);
            }
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
            if(texture == null) GL11.glEnable(GL11.GL_TEXTURE_2D);
            drawText();
        }
    }

    protected boolean isHovered(int mouseX, int mouseY) {
        return this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
    }

    protected void drawText() {
        int j;
        if(enabled) {
            int r = ((0x959595 & 0xFF0000) >> 16) + (int)((255 - ((0x959595 & 0xFF0000) >> 16)) * textBlending);
            int g = ((0x959595 & 0xFF00) >> 8) + (int)((255 - ((0x959595 & 0xFF00) >> 8)) * textBlending);
            int b = (0x959595 & 0xFF) + (int)((255 - ((0x959595 & 0xFF))) * textBlending);
            j = (int) Long.parseLong(Integer.toHexString(new Color(r,g,b).getRGB()), 16);
        } else {
            j = 0x333333;
        }

        if (packedFGColour != 0) {
            j = packedFGColour;
        }

        float paddingY = textScale * 4f;
        GuiDrawUtils.drawStringNoScale(FontType.FUTURA_PT_DEMI.getFontContainer(), displayString, this.xPosition + this.width / 2.0f
                - FontType.FUTURA_PT_DEMI.getFontContainer().width(displayString) * textScale / 2f, this.yPosition + this.height / 2.0f - paddingY, textScale, j);
    }

    public void addXPosition(float x) {
        this.xPosition = xBase + x;
    }

    public void addYPosition(float y) {
        this.yPosition = yBase + y;
    }

    public void scaleXPosition(float x) {
        this.xPosition = xBase * x;
    }

    public void scaleYPosition(float y) {
        this.yPosition = yBase * y;
    }

    public void setXPosition(float x) {
        this.xBase = this.xPosition = x;
    }

    public void setYPosition(float y) {
        this.yBase = this.yPosition = y;
    }

    public void setHeight(float height) {
        this.heightBase = this.height = height;
    }

    public void setWidth(float width) {
        this.widthBase = this.width = width;
    }

    public void scaleWidth(float x) {
        this.width = widthBase * x;
    }

    public void scaleHeight(float y) {
        this.height = heightBase * y;
    }
}
