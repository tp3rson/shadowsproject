package ru.krogenit.client.gui.api;

public interface IGuiSortable {
    void sort(int id, EnumSortType state);
}
