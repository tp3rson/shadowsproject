package ru.krogenit.client.gui.api;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import ru.xlv.customfont.FontType;

import java.util.List;

public class GuiConfirmPopup {

    private final static Minecraft mc = Minecraft.getMinecraft();

    private final IGuiWithPopup guiWithPopup;
    private final List<GuiButton> buttons;
    private final String header, description;

    public GuiConfirmPopup(IGuiWithPopup guiWithPopup, List<GuiButton> buttons, String header, String description) {
        this.guiWithPopup = guiWithPopup;
        this.buttons = buttons;
        this.header = header;
        this.description = description;
    }

    public void drawPopup(int mouseX, int mouseY) {
        GuiDrawUtils.renderConfirmPopup(960 -317f, 540-220f, 960+317f, 540 + 90f, 40f);
        for(GuiButton button : buttons) {
            button.drawButton(mc, mouseX, mouseY);
        }

        float x = 960;
        float y = 540-164;
        float fs = 1.4f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, header, x, y, fs, 0xffffff);

        float iconWidth = 502;
        float iconHeight = 1;
        y += 26;
        GuiDrawUtils.drawCenterCentered(new ResourceLocation("skilltree", "textures/border_confirm.png"), x , y, iconWidth, iconHeight);
        y += 20;
        fs = 0.9f;
        y += GuiDrawUtils.drawSplittedStringCenter(FontType.HelveticaNeueCyrLight, description, x, y, fs, ScaleGui.get(600), -1, 0xffffff, EnumStringRenderType.CENTERED);
        y += 12;
        GuiDrawUtils.drawCenterCentered(new ResourceLocation("skilltree", "textures/border_confirm.png"), x , y, iconWidth, iconHeight);
    }

    public boolean mouseClicked(int mouseX, int mouseY) {
        for(GuiButton button : buttons) {
            if(button.mousePressed(mc, mouseX, mouseY)) {
                guiWithPopup.popupAction(button);
                return true;
            }
        }

        return false;
    }
}

