package ru.krogenit.client.gui.item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.lwjgl.util.vector.Vector3f;

@Getter
@AllArgsConstructor
public class ItemTag {
    private final String name;
    private final Vector3f color;
}
