package ru.krogenit.client.key;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;

public abstract class AbstractKey {
    private final KeyBinding key;
    protected static final Minecraft mc = Minecraft.getMinecraft();

    public AbstractKey(KeyBinding key) {
        this.key = key;
    }

    public KeyBinding getKeyBinding() {
        return this.key;
    }

    public abstract void keyDown();

    public abstract void keyUp();
}
