package ru.krogenit.client.renderer;

public interface IReloadableRenderer {
    void reload();
}
