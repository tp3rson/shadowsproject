package ru.krogenit.lighting;

import net.minecraft.block.Block;
import org.lwjgl.util.vector.Vector3f;

public class LightAddingForBlock extends Thread {
	private Block b;
	private float x, y, z;
	private PointLight p;
	
	LightAddingForBlock(Block b, float x, float y, float z) {
		this.b = b;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	LightAddingForBlock(PointLight p, float x, float y, float z) {
		this.p = p;
	}
	
	@Override
	public void run() {
		if(p != null) {
			LightManagerClient.createLight(p);
		} else {
			PointLight light = LightManagerClient.blockLightTypes.get(b);
			if (light != null) {
				LightManagerClient.createLight(new PointLight(new Vector3f(x + LightManagerClient.BLOCK_OFFSET, y + LightManagerClient.BLOCK_OFFSET, z + LightManagerClient.BLOCK_OFFSET), light.color, light.power));
			}
		}
	}
}
