package ru.krogenit.lighting;

public class LightValuePoint {
    public int x;
    public int y;
    public int z;
    public int lifetime;

    public LightValuePoint(int var1, int var2, int var3, int lifetime) {
        this.x = var1;
        this.y = var2;
        this.z = var3;
        this.lifetime = lifetime;
    }
}