package ru.xlv.post.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostIO;
import ru.xlv.post.common.PostObject;

import java.io.IOException;

@NoArgsConstructor
public class PacketPostNew implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        PostObject postObject = PostIO.readPostObject(bbis);
        XlvsPostMod.INSTANCE.getPostHandler().onNewPost(postObject);
    }
}
