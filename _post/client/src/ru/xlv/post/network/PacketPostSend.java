package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostAttachmentClientData;
import ru.xlv.post.common.PostIO;
import ru.xlv.post.common.PostObject;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class PacketPostSend implements IPacketCallbackEffective<PacketPostSend.Result> {

    @Getter
    public static class Result {
        private boolean isSuccess;
        private String responseMessage;
        private PostObject postObject;
    }

    private Result result;
    private String recipient, article, text;
    private List<PostAttachmentClientData> attachments = new ArrayList<>();

    public PacketPostSend(String recipient, String article, String text, List<PostAttachmentClientData> postAttachmentSimpleData) {
        this.recipient = recipient;
        this.article = article;
        this.text = text;
        this.attachments.addAll(postAttachmentSimpleData);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        boolean success = bbis.readBoolean();
        result = new Result();
        result.isSuccess = success;
        if(success) {
            PostObject postObject = PostIO.readPostObject(bbis);
            result.postObject = postObject;
            XlvsPostMod.INSTANCE.getPostHandler().getPostObjectList().add(postObject);
        } else {
            result.responseMessage = bbis.readUTF();
        }
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(recipient);
        bbos.writeUTF(article);
        bbos.writeUTF(text);
        bbos.writeInt(attachments.size());
        for (PostAttachmentClientData attachment : attachments) {
            attachment.write(bbos);
        }
    }
}
