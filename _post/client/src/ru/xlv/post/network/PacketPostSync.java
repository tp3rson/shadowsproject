package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.post.common.PostIO;
import ru.xlv.post.common.PostObject;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class PacketPostSync implements IPacketCallbackEffective<PacketPostSync.Result> {

    @Getter
    public static class Result {
        private boolean isSuccess;
        private List<PostObject> postObjectList;
        private String responseMessage;
    }

    private final Result result = new Result();

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        boolean success = bbis.readBoolean();
        result.isSuccess = success;
        if(success) {
            List<PostObject> postObjects = new ArrayList<>();
            int c = bbis.readInt();
            for (int i = 0; i < c; i++) {
                PostObject postObject = PostIO.readPostObject(bbis);
                postObjects.add(postObject);
            }
            result.postObjectList = postObjects;
        } else {
            result.responseMessage = bbis.readUTF();
        }
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {}
}
