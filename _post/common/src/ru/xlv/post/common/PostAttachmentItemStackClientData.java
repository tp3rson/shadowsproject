package ru.xlv.post.common;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.IOException;

@Getter
@AllArgsConstructor
public class PostAttachmentItemStackClientData extends PostAttachmentClientData {

    private int matrixX, matrixY;

    @Override
    public void write(ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(matrixX);
        byteBufOutputStream.writeInt(matrixY);
    }
}
