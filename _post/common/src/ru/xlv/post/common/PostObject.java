package ru.xlv.post.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
public class PostObject {

    private final UUID uuid;
    private final String title;
    private final String text;
    private final String sender;
    private final String recipient;
    private long creationTimeMills;
    private boolean viewed;
    private List<PostAttachment<?>> attachments = new ArrayList<>();

    public PostObject(String title, String text, String sender, String recipient) {
        this(UUID.randomUUID(), title, text, sender, recipient);
    }

    public PostObject(UUID uuid, String title, String text, String sender, String recipient) {
        this.uuid = uuid;
        this.title = title;
        this.text = text;
        this.sender = sender;
        this.recipient = recipient;
    }
}
