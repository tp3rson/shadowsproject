package ru.xlv.post.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.post.XlvsPostMod;

@Getter
@RequiredArgsConstructor
public enum PostTakeAttachmentsResult {

    SUCCESS(XlvsPostMod.INSTANCE.getLocalization().getResponseTakeAttResultSuccessMessage()),
    POST_OBJECT_NOT_FOUND(XlvsPostMod.INSTANCE.getLocalization().getResponseTakeAttResultPostObjectNotFoundMessage()),
    NO_FREE_INV_SPACE(XlvsPostMod.INSTANCE.getLocalization().getResponseTakeAttResultNotFreeInvSpaceMessage()),
    PLAYER_NOT_RECIPIENT(XlvsPostMod.INSTANCE.getLocalization().getResponseTakeAttResultPlayerNotRecipientMessage()),
    UNKNOWN_ERROR(XlvsPostMod.INSTANCE.getLocalization().getResponseTakeAttResultUnknownErrorMessage());

    private final String responseMessage;
}
