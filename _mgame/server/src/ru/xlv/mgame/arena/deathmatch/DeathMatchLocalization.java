package ru.xlv.mgame.arena.deathmatch;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class DeathMatchLocalization extends Localization {

    private final String responseDeathMatchJoinResultSuccessMessage = "";
    private final String responseDeathMatchJoinResultNoAvailableArenaMessage = "";
    private final String responseDeathMatchJoinResultPlayerAlreadyJoinedMessage = "";
    private final String responseDeathMatchJoinResultUnknownMessage = "";

    @Override
    public File getConfigFile() {
        return new File("config/mini_games/death_match/localization.json");
    }
}
