package ru.xlv.mgame.arena.deathmatch;

import lombok.RequiredArgsConstructor;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;
import ru.xlv.mgame.arena.ArenaManager;
import ru.xlv.mgame.arena.PlayerRatingManager;
import ru.xlv.mgame.util.ArenaLocalization;

import java.util.Optional;

@RequiredArgsConstructor
public class ArenaDeathMatchManager extends ArenaManager<ArenaDeathMatch> {

    private final PlayerRatingManager playerRatingManager = new PlayerRatingManager();

    private final DeathMatchConfig config;
    private final ArenaLocalization localization;

    public DeathMatchJoinResult joinArena(String username) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(username);
        if (serverPlayer == null) {
            return DeathMatchJoinResult.UNKNOWN;
        }
        return joinArena(serverPlayer, true);
    }

    private DeathMatchJoinResult joinArena(ServerPlayer serverPlayer, boolean handleGroup) {
        if(handleGroup) {
            GroupServer group = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
            if (group != null) {
                group.getPlayers().forEach(serverPlayer1 -> joinArena(serverPlayer1, false));
            }
        }
        synchronized (getArenaList()) {
            Optional<ArenaDeathMatch> first = getArenaList()
                    .stream()
                    .filter(arenaDeathMatch -> arenaDeathMatch.getPlayerList().contains(serverPlayer))
                    .findFirst();
            if (first.isPresent()) {
                return DeathMatchJoinResult.PLAYER_ALREADY_JOINED;
            }
        }
        return joinAvailableArenaFor(serverPlayer) ? DeathMatchJoinResult.SUCCESS : DeathMatchJoinResult.NO_AVAILABLE_ARENA;
    }

    private boolean joinAvailableArenaFor(ServerPlayer serverPlayer) {
        synchronized (getArenaList()) {
            if (getArenaList().isEmpty()) {
                ArenaDeathMatch arenaDeathMatch = new ArenaDeathMatch(config, localization, playerRatingManager);
                arenaDeathMatch.start();
                getArenaList().add(arenaDeathMatch);
            }
            for (ArenaDeathMatch arenaDeathMatch : getArenaList()) {
                if(arenaDeathMatch.getPlayerList().size() < config.getMaxPlayersPerArena()) {
                    arenaDeathMatch.addPlayer(serverPlayer);
                    return true;
                }
            }
        }
        return false;
    }

    public void leaveArena(String username) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(username);
        if (serverPlayer == null) {
            return;
        }
        synchronized (getArenaList()) {
            getArenaList()
                    .stream()
                    .filter(arenaDeathMatch -> arenaDeathMatch.getPlayerList().contains(serverPlayer))
                    .findFirst()
                    .ifPresent(arenaDeathMatch -> arenaDeathMatch.removePlayer(serverPlayer));
        }
    }
}
