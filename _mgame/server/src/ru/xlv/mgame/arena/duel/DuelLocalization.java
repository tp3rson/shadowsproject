package ru.xlv.mgame.arena.duel;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class DuelLocalization extends Localization {

    private final String responseInviteResultSuccessMessage = "";
    private final String responseInviteResultPlayerNotFoundMessage = "";
    private final String responseInviteResultInviteSpamMessage = "";
    private final String responseInviteResultUnknownMessage = "";

    private final String responseAcceptResultSuccessMessage = "";
    private final String responseAcceptResultNotValidInviteMessage = "";

    private final String newInviteChatMessage = "You have a new invite to duel from {0}!";

    @Override
    public File getConfigFile() {
        return null;
    }
}
