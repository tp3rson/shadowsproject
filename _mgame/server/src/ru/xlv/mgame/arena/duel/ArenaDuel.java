package ru.xlv.mgame.arena.duel;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import lombok.Getter;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.PlayerKilledEvent;
import ru.xlv.core.event.ServerPlayerLogoutEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mgame.arena.Arena;
import ru.xlv.mgame.arena.ArenaState;
import ru.xlv.mochar.util.Utils;

@Getter
public class ArenaDuel extends Arena {

    private final int registrationTimer = 0, preparationTimer = 3, matchProcessTimer = 120, postMatchTimer = 5;

    private DuelConfig.SpawnPositions spawnPositions;

    public void setSpawnPositions(DuelConfig.SpawnPositions spawnPositions) {
        this.spawnPositions = spawnPositions;
    }

    @Override
    public void start() {
        super.start();
        if(getPlayerList().size() != 2) {
            setArenaState(ArenaState.DONE);
            return;
        }
        XlvsCoreCommon.EVENT_BUS.register(this);
        ServerPlayer first = getPlayerList().get(0);
        ServerPlayer second = getPlayerList().get(1);
        savePlayerSourcePosition(first);
        savePlayerSourcePosition(second);
        movePlayer(first, spawnPositions.getFirstPosition());
        movePlayer(second, spawnPositions.getSecondsPosition());
    }

    @Override
    public void stop() {
        super.stop();
        getPlayerList().forEach(this::removePlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @Override
    public void updatePerTick() {
        if(getPlayerList().size() == 1) {
            processWin(getPlayerList().get(0));
            setArenaState(ArenaState.POST_MATCH);
        }
        switch (getArenaState()) {
            case PREPARATION:
                processInitState(() -> sendMessageToAllPlayers("PREPARATION"));
                controlStateTimeout();
                break;
            case MATCH_PROCESS:
                processInitState(() -> sendMessageToAllPlayers("MATCH PROCESS"));
                if (controlStateTimeout()) {
                    processEndWithDraw();
                }
                break;
            case POST_MATCH:
                processInitState(() -> sendMessageToAllPlayers("POST MATCH"));
                controlStateTimeout();
                break;
            case DONE:
                processInitState(() -> sendMessageToAllPlayers("DONE"));
                stop();
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerKilledEvent event) {
        if(getPlayerList().size() == 2) {
            ServerPlayer firstPlayer = getPlayerList().get(0);
            ServerPlayer secondPlayer = getPlayerList().get(1);
            if((event.getKillerServerPlayer() == firstPlayer || event.getKillerServerPlayer() == secondPlayer)
                    && (event.getTargetServerPlayer() == firstPlayer || event.getTargetServerPlayer() == secondPlayer)) {
                processWin(event.getKillerServerPlayer());
                processLose(event.getTargetServerPlayer());
                setArenaState(ArenaState.POST_MATCH);
            }
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(ServerPlayerLogoutEvent event) {
        for (ServerPlayer serverPlayer : getPlayerList()) {
            if (serverPlayer == event.getServerPlayer()) {
                getPlayerList().remove(serverPlayer);
                break;
            }
        }
    }

    public void processWin(ServerPlayer serverPlayer) {
        Utils.sendMessage(serverPlayer, "You won!");
    }

    public void processLose(ServerPlayer serverPlayer) {
        Utils.sendMessage(serverPlayer, "You lost!");
    }

    public void processEndWithDraw() {
        sendMessageToAllPlayers("Timeout! The match ended with a draw.");
    }

    @Override
    public boolean addPlayer(ServerPlayer serverPlayer) {
        if(getPlayerList().size() < 2) {
            return super.addPlayer(serverPlayer);
        }
        return false;
    }
}
