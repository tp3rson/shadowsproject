package ru.xlv.mgame.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mgame.XlvsMGameMod;
import ru.xlv.mgame.arena.duel.DuelInviteResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketArenaDuelInvite implements IPacketCallbackOnServer {

    private DuelInviteResult duelInviteResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String playerName = bbis.readUTF();
        duelInviteResult = XlvsMGameMod.INSTANCE.getArenaDuelHandler().initiateDuel(entityPlayer.getCommandSenderName(), playerName);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(duelInviteResult == DuelInviteResult.SUCCESS);
        bbos.writeUTF(duelInviteResult.getResponseMessage());
    }
}
