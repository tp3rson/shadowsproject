package ru.xlv.mgame.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mgame.XlvsMGameMod;
import ru.xlv.mgame.arena.duel.DuelAcceptResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketArenaDuelAccept implements IPacketCallbackOnServer {

    private DuelAcceptResult duelAcceptResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String playerName = bbis.readUTF();
        duelAcceptResult = XlvsMGameMod.INSTANCE.getArenaDuelHandler().acceptDuel(entityPlayer.getCommandSenderName(), playerName);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(duelAcceptResult == DuelAcceptResult.SUCCESS);
        bbos.writeUTF(duelAcceptResult.getResponseMessage());
    }
}
