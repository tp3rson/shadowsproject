package ru.xlv.mgame;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.mgame.arena.deathmatch.ArenaDeathMatchManager;
import ru.xlv.mgame.arena.deathmatch.DeathMatchConfig;
import ru.xlv.mgame.arena.deathmatch.DeathMatchLocalization;
import ru.xlv.mgame.arena.duel.ArenaDuelManager;
import ru.xlv.mgame.arena.duel.DuelConfig;
import ru.xlv.mgame.arena.duel.DuelLocalization;
import ru.xlv.mgame.arena.teambattle.ArenaTeamBattleManager;
import ru.xlv.mgame.arena.teambattle.TeamBattleConfig;
import ru.xlv.mgame.arena.teambattle.TeamBattleLocalization;
import ru.xlv.mgame.util.ArenaLocalization;

import static ru.xlv.mgame.XlvsMGameMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
@Getter
public class XlvsMGameMod {

    static final String MODID = "xlvsmgame";

    @Mod.Instance(MODID)
    public static XlvsMGameMod INSTANCE;

    private final DuelLocalization duelLocalization = new DuelLocalization();
    private final DeathMatchLocalization deathMatchLocalization = new DeathMatchLocalization();
    private final TeamBattleLocalization teamBattleLocalization = new TeamBattleLocalization();

    private ArenaDuelManager arenaDuelHandler;
    private ArenaDeathMatchManager arenaDeathMatchManager;
    private ArenaTeamBattleManager arenaTeamBattleManager;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        ArenaLocalization arenaLocalization = new ArenaLocalization();
        arenaLocalization.load();
        DuelConfig duelConfig = new DuelConfig();
        duelConfig.load();
        duelLocalization.load();
        arenaDuelHandler = new ArenaDuelManager(duelConfig, duelLocalization);
        DeathMatchConfig deathMatchConfig = new DeathMatchConfig();
        deathMatchConfig.load();
        arenaDeathMatchManager = new ArenaDeathMatchManager(deathMatchConfig, arenaLocalization);
        TeamBattleConfig teamBattleConfig = new TeamBattleConfig();
        teamBattleConfig.load();
        teamBattleLocalization.load();
        arenaTeamBattleManager = new ArenaTeamBattleManager(teamBattleConfig, arenaLocalization, teamBattleLocalization);
    }
}
