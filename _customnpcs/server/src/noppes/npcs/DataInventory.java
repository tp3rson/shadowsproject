package noppes.npcs;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.ForgeHooks;
import noppes.npcs.entity.EntityNPCInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class DataInventory implements IInventory
{
    public HashMap<Integer, ItemStack> items = new HashMap();
    public HashMap<Integer, Integer> dropchance = new HashMap();
    public HashMap<Integer, ItemStack> weapons = new HashMap();
    public HashMap<Integer, ItemStack> armor = new HashMap();
    public int minExp = 0;
    public int maxExp = 0;
    public int lootMode = 0;
    private EntityNPCInterface npc;

    public DataInventory(EntityNPCInterface npc)
    {
        this.npc = npc;
    }

    public NBTTagCompound writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setInteger("MinExp", this.minExp);
        nbttagcompound.setInteger("MaxExp", this.maxExp);
        nbttagcompound.setTag("NpcInv", NBTTags.nbtItemStackList(this.items));
        nbttagcompound.setTag("Armor", NBTTags.nbtItemStackList(this.getArmor()));
        nbttagcompound.setTag("Weapons", NBTTags.nbtItemStackList(this.getWeapons()));
        nbttagcompound.setTag("DropChance", NBTTags.nbtIntegerIntegerMap(this.dropchance));
        nbttagcompound.setInteger("LootMode", this.lootMode);
        return nbttagcompound;
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        this.minExp = nbttagcompound.getInteger("MinExp");
        this.maxExp = nbttagcompound.getInteger("MaxExp");
        this.items = NBTTags.getItemStackList(nbttagcompound.getTagList("NpcInv", 10));
        this.setArmor(NBTTags.getItemStackList(nbttagcompound.getTagList("Armor", 10)));
        this.setWeapons(NBTTags.getItemStackList(nbttagcompound.getTagList("Weapons", 10)));
        this.dropchance = NBTTags.getIntegerIntegerMap(nbttagcompound.getTagList("DropChance", 10));
        this.lootMode = nbttagcompound.getInteger("LootMode");
    }

    public HashMap<Integer, ItemStack> getWeapons()
    {
        return this.weapons;
    }

    public void setWeapons(HashMap<Integer, ItemStack> list)
    {
        this.weapons = list;
    }

    public HashMap<Integer, ItemStack> getArmor()
    {
        return this.armor;
    }

    public void setArmor(HashMap<Integer, ItemStack> list)
    {
        this.armor = list;
    }

    public ItemStack getWeapon()
    {
        return (ItemStack)this.weapons.get(Integer.valueOf(0));
    }

    public void setWeapon(ItemStack item)
    {
        this.weapons.put(Integer.valueOf(0), item);
    }

    public ItemStack getProjectile()
    {
        return (ItemStack)this.weapons.get(Integer.valueOf(1));
    }

    public void setProjectile(ItemStack item)
    {
        this.weapons.put(Integer.valueOf(1), item);
    }

    public ItemStack getOffHand()
    {
        return (ItemStack)this.weapons.get(Integer.valueOf(2));
    }

    public void setOffHand(ItemStack item)
    {
        this.weapons.put(Integer.valueOf(2), item);
    }

    public void dropStuff(Entity entity, DamageSource damagesource)
    {
        ArrayList list = new ArrayList();
        Iterator enchant = this.items.keySet().iterator();
        int var1;

        while (enchant.hasNext())
        {
            var1 = ((Integer)enchant.next()).intValue();
            ItemStack var2 = (ItemStack)this.items.get(Integer.valueOf(var1));

            if (var2 != null)
            {
                int player = 100;

                if (this.dropchance.containsKey(Integer.valueOf(var1)))
                {
                    player = ((Integer)this.dropchance.get(Integer.valueOf(var1))).intValue();
                }

                int stack = this.npc.worldObj.rand.nextInt(100) + player;

                if (stack >= 100)
                {
                    EntityItem i = this.getEntityItem(var2.copy());

                    if (i != null)
                    {
                        list.add(i);
                    }
                }
            }
        }

        int enchant1 = 0;

        if (damagesource.getEntity() instanceof EntityPlayer)
        {
            enchant1 = EnchantmentHelper.getLootingModifier((EntityLivingBase)damagesource.getEntity());
        }

        if (!ForgeHooks.onLivingDrops(this.npc, damagesource, list, enchant1, true, 0))
        {
            Iterator var11 = list.iterator();

            while (var11.hasNext())
            {
                EntityItem var21 = (EntityItem)var11.next();

                if (this.lootMode == 1 && entity instanceof EntityPlayer)
                {
                    EntityPlayer player1 = (EntityPlayer)entity;
                    var21.delayBeforeCanPickup = 2;
                    this.npc.worldObj.spawnEntityInWorld(var21);
                    ItemStack stack1 = var21.getEntityItem();
                    int i1 = stack1.stackSize;

                    if (player1.inventory.addItemStackToInventory(stack1))
                    {
                        this.npc.worldObj.playSoundAtEntity(var21, "random.pop", 0.2F, ((this.npc.getRNG().nextFloat() - this.npc.getRNG().nextFloat()) * 0.7F + 1.0F) * 2.0F);
                        player1.onItemPickup(var21, i1);

                        if (stack1.stackSize <= 0)
                        {
                            var21.setDead();
                        }
                    }
                }
                else
                {
                    this.npc.worldObj.spawnEntityInWorld(var21);
                }
            }
        }

        var1 = this.minExp;

        if (this.maxExp - this.minExp > 0)
        {
            var1 += this.npc.worldObj.rand.nextInt(this.maxExp - this.minExp);
        }

        while (var1 > 0)
        {
            int var22 = EntityXPOrb.getXPSplit(var1);
            var1 -= var22;

            if (this.lootMode == 1 && entity instanceof EntityPlayer)
            {
                this.npc.worldObj.spawnEntityInWorld(new EntityXPOrb(entity.worldObj, entity.posX, entity.posY, entity.posZ, var22));
            }
            else
            {
                this.npc.worldObj.spawnEntityInWorld(new EntityXPOrb(this.npc.worldObj, this.npc.posX, this.npc.posY, this.npc.posZ, var22));
            }
        }
    }

    public EntityItem getEntityItem(ItemStack itemstack)
    {
        if (itemstack == null)
        {
            return null;
        }
        else
        {
            EntityItem entityitem = new EntityItem(this.npc.worldObj, this.npc.posX, this.npc.posY - 0.30000001192092896D + (double)this.npc.getEyeHeight(), this.npc.posZ, itemstack);
            entityitem.delayBeforeCanPickup = 40;
            float f2 = this.npc.getRNG().nextFloat() * 0.5F;
            float f4 = this.npc.getRNG().nextFloat() * (float)Math.PI * 2.0F;
            entityitem.motionX = (double)(-MathHelper.sin(f4) * f2);
            entityitem.motionZ = (double)(MathHelper.cos(f4) * f2);
            entityitem.motionY = 0.20000000298023224D;
            return entityitem;
        }
    }

    public ItemStack armorItemInSlot(int i)
    {
        return this.getArmor().get(i);
    }

    /**
     * Returns the number of slots in the inventory.
     */
    public int getSizeInventory()
    {
        return 15;
    }

    /**
     * Returns the stack in slot i
     */
    public ItemStack getStackInSlot(int i)
    {
        return i < 5 ? this.armorItemInSlot(i) : (i < 8 ? (ItemStack)this.getWeapons().get(Integer.valueOf(i - 5)) : (ItemStack)this.items.get(Integer.valueOf(i - 8)));
    }

    /**
     * Removes from an inventory slot (first arg) up to a specified number (second arg) of items and returns them in a
     * new stack.
     */
    public ItemStack decrStackSize(int par1, int par2)
    {
        byte i = 0;
        HashMap var3;

        if (par1 >= 7)
        {
            var3 = this.items;
            par1 -= 7;
        }
        else if (par1 >= 4)
        {
            var3 = this.getWeapons();
            par1 -= 4;
            i = 1;
        }
        else
        {
            var3 = this.getArmor();
            i = 2;
        }

        ItemStack var4 = null;

        if (var3.get(Integer.valueOf(par1)) != null)
        {
            if (((ItemStack)var3.get(Integer.valueOf(par1))).stackSize <= par2)
            {
                var4 = (ItemStack)var3.get(Integer.valueOf(par1));
                var3.put(Integer.valueOf(par1), (Object)null);
            }
            else
            {
                var4 = ((ItemStack)var3.get(Integer.valueOf(par1))).splitStack(par2);

                if (((ItemStack)var3.get(Integer.valueOf(par1))).stackSize == 0)
                {
                    var3.put(Integer.valueOf(par1), (Object)null);
                }
            }
        }

        if (i == 1)
        {
            this.setWeapons(var3);
        }

        if (i == 2)
        {
            this.setArmor(var3);
        }

        return var4;
    }

    /**
     * When some containers are closed they call this on each slot, then drop whatever it returns as an EntityItem -
     * like when you close a workbench GUI.
     */
    public ItemStack getStackInSlotOnClosing(int par1)
    {
        byte i = 0;
        HashMap var2;

        if (par1 >= 7)
        {
            var2 = this.items;
            par1 -= 7;
        }
        else if (par1 >= 4)
        {
            var2 = this.getWeapons();
            par1 -= 4;
            i = 1;
        }
        else
        {
            var2 = this.getArmor();
            i = 2;
        }

        if (var2.get(Integer.valueOf(par1)) != null)
        {
            ItemStack var3 = (ItemStack)var2.get(Integer.valueOf(par1));
            var2.put(Integer.valueOf(par1), (Object)null);

            if (i == 1)
            {
                this.setWeapons(var2);
            }

            if (i == 2)
            {
                this.setArmor(var2);
            }

            return var3;
        }
        else
        {
            return null;
        }
    }

    /**
     * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
     */
    public void setInventorySlotContents(int par1, ItemStack par2ItemStack)
    {
        byte i = 0;
        HashMap var3;

        if (par1 >= 8)
        {
            var3 = this.items;
            par1 -= 8;
        }
        else if (par1 >= 5)
        {
            var3 = this.getWeapons();
            par1 -= 5;
            i = 1;
        }
        else
        {
            var3 = this.getArmor();
            i = 2;
        }

        var3.put(Integer.valueOf(par1), par2ItemStack);

        if (i == 1)
        {
            this.setWeapons(var3);
        }

        if (i == 2)
        {
            this.setArmor(var3);
        }
    }

    /**
     * Returns the maximum stack size for a inventory slot.
     */
    public int getInventoryStackLimit()
    {
        return 64;
    }

    /**
     * Do not make give this method the name canInteractWith because it clashes with Container
     */
    public boolean isUseableByPlayer(EntityPlayer var1)
    {
        return true;
    }

    /**
     * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
     */
    public boolean isItemValidForSlot(int i, ItemStack itemstack)
    {
        return true;
    }

    /**
     * Returns the name of the inventory
     */
    public String getInventoryName()
    {
        return "NPC Inventory";
    }

    /**
     * Returns if the inventory name is localized
     */
    public boolean hasCustomInventoryName()
    {
        return true;
    }

    /**
     * Called when an the contents of an Inventory change, usually
     */
    public void markDirty() {}

    public void openInventory() {}

    public void closeInventory() {}
}
