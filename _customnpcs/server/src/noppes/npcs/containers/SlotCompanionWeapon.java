package noppes.npcs.containers;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import noppes.npcs.roles.RoleCompanion;

class SlotCompanionWeapon extends Slot
{
    final RoleCompanion role;

    public SlotCompanionWeapon(RoleCompanion role, IInventory iinventory, int id, int x, int y)
    {
        super(iinventory, id, x, y);
        this.role = role;
    }

    /**
     * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the case
     * of armor slots)
     */
    public int getSlotStackLimit()
    {
        return 1;
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack itemstack)
    {
        return this.role.canWearSword(itemstack);
    }
}
