package noppes.npcs.containers;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

class SlotNpcTraderItems extends Slot
{
    public SlotNpcTraderItems(IInventory iinventory, int i, int j, int k)
    {
        super(iinventory, i, j, k);
    }

    public void onPickupFromSlot(ItemStack itemstack)
    {
        if (itemstack != null)
        {
            if (this.getStack() != null)
            {
                if (itemstack.getItem() == this.getStack().getItem())
                {
                    --itemstack.stackSize;
                }
            }
        }
    }

    /**
     * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the case
     * of armor slots)
     */
    public int getSlotStackLimit()
    {
        return 64;
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack itemstack)
    {
        return false;
    }
}
