package noppes.npcs;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NpcMiscInventory implements IInventory
{
    public Map<Integer, ItemStack> items = new HashMap<>();
    public int stackLimit = 64;
    private int size;

    public NpcMiscInventory(int size)
    {
        this.size = size;
    }

    public NBTTagCompound getToNBT()
    {
        NBTTagCompound nbttagcompound = new NBTTagCompound();
        nbttagcompound.setTag("NpcMiscInv", NBTTags.nbtItemStackList(this.items));
        return nbttagcompound;
    }

    public void setFromNBT(NBTTagCompound nbttagcompound)
    {
        this.items = NBTTags.getItemStackList(nbttagcompound.getTagList("NpcMiscInv", 10));
    }

    /**
     * Returns the number of slots in the inventory.
     */
    public int getSizeInventory()
    {
        return this.size;
    }

    /**
     * Returns the stack in slot i
     */
    public ItemStack getStackInSlot(int var1)
    {
        return this.items.get(var1);
    }

    /**
     * Removes from an inventory slot (first arg) up to a specified number (second arg) of items and returns them in a
     * new stack.
     */
    public ItemStack decrStackSize(int par1, int par2)
    {
        if (this.items.get(par1) == null)
        {
            return null;
        }
        else
        {
            ItemStack var4;

            if (this.items.get(par1).stackSize <= par2)
            {
                var4 = this.items.get(par1);
                this.items.put(par1, null);
            }
            else
            {
                var4 = this.items.get(par1).splitStack(par2);

                if (this.items.get(par1).stackSize == 0)
                {
                    this.items.put(par1, null);
                }
            }

            return var4;
        }
    }

    public boolean decrStackSize(ItemStack eating, int decrease)
    {
        Iterator<Integer> var3 = this.items.keySet().iterator();
        int slot;
        ItemStack item;

        do
        {
            if (!var3.hasNext())
            {
                return false;
            }

            slot = var3.next();
            item = this.items.get(slot);
        }
        while (this.items == null || eating != item || item.stackSize < decrease);

        item.splitStack(decrease);

        if (item.stackSize <= 0)
        {
            this.items.put(slot, null);
        }

        return true;
    }

    /**
     * When some containers are closed they call this on each slot, then drop whatever it returns as an EntityItem -
     * like when you close a workbench GUI.
     */
    public ItemStack getStackInSlotOnClosing(int var1)
    {
        if (this.items.get(var1) != null)
        {
            ItemStack var3 = this.items.get(var1);
            this.items.put(var1, null);
            return var3;
        }
        else
        {
            return null;
        }
    }

    /**
     * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
     */
    public void setInventorySlotContents(int var1, ItemStack var2)
    {
        if (var1 < this.getSizeInventory())
        {
            this.items.put(var1, var2);
        }
    }

    /**
     * Returns the maximum stack size for a inventory slot.
     */
    public int getInventoryStackLimit()
    {
        return this.stackLimit;
    }

    /**
     * Do not make give this method the name canInteractWith because it clashes with Container
     */
    public boolean isUseableByPlayer(EntityPlayer var1)
    {
        return true;
    }

    /**
     * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
     */
    public boolean isItemValidForSlot(int i, ItemStack itemstack)
    {
        return true;
    }

    /**
     * Returns the name of the inventory
     */
    public String getInventoryName()
    {
        return "Npc Misc Inventory";
    }

    /**
     * Returns if the inventory name is localized
     */
    public boolean hasCustomInventoryName()
    {
        return true;
    }

    /**
     * Called when an the contents of an Inventory change, usually
     */
    public void markDirty() {}

    public void openInventory() {}

    public void closeInventory() {}

    public boolean addItemStack(ItemStack item)
    {
        boolean merged = false;
        ItemStack mergable;
        int slot;

        while ((mergable = this.getMergableItem(item)) != null && mergable.stackSize > 0)
        {
            slot = mergable.getMaxStackSize() - mergable.stackSize;

            if (slot > item.stackSize)
            {
                mergable.stackSize = mergable.getMaxStackSize();
                item.stackSize -= slot;
                merged = true;
            }
            else
            {
                mergable.stackSize += item.stackSize;
                item.stackSize = 0;
            }
        }

        if (item.stackSize <= 0)
        {
            return true;
        }
        else
        {
            slot = this.firstFreeSlot();

            if (slot >= 0)
            {
                this.items.put(slot, item.copy());
                item.stackSize = 0;
                return true;
            }
            else
            {
                return merged;
            }
        }
    }

    public ItemStack getMergableItem(ItemStack item)
    {
        Iterator<ItemStack> var2 = this.items.values().iterator();
        ItemStack is;

        do
        {
            if (!var2.hasNext())
            {
                return null;
            }

            is = var2.next();
        }
        while (!NoppesUtilPlayer.compareItems(item, is, false, false) || is.stackSize >= is.getMaxStackSize());

        return is;
    }

    public int firstFreeSlot()
    {
        for (int i = 0; i < this.getSizeInventory(); ++i)
        {
            if (this.items.get(i) == null)
            {
                return i;
            }
        }

        return -1;
    }

    public void setSize(int i)
    {
        this.size = i;
    }
}
