package noppes.npcs.entity.old;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.ModelData;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityNPCElfMale extends EntityNPCInterface
{
    public EntityNPCElfMale(World world)
    {
        super(world);
        this.scaleX = 0.85F;
        this.scaleY = 1.07F;
        this.scaleZ = 0.85F;
        this.display.texture = "customnpcs:textures/entity/elfmale/ElfMale.png";
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            ModelData data = npc.modelData;
            data.legs.setScale(0.85F, 1.15F);
            data.arms.setScale(0.85F, 1.15F);
            data.body.setScale(0.85F, 1.15F);
            data.head.setScale(0.85F, 0.95F);
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
