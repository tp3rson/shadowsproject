package noppes.npcs.blocks.tiles;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import noppes.npcs.CustomNpcs;
import noppes.npcs.blocks.BlockNpcRedstone;
import noppes.npcs.controllers.Availability;

import java.util.Iterator;
import java.util.List;

public class TileRedstoneBlock extends TileEntity
{
    public int onRange = 6;
    public int offRange = 10;
    public int onRangeX = 6;
    public int onRangeY = 6;
    public int onRangeZ = 6;
    public int offRangeX = 10;
    public int offRangeY = 10;
    public int offRangeZ = 10;
    public boolean isDetailed = false;
    public Availability availability = new Availability();
    public boolean isActivated = false;
    private int ticks = 10;

    public void updateEntity()
    {
        if (!this.worldObj.isRemote)
        {
            --this.ticks;

            if (this.ticks <= 0)
            {
                this.ticks = 20;
                Block block = this.worldObj.getBlock(this.xCoord, this.yCoord, this.zCoord);

                if (block != null && block instanceof BlockNpcRedstone)
                {
                    if (CustomNpcs.FreezeNPCs)
                    {
                        if (this.isActivated)
                        {
                            this.setActive(block, false);
                        }
                    }
                    else
                    {
                        int x;
                        int y;
                        int z;
                        List list;
                        Iterator var6;
                        EntityPlayer player;

                        if (!this.isActivated)
                        {
                            x = this.isDetailed ? this.onRangeX : this.onRange;
                            y = this.isDetailed ? this.onRangeY : this.onRange;
                            z = this.isDetailed ? this.onRangeZ : this.onRange;
                            list = this.getPlayerList(x, y, z);

                            if (list.isEmpty())
                            {
                                return;
                            }

                            var6 = list.iterator();

                            while (var6.hasNext())
                            {
                                player = (EntityPlayer)var6.next();

                                if (this.availability.isAvailable(player))
                                {
                                    this.setActive(block, true);
                                    return;
                                }
                            }
                        }
                        else
                        {
                            x = this.isDetailed ? this.offRangeX : this.offRange;
                            y = this.isDetailed ? this.offRangeY : this.offRange;
                            z = this.isDetailed ? this.offRangeZ : this.offRange;
                            list = this.getPlayerList(x, y, z);
                            var6 = list.iterator();

                            while (var6.hasNext())
                            {
                                player = (EntityPlayer)var6.next();

                                if (this.availability.isAvailable(player))
                                {
                                    return;
                                }
                            }

                            this.setActive(block, false);
                        }
                    }
                }
            }
        }
    }

    private void setActive(Block block, boolean bo)
    {
        this.isActivated = bo;
        this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, this.isActivated ? 1 : 0, 2);
        this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
        block.onBlockAdded(this.worldObj, this.xCoord, this.yCoord, this.zCoord);
    }

    private List<EntityPlayer> getPlayerList(int x, int y, int z)
    {
        return this.worldObj.getEntitiesWithinAABB(EntityPlayer.class, AxisAlignedBB.getBoundingBox((double)this.xCoord, (double)this.yCoord, (double)this.zCoord, (double)(this.xCoord + 1), (double)(this.yCoord + 1), (double)(this.zCoord + 1)).expand((double)x, (double)y, (double)z));
    }

    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        this.onRange = compound.getInteger("BlockOnRange");
        this.offRange = compound.getInteger("BlockOffRange");
        this.isDetailed = compound.getBoolean("BlockIsDetailed");

        if (compound.hasKey("BlockOnRangeX"))
        {
            this.isDetailed = true;
            this.onRangeX = compound.getInteger("BlockOnRangeX");
            this.onRangeY = compound.getInteger("BlockOnRangeY");
            this.onRangeZ = compound.getInteger("BlockOnRangeZ");
            this.offRangeX = compound.getInteger("BlockOffRangeX");
            this.offRangeY = compound.getInteger("BlockOffRangeY");
            this.offRangeZ = compound.getInteger("BlockOffRangeZ");
        }

        this.isActivated = compound.getBoolean("BlockActivated");
        this.availability.readFromNBT(compound);

        if (this.worldObj != null)
        {
            this.setActive(this.getBlockType(), this.isActivated);
        }
    }

    public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        compound.setInteger("BlockOnRange", this.onRange);
        compound.setInteger("BlockOffRange", this.offRange);
        compound.setBoolean("BlockActivated", this.isActivated);
        compound.setBoolean("BlockIsDetailed", this.isDetailed);

        if (this.isDetailed)
        {
            compound.setInteger("BlockOnRangeX", this.onRangeX);
            compound.setInteger("BlockOnRangeY", this.onRangeY);
            compound.setInteger("BlockOnRangeZ", this.onRangeZ);
            compound.setInteger("BlockOffRangeX", this.offRangeX);
            compound.setInteger("BlockOffRangeY", this.offRangeY);
            compound.setInteger("BlockOffRangeZ", this.offRangeZ);
        }

        this.availability.writeToNBT(compound);
    }

    public boolean canUpdate()
    {
        return true;
    }
}
