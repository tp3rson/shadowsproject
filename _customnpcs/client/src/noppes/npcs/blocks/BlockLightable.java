package noppes.npcs.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.Random;

public abstract class BlockLightable extends BlockRotated
{
    protected BlockLightable(Block block, boolean lit)
    {
        super(block);

        if (lit)
        {
            this.setLightLevel(1.0F);
        }
    }

    public abstract Block unlitBlock();

    public abstract Block litBlock();

    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
        TileEntity tile = world.getTileEntity(x, y, z);

        if (this.litBlock() == this)
        {
            world.setBlock(x, y, z, this.unlitBlock(), world.getBlockMetadata(x, y, z), 2);
        }
        else
        {
            world.setBlock(x, y, z, this.litBlock(), world.getBlockMetadata(x, y, z), 2);
        }

        tile.validate();
        world.setTileEntity(x, y, z, tile);
        return true;
    }

    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
    {
        return Item.getItemFromBlock(this.litBlock());
    }

    @SideOnly(Side.CLIENT)

    /**
     * Gets an item for the block being called on. Args: world, x, y, z
     */
    public Item getItem(World p_149694_1_, int p_149694_2_, int p_149694_3_, int p_149694_4_)
    {
        return Item.getItemFromBlock(this.litBlock());
    }

    /**
     * Returns an item stack containing a single instance of the current block type. 'i' is the block's subtype/damage
     * and is ignored for blocks which do not support subtypes. Blocks which cannot be harvested should return null.
     */
    protected ItemStack createStackedBlock(int p_149644_1_)
    {
        return new ItemStack(this.litBlock());
    }
}
