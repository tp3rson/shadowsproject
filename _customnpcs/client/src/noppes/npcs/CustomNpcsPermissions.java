package noppes.npcs;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import org.apache.logging.log4j.LogManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CustomNpcsPermissions
{
    public static final CustomNpcsPermissions.Permission NPC_DELETE = new CustomNpcsPermissions.Permission("customnpcs.npc.delete");
    public static final CustomNpcsPermissions.Permission NPC_CREATE = new CustomNpcsPermissions.Permission("customnpcs.npc.create");
    public static final CustomNpcsPermissions.Permission NPC_GUI = new CustomNpcsPermissions.Permission("customnpcs.npc.gui");
    public static final CustomNpcsPermissions.Permission NPC_FREEZE = new CustomNpcsPermissions.Permission("customnpcs.npc.freeze");
    public static final CustomNpcsPermissions.Permission NPC_RESET = new CustomNpcsPermissions.Permission("customnpcs.npc.reset");
    public static final CustomNpcsPermissions.Permission NPC_AI = new CustomNpcsPermissions.Permission("customnpcs.npc.ai");
    public static final CustomNpcsPermissions.Permission NPC_ADVANCED = new CustomNpcsPermissions.Permission("customnpcs.npc.advanced");
    public static final CustomNpcsPermissions.Permission NPC_DISPLAY = new CustomNpcsPermissions.Permission("customnpcs.npc.display");
    public static final CustomNpcsPermissions.Permission NPC_INVENTORY = new CustomNpcsPermissions.Permission("customnpcs.npc.inventory");
    public static final CustomNpcsPermissions.Permission NPC_STATS = new CustomNpcsPermissions.Permission("customnpcs.npc.stats");
    public static final CustomNpcsPermissions.Permission NPC_CLONE = new CustomNpcsPermissions.Permission("customnpcs.npc.clone");
    public static final CustomNpcsPermissions.Permission GLOBAL_LINKED = new CustomNpcsPermissions.Permission("customnpcs.global.linked");
    public static final CustomNpcsPermissions.Permission GLOBAL_PLAYERDATA = new CustomNpcsPermissions.Permission("customnpcs.global.playerdata");
    public static final CustomNpcsPermissions.Permission GLOBAL_BANK = new CustomNpcsPermissions.Permission("customnpcs.global.bank");
    public static final CustomNpcsPermissions.Permission GLOBAL_DIALOG = new CustomNpcsPermissions.Permission("customnpcs.global.dialog");
    public static final CustomNpcsPermissions.Permission GLOBAL_QUEST = new CustomNpcsPermissions.Permission("customnpcs.global.quest");
    public static final CustomNpcsPermissions.Permission GLOBAL_FACTION = new CustomNpcsPermissions.Permission("customnpcs.global.faction");
    public static final CustomNpcsPermissions.Permission GLOBAL_TRANSPORT = new CustomNpcsPermissions.Permission("customnpcs.global.transport");
    public static final CustomNpcsPermissions.Permission GLOBAL_RECIPE = new CustomNpcsPermissions.Permission("customnpcs.global.recipe");
    public static final CustomNpcsPermissions.Permission GLOBAL_NATURALSPAWN = new CustomNpcsPermissions.Permission("customnpcs.global.naturalspawn");
    public static final CustomNpcsPermissions.Permission SPAWNER_MOB = new CustomNpcsPermissions.Permission("customnpcs.spawner.mob");
    public static final CustomNpcsPermissions.Permission SPAWNER_CREATE = new CustomNpcsPermissions.Permission("customnpcs.spawner.create");
    public static final CustomNpcsPermissions.Permission TOOL_MOUNTER = new CustomNpcsPermissions.Permission("customnpcs.tool.mounter");
    public static final CustomNpcsPermissions.Permission TOOL_PATHER = new CustomNpcsPermissions.Permission("customnpcs.tool.pather");
    public static final CustomNpcsPermissions.Permission TOOL_SCRIPTER = new CustomNpcsPermissions.Permission("customnpcs.tool.scripter");
    public static final CustomNpcsPermissions.Permission EDIT_VILLAGER = new CustomNpcsPermissions.Permission("customnpcs.edit.villager");
    public static final CustomNpcsPermissions.Permission EDIT_BLOCKS = new CustomNpcsPermissions.Permission("customnpcs.edit.blocks");
    public static final CustomNpcsPermissions.Permission SOULSTONE_ALL = new CustomNpcsPermissions.Permission("customnpcs.soulstone.all");
    public static CustomNpcsPermissions Instance;
    private Class<?> bukkit;
    private Method getPlayer;
    private Method hasPermission;

    public CustomNpcsPermissions()
    {
        Instance = this;

        try
        {
            this.bukkit = Class.forName("org.bukkit.Bukkit");
            this.getPlayer = this.bukkit.getMethod("getPlayer", new Class[] {String.class});
            this.hasPermission = Class.forName("org.bukkit.entity.Player").getMethod("hasPermission", new Class[] {String.class});
            LogManager.getLogger(CustomNpcs.class).info("Bukkit permissions enabled");
            LogManager.getLogger(CustomNpcs.class).info("Permissions available:");
            Collections.sort(CustomNpcsPermissions.Permission.permissions, String.CASE_INSENSITIVE_ORDER);
            Iterator e = CustomNpcsPermissions.Permission.permissions.iterator();

            while (e.hasNext())
            {
                String p = (String)e.next();
                LogManager.getLogger(CustomNpcs.class).info(p);
            }
        }
        catch (ClassNotFoundException var3)
        {
            ;
        }
        catch (NoSuchMethodException var4)
        {
            var4.printStackTrace();
        }
        catch (SecurityException var5)
        {
            var5.printStackTrace();
        }
    }

    public static boolean hasPermission(EntityPlayer player, CustomNpcsPermissions.Permission permission)
    {
        return Instance.bukkit != null ? Instance.bukkitPermission(player.getCommandSenderName(), permission.name) : true;
    }

    private boolean bukkitPermission(String username, String permission)
    {
        try
        {
            Object e = this.getPlayer.invoke((Object)null, new Object[] {username});
            return ((Boolean)this.hasPermission.invoke(e, new Object[] {permission})).booleanValue();
        }
        catch (IllegalAccessException var4)
        {
            var4.printStackTrace();
        }
        catch (IllegalArgumentException var5)
        {
            var5.printStackTrace();
        }
        catch (InvocationTargetException var6)
        {
            var6.printStackTrace();
        }

        return false;
    }

    public static boolean hasPermissionString(EntityPlayerMP player, String permission)
    {
        return Instance.bukkit != null ? Instance.bukkitPermission(player.getCommandSenderName(), permission) : true;
    }

    public static boolean enabled()
    {
        return Instance.bukkit != null;
    }

    public static class Permission
    {
        private static final List<String> permissions = new ArrayList();
        public String name;

        public Permission(String name)
        {
            this.name = name;

            if (!permissions.contains(name))
            {
                permissions.add(name);
            }
        }
    }
}
