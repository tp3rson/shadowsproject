package noppes.npcs.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.event.ClickEvent;
import net.minecraft.util.ChatComponentTranslation;

public class VersionChecker extends Thread
{
    private int revision = 15;

    public void run()
    {
        String name = "\u00a72CustomNpcs\u00a7f";
        String link = "\u00a79\u00a7nClick here";
        String text = name + " installed. For more info " + link;
        EntityClientPlayerMP player;

        try
        {
            player = Minecraft.getMinecraft().thePlayer;
        }
        catch (NoSuchMethodError var7)
        {
            return;
        }

        while ((player = Minecraft.getMinecraft().thePlayer) == null)
        {
            try
            {
                Thread.sleep(2000L);
            }
            catch (InterruptedException var6)
            {
                var6.printStackTrace();
            }
        }

        ChatComponentTranslation message = new ChatComponentTranslation(text, new Object[0]);
        message.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://www.kodevelopment.nl/minecraft/customnpcs/"));
        player.addChatMessage(message);
    }
}
