package noppes.npcs.client.renderer.blocks;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.tileentity.TileEntity;
import noppes.npcs.CustomItems;
import noppes.npcs.blocks.BlockRotated;
import noppes.npcs.blocks.tiles.TileColorable;
import noppes.npcs.client.model.blocks.ModelCampfire;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class BlockCampfireRenderer extends BlockRendererInterface
{
    private final ModelCampfire model = new ModelCampfire();

    public BlockCampfireRenderer()
    {
        ((BlockRotated)CustomItems.campfire).renderId = RenderingRegistry.getNextAvailableRenderId();
        ((BlockRotated)CustomItems.campfire_unlit).renderId = ((BlockRotated)CustomItems.campfire).renderId;
        RenderingRegistry.registerBlockHandler(this);
    }

    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float timeTick)
    {
        TileColorable tile = (TileColorable) tileEntity;
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef((float)(45 * tile.rotation), 0.0F, 1.0F, 0.0F);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        Minecraft.getMinecraft().getTextureManager().bindTexture(PlanksOak);
        this.model.renderLog(0.0625F);
        Minecraft.getMinecraft().getTextureManager().bindTexture(Stone);
        this.model.renderRock(0.0625F);
        GL11.glPopMatrix();
    }

    public void renderTileEntityAtPost(TileEntity tileEntity, double x, double y, double z, float timeTick) {

    }

    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, 1.2F, 0.0F);
        GL11.glScalef(1.0F, 1.0F, 1.0F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        Minecraft.getMinecraft().getTextureManager().bindTexture(PlanksOak);
        this.model.renderLog(0.0625F);
        Minecraft.getMinecraft().getTextureManager().bindTexture(Stone);
        this.model.renderRock(0.0625F);
        GL11.glPopMatrix();
    }

    public int getRenderId()
    {
        return CustomItems.campfire.getRenderType();
    }
}
