package noppes.npcs.client.renderer.blocks;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBook;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import noppes.npcs.CustomItems;
import noppes.npcs.blocks.BlockRotated;
import noppes.npcs.blocks.tiles.TileColorable;
import noppes.npcs.client.model.blocks.ModelInk;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class BlockBookRenderer extends BlockRendererInterface
{
    private final ModelInk ink = new ModelInk();
    private final ResourceLocation resource = new ResourceLocation("textures/entity/enchanting_table_book.png");
    private final ResourceLocation resource2 = new ResourceLocation("customnpcs:textures/models/Ink.png");
    private final ModelBook book = new ModelBook();

    public BlockBookRenderer()
    {
        ((BlockRotated)CustomItems.book).renderId = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(this);
    }

    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float timeTick)
    {
        TileColorable tile = (TileColorable) tileEntity;
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef((float)(90 * tile.rotation - 90), 0.0F, 1.0F, 0.0F);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        TextureManager manager = Minecraft.getMinecraft().getTextureManager();
        manager.bindTexture(this.resource2);

        if (!this.playerTooFar(tile))
        {
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        }

        this.ink.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        manager.bindTexture(this.resource);
        GL11.glRotatef(-90.0F, 0.0F, 0.0F, 1.0F);
        GL11.glTranslatef(-1.49F, -0.18F, 0.0F);
        this.book.render((Entity)null, 0.0F, 0.0F, 1.0F, 1.24F, 1.0F, 0.0625F);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glPopMatrix();
    }

    public void renderTileEntityAtPost(TileEntity tileEntity, double x, double y, double z, float timeTick) {

    }

    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef(0.2F, 1.7F, 0.0F);
        GL11.glScalef(1.4F, 1.4F, 1.4F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        GL11.glEnable(GL11.GL_CULL_FACE);
        TextureManager manager = Minecraft.getMinecraft().getTextureManager();
        manager.bindTexture(this.resource2);
        this.ink.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        manager.bindTexture(this.resource);
        GL11.glRotatef(-90.0F, 0.0F, 0.0F, 1.0F);
        GL11.glTranslatef(-1.45F, -0.18F, 0.0F);
        this.book.render((Entity)null, 0.0F, 0.0F, 1.0F, 1.24F, 1.0F, 0.0625F);
        GL11.glPopMatrix();
    }

    public int getRenderId()
    {
        return CustomItems.book.getRenderType();
    }
}
