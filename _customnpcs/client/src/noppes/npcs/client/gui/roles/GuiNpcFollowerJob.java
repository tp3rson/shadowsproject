package noppes.npcs.client.gui.roles;

import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.client.Client;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.entity.EntityNPCInterface;
import noppes.npcs.roles.JobFollower;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GuiNpcFollowerJob extends GuiNPCInterface2 implements ICustomScrollListener
{
    private JobFollower job;
    private GuiCustomScroll scroll;

    public GuiNpcFollowerJob(EntityNPCInterface npc)
    {
        super(npc);
        this.job = (JobFollower)npc.jobInterface;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.addLabel(new GuiNpcLabel(1, "gui.name", this.guiLeft + 6, this.guiTop + 110));
        this.addTextField(new GuiNpcTextField(1, this, this.fontRendererObj, this.guiLeft + 50, this.guiTop + 105, 200, 20, this.job.name));
        this.scroll = new GuiCustomScroll(this, 0);
        this.scroll.setSize(143, 208);
        this.scroll.guiLeft = this.guiLeft + 268;
        this.scroll.guiTop = this.guiTop + 4;
        this.addScroll(this.scroll);
        ArrayList names = new ArrayList();
        List list = this.npc.worldObj.getEntitiesWithinAABB(EntityNPCInterface.class, this.npc.boundingBox.expand(40.0D, 40.0D, 40.0D));
        Iterator var3 = list.iterator();

        while (var3.hasNext())
        {
            EntityNPCInterface npc = (EntityNPCInterface)var3.next();

            if (npc != this.npc && !names.contains(npc.display.name))
            {
                names.add(npc.display.name);
            }
        }

        this.scroll.setList(names);
    }

    public void save()
    {
        this.job.name = this.getTextField(1).getText();
        Client.sendData(EnumPacketServer.JobSave, new Object[] {this.job.writeToNBT(new NBTTagCompound())});
    }

    public void customScrollClicked(int i, int j, int k, GuiCustomScroll guiCustomScroll)
    {
        this.getTextField(1).setText(guiCustomScroll.getSelected());
    }
}
