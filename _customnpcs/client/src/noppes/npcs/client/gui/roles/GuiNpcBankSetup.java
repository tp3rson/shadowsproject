package noppes.npcs.client.gui.roles;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.client.Client;
import noppes.npcs.client.gui.util.GuiCustomScroll;
import noppes.npcs.client.gui.util.GuiNPCInterface2;
import noppes.npcs.client.gui.util.ICustomScrollListener;
import noppes.npcs.client.gui.util.IScrollData;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.controllers.Bank;
import noppes.npcs.entity.EntityNPCInterface;
import noppes.npcs.roles.RoleBank;

import java.util.HashMap;
import java.util.Vector;

public class GuiNpcBankSetup extends GuiNPCInterface2 implements IScrollData, ICustomScrollListener
{
    private GuiCustomScroll scroll;
    private HashMap<String, Integer> data = new HashMap();
    private RoleBank role;

    public GuiNpcBankSetup(EntityNPCInterface npc)
    {
        super(npc);
        Client.sendData(EnumPacketServer.BanksGet, new Object[0]);
        this.role = (RoleBank)npc.roleInterface;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();

        if (this.scroll == null)
        {
            this.scroll = new GuiCustomScroll(this, 0);
        }

        this.scroll.setSize(200, 152);
        this.scroll.guiLeft = this.guiLeft + 85;
        this.scroll.guiTop = this.guiTop + 20;
        this.addScroll(this.scroll);
    }

    protected void actionPerformed(GuiButton guiButton) {}

    public void setData(Vector<String> list, HashMap<String, Integer> data)
    {
        String name = null;
        Bank bank = this.role.getBank();

        if (bank != null)
        {
            name = bank.name;
        }

        this.data = data;
        this.scroll.setList(list);

        if (name != null)
        {
            this.setSelected(name);
        }
    }

    /**
     * Called when the mouse is clicked.
     */
    public void mouseClicked(int mouseX, int mouseY, int mouseButton)
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);

        if (mouseButton == 0 && this.scroll != null)
        {
            this.scroll.mouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    public void setSelected(String selected)
    {
        this.scroll.setSelected(selected);
    }

    public void customScrollClicked(int i, int j, int k, GuiCustomScroll guiCustomScroll)
    {
        if (guiCustomScroll.id == 0)
        {
            this.role.bankId = ((Integer)this.data.get(this.scroll.getSelected())).intValue();
            this.save();
        }
    }

    public void save()
    {
        Client.sendData(EnumPacketServer.RoleSave, new Object[] {this.role.writeToNBT(new NBTTagCompound())});
    }
}
