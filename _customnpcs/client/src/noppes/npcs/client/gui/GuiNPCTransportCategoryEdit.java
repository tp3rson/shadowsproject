package noppes.npcs.client.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import noppes.npcs.client.Client;
import noppes.npcs.client.NoppesUtil;
import noppes.npcs.client.gui.util.GuiNPCInterface;
import noppes.npcs.client.gui.util.GuiNpcButton;
import noppes.npcs.client.gui.util.GuiNpcLabel;
import noppes.npcs.client.gui.util.GuiNpcTextField;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.entity.EntityNPCInterface;

public class GuiNPCTransportCategoryEdit extends GuiNPCInterface
{
    private GuiScreen parent;
    private String name;
    private int id;

    public GuiNPCTransportCategoryEdit(EntityNPCInterface npc, GuiScreen parent, String name, int id)
    {
        super(npc);
        this.parent = parent;
        this.name = name;
        this.id = id;
        this.title = "Npc Transport Category";
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.addTextField(new GuiNpcTextField(1, this, this.fontRendererObj, this.width / 2 - 40, 100, 140, 20, this.name));
        this.addLabel(new GuiNpcLabel(1, "Title:", this.width / 2 - 100 + 4, 105, 16777215));
        this.addButton(new GuiNpcButton(2, this.width / 2 - 100, 210, 98, 20, "gui.back"));
        this.addButton(new GuiNpcButton(3, this.width / 2 + 2, 210, 98, 20, "Save"));
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        super.drawScreen(mouseX, mouseY, partialTick);
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        int id = guiButton.id;

        if (id == 2)
        {
            NoppesUtil.openGUI(this.player, this.parent);
            Client.sendData(EnumPacketServer.TransportCategoriesGet, new Object[0]);
        }

        if (id == 3)
        {
            this.save();
            NoppesUtil.openGUI(this.player, this.parent);
            Client.sendData(EnumPacketServer.TransportCategoriesGet, new Object[0]);
        }
    }

    public void save()
    {
        String name = this.getTextField(1).getText();

        if (!name.trim().isEmpty())
        {
            Client.sendData(EnumPacketServer.TransportCategorySave, new Object[] {name, Integer.valueOf(this.id)});
        }
    }
}
