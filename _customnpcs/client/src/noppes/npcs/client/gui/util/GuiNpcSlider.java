package noppes.npcs.client.gui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import noppes.npcs.NoppesStringUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class GuiNpcSlider extends GuiButton
{
    private ISliderListener listener;
    public int id;
    public float sliderValue;
    public boolean dragging;

    public GuiNpcSlider(GuiScreen parent, int id, int xPos, int yPos, String displayString, float sliderValue)
    {
        super(id, xPos, yPos, 150, 20, NoppesStringUtils.translate(new Object[] {displayString}));
        this.sliderValue = 1.0F;
        this.id = id;
        this.sliderValue = sliderValue;

        if (parent instanceof ISliderListener)
        {
            this.listener = (ISliderListener)parent;
        }
    }

    public GuiNpcSlider(GuiScreen parent, int id, int xPos, int yPos, float sliderValue)
    {
        this(parent, id, xPos, yPos, "", sliderValue);

        if (this.listener != null)
        {
            this.listener.mouseDragged(this);
        }
    }

    public GuiNpcSlider(GuiScreen parent, int id, int xPos, int yPos, int width, int height, float sliderValue)
    {
        this(parent, id, xPos, yPos, "", sliderValue);
        this.width = width;
        this.height = height;

        if (this.listener != null)
        {
            this.listener.mouseDragged(this);
        }
    }

    /**
     * Fired when the mouse button is dragged. Equivalent of MouseListener.mouseDragged(MouseEvent e).
     */
    public void mouseDragged(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.visible)
        {
            mc.getTextureManager().bindTexture(buttonTextures);

            if (this.dragging)
            {
                this.sliderValue = (float)(mouseX - (this.xPosition + 4)) / (float)(this.width - 8);

                if (this.sliderValue < 0.0F)
                {
                    this.sliderValue = 0.0F;
                }

                if (this.sliderValue > 1.0F)
                {
                    this.sliderValue = 1.0F;
                }

                if (this.listener != null)
                {
                    this.listener.mouseDragged(this);
                }

                if (!Mouse.isButtonDown(0))
                {
                    this.func_146111_b(0, 0);
                }
            }

            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.drawTexturedModalRect(this.xPosition + (int)(this.sliderValue * (float)(this.width - 8)), this.yPosition, 0, 66, 4, 20);
            this.drawTexturedModalRect(this.xPosition + (int)(this.sliderValue * (float)(this.width - 8)) + 4, this.yPosition, 196, 66, 4, 20);
        }
    }

    public String getDisplayString()
    {
        return this.displayString;
    }

    public void setString(String str)
    {
        this.displayString = NoppesStringUtils.translate(new Object[] {str});
    }

    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.enabled && this.visible && mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height)
        {
            this.sliderValue = (float)(mouseX - (this.xPosition + 4)) / (float)(this.width - 8);

            if (this.sliderValue < 0.0F)
            {
                this.sliderValue = 0.0F;
            }

            if (this.sliderValue > 1.0F)
            {
                this.sliderValue = 1.0F;
            }

            if (this.listener != null)
            {
                this.listener.mousePressed(this);
            }

            this.dragging = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void func_146111_b(int par1, int par2)
    {
        this.dragging = false;

        if (this.listener != null)
        {
            this.listener.mouseReleased(this);
        }
    }

    public int getHoverState(boolean field_146123_n)
    {
        return 0;
    }
}
