package noppes.npcs.client.gui.util;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.opengl.GL11;

public abstract class GuiNPCInterface2 extends GuiNPCInterface
{
    private ResourceLocation background;
    private GuiNpcMenu menu;

    public GuiNPCInterface2(EntityNPCInterface npc)
    {
        this(npc, -1);
    }

    public GuiNPCInterface2(EntityNPCInterface npc, int activeMenu)
    {
        super(npc);
        this.background = new ResourceLocation("customnpcs:textures/gui/menubg.png");
        this.xSize = 420;
        this.ySize = 200;
        this.menu = new GuiNpcMenu(this, activeMenu, npc);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.menu.initGui(this.guiLeft, this.guiTop, this.xSize);
    }

    /**
     * Called when the mouse is clicked.
     */
    public void mouseClicked(int mouseX, int mouseY, int mouseButton)
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);

        if (!this.hasSubGui())
        {
            this.menu.mouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    public void delete()
    {
        this.npc.delete();
        this.displayGuiScreen((GuiScreen)null);
        this.mc.setIngameFocus();
    }

    public abstract void save();

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        if (this.drawDefaultBackground)
        {
            this.drawDefaultBackground();
        }

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(this.background);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, 200, 220);
        this.drawTexturedModalRect(this.guiLeft + this.xSize - 230, this.guiTop, 26, 0, 230, 220);
        this.menu.drawElements(this.getFontRenderer(), mouseX, mouseY, this.mc, partialTick);
        boolean bo = this.drawDefaultBackground;
        this.drawDefaultBackground = false;
        super.drawScreen(mouseX, mouseY, partialTick);
        this.drawDefaultBackground = bo;
    }
}
