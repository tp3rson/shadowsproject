package noppes.npcs.client.gui.player;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import noppes.npcs.NoppesUtilPlayer;
import noppes.npcs.client.CustomNpcResourceListener;
import noppes.npcs.client.gui.util.GuiButtonNextPage;
import noppes.npcs.client.gui.util.GuiNPCInterface;
import noppes.npcs.client.gui.util.IGuiData;
import noppes.npcs.constants.EnumPlayerPacket;
import noppes.npcs.controllers.Faction;
import noppes.npcs.controllers.PlayerFactionData;
import org.lwjgl.opengl.GL11;
import tconstruct.client.tabs.InventoryTabFactions;
import tconstruct.client.tabs.TabRegistry;

import java.util.ArrayList;
import java.util.Iterator;

public class GuiFaction extends GuiNPCInterface implements IGuiData
{
    private int xSize = 200;
    private int ySize = 195;
    private int guiLeft;
    private int guiTop;
    private ArrayList<Faction> playerFactions = new ArrayList();
    private int page = 0;
    private int pages = 1;
    private GuiButtonNextPage buttonNextPage;
    private GuiButtonNextPage buttonPreviousPage;
    private ResourceLocation indicator;

    public GuiFaction()
    {
        this.drawDefaultBackground = false;
        this.title = "";
        NoppesUtilPlayer.sendData(EnumPlayerPacket.FactionsGet, new Object[0]);
        this.indicator = this.getResource("standardbg.png");
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.guiLeft = (this.width - this.xSize) / 2;
        this.guiTop = (this.height - this.ySize) / 2 + 12;
        TabRegistry.updateTabValues(this.guiLeft, this.guiTop + 8, InventoryTabFactions.class);
        TabRegistry.addTabsToList(this.buttonList);
        this.buttonList.add(this.buttonNextPage = new GuiButtonNextPage(1, this.guiLeft + this.xSize - 43, this.guiTop + 180, true));
        this.buttonList.add(this.buttonPreviousPage = new GuiButtonNextPage(2, this.guiLeft + 20, this.guiTop + 180, false));
        this.updateButtons();
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        this.drawDefaultBackground();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(this.indicator);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop + 8, 0, 0, this.xSize, this.ySize);
        this.drawTexturedModalRect(this.guiLeft + 4, this.guiTop + 8, 56, 0, 200, this.ySize);

        if (this.playerFactions.isEmpty())
        {
            String noFaction = StatCollector.translateToLocal("faction.nostanding");
            this.fontRendererObj.drawString(noFaction, this.guiLeft + (this.xSize - this.fontRendererObj.getStringWidth(noFaction)) / 2, this.guiTop + 80, CustomNpcResourceListener.DefaultTextColor);
        }
        else
        {
            this.renderScreen();
        }

        super.drawScreen(mouseX, mouseY, partialTick);
    }

    private void renderScreen()
    {
        int size = 5;

        if (this.playerFactions.size() % 5 != 0 && this.page == this.pages)
        {
            size = this.playerFactions.size() % 5;
        }

        for (int s = 0; s < size; ++s)
        {
            this.drawHorizontalLine(this.guiLeft + 2, this.guiLeft + this.xSize, this.guiTop + 14 + s * 30, -16777216 + CustomNpcResourceListener.DefaultTextColor);
            Faction faction = (Faction)this.playerFactions.get((this.page - 1) * 5 + s);
            String name = faction.name;
            String points = " : " + faction.defaultPoints;
            String standing = StatCollector.translateToLocal("faction.friendly");
            int color = 65280;

            if (faction.defaultPoints < faction.neutralPoints)
            {
                standing = StatCollector.translateToLocal("faction.unfriendly");
                color = 16711680;
                points = points + "/" + faction.neutralPoints;
            }
            else if (faction.defaultPoints < faction.friendlyPoints)
            {
                standing = StatCollector.translateToLocal("faction.neutral");
                color = 15924992;
                points = points + "/" + faction.friendlyPoints;
            }
            else
            {
                points = points + "/-";
            }

            this.fontRendererObj.drawString(name, this.guiLeft + (this.xSize - this.fontRendererObj.getStringWidth(name)) / 2, this.guiTop + 19 + s * 30, faction.color);
            this.fontRendererObj.drawString(standing, this.width / 2 - this.fontRendererObj.getStringWidth(standing) - 1, this.guiTop + 33 + s * 30, color);
            this.fontRendererObj.drawString(points, this.width / 2, this.guiTop + 33 + s * 30, CustomNpcResourceListener.DefaultTextColor);
        }

        this.drawHorizontalLine(this.guiLeft + 2, this.guiLeft + this.xSize, this.guiTop + 14 + size * 30, -16777216 + CustomNpcResourceListener.DefaultTextColor);

        if (this.pages > 1)
        {
            String var8 = this.page + "/" + this.pages;
            this.fontRendererObj.drawString(var8, this.guiLeft + (this.xSize - this.fontRendererObj.getStringWidth(var8)) / 2, this.guiTop + 203, CustomNpcResourceListener.DefaultTextColor);
        }
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        if (guiButton instanceof GuiButtonNextPage)
        {
            int id = guiButton.id;

            if (id == 1)
            {
                ++this.page;
            }

            if (id == 2)
            {
                --this.page;
            }

            this.updateButtons();
        }
    }

    private void updateButtons()
    {
        this.buttonNextPage.setVisible(this.page < this.pages);
        this.buttonPreviousPage.setVisible(this.page > 1);
    }

    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {}

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        if (key == 1 || this.isInventoryKey(key))
        {
            this.close();
        }
    }

    public void save() {}

    public void setGuiData(NBTTagCompound compound)
    {
        this.playerFactions = new ArrayList();
        NBTTagList list = compound.getTagList("FactionList", 10);

        for (int data = 0; data < list.tagCount(); ++data)
        {
            Faction faction = new Faction();
            faction.readNBT(list.getCompoundTagAt(data));
            this.playerFactions.add(faction);
        }

        PlayerFactionData var9 = new PlayerFactionData();
        var9.loadNBTData(compound);
        Iterator var10 = var9.factionData.keySet().iterator();

        while (var10.hasNext())
        {
            int id = ((Integer)var10.next()).intValue();
            int points = ((Integer)var9.factionData.get(Integer.valueOf(id))).intValue();
            Iterator var7 = this.playerFactions.iterator();

            while (var7.hasNext())
            {
                Faction faction1 = (Faction)var7.next();

                if (faction1.id == id)
                {
                    faction1.defaultPoints = points;
                }
            }
        }

        this.pages = (this.playerFactions.size() - 1) / 5;
        ++this.pages;
        this.page = 1;
        this.updateButtons();
    }
}
