package noppes.npcs.client.gui.model;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.IResource;
import net.minecraft.util.ResourceLocation;
import noppes.npcs.ModelPartData;
import noppes.npcs.client.gui.util.GuiModelInterface;
import noppes.npcs.client.gui.util.GuiNpcTextField;
import noppes.npcs.client.gui.util.ITextfieldListener;
import noppes.npcs.entity.EntityCustomNpc;
import org.lwjgl.opengl.GL11;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class GuiModelColor extends GuiModelInterface implements ITextfieldListener
{
    private GuiScreen parent;
    private static final ResourceLocation color = new ResourceLocation("customnpcs:textures/gui/color.png");
    private int colorX;
    private int colorY;
    private GuiNpcTextField textfield;
    private ModelPartData data;

    public GuiModelColor(GuiScreen parent, ModelPartData data, EntityCustomNpc npc)
    {
        super(npc);
        this.parent = parent;
        this.data = data;
        this.xOffset = 60;
        this.ySize = 230;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.colorX = this.guiLeft + 4;
        this.colorY = this.guiTop + 50;
        this.addTextField(this.textfield = new GuiNpcTextField(0, this, this.guiLeft + 25, this.guiTop + 20, 70, 20, this.data.getColor()));
        this.textfield.setTextColor(this.data.color);
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        String prev = this.textfield.getText();
        super.keyTyped(character, key);
        String newText = this.textfield.getText();

        if (!newText.equals(prev))
        {
            try
            {
                int e = Integer.parseInt(this.textfield.getText(), 16);
                this.data.color = e;
                this.textfield.setTextColor(e);
            }
            catch (NumberFormatException var6)
            {
                this.textfield.setText(prev);
            }
        }
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        super.actionPerformed(guiButton);
    }

    public void close()
    {
        this.mc.displayGuiScreen(this.parent);
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        super.drawScreen(mouseX, mouseY, partialTick);
        this.mc.getTextureManager().bindTexture(color);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.drawTexturedModalRect(this.colorX, this.colorY, 0, 0, 120, 120);
    }

    /**
     * Called when the mouse is clicked.
     */
    public void mouseClicked(int mouseX, int mouseY, int mouseButton)
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);

        if (mouseX >= this.colorX && mouseX <= this.colorX + 120 && mouseY >= this.colorY && mouseY <= this.colorY + 120)
        {
            InputStream stream = null;

            try
            {
                IResource resource = this.mc.getResourceManager().getResource(color);
                BufferedImage bufferedimage = ImageIO.read(stream = resource.getInputStream());
                int color = bufferedimage.getRGB((mouseX - this.guiLeft - 4) * 4, (mouseY - this.guiTop - 50) * 4) & 16777215;

                if (color != 0)
                {
                    this.data.color = color;
                    this.textfield.setTextColor(color);
                    this.textfield.setText(this.data.getColor());
                }
            }
            catch (IOException var16)
            {
                ;
            }
            finally
            {
                if (stream != null)
                {
                    try
                    {
                        stream.close();
                    }
                    catch (IOException var15)
                    {
                        ;
                    }
                }
            }
        }
    }

    public void unFocused(GuiNpcTextField textfield)
    {
        boolean color = false;
        int color1;

        try
        {
            color1 = Integer.parseInt(textfield.getText(), 16);
        }
        catch (NumberFormatException var4)
        {
            color1 = 0;
        }

        this.data.color = color1;
        textfield.setTextColor(color1);
    }
}
