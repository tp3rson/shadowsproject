package noppes.npcs.client.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.entity.Entity;
import net.minecraft.util.StatCollector;
import noppes.npcs.client.Client;
import noppes.npcs.client.NoppesUtil;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.entity.EntityNPCInterface;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class GuiNpcRemoteEditor extends GuiNPCInterface implements IScrollData, GuiYesNoCallback
{
    private GuiCustomScroll scroll;
    private HashMap<String, Integer> data = new HashMap();

    public GuiNpcRemoteEditor()
    {
        this.xSize = 256;
        this.setBackground("menubg.png");
        Client.sendData(EnumPacketServer.RemoteNpcsGet, new Object[0]);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();

        if (this.scroll == null)
        {
            this.scroll = new GuiCustomScroll(this, 0);
            this.scroll.setSize(165, 208);
        }

        this.scroll.guiLeft = this.guiLeft + 4;
        this.scroll.guiTop = this.guiTop + 4;
        this.addScroll(this.scroll);
        String title = StatCollector.translateToLocal("remote.title");
        int x = (this.xSize - this.fontRendererObj.getStringWidth(title)) / 2;
        this.addLabel(new GuiNpcLabel(0, title, this.guiLeft + x, this.guiTop - 8));
        this.addButton(new GuiNpcButton(0, this.guiLeft + 170, this.guiTop + 6, 82, 20, "selectServer.edit"));
        this.addButton(new GuiNpcButton(1, this.guiLeft + 170, this.guiTop + 28, 82, 20, "selectWorld.deleteButton"));
        this.addButton(new GuiNpcButton(2, this.guiLeft + 170, this.guiTop + 50, 82, 20, "remote.reset"));
        this.addButton(new GuiNpcButton(4, this.guiLeft + 170, this.guiTop + 72, 82, 20, "remote.tp"));
        this.addButton(new GuiNpcButton(5, this.guiLeft + 170, this.guiTop + 110, 82, 20, "remote.resetall"));
        this.addButton(new GuiNpcButton(3, this.guiLeft + 170, this.guiTop + 132, 82, 20, "remote.freeze"));
    }

    public void confirmClicked(boolean flag, int i)
    {
        if (flag)
        {
            Client.sendData(EnumPacketServer.RemoteDelete, new Object[] {this.data.get(this.scroll.getSelected())});
        }

        NoppesUtil.openGUI(this.player, this);
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        int id = guiButton.id;

        if (id == 3)
        {
            Client.sendData(EnumPacketServer.RemoteFreeze, new Object[0]);
        }

        if (id == 5)
        {
            Iterator entity = this.data.values().iterator();

            while (entity.hasNext())
            {
                int ids = ((Integer)entity.next()).intValue();
                Client.sendData(EnumPacketServer.RemoteReset, new Object[] {Integer.valueOf(ids)});
                Entity entity1 = this.player.worldObj.getEntityByID(ids);

                if (entity1 != null && entity1 instanceof EntityNPCInterface)
                {
                    ((EntityNPCInterface)entity1).reset();
                }
            }
        }

        if (this.data.containsKey(this.scroll.getSelected()))
        {
            if (id == 0)
            {
                Client.sendData(EnumPacketServer.RemoteMainMenu, new Object[] {this.data.get(this.scroll.getSelected())});
            }

            if (id == 1)
            {
                GuiYesNo entity2 = new GuiYesNo(this, "Confirm", StatCollector.translateToLocal("gui.delete"), 0);
                this.displayGuiScreen(entity2);
            }

            if (id == 2)
            {
                Client.sendData(EnumPacketServer.RemoteReset, new Object[] {this.data.get(this.scroll.getSelected())});
                Entity entity3 = this.player.worldObj.getEntityByID(((Integer)this.data.get(this.scroll.getSelected())).intValue());

                if (entity3 != null && entity3 instanceof EntityNPCInterface)
                {
                    ((EntityNPCInterface)entity3).reset();
                }
            }

            if (id == 4)
            {
                Client.sendData(EnumPacketServer.RemoteTpToNpc, new Object[] {this.data.get(this.scroll.getSelected())});
                this.close();
            }
        }
    }

    /**
     * Called when the mouse is clicked.
     */
    public void mouseClicked(int mouseX, int mouseY, int mouseButton)
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.scroll.mouseClicked(mouseX, mouseY, mouseButton);
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        if (key == 1 || this.isInventoryKey(key))
        {
            this.close();
        }
    }

    public void save() {}

    public void setData(Vector<String> list, HashMap<String, Integer> data)
    {
        this.scroll.setList(list);
        this.data = data;
    }

    public void setSelected(String selected)
    {
        this.getButton(3).setDisplayText(selected);
    }
}
