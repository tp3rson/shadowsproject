package noppes.npcs.client.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.StatCollector;
import noppes.npcs.client.Client;
import noppes.npcs.client.NoppesUtil;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumPacketServer;

import java.util.HashMap;
import java.util.Vector;

public class GuiNpcDimension extends GuiNPCInterface implements IScrollData
{
    private GuiCustomScroll scroll;
    private HashMap<String, Integer> data = new HashMap();

    public GuiNpcDimension()
    {
        this.xSize = 256;
        this.setBackground("menubg.png");
        Client.sendData(EnumPacketServer.DimensionsGet, new Object[0]);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();

        if (this.scroll == null)
        {
            this.scroll = new GuiCustomScroll(this, 0);
            this.scroll.setSize(165, 208);
        }

        this.scroll.guiLeft = this.guiLeft + 4;
        this.scroll.guiTop = this.guiTop + 4;
        this.addScroll(this.scroll);
        String title = StatCollector.translateToLocal("Dimensions");
        int x = (this.xSize - this.fontRendererObj.getStringWidth(title)) / 2;
        this.addLabel(new GuiNpcLabel(0, title, this.guiLeft + x, this.guiTop - 8));
        this.addButton(new GuiNpcButton(4, this.guiLeft + 170, this.guiTop + 72, 82, 20, "remote.tp"));
    }

    public void confirmClicked(boolean flag, int i)
    {
        if (flag)
        {
            Client.sendData(EnumPacketServer.RemoteDelete, new Object[] {this.data.get(this.scroll.getSelected())});
        }

        NoppesUtil.openGUI(this.player, this);
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        int id = guiButton.id;

        if (this.data.containsKey(this.scroll.getSelected()))
        {
            if (id == 4)
            {
                Client.sendData(EnumPacketServer.DimensionTeleport, new Object[] {this.data.get(this.scroll.getSelected())});
                this.close();
            }
        }
    }

    /**
     * Called when the mouse is clicked.
     */
    public void mouseClicked(int mouseX, int mouseY, int mouseButton)
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.scroll.mouseClicked(mouseX, mouseY, mouseButton);
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        if (key == 1 || this.isInventoryKey(key))
        {
            this.close();
        }
    }

    public void save() {}

    public void setData(Vector<String> list, HashMap<String, Integer> data)
    {
        this.scroll.setList(list);
        this.data = data;
    }

    public void setSelected(String selected)
    {
        this.getButton(3).setDisplayText(selected);
    }
}
