package ru.xlv.shop.handle;

import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Maps;
import ru.xlv.shop.common.ShopItem;
import ru.xlv.shop.common.ShopItemCategory;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.handle.item.ShopItemBuyHandler;
import ru.xlv.shop.handle.result.ShopBuyResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ShopHandler {

    private final Map<ShopItemCategory, List<ShopItem>> shopItemMap = new HashMap<>();
    private final Map<Class<? extends ShopItem>, ShopItemBuyHandler<?>> shopItemBuyHandlerMap = new HashMap<>();
    private final List<String> handleBlock = new ArrayList<>();

    private final Logger logger = Logger.getLogger("ShopHandler");

    private final ShopItemManager shopItemManager;

    {
        try {
            File file = new File("logs/shopHandler.log");
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileHandler fileHandler = new FileHandler(file.getAbsolutePath());
            SimpleFormatter simpleFormatter = new SimpleFormatter();
            fileHandler.setFormatter(simpleFormatter);
            logger.addHandler(fileHandler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ShopHandler(ShopItemManager shopItemManager) {
        this.shopItemManager = shopItemManager;
        for (ShopItemCategory value : ShopItemCategory.values()) {
            shopItemMap.put(value, new ArrayList<>());
        }
        shopItemManager.getShopItems().forEach(shopItem -> shopItem.getCategories().forEach(shopItemCategory -> Maps.addElemToMappedList(shopItemMap, shopItemCategory, shopItem)));
    }

    public CompletableFuture<ShopBuyResult> buy(String username, int shopItemId, ShopItemCost.Type type) {
        return CompletableFuture.supplyAsync(() -> {
            synchronized (handleBlock) {
                if (handleBlock.contains(username)) {
                    return ShopBuyResult.UNKNOWN_ERROR;
                }
            }
            ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(username);
            if (serverPlayer == null) {
                return ShopBuyResult.UNKNOWN_ERROR;
            }
            ShopItem shopItem = shopItemManager.getShopItemById(shopItemId);
            if (shopItem == null) {
                return ShopBuyResult.ITEM_NOT_FOUND;
            }
            ShopItemBuyHandler<?> buyHandler = getBuyHandler(shopItem);
            ShopBuyResult checkConditions = buyHandler.checkConditions(serverPlayer, shopItem, type);
            if(checkConditions != ShopBuyResult.SUCCESS) {
                return checkConditions;
            }
            ShopBuyResult consumeCosts = buyHandler.consumeCosts(serverPlayer, shopItem, type);
            if(consumeCosts != ShopBuyResult.SUCCESS) {
                return consumeCosts;
            }
            if (!giveOut(serverPlayer, shopItem)) {
                return ShopBuyResult.UNKNOWN_ERROR;
            }
            logger.info(String.format("User %s bought a product by %s with id %s.", username, type.name(), shopItem.getId()));
            synchronized (handleBlock) {
                handleBlock.remove(username);
            }
            return ShopBuyResult.SUCCESS;
        });
    }

    private boolean giveOut(ServerPlayer serverPlayer, ShopItem shopItem) {
        return getBuyHandler(shopItem).giveOut(serverPlayer, shopItem);
    }

    public void setShopItemBuyHandler(Class<? extends ShopItem> aClass, ShopItemBuyHandler<?> shopItemBuyHandler) {
        shopItemBuyHandlerMap.put(aClass, shopItemBuyHandler);
    }

    private synchronized ShopItemBuyHandler<?> getBuyHandler(ShopItem shopItem) {
        synchronized (shopItemBuyHandlerMap){
            return shopItemBuyHandlerMap.get(shopItem.getClass());
        }
    }

    public synchronized Map<ShopItemCategory, List<ShopItem>> getShopItemMap() {
        return shopItemMap;
    }
}
