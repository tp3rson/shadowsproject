package ru.xlv.shop.handle.item;

import net.minecraft.item.ItemStack;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.result.AddItemResult;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.item.ItemStackFactory;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.network.matrix.PacketMatrixPlayerInventorySync;
import ru.xlv.mochar.util.Utils;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.handle.result.PostSendResult;
import ru.xlv.post.util.PostFactory;
import ru.xlv.shop.XlvsShopMod;

public class ShopItemStackBuyHandler extends ShopItemBuyHandler<ShopItemStack> {

    @Override
    protected boolean giveOut0(ServerPlayer serverPlayer, ShopItemStack shopItemStack) {
        ItemStack itemStack = ItemStackFactory.create(shopItemStack.getItemUnlocalizedName(), shopItemStack.getAmount(), shopItemStack.getMetadata());
        if (itemStack == null) {
            return false;
        }
        EnumItemTag.setItemTags(itemStack, EnumItemTag.PERSONAL);
        AddItemResult addItemResult = serverPlayer.getSelectedCharacter().getMatrixInventory().addItem(itemStack);
        if (addItemResult != AddItemResult.SUCCESS) {
            PostObject adminPostObject = PostFactory.createAdminPostObject(XlvsShopMod.INSTANCE.getLocalization().getFormatted(XlvsShopMod.INSTANCE.getLocalization().getNotEnoughInvSpacePostText(), itemStack), serverPlayer.getPlayerName());
            PostSendResult postSendResult = XlvsPostMod.INSTANCE.getPostHandler().sendPostSync(adminPostObject);
            if(postSendResult == PostSendResult.SUCCESS) {
                Utils.sendMessage(serverPlayer, XlvsShopMod.INSTANCE.getLocalization().getNotEnoughInvSpaceMessage());
                return true;
            }
            return false;
        } else {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketsToPlayer(serverPlayer.getEntityPlayer(), new PacketMatrixPlayerInventorySync(serverPlayer));
        }
        return true;
    }
}
