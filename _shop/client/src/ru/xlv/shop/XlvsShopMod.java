package ru.xlv.shop;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.shop.network.PacketShopBuy;
import ru.xlv.shop.network.PacketShopSyncCategory;

import static ru.xlv.shop.XlvsShopMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
@Getter
public class XlvsShopMod {

    static final String MODID = "xlvsshop";

    @Mod.Instance(MODID)
    public static XlvsShopMod INSTANCE;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketShopBuy(),
                new PacketShopSyncCategory()
        );
    }
}
