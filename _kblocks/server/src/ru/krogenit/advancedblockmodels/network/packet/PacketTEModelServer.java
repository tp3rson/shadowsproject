package ru.krogenit.advancedblockmodels.network.packet;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.BlockModels;
import ru.krogenit.advancedblockmodels.block.BlockModelsServer;
import ru.krogenit.advancedblockmodels.block.te.TEBlockModelServer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public class PacketTEModelServer implements IPacketInOnServer {

    private AxisAlignedBB aabb;
    private float x, y, z, hitX, hitY, hitZ, rotX, rotY, rotZ;
    private Vector3f scale;
    private String model;
    private byte byteType;

    @Override
    public void read(EntityPlayer p, ByteBufInputStream data) throws IOException {
        x = data.readFloat();
        y = data.readFloat();
        z = data.readFloat();
        hitX = data.readFloat();
        hitY = data.readFloat();
        hitZ = data.readFloat();
        rotX = data.readFloat();
        rotY = data.readFloat();
        rotZ = data.readFloat();
        scale = new Vector3f(data.readFloat(), data.readFloat(), data.readFloat());
        model = data.readUTF();
        byteType = data.readByte();
        boolean hasAABB = data.readBoolean();

        if (hasAABB) aabb = AxisAlignedBB.getBoundingBox(data.readFloat(), data.readFloat(), data.readFloat(), data.readFloat(), data.readFloat(), data.readFloat());
        handleMessageOnServerSide(p);
    }

    private void handleMessageOnServerSide(EntityPlayer p) {
        World w = p.worldObj;
        BlockModel.ModelCollideType type = BlockModel.ModelCollideType.values()[byteType];
        int blockX = MathHelper.floor_double(x);
        int blockY = MathHelper.floor_double(y);
        int blockZ = MathHelper.floor_double(z);

        TileEntity tile = w.getTileEntity(blockX, blockY, blockZ);
        Block b = w.getBlock(blockX, blockY, blockZ);

        if (tile != null) {
            if(tile instanceof TEBlockModelServer) {
                if (!p.capabilities.isCreativeMode) {
                    if (p.getCurrentEquippedItem() != null) {
                        p.getCurrentEquippedItem().stackSize--;
                        if (p.getCurrentEquippedItem().stackSize <= 0) {
                            p.inventory.mainInventory[p.inventory.currentItem] = null;
                        }
                    } else return;
                }
                TEBlockModelServer te = (TEBlockModelServer) tile;
                te.addNewModelOnServer(model, new Vector3f(hitX, hitY, hitZ), new Vector3f(rotX, rotY, rotZ), scale, aabb, type);

                p.worldObj.playSoundEffect(x + 0.5F, y + 0.5F, z + 0.5F, BlockModels.modelBlock.stepSound.getBreakSound(), (BlockModels.modelBlock.stepSound.getVolume() + 1.0F) / 2.0F, BlockModels.modelBlock.stepSound.getPitch() * 0.8F);
                p.worldObj.markBlockForUpdate(blockX, blockY, blockZ);
            }
        } else if (b == Blocks.air) {
            if (!p.capabilities.isCreativeMode) {
                if (p.getCurrentEquippedItem() != null) {
                    p.getCurrentEquippedItem().stackSize--;
                    if (p.getCurrentEquippedItem().stackSize <= 0) {
                        p.inventory.mainInventory[p.inventory.currentItem] = null;
                    }
                } else return;
            }
            w.setBlock(blockX, blockY, blockZ, BlockModelsServer.modelBlock);
            TEBlockModelServer te = (TEBlockModelServer) w.getTileEntity(blockX, blockY, blockZ);

            te.addNewModelOnServer(model, new Vector3f(hitX, hitY, hitZ), new Vector3f(rotX, rotY, rotZ), scale, aabb, type);
            p.worldObj.playSoundEffect(x + 0.5F, y + 0.5F, z + 0.5F, BlockModels.modelBlock.stepSound.getBreakSound(), (BlockModels.modelBlock.stepSound.getVolume() + 1.0F) / 2.0F, BlockModels.modelBlock.stepSound.getPitch() * 0.8F);
            p.worldObj.markBlockForUpdate(blockX, blockY, blockZ);
        }
    }
}
