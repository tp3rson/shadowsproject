package ru.krogenit.advancedblockmodels.item;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.te.TEBlockModelServer;

public class ItemBlockModelServer extends ItemBlockModel {

    public ItemBlockModelServer(Block block) {
        super(block);
    }


    public void standartPlaceBlock(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int x,
                                   int y, int z, int side, float hitX, float hitY, float hitZ, Vector3f scale) {
        Block b = par3World.getBlock(x, y, z);
        if (b == Blocks.snow_layer && (par3World.getBlockMetadata(x, y, z) & 7) < 1) {
            side = 1;
        } else if (b != Blocks.vine && b != Blocks.tallgrass && field_150939_a != Blocks.deadbush
                && !b.isReplaceable(par3World, x, y, z)) {
            if (side == 0) {
                --y;
            }

            if (side == 1) {
                ++y;
            }

            if (side == 2) {
                --z;
            }

            if (side == 3) {
                ++z;
            }

            if (side == 4) {
                --x;
            }

            if (side == 5) {
                ++x;
            }
        }

        if (par1ItemStack.stackSize == 0 || !par2EntityPlayer.canPlayerEdit(x, y, z, side, par1ItemStack) || y == 255 && this.field_150939_a.getMaterial().isSolid()) {

        } else if (par3World.canPlaceEntityOnSide(this.field_150939_a, x, y, z, false, side, par2EntityPlayer,
                par1ItemStack)) {
            if (!par2EntityPlayer.capabilities.isCreativeMode)
                if (par2EntityPlayer.getCurrentEquippedItem() != null) {
                    par2EntityPlayer.getCurrentEquippedItem().stackSize--;
                    if (par2EntityPlayer.getCurrentEquippedItem().stackSize <= 0) {
                        par2EntityPlayer.inventory.mainInventory[par2EntityPlayer.inventory.currentItem] = null;
                    }
                } else
                    return;

            int j1 = this.getMetadata(par1ItemStack.getItemDamage());
            int k1 = this.field_150939_a.onBlockPlaced(par3World, x, y, z, side, hitX, hitY, hitZ, j1);

            if (placeBlockAt(par1ItemStack, par2EntityPlayer, par3World, x, y, z, side, hitX, hitY, hitZ, k1)) {
                par3World.playSoundEffect((double) ((float) x + 0.5F), (double) ((float) y + 0.5F),
                        (double) ((float) z + 0.5F), field_150939_a.stepSound.getBreakSound(),
                        (field_150939_a.stepSound.getVolume() + 1.0F) / 2.0F,
                        field_150939_a.stepSound.getPitch() * 0.8F);

                TEBlockModelServer tile = (TEBlockModelServer) par3World.getTileEntity(x, y, z);
                if (tile != null) {
                    float rotationY;
                    int l = MathHelper.floor_double((double) (par2EntityPlayer.rotationYaw * 8.0F / 360.0F) + 0.5D) & 7;
                    ++l;
                    l %= 8;
                    if (l == 0) {
                        rotationY = 225;
                    } else {
                        rotationY = 225 - 45 * l;
                    }

                    if (rotationY > 360)
                        rotationY -= 360;
                    else if (rotationY < 0)
                        rotationY += 360;

                    String patch = par1ItemStack.getTagCompound().getString("Model");
                    tile.addNewModelOnServer(patch, new Vector3f(0.5f, 0.5f, 0.5f), new Vector3f(0, rotationY, 0),
                            scale, null, BlockModel.ModelCollideType.STANDARD);
                    par3World.markBlockForUpdate(x, y, z);
                }
            }
        }
    }
}
