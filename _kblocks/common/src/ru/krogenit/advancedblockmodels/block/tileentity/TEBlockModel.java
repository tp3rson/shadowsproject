package ru.krogenit.advancedblockmodels.block.tileentity;

import lombok.Getter;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel.ModelCollideType;
import ru.krogenit.advancedblockmodels.block.BlockModels;

import java.util.*;

public class TEBlockModel extends TileEntity {

	public static final List<VectorAABB> posToAdd = Collections.synchronizedList(new ArrayList<>());
	public static final Map<String, ArrayList<TEModel>> partsToLoad = Collections.synchronizedMap(new HashMap<>());
	public static final List<TEModel> partsToRemove = Collections.synchronizedList(new ArrayList<>());

	public enum HelpPlaceType {
		LOAD, PLACE
    }

	public static class VectorAABB {
		public TEModel aabb;
		public Vector3f vect;
		public boolean complete;

		VectorAABB(TEModel aabb, Vector3f vect) {
			this.aabb = aabb;
			this.vect = vect;
		}
	}

	@Getter
	protected List<TEModel> parts = new ArrayList<>();
	@Getter
	protected List<TEModel> childParts = new ArrayList<>();

	public void placeHelpBlocks(World w, int basex, int basey, int basez, float newX, float newY, float newZ, TEModel part, HelpPlaceType ptype) {
		if (part.aabbCalculated) {
			AxisAlignedBB aabb = part.aabb;
			int minx = MathHelper.floor_double(aabb.minX + newX);
			int miny = MathHelper.floor_double(aabb.minY + newY);
			int minz = MathHelper.floor_double(aabb.minZ + newZ);
			int maxx = MathHelper.floor_double(aabb.maxX + newX) + 1;
			int maxy = MathHelper.floor_double(aabb.maxY + newY) + 1;
			int maxz = MathHelper.floor_double(aabb.maxZ + newZ) + 1;
			
			if (ptype == HelpPlaceType.LOAD) {
				for (int x = minx; x < maxx; x++)
					for (int y = miny; y < maxy; y++)
						for (int z = minz; z < maxz; z++) {
							if (x != basex || y != basey || z != basez) {
								loadTEHelp(w, part, x, y, z);
							}
						}
			} else {
				for (int x = minx; x < maxx; x++)
					for (int y = miny; y < maxy; y++)
						for (int z = minz; z < maxz; z++) {
							if (x != basex || y != basey || z != basez) {
								placeTEHelp(w, part, x, y, z);
							}
						}
			}
		}
	}

	public static boolean placeTEHelp(World w, TEModel teModel, int x, int y, int z, int i) {
		if((w.isRemote && w.getChunkFromBlockCoords(x, z).isChunkLoaded) || (!w.isRemote && w.blockExists(x, y, z))) {
			Block b = w.getBlock(x, y, z);
			if (b == BlockModels.modelBlock) {
				TileEntity tile = w.getTileEntity(x, y, z);

				if (tile != null) {
					if(tile instanceof TEBlockModel) {
						TEBlockModel te = (TEBlockModel) tile;
						if (!te.childParts.contains(teModel)) te.childParts.add(teModel);
						return true;
					} 
				}
			} else {
				return true;
			}
		}

		return false;
	}

	private void placeTEHelp(World w, TEModel teModel, int x, int y, int z) {
		TileEntity tile = w.getTileEntity(x, y, z);
		Block b = w.getBlock(x, y, z);
		
		if (tile instanceof TEBlockModel) {
			TEBlockModel te = (TEBlockModel) tile;
			if (!te.childParts.contains(teModel))
				te.childParts.add(teModel);
		} else if (b == Blocks.air && teModel.type != ModelCollideType.EMPTY) {
			w.setBlock(x, y, z, BlockModels.modelBlock);
			TEBlockModel te = (TEBlockModel) w.getTileEntity(x, y, z);

			if (!te.childParts.contains(teModel)) te.childParts.add(teModel);
		}
	}

	private void loadTEHelp(World w, TEModel aabb, int x, int y, int z) {
		synchronized (posToAdd) {
			posToAdd.add(new VectorAABB(aabb, new Vector3f(x, y, z)));
		}
	}

	public static boolean isAlreadyHave(ArrayList<Vector3f> list, Vector3f vect) {
		for (Vector3f vector3f : list) {
			if (vector3f.equals(vect)) return true;
		}
		return false;
	}
	
	private boolean isMarked = false;

	@Override
	public void markDirty() {
		super.markDirty();
		isMarked = true;

		for (TEModel child : childParts) {
			if (!child.te.isMarked)
				child.te.superMarkDirty();
		}

		isMarked = false;
	}
	
	private void superMarkDirty() {
		super.markDirty();
	}

	public boolean isSolid() {
		boolean isSolid = false;
		for (TEModel part : parts) {
			if (part.type == ModelCollideType.STANDARD) {
				isSolid = true;
				break;
			}
		}

		return isSolid;
	}

	@Override
	public void updateEntity() { }

	public boolean canUpdate() {
		return false;
	}
}
