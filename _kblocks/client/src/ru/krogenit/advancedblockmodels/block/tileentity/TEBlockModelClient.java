package ru.krogenit.advancedblockmodels.block.tileentity;

import com.google.common.io.ByteStreams;
import lombok.Getter;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.Settings;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.render.block.IBlockModelRenderer;
import ru.krogenit.math.MathRotateHelper;

import java.io.DataInput;
import java.util.ArrayList;
import java.util.List;

public class TEBlockModelClient extends TEBlockModel {

    private AxisAlignedBB aabbRender = INFINITE_EXTENT_AABB;
    private float renderDist;
    @Getter private boolean postRender;
    @Getter private final List<TEModelClient> partsWithPostRender = new ArrayList<>();

    public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
        if(pkt.func_148857_g().hasKey("M")) readDescriptionBytes(pkt.func_148857_g().getByteArray("M"));
    }

    private void readDescriptionBytes(byte[] input) {
        try {
            parts.clear();
            DataInput data = ByteStreams.newDataInput(input, 0);
            int count = data.readInt();
            for (int i = 0; i < count; i++) {
                TEModelClient part = new TEModelClient(data, this);
                parts.add(part);

                if(part.getRenderer().isPostRender()) {
                    postRender = true;
                    partsWithPostRender.add(part);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void refreshRenderBoxAndDist(TEModelClient part) {
        Vector3f offset = new Vector3f();
        IBlockModelRenderer renderer = part.getRenderer();
        AxisAlignedBB aabb = renderer.getRenderAABB(part);
        if (part.type == BlockModel.ModelCollideType.STANDARD) {
            Vector3f pos = renderer.getTranslate();
            Vector3f scale = renderer.getScale();
            offset = new Vector3f(pos.x * scale.x, pos.y * scale.y, pos.z * scale.z);
            MathRotateHelper.rotateAboutY(offset, Math.toRadians(part.rotation.y));
        }
        if (aabbRender == INFINITE_EXTENT_AABB) aabbRender = AxisAlignedBB.getBoundingBox(aabb.minX - 1 + offset.x + xCoord, aabb.minY - 1 + offset.y + yCoord, aabb.minZ - 1 + offset.z + zCoord, aabb.maxX + 1 + offset.x + xCoord, aabb.maxY + 1 + offset.y + yCoord, aabb.maxZ + 1 + offset.z + zCoord);
        else {
            if (aabb.minX + part.position.x - 1 + offset.x + xCoord < aabbRender.minX) aabbRender.minX = (aabb.minX + part.position.x - 1 + offset.x + xCoord);
            if (aabb.minY + part.position.y - 1 + offset.y + yCoord < aabbRender.minY) aabbRender.minY = (aabb.minY + part.position.y - 1 + offset.y + yCoord);
            if (aabb.minZ + part.position.z - 1 + offset.z + zCoord < aabbRender.minZ) aabbRender.minZ = (aabb.minZ + part.position.z - 1 + offset.z + zCoord);
            if (aabb.maxX + part.position.x + 1 + offset.x + xCoord > aabbRender.maxX) aabbRender.maxX = (aabb.maxX + part.position.x + 1 + offset.x + xCoord);
            if (aabb.maxY + part.position.y + 1 + offset.y + yCoord > aabbRender.maxY) aabbRender.maxY = (aabb.maxY + part.position.y + 1 + offset.y + yCoord);
            if (aabb.maxZ + part.position.z + 1 + offset.z + zCoord > aabbRender.maxZ) aabbRender.maxZ = (aabb.maxZ + part.position.z + 1 + offset.z + zCoord);
        }

        renderDist = (float) Math.sqrt(Math.pow(aabbRender.maxX - aabbRender.minX, 2) + Math.pow(aabbRender.maxY - aabbRender.minY, 2) + Math.pow(aabbRender.maxZ - aabbRender.minZ, 2));
        renderDist *= renderDist;
    }

    @Override
    public double getMaxRenderDistanceSquared() {
        return Settings.MODEL_RENDER_DISTANCE + renderDist;
    }

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return aabbRender;
    }
}
