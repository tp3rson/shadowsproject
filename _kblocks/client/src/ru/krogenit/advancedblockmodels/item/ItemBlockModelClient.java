package ru.krogenit.advancedblockmodels.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.PlacementHighlightHandler;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.BlockModels;
import ru.krogenit.advancedblockmodels.block.BlocksModelsClient;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.key.KeyModelPlaceType;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelClient;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelStaticClient;
import ru.krogenit.advancedblockmodels.render.block.IBlockModelRenderer;
import ru.krogenit.advancedblockmodels.render.block.RenderMetroModelPreview;
import ru.krogenit.model_loader.Model;
import ru.xlv.core.XlvsCore;

import java.util.List;

public class ItemBlockModelClient extends ItemBlockModel {

    public ItemBlockModelClient(Block block) {
        super(block);
    }

    @Override
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        if (PlacementHighlightHandler.modelPreview != null) {
            BlockModel.ModelCollideType type = KeyModelPlaceType.placeType;
            if (type == BlockModel.ModelCollideType.STANDARD) {
                if (!par2EntityPlayer.capabilities.isCreativeMode) {
                    par2EntityPlayer.getCurrentEquippedItem().stackSize--;
                    if (par2EntityPlayer.getCurrentEquippedItem().stackSize <= 0) {
                        par2EntityPlayer.inventory.mainInventory[par2EntityPlayer.inventory.currentItem] = null;
                    }
                }

                XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketTEModelStaticClient(x, y, z, side, hitX, hitY, hitZ,
                        PlacementHighlightHandler.modelPreview.getScale()));

                return true;
            } else {
                return placeMetroModelBlockAt(par1ItemStack, par2EntityPlayer, par3World, x, y, z, side, hitX, hitY,
                        hitZ, PlacementHighlightHandler.modelPreview.getScale());
            }
        } else
            return false;
    }

    @SideOnly(Side.CLIENT)
    private boolean placeMetroModelBlockAt(ItemStack stack, EntityPlayer player, World world, int x, int y, int z,
                                           int side, float hitX, float hitY, float hitZ, Vector3f scale) {
        if (stack == null || stack.getTagCompound() == null)
            return false;

        RenderMetroModelPreview preview = PlacementHighlightHandler.modelPreview;
        BlockModel.ModelCollideType type = preview.getType();
        Vec3 lastHit = Vec3.createVectorHelper(hitX + x, hitY + y, hitZ + z);

        double addX = preview.getAddPos().x;
        double addY = preview.getAddPos().y;
        double addZ = preview.getAddPos().z;

        if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            int fX = MathHelper.floor_double(lastHit.xCoord);
            int fY = MathHelper.floor_double(lastHit.yCoord);
            int fZ = MathHelper.floor_double(lastHit.zCoord);
            lastHit = Vec3.createVectorHelper(fX + 0.5D, fY, fZ + 0.5D);
        }
        Vector3f rotate = preview.getRotation();

        if (canPlaceBlockModel(preview, side, lastHit, world, rotate, scale)) {
            AxisAlignedBB aabb = preview.getRenderer().getAABB(rotate, scale);
            switch (side) {
                case 0:
                    if (aabb.maxY > 0)
                        addY -= (aabb.maxY + 0.001D);
                    else
                        addY += (aabb.maxY - 0.001D);
                    break;
                case 1:
                    if (aabb.minY < 0)
                        addY -= aabb.minY;
                    else
                        addY += aabb.minY;
                    break;
                case 2:
                    addZ -= aabb.maxZ;
                    break;
                case 3:
                    addZ -= aabb.minZ;
                    break;
                case 4:
                    addX -= aabb.maxX;
                    break;
                case 5:
                    addX -= aabb.minX;
                    break;
            }

            float newX = (float) (lastHit.xCoord + addX);
            float newY = (float) (lastHit.yCoord + addY);
            float newZ = (float) (lastHit.zCoord + addZ);

            int basex = MathHelper.floor_double(newX);
            int basey = MathHelper.floor_double(newY);
            int basez = MathHelper.floor_double(newZ);

            float partX = newX - basex;
            float partY = newY - basey;
            float partZ = newZ - basez;

            if (type == BlockModel.ModelCollideType.ADVANCED_NO_COLLISION)
                type = BlockModel.ModelCollideType.ADVANCED;

            if (!player.capabilities.isCreativeMode) {
                player.getCurrentEquippedItem().stackSize--;
                if (player.getCurrentEquippedItem().stackSize <= 0) {
                    player.inventory.mainInventory[player.inventory.currentItem] = null;
                }
            }
            if (rotate.x > 360)
                rotate.x -= 360;
            else if (rotate.x < 0)
                rotate.x += 360;
            if (rotate.y > 360)
                rotate.y -= 360;
            else if (rotate.y < 0)
                rotate.y += 360;
            if (rotate.z > 360)
                rotate.z -= 360;
            else if (rotate.z < 0)
                rotate.z += 360;

            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketTEModelClient(new Vector3f(newX, newY, newZ),
                    new Vector3f(partX, partY, partZ), rotate, preview.getScale(), stack.getTagCompound().getString("Model"), type, aabb));

            return true;
        } else {
            return false;
        }
    }

    @SideOnly(Side.CLIENT)
    public static boolean canPlaceBlockModel(RenderMetroModelPreview preview, int side, Vec3 hitVec,
                                             World w, Vector3f rot, Vector3f scale) {
        double addX = preview.getAddPos().x;
        double addY = preview.getAddPos().y;
        double addZ = preview.getAddPos().z;

        BlockModel.ModelCollideType type = preview.getType();
        IBlockModelRenderer renderer = preview.getRenderer();

        if (renderer != null && renderer.isModelsLoaded() && hitVec != null) {
            AxisAlignedBB aabb = renderer.getAABB(rot, scale);

            switch (side) {
                case 0:
                    if (aabb.maxY > 0)
                        addY -= (aabb.maxY + 0.001D);
                    else
                        addY += (aabb.maxY - 0.001D);
                    break;
                case 1:
                    if (aabb.minY < 0)
                        addY -= aabb.minY;
                    else
                        addY += aabb.minY;
                    break;
                case 2:
                    addZ -= aabb.maxZ;
                    break;
                case 3:
                    addZ -= aabb.minZ;
                    break;
                case 4:
                    addX -= aabb.maxX;
                    break;
                case 5:
                    addX -= aabb.minX;
                    break;
            }
            switch (type) {
                case ADVANCED:
                    AxisAlignedBB aabb1 = AxisAlignedBB.getBoundingBox(aabb.minX + hitVec.xCoord + addX + 0.001D,
                            aabb.minY + hitVec.yCoord + addY + 0.001D, aabb.minZ + hitVec.zCoord + addZ + 0.001D,
                            aabb.maxX + hitVec.xCoord + addX - 0.001D, aabb.maxY + hitVec.yCoord + addY - 0.001D,
                            aabb.maxZ + hitVec.zCoord + addZ - 0.001D);
                    List list = w.func_147461_a(aabb1);

                    float newX = (float) (hitVec.xCoord + addX);
                    float newY = (float) (hitVec.yCoord + addY);
                    float newZ = (float) (hitVec.zCoord + addZ);

                    Block b = w.getBlock(MathHelper.floor_double(newX), MathHelper.floor_double(newY),
                            MathHelper.floor_double(newZ));
                    return list.size() == 0 && (b == Blocks.air || b == BlockModels.modelBlock);
                case ADVANCED_NO_COLLISION:
                case EMPTY:
                    int x = MathHelper.floor_double(hitVec.xCoord + addX);
                    int y = MathHelper.floor_double(hitVec.yCoord + addY);
                    int z = MathHelper.floor_double(hitVec.zCoord + addZ);
                    Block b1 = w.getBlock(x, y, z);
                    TileEntity te = w.getTileEntity(x, y, z);

                    return b1 == Blocks.air || (te instanceof TEBlockModel);
            }
        }

        return false;
    }

    @Override
    public boolean func_150936_a(World w, int x, int y, int z, int side, EntityPlayer ply, ItemStack stack) {
        BlockModel.ModelCollideType type = KeyModelPlaceType.placeType;
        switch (type) {
            case ADVANCED:
            case EMPTY:
            case ADVANCED_NO_COLLISION:
                return true;
            case STANDARD:
                return super.func_150936_a(w, x, y, z, side, ply, stack);
            default:
                return false;
        }
    }

    public static ItemStack getStackWithName(String name) {
        for (int i = 0; i < BlocksModelsClient.names.size(); i++) {
            BlocksModelsClient.ModelPart part = BlocksModelsClient.names.get(i);
            if (part.getModel().equals(name)) {
                ItemStack rv = new ItemStack(BlockModels.modelBlock, 1, 0);
                rv.stackTagCompound = new NBTTagCompound();
                rv.stackTagCompound.setString("Model", part.getModel());
                rv.stackTagCompound.setString("Name", part.getName());

                return rv;
            }
        }
        return null;
    }

    @Override
    public void getSubItems(Item item, CreativeTabs tab, List list) {
        for(BlocksModelsClient.ModelPart part : BlocksModelsClient.names) {
            ItemStack stack = new ItemStack(field_150939_a, 1, 0);
            stack.stackTagCompound = new NBTTagCompound();
            stack.stackTagCompound.setString("Model", part.getModel());
            stack.stackTagCompound.setString("Name", part.getName());
            list.add(stack);
        }
    }

    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List list, boolean par4) {
        if (par1ItemStack.hasTagCompound() && par1ItemStack.getTagCompound().hasKey("Model")) {
            IBlockModelRenderer renderer = BlocksModelsClient.getRenderer(par1ItemStack.getTagCompound().getString("Model"));
            if (renderer != null) {
                Model model = renderer.getModel();
                list.add(model.getPath().substring(0, model.getPath().indexOf(".")));
                list.add(renderer.getType().toString());
            }
        }
    }
}
