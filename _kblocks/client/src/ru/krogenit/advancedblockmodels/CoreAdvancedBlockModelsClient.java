package ru.krogenit.advancedblockmodels;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import ru.krogenit.advancedblockmodels.block.BlockModels;
import ru.krogenit.advancedblockmodels.block.BlocksModelsClient;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.blockhandler.BHBlockModel;
import ru.krogenit.advancedblockmodels.gui.GuiChatBlockModelsEvent;
import ru.krogenit.advancedblockmodels.gui.GuiRenderHelper;
import ru.krogenit.advancedblockmodels.key.KeyBindings;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelClient;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelRemoveClient;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelStaticClient;
import ru.krogenit.advancedblockmodels.render.block.TERenderModel;
import ru.krogenit.advancedblockmodels.render.item.ItemBlockModelRenderer;
import ru.xlv.core.XlvsCore;

@Mod(modid = CoreAdvancedBlockModelsCommon.MODID, name = "Advanced Block Models Client", version = "1.0.0")
public class CoreAdvancedBlockModelsClient {

	@Getter private GuiChatBlockModelsEvent chat;

	public static int blockModelRenderID;

	@Instance(CoreAdvancedBlockModelsCommon.MODID)
	public static CoreAdvancedBlockModelsClient instance;

	@EventHandler
	@SuppressWarnings("unused")
	public void event(FMLInitializationEvent event) {
		XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(CoreAdvancedBlockModelsCommon.MODID,
				new PacketTEModelRemoveClient(),
				new PacketTEModelStaticClient(),
				new PacketTEModelClient());

		BlocksModelsClient.registerTileEntities();
		BlocksModelsClient.registerModelBlocks();

		registerKeyBindings();
		registerRenders();
		registerBlockHandlers();
		registerBlockRenders();

		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(BlockModels.modelBlock), new ItemBlockModelRenderer());

		FMLCommonHandler.instance().bus().register(new ClientTickHandlerBlocks());
		MinecraftForge.EVENT_BUS.register(new PlacementHighlightHandler());
		MinecraftForge.EVENT_BUS.register(new GuiRenderHelper());
		MinecraftForge.EVENT_BUS.register(new GuiEvents());
		chat = new GuiChatBlockModelsEvent();
		MinecraftForge.EVENT_BUS.register(chat);
	}

	private static void registerRenders() {
		blockModelRenderID = RenderingRegistry.getNextAvailableRenderId();
	}

	private static void registerBlockHandlers() {
		RenderingRegistry.registerBlockHandler(blockModelRenderID, new BHBlockModel());
	}

	private void registerBlockRenders() {
		ClientRegistry.bindTileEntitySpecialRenderer(TEBlockModel.class, new TERenderModel());
	}

	private void registerKeyBindings() {
		FMLCommonHandler.instance().bus().register(new KeyBindings().init());
	}
}
