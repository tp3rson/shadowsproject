package ru.krogenit.advancedblockmodels.render.item;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.IItemRenderer;
import ru.krogenit.advancedblockmodels.block.BlocksModelsClient;
import ru.krogenit.advancedblockmodels.render.block.IBlockModelRenderer;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;

public class ItemBlockModelRenderer implements IItemRenderer {
    private static final Minecraft mc = Minecraft.getMinecraft();

    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
        return true;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data) {
        NBTTagCompound tagCompound = itemStack.getTagCompound();
        if (tagCompound != null && tagCompound.hasKey("Model")) {
            String name = tagCompound.getString("Model");
            IBlockModelRenderer render = BlocksModelsClient.getRenderer(name);
            if(render != null) {
                switch (type) {
                    case EQUIPPED_FIRST_PERSON:
                        render.renderFirstPerson();
                        break;
                    case EQUIPPED:
                        render.render3rdPerson();
                        break;
                    case ENTITY:
                        render.renderAsEntity();
                        break;
                    case INVENTORY:
                        render.renderInventory();
                        ITextureObject iTextureObject = mc.renderEngine.getTexture(TextureMap.locationBlocksTexture);
                        glBindTexture(GL_TEXTURE_2D, iTextureObject.getGlTextureId());
                        break;
                }
            }
        }
    }

    @Override
    public void renderItemPost(ItemRenderType type, ItemStack itemStack, Object... data) {
        NBTTagCompound tagCompound = itemStack.getTagCompound();
        if (tagCompound != null && tagCompound.hasKey("Model")) {
            String name = tagCompound.getString("Model");
            IBlockModelRenderer render = BlocksModelsClient.getRenderer(name);
            if(render != null && render.isPostRender()) {
                switch (type) {
                    case EQUIPPED:
                        render.render3rdPersonPost();
                        break;
                    case ENTITY:
                        render.renderAsEntityPost();
                        break;
                }
            }
        }
    }


}
