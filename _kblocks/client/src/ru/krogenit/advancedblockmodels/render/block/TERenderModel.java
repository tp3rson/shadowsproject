package ru.krogenit.advancedblockmodels.render.block;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.BlocksModelsClient;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModelClient;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;
import ru.krogenit.advancedblockmodels.render.block.part.RenderModelAdvanced_ADVANCED;
import ru.krogenit.advancedblockmodels.render.block.part.RenderModelStatic_ADVANCED;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class TERenderModel extends TileEntitySpecialRenderer {

	private static final ITypeRenderer rmaa = new RenderModelAdvanced_ADVANCED();
	private static final ITypeRenderer rmsa = new RenderModelStatic_ADVANCED();
	private static final Minecraft mc = Minecraft.getMinecraft();

	public TERenderModel() {
		postRender = true;
	}

	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float timeTick) {
		this.renderTileEntity((TEBlockModel) tileEntity, x, y, z);
	}

	@Override
	public void renderTileEntityAtPost(TileEntity tileEntity, double x, double y, double z, float timeTick) {
		TEBlockModelClient te = (TEBlockModelClient) tileEntity;
		if(te.isPostRender()) {
			List<TEModelClient> parts = te.getPartsWithPostRender();
			for (TEModelClient part : parts) {
				part.getRender().renderPost(part);
			}
		}

		if(mc.thePlayer.capabilities.isCreativeMode) {
			//TODO: реализвать отдельный проход с этим состоянием
			IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
			shader.useLighting(false);
			shader.setLightMapping(false);
			shader.setUseTexture(false);
			GL11.glEnable(GL_BLEND);
			GL11.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			renderTileEntityUtils(tileEntity, x, y, z);
			GL11.glEnable(GL_TEXTURE_2D);
			GL11.glDisable(GL_BLEND);
			shader.useLighting(true);
			shader.setLightMapping(true);
			shader.setUseTexture(true);
		}
	}

	public void renderTileEntityUtils(TileEntity tile, double d1, double d2, double d3) {
		glPushMatrix();
		glTranslatef((float) d1, (float) d2, (float) d3);
		TEBlockModel te = (TEBlockModel) tile;
		for (TEModel part : te.getParts()) {
			((TEModelClient)part).getRender().renderUtils((TEModelClient)part);
		}
		glPopMatrix();
	}

	private void renderTileEntity(TEBlockModel te, double d1, double d2, double d3) {
		glPushMatrix();
		glTranslatef((float)d1, (float)d2, (float)d3);
		render(te);
		glPopMatrix();
	}

	public void render(TEBlockModel te) {
		List<TEModel> parts = te.getParts();
		for (TEModel teModel : parts) {
			TEModelClient part = (TEModelClient) teModel;
			part.getRender().render(part);
		}
	}

	public static ITypeRenderer getRenderer(BlockModel.ModelCollideType type) {
		if (type == BlockModel.ModelCollideType.STANDARD) return rmsa;
		return rmaa;
	}

	private static final Vector3f min = new Vector3f();
	private static final Vector3f max = new Vector3f();
	private static final Vector3f zero = new Vector3f();
	private static final Vector3f one = new Vector3f();

	public static void renderDebug(Model model) {
		IBlockModelRenderer renderer = BlocksModelsClient.getRenderer(model.getPath());
		if (!renderer.isModelsLoaded())
			return;
		AxisAlignedBB aabb = renderer.getAABB(zero, one);
		max.x = (float) aabb.maxX;
		max.y = (float) aabb.maxY;
		max.z = (float) aabb.maxZ;
		min.x = (float) aabb.minX;
		min.y = (float) aabb.minY;
		min.z = (float) aabb.minZ;
		Tessellator t = Tessellator.instance;
		t.startDrawing(GL_LINES);
		t.setColorRGBA_F(1f, 0.25f, 0.25f, 0.5f);
		t.addVertex(max.x, min.y, min.z);
		t.addVertex(max.x, min.y, max.z);
		t.addVertex(max.x, min.y, max.z);
		t.addVertex(min.x, min.y, max.z);
		t.addVertex(max.x, min.y, min.z);
		t.addVertex(min.x, min.y, min.z);
		t.addVertex(min.x, min.y, min.z);
		t.addVertex(min.x, min.y, max.z);
		t.addVertex(min.x, min.y, max.z);
		t.addVertex(min.x, max.y, max.z);
		t.addVertex(min.x, max.y, max.z);
		t.addVertex(max.x, max.y, max.z);
		t.addVertex(max.x, max.y, max.z);
		t.addVertex(max.x, min.y, max.z);
		t.addVertex(min.x, max.y, max.z);
		t.addVertex(min.x, max.y, min.z);
		t.addVertex(min.x, max.y, min.z);
		t.addVertex(min.x, min.y, min.z);
		t.addVertex(min.x, max.y, min.z);
		t.addVertex(max.x, max.y, min.z);
		t.addVertex(max.x, max.y, min.z);
		t.addVertex(max.x, max.y, max.z);
		t.addVertex(max.x, max.y, min.z);
		t.addVertex(max.x, min.y, min.z);
		t.draw();
	}

	public static void renderBaseAABB(TEModelClient part) {
		if (part.getAabb() == null || part.getType() == BlockModel.ModelCollideType.EMPTY) {
			glLineWidth(4f);
			Tessellator t = Tessellator.instance;
			t.startDrawing(GL_LINES);
			t.setColorRGBA_F(0.5f, 0.5f, 0.5f, 0.5f);
			t.addVertex(1, 0, 0);
			t.addVertex(1, 0, 1);
			t.addVertex(1, 0, 1);
			t.addVertex(0, 0, 1);
			t.addVertex(1, 0, 0);
			t.addVertex(0, 0, 0);
			t.addVertex(0, 0, 0);
			t.addVertex(0, 0, 1);
			t.addVertex(0, 0, 1);
			t.addVertex(0, 1, 1);
			t.addVertex(0, 1, 1);
			t.addVertex(1, 1, 1);
			t.addVertex(1, 1, 1);
			t.addVertex(1, 0, 1);
			t.addVertex(0, 1, 1);
			t.addVertex(0, 1, 0);
			t.addVertex(0, 1, 0);
			t.addVertex(0, 0, 0);
			t.addVertex(0, 1, 0);
			t.addVertex(1, 1, 0);
			t.addVertex(1, 1, 0);
			t.addVertex(1, 1, 1);
			t.addVertex(1, 1, 0);
			t.addVertex(1, 0, 0);
			t.draw();
			glLineWidth(2f);
		}
	}

	public static void renderAABB(Model model, float rotX, float rotY, float rotZ, Vector3f scale) {
		IBlockModelRenderer renderer = BlocksModelsClient.getRenderer(model.getPath());
		if (!renderer.isModelsLoaded())
			return;
		Tessellator t = Tessellator.instance;
		t.startDrawing(GL_LINES);
		t.setColorRGBA_F(1f, 0.25f, 0.25f, 0.5f);

		min.x = rotX;
		min.y = rotY;
		min.z = rotZ;
		AxisAlignedBB aabb = renderer.getAABB(min, scale);
		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.draw();
	}
}