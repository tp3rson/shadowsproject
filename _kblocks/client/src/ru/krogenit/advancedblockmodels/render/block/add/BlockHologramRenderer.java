package ru.krogenit.advancedblockmodels.render.block.add;

import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.CoreAdvancedBlockModelsCommon;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.krogenit.utils.AnimationHelper;

public class BlockHologramRenderer extends AbstractBlockModelRenderer {

    private final TextureDDS textureNormal;
    private final TextureDDS textureDiffuse;
    private final TextureDDS textureSpecular;
    private final TextureDDS textureEmission;
    private final TextureDDS textureGloss;

    private final TextureDDS holoDiff;

    public BlockHologramRenderer(String modelName, String name, BlockModel.ModelRenderType type) {
        super(type, new Model(new ResourceLocation(modelName)));
//        ResourceLocationStateful.Builder builder = ResourceLocationStateful.Builder.builder().withDomain(CoreAdvancedBlockModelsCommon.MODID).temporary();
//        this.textureDiffuse = builder.of("textures/props/" + name + "/Diffuse.dds");
//        this.textureNormal = builder.of("textures/props/" + name + "/Normal.dds");
//        this.textureSpecular = builder.of("textures/props/" + name + "/Specular.dds");
//        this.textureEmission = builder.of("textures/props/" + name + "/Emissive.dds");
//        this.textureGloss = builder.of("textures/props/" + name + "/Gloss.dds");
//        this.holoDiff = builder.of("textures/props/" + name + "/hologram/Diffuse.dds");

        this.textureDiffuse = new TextureDDS(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID, "textures/props/" + name + "/Diffuse.dds"));
        this.textureNormal = new TextureDDS(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID, "textures/props/" + name + "/Normal.dds"));
        this.textureSpecular = new TextureDDS(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID, "textures/props/" + name + "/Specular.dds"));
        this.textureEmission = new TextureDDS(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID, "textures/props/" + name + "/Emissive.dds"));
        this.textureGloss = new TextureDDS(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID, "textures/props/" + name + "/Gloss.dds"));
        this.holoDiff = new TextureDDS(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID, "textures/props/" + name + "/hologram/Diffuse.dds"));
    }

    private void updateAnimation(TEModelClient part) {
        Vector3f animation = part.getAnimation();
        if(animation.x < 1.0f) {
            animation.x += AnimationHelper.getAnimationSpeed() * 0.001f;
            if(animation.x > 1f) {
                animation.x -= 1f;
            }
        } else {
            animation.x -= 1f;
        }
    }

    @Override
    public void renderPost(TEModelClient part) {

    }

    @Override
    public void render(TEModelClient part) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(textureDiffuse);
        TextureLoaderDDS.bindNormalMap(textureNormal, shader);
        TextureLoaderDDS.bindSpecularMap(textureSpecular, shader);
        TextureLoaderDDS.bindGlossMap(textureGloss, shader);
        TextureLoaderDDS.bindEmissionMap(textureEmission, shader, 1.5f);
        model.renderPart("Table_Cylinder", shader);
        shader.setGlossMapping(false);
        shader.setSpecularMapping(false);
        shader.setNormalMapping(false);
        TextureLoaderDDS.unbind();
        TextureLoaderDDS.bindTexture(holoDiff);
        TextureLoaderDDS.bindEmissionMap(holoDiff, shader, 1.75f);
        GL11.glPushMatrix();
        if(part != null) {
            updateAnimation(part);
            GL11.glRotatef(part.getAnimation().x*360f,0,1,0);
        }
        model.renderPart("Hologram_Sphere.007", shader);
        GL11.glPopMatrix();
        shader.setEmissionMapping(false);
        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }
}
