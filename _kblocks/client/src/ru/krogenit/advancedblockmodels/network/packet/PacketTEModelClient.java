package ru.krogenit.advancedblockmodels.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketTEModelClient implements IPacketOut {
    private Vector3f pos, hitVec, rotVec;
    private Vector3f scale;
    private String path;
    private BlockModel.ModelCollideType type;
    private AxisAlignedBB aabb;

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeFloat(pos.x);
        data.writeFloat(pos.y);
        data.writeFloat(pos.z);
        data.writeFloat(hitVec.x);
        data.writeFloat(hitVec.y);
        data.writeFloat(hitVec.z);
        data.writeFloat(rotVec.x);
        data.writeFloat(rotVec.y);
        data.writeFloat(rotVec.z);
        data.writeFloat(scale.x);
        data.writeFloat(scale.y);
        data.writeFloat(scale.z);
        data.writeUTF(path);
        data.writeByte(type.toByte());
        if (aabb != null) {
            data.writeBoolean(true);
            data.writeFloat((float) aabb.minX);
            data.writeFloat((float) aabb.minY);
            data.writeFloat((float) aabb.minZ);
            data.writeFloat((float) aabb.maxX);
            data.writeFloat((float) aabb.maxY);
            data.writeFloat((float) aabb.maxZ);
        } else data.writeBoolean(false);
    }
}
