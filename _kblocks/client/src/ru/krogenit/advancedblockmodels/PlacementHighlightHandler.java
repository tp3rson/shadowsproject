package ru.krogenit.advancedblockmodels;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.common.util.ForgeDirection;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel.ModelCollideType;
import ru.krogenit.advancedblockmodels.block.BlocksModelsClient;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;
import ru.krogenit.advancedblockmodels.item.ItemBlockModel;
import ru.krogenit.advancedblockmodels.item.ItemBlockModelClient;
import ru.krogenit.advancedblockmodels.key.KeyModelPlaceType;
import ru.krogenit.advancedblockmodels.render.block.RenderMetroModelPreview;
import ru.krogenit.shaders.GL;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.krogenit.utils.AnimationHelper;

import java.util.List;

public class PlacementHighlightHandler {

	@SubscribeEvent
	public void event(DrawBlockHighlightEvent evt) {
		MovingObjectPosition pos = evt.target;
		int pass = evt.subID;
		EntityPlayer p = evt.player;
		ItemStack holding = evt.currentItem;

		if(!p.capabilities.isCreativeMode) {
			evt.setCanceled(true);
			return;
		}

		if (pos.typeOfHit != MovingObjectType.BLOCK) return;
		if (pass != 0) return;

		AxisAlignedBB hitBB = null;
		TileEntity te = p.worldObj.getTileEntity(pos.blockX, pos.blockY, pos.blockZ);

		if (te instanceof TEBlockModel) {
			if (pos.hitInfo instanceof TEModel) {
				TEModel part = (TEModel) pos.hitInfo;
				AxisAlignedBB aabb = part.getAabb();
				if (aabb != null) {
					Vector3f position = part.getPosition();
					TEBlockModel te1 = part.getTe();
					double dx = (te1.xCoord - te.xCoord) + position.x;
					double dy = (te1.yCoord - te.yCoord) + position.y;
					double dz = (te1.zCoord - te.zCoord) + position.z;
					hitBB = AxisAlignedBB.getBoundingBox(aabb.minX + dx, aabb.minY + dy, aabb.minZ + dz, aabb.maxX + dx, aabb.maxY + dy, aabb.maxZ + dz);
				}
			}
		}

		if (holding != null) {
			Item item = holding.getItem();
			if (item instanceof ItemBlockModel && holding.hasTagCompound() && holding.getTagCompound().hasKey("Model")) {
				renderPreviewMetroModel(holding, pos);
			}
		}

		if (hitBB != null) {
			drawSelectionBox(p, pos, evt.partialTicks, hitBB.offset(pos.blockX, pos.blockY, pos.blockZ));
			evt.setCanceled(true);
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD9)) {
			GL11.glDisable(GL11.GL_DEPTH_TEST);
			GL11.glDepthMask(true);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
			if (te instanceof TEBlockModel) {
				TEBlockModel tem = (TEBlockModel) te;
				List<TEModel> parts = tem.getParts();
				for (TEModel part : parts) {
					TEBlockModel te1 = part.getTe();
					Vector3f position = part.getPosition();
					float xsize = 0.075f;
					float ysize = 0.075f;
					float zsize = 0.075f;
					GL11.glColor4f(0.4f, 1f, 0.4f, 0.5f);
					GL11.glPushMatrix();
					GL11.glTranslatef(te1.xCoord + position.x - (float) TileEntityRendererDispatcher.staticPlayerX,
							te1.yCoord + position.y - (float) TileEntityRendererDispatcher.staticPlayerY,
							te1.zCoord + position.z - (float) TileEntityRendererDispatcher.staticPlayerZ);
					GL11.glBegin(GL11.GL_QUADS);
					// up
					GL11.glNormal3f(0, 1, 0);
					GL11.glVertex3f(xsize, ysize, zsize);
					GL11.glVertex3f(xsize, ysize, -zsize);
					GL11.glVertex3f(-xsize, ysize, -zsize);
					GL11.glVertex3f(-xsize, ysize, zsize);
					// down
					GL11.glNormal3f(0, -1, 0);
					GL11.glVertex3f(-xsize, -ysize, zsize);
					GL11.glVertex3f(-xsize, -ysize, -zsize);
					GL11.glVertex3f(xsize, -ysize, -zsize);
					GL11.glVertex3f(xsize, -ysize, zsize);
					// side
					GL11.glNormal3f(1, 0, 0);
					GL11.glVertex3f(xsize, ysize, -zsize);
					GL11.glVertex3f(xsize, ysize, zsize);
					GL11.glVertex3f(xsize, -ysize, zsize);
					GL11.glVertex3f(xsize, -ysize, -zsize);

					GL11.glNormal3f(-1, 0, 0);
					GL11.glVertex3f(-xsize, ysize, zsize);
					GL11.glVertex3f(-xsize, ysize, -zsize);
					GL11.glVertex3f(-xsize, -ysize, -zsize);
					GL11.glVertex3f(-xsize, -ysize, zsize);

					GL11.glNormal3f(0, 0, 1);
					GL11.glVertex3f(xsize, ysize, zsize);
					GL11.glVertex3f(-xsize, ysize, zsize);
					GL11.glVertex3f(-xsize, -ysize, zsize);
					GL11.glVertex3f(xsize, -ysize, zsize);

					GL11.glNormal3f(0, 0, -1);
					GL11.glVertex3f(-xsize, ysize, -zsize);
					GL11.glVertex3f(xsize, ysize, -zsize);
					GL11.glVertex3f(xsize, -ysize, -zsize);
					GL11.glVertex3f(-xsize, -ysize, -zsize);
					GL11.glEnd();
					GL11.glPopMatrix();
				}

				for (TEModel part : tem.getChildParts()) {
					float xsize = 0.075f;
					float ysize = 0.075f;
					float zsize = 0.075f;
					GL11.glColor4f(0.4f, 0.4f, 1f, 0.5f);
					GL11.glPushMatrix();
					TEBlockModel te1 = part.getTe();
					Vector3f position = part.getPosition();
					GL11.glTranslatef(te1.xCoord + position.x - (float) TileEntityRendererDispatcher.staticPlayerX,
							te1.yCoord + position.y - (float) TileEntityRendererDispatcher.staticPlayerY,
							te1.zCoord + position.z - (float) TileEntityRendererDispatcher.staticPlayerZ);
					GL11.glBegin(GL11.GL_QUADS);
					// up
					GL11.glNormal3f(0, 1, 0);
					GL11.glVertex3f(xsize, ysize, zsize);
					GL11.glVertex3f(xsize, ysize, -zsize);
					GL11.glVertex3f(-xsize, ysize, -zsize);
					GL11.glVertex3f(-xsize, ysize, zsize);
					// down
					GL11.glNormal3f(0, -1, 0);
					GL11.glVertex3f(-xsize, -ysize, zsize);
					GL11.glVertex3f(-xsize, -ysize, -zsize);
					GL11.glVertex3f(xsize, -ysize, -zsize);
					GL11.glVertex3f(xsize, -ysize, zsize);
					// side
					GL11.glNormal3f(1, 0, 0);
					GL11.glVertex3f(xsize, ysize, -zsize);
					GL11.glVertex3f(xsize, ysize, zsize);
					GL11.glVertex3f(xsize, -ysize, zsize);
					GL11.glVertex3f(xsize, -ysize, -zsize);

					GL11.glNormal3f(-1, 0, 0);
					GL11.glVertex3f(-xsize, ysize, zsize);
					GL11.glVertex3f(-xsize, ysize, -zsize);
					GL11.glVertex3f(-xsize, -ysize, -zsize);
					GL11.glVertex3f(-xsize, -ysize, zsize);

					GL11.glNormal3f(0, 0, 1);
					GL11.glVertex3f(xsize, ysize, zsize);
					GL11.glVertex3f(-xsize, ysize, zsize);
					GL11.glVertex3f(-xsize, -ysize, zsize);
					GL11.glVertex3f(xsize, -ysize, zsize);

					GL11.glNormal3f(0, 0, -1);
					GL11.glVertex3f(-xsize, ysize, -zsize);
					GL11.glVertex3f(xsize, ysize, -zsize);
					GL11.glVertex3f(xsize, -ysize, -zsize);
					GL11.glVertex3f(-xsize, -ysize, -zsize);
					GL11.glEnd();
					GL11.glPopMatrix();
				}
			}

			float xsize = 0.2f;
			float ysize = 0.2f;
			float zsize = 0.2f;
			GL11.glColor4f(0.5f, 0.5f, 0.5f, 0.5f);
			GL11.glPushMatrix();
			GL11.glTranslatef(pos.blockX - (float) TileEntityRendererDispatcher.staticPlayerX + 0.5f,
					pos.blockY - (float) TileEntityRendererDispatcher.staticPlayerY + 0.5f,
					pos.blockZ - (float) TileEntityRendererDispatcher.staticPlayerZ + 0.5f);
			GL11.glBegin(GL11.GL_QUADS);
			// up
			GL11.glNormal3f(0, 1, 0);
			GL11.glVertex3f(xsize, ysize, zsize);
			GL11.glVertex3f(xsize, ysize, -zsize);
			GL11.glVertex3f(-xsize, ysize, -zsize);
			GL11.glVertex3f(-xsize, ysize, zsize);
			// down
			GL11.glNormal3f(0, -1, 0);
			GL11.glVertex3f(-xsize, -ysize, zsize);
			GL11.glVertex3f(-xsize, -ysize, -zsize);
			GL11.glVertex3f(xsize, -ysize, -zsize);
			GL11.glVertex3f(xsize, -ysize, zsize);
			// side
			GL11.glNormal3f(1, 0, 0);
			GL11.glVertex3f(xsize, ysize, -zsize);
			GL11.glVertex3f(xsize, ysize, zsize);
			GL11.glVertex3f(xsize, -ysize, zsize);
			GL11.glVertex3f(xsize, -ysize, -zsize);

			GL11.glNormal3f(-1, 0, 0);
			GL11.glVertex3f(-xsize, ysize, zsize);
			GL11.glVertex3f(-xsize, ysize, -zsize);
			GL11.glVertex3f(-xsize, -ysize, -zsize);
			GL11.glVertex3f(-xsize, -ysize, zsize);

			GL11.glNormal3f(0, 0, 1);
			GL11.glVertex3f(xsize, ysize, zsize);
			GL11.glVertex3f(-xsize, ysize, zsize);
			GL11.glVertex3f(-xsize, -ysize, zsize);
			GL11.glVertex3f(xsize, -ysize, zsize);

			GL11.glNormal3f(0, 0, -1);
			GL11.glVertex3f(-xsize, ysize, -zsize);
			GL11.glVertex3f(xsize, ysize, -zsize);
			GL11.glVertex3f(xsize, -ysize, -zsize);
			GL11.glVertex3f(-xsize, -ysize, -zsize);
			GL11.glEnd();
			GL11.glPopMatrix();

			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glEnable(GL11.GL_DEPTH_TEST);
		}
	}

	private void drawSelectionBox(EntityPlayer par1EntityPlayer, MovingObjectPosition par2MovingObjectPosition, float par5, AxisAlignedBB box) {
		if (par2MovingObjectPosition.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK) {
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.4F);
			GL11.glLineWidth(2.0F);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glDepthMask(false);
			float var6 = 0.002F;

			double var8 = par1EntityPlayer.lastTickPosX + (par1EntityPlayer.posX - par1EntityPlayer.lastTickPosX) * (double) par5;
			double var10 = par1EntityPlayer.lastTickPosY + (par1EntityPlayer.posY - par1EntityPlayer.lastTickPosY) * (double) par5;
			double var12 = par1EntityPlayer.lastTickPosZ + (par1EntityPlayer.posZ - par1EntityPlayer.lastTickPosZ) * (double) par5;
			drawOutlinedBoundingBox(box.expand(var6, var6, var6).getOffsetBoundingBox(-var8, -var10, -var12));

			GL11.glDepthMask(true);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glDisable(GL11.GL_BLEND);
		}
	}

	private void drawOutlinedBoundingBox(AxisAlignedBB par1AxisAlignedBB) {
		Tessellator var2 = Tessellator.instance;
		var2.startDrawing(3);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
		var2.draw();
		var2.startDrawing(3);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
		var2.draw();
		var2.startDrawing(1);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
		var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
		var2.draw();
	}

	public static RenderMetroModelPreview modelPreview;
	private static Minecraft mc = Minecraft.getMinecraft();

	private static boolean isMouseClick, isKeyPress;
	private static int mouseX, mouseY;

	private static void renderPreviewMetroModel(ItemStack item, MovingObjectPosition pos) {
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDepthMask(false);

		ModelCollideType type = KeyModelPlaceType.placeType;
		item.getTagCompound().setInteger("ModelType", type.toByte());

		boolean isFull = (type == ModelCollideType.ADVANCED || type == ModelCollideType.EMPTY || type == ModelCollideType.ADVANCED_NO_COLLISION);

		if ((Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) && !(mc.currentScreen instanceof GuiChat)) || type == ModelCollideType.STANDARD) {
			int fX = MathHelper.floor_double(pos.hitVec.xCoord);
			int fY = MathHelper.floor_double(pos.hitVec.yCoord);
			int fZ = MathHelper.floor_double(pos.hitVec.zCoord);
			pos.hitVec = Vec3.createVectorHelper(fX + 0.5D, fY, fZ + 0.5D);
		}

		String modelName = item.getTagCompound().getString("Model");

		if (modelPreview == null) {
			modelPreview = new RenderMetroModelPreview(modelName);
		} else if (modelPreview.getRenderer() != null && !modelPreview.getRenderer().getModel().getPath().equals(modelName)) {
			modelPreview.setRenderer(BlocksModelsClient.getRenderer(modelName));
		}

		modelPreview.setType(type);

		previewMove();

		double x = pos.hitVec.xCoord - TileEntityRendererDispatcher.staticPlayerX;
		double y = pos.hitVec.yCoord - TileEntityRendererDispatcher.staticPlayerY;
		double z = pos.hitVec.zCoord - TileEntityRendererDispatcher.staticPlayerZ;

		Vector3f addPos = modelPreview.getAddPos();
		double addX = addPos.x;
		double addY = addPos.y;
		double addZ = addPos.z;

		Vector3f scale = modelPreview.getScale();

		if (modelPreview.getRenderer() != null && modelPreview.getRenderer().isModelsLoaded() && isFull) {
			AxisAlignedBB aabb = modelPreview.getRenderer().getAABB(modelPreview.getRotation(), scale);

			switch (pos.sideHit) {
				case 0:
					addY -= aabb.maxY;
					break;
				case 1:
					addY -= aabb.minY;
					break;
				case 2:
					addZ -= aabb.maxZ;
					break;
				case 3:
					addZ -= aabb.minZ;
					break;
				case 4:
					addX -= aabb.maxX;
					break;
				case 5:
					addX -= aabb.minX;
					break;
			}
		} else {
			switch (pos.sideHit) {
				case 0:
					addY -= 1;
					break;
				case 2:
					addZ -= 1;
					break;
				case 4:
					addX -= 1;
					break;
			}
		}

		IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
		shader.setUseTexture(false);
		shader.setLightMapping(false);

		if (isFull) {
			Vector3f rotate = modelPreview.getRotation();
			if (ItemBlockModelClient.canPlaceBlockModel(modelPreview, pos.sideHit, pos.hitVec, mc.theWorld, rotate, scale)) {
				GL.color(0.5f, 1f, 0.5f, 0.25f);
			} else {
				GL.color(1f, 0.5f, 0.5f, 0.25f);
			}
			previewRotate();
		} else {
			int l = MathHelper.floor_double((double) (mc.thePlayer.rotationYaw * 8.0F / 360.0F) + 0.5D) & 7;
			++l;
			l %= 8;
			if (l == 0) {
				modelPreview.getRotation().y = 225;
			} else {
				modelPreview.getRotation().y = 225 - 45 * l;
			}

			if (modelPreview.getRotation().y > 360) modelPreview.getRotation().y -= 360;
			else if (modelPreview.getRotation().y < 0) modelPreview.getRotation().y += 360;

			GL.color(0.5f, 0.5f, 0.5f, 0.25f);
		}

		if (type == ModelCollideType.STANDARD) {
			x = MathHelper.floor_double(pos.hitVec.xCoord) - MathHelper.floor_double(TileEntityRendererDispatcher.staticPlayerX);
			y = MathHelper.floor_double(pos.hitVec.yCoord) - MathHelper.floor_double(TileEntityRendererDispatcher.staticPlayerY);
			z = MathHelper.floor_double(pos.hitVec.zCoord) - MathHelper.floor_double(TileEntityRendererDispatcher.staticPlayerZ);
			double testX = MathHelper.floor_double(TileEntityRendererDispatcher.staticPlayerX) - TileEntityRendererDispatcher.staticPlayerX;
			double testY = MathHelper.floor_double(TileEntityRendererDispatcher.staticPlayerY) - TileEntityRendererDispatcher.staticPlayerY;
			double testZ = MathHelper.floor_double(TileEntityRendererDispatcher.staticPlayerZ) - TileEntityRendererDispatcher.staticPlayerZ;
			modelPreview.render(x + testX + addX, y + testY + addY, z + testZ + addZ, shader);
		} else {
			modelPreview.render(x + addX, y + addY, z + addZ, shader);
		}

		GL.color(1f, 1f, 1f, 1f);
		shader.setUseTexture(true);
		shader.setLightMapping(true);
		KrogenitShaders.finishCurrentShader();
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDepthMask(true);
	}

	private static void previewMove() {
		if (mc.currentScreen instanceof GuiChat && modelPreview.getType() != ModelCollideType.STANDARD) {
			float animSpeed = AnimationHelper.getAnimationSpeed() / 10f;
			if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
				animSpeed *= 5;
			}

			ForgeDirection dir = getDirection(mc.thePlayer);
			if (Keyboard.isKeyDown(Keyboard.KEY_R)) {
				modelPreview.getAddPos().y += 0.1f * animSpeed;
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_F)) {
				modelPreview.getAddPos().y -= 0.1f * animSpeed;
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
				if (dir == ForgeDirection.NORTH) modelPreview.getAddPos().x -= 0.1f * animSpeed;
				else if (dir == ForgeDirection.SOUTH) modelPreview.getAddPos().x += 0.1f * animSpeed;
				else if (dir == ForgeDirection.EAST) modelPreview.getAddPos().z -= 0.1f * animSpeed;
				else if (dir == ForgeDirection.WEST) modelPreview.getAddPos().z += 0.1f * animSpeed;
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
				if (dir == ForgeDirection.NORTH) modelPreview.getAddPos().x += 0.1f * animSpeed;
				else if (dir == ForgeDirection.SOUTH) modelPreview.getAddPos().x -= 0.1f * animSpeed;
				else if (dir == ForgeDirection.EAST) modelPreview.getAddPos().z += 0.1f * animSpeed;
				else if (dir == ForgeDirection.WEST) modelPreview.getAddPos().z -= 0.1f * animSpeed;
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
				if (dir == ForgeDirection.NORTH) modelPreview.getAddPos().z -= 0.1f * animSpeed;
				else if (dir == ForgeDirection.SOUTH) modelPreview.getAddPos().z += 0.1f * animSpeed;
				else if (dir == ForgeDirection.EAST) modelPreview.getAddPos().x += 0.1f * animSpeed;
				else if (dir == ForgeDirection.WEST) modelPreview.getAddPos().x -= 0.1f * animSpeed;
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
				if (dir == ForgeDirection.NORTH) modelPreview.getAddPos().z += 0.1f * animSpeed;
				else if (dir == ForgeDirection.SOUTH) modelPreview.getAddPos().z -= 0.1f * animSpeed;
				else if (dir == ForgeDirection.EAST) modelPreview.getAddPos().x -= 0.1f * animSpeed;
				else if (dir == ForgeDirection.WEST) modelPreview.getAddPos().x += 0.1f * animSpeed;
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
			}
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_RMENU)) {
			modelPreview.setAddPos(new Vector3f());
			modelPreview.setScale(new Vector3f(1, 1, 1));
			if (mc.currentScreen instanceof GuiChat)
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
		}
	}

	private static ForgeDirection getDirection(EntityPlayer p) {
		int heading = MathHelper.floor_double((double) (p.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;

		switch (Direction.directions[heading]) {
			case "EAST":
				return ForgeDirection.EAST;
			case "SOUTH":
				return ForgeDirection.SOUTH;
			case "WEST":
				return ForgeDirection.WEST;
			default:
				return ForgeDirection.NORTH;
		}
	}

	private static void previewRotate() {
		if (mc.currentScreen instanceof GuiChat) {
			if (Mouse.isButtonDown(0)) {
				if (!isMouseClick) {
					isMouseClick = true;
					mouseX = Mouse.getX();
					mouseY = Mouse.getY();
				} else {
					int newmouseX = Mouse.getX();
					int newmouseY = Mouse.getY();

					if ((newmouseX != mouseX || newmouseY != mouseY) && Keyboard.isKeyDown(Keyboard.KEY_TAB)) {
						int difXZ = Mouse.getX() - mouseX;
						int difY = Mouse.getY() - mouseY;

						modelPreview.getScale().x += difXZ / 100f;
						modelPreview.getScale().z += difXZ / 100f;
						modelPreview.getScale().y += difY / 100f;
					} else {
						if (newmouseX != mouseX && !Keyboard.isKeyDown(Keyboard.KEY_LCONTROL))
							modelPreview.getRotation().y += (float) (Mouse.getX() - mouseX);
						if (newmouseY != mouseY && !Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
							int dif = Mouse.getY() - mouseY;

							double cos = Math.cos(Math.toRadians(modelPreview.getRotation().y));
							double sin = Math.sin(Math.toRadians(modelPreview.getRotation().y));
							modelPreview.getRotation().x -= dif * cos;
							modelPreview.getRotation().z -= dif * sin;
						}
					}

					if (modelPreview.getRotation().x > 360) modelPreview.getRotation().x -= 360;
					if (modelPreview.getRotation().y > 360) modelPreview.getRotation().y -= 360;
					if (modelPreview.getRotation().z > 360) modelPreview.getRotation().z -= 360;
					if (modelPreview.getRotation().x < 0) modelPreview.getRotation().x += 360;
					if (modelPreview.getRotation().y < 0) modelPreview.getRotation().y += 360;
					if (modelPreview.getRotation().z < 0) modelPreview.getRotation().z += 360;
					if (modelPreview.getScale().x > 2) modelPreview.getScale().x = 2;
					else if (modelPreview.getScale().x < 0.25f) modelPreview.getScale().x = 0.25f;
					if (modelPreview.getScale().y > 2) modelPreview.getScale().y = 2;
					else if (modelPreview.getScale().y < 0.25f) modelPreview.getScale().y = 0.25f;
					if (modelPreview.getScale().z > 2) modelPreview.getScale().z = 2;
					else if (modelPreview.getScale().z < 0.25f) modelPreview.getScale().z = 0.25f;

					mouseX = newmouseX;
					mouseY = newmouseY;
					CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
				}
			} else {
				isMouseClick = false;
			}
		}

		float animSpeed = AnimationHelper.getAnimationSpeed();
		boolean left = false;
		boolean right = false;
		boolean down = false;
		boolean up = false;
		boolean pd = false;
		boolean pu = false;
		float dif = 0;
		float speed = 1f;
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) left = true;
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) right = true;
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) down = true;
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) up = true;
		if (Keyboard.isKeyDown(Keyboard.KEY_PRIOR)) pu = true;
		if (Keyboard.isKeyDown(Keyboard.KEY_NEXT)) pd = true;
		if (!left && !right && !down && !up && !pu && !pd) isKeyPress = false;

		if (left) if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			if (!isKeyPress) {
				modelPreview.getRotation().y += 45;
				isKeyPress = true;
			}
		} else {
			modelPreview.getRotation().y += speed * animSpeed;
		}
		if (right) if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			if (!isKeyPress) {
				modelPreview.getRotation().y -= 45;
				isKeyPress = true;
			}
		} else {
			modelPreview.getRotation().y -= speed * animSpeed;
		}
		if (down) if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			if (!isKeyPress) {
				modelPreview.getRotation().x += 45;
				isKeyPress = true;
			}
		} else modelPreview.getRotation().x += speed * animSpeed;
		if (up) if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			if (!isKeyPress) {
				modelPreview.getRotation().x -= 45;
				isKeyPress = true;
			}
		} else modelPreview.getRotation().x -= speed * animSpeed;
		if (pu) if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			if (!isKeyPress) {
				modelPreview.getRotation().z -= 45;
				isKeyPress = true;
			}
		} else {
			modelPreview.getRotation().z -= speed * animSpeed;
		}
		if (pd) if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			if (!isKeyPress) {
				modelPreview.getRotation().z += 45;
				isKeyPress = true;
			}
		} else {
			modelPreview.getRotation().z += speed * animSpeed;
		}

		if (down || left || pd || pu || right || up) if (mc.currentScreen instanceof GuiChat)
			CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();

		if (isKeyPress) {
			double cos = Math.cos(Math.toRadians(modelPreview.getRotation().y));
			double sin = Math.sin(Math.toRadians(modelPreview.getRotation().y));
			modelPreview.getRotation().x -= dif * cos;
			modelPreview.getRotation().z -= dif * sin;
			if (mc.currentScreen instanceof GuiChat)
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_LMENU)) {
			modelPreview.getRotation().x = 0;
			modelPreview.getRotation().y = 0;
			modelPreview.getRotation().z = 0;
			if (mc.currentScreen instanceof GuiChat)
				CoreAdvancedBlockModelsClient.instance.getChat().updateModelInfo();
		}
	}
}
