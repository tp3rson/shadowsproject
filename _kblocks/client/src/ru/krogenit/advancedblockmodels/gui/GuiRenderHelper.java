package ru.krogenit.advancedblockmodels.gui;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import ru.krogenit.advancedblockmodels.CoreAdvancedBlockModelsCommon;
import ru.krogenit.advancedblockmodels.PlacementHighlightHandler;
import ru.krogenit.advancedblockmodels.item.ItemBlockModel;
import ru.krogenit.utils.Utils;

public class GuiRenderHelper {
	private static boolean keyPress, isNotRender;
	private Minecraft mc = Minecraft.getMinecraft();

	private static final ResourceLocation resLocKeyT = new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_t.png");
	private static final ResourceLocation resLocMouseLeft2 = new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/mouse_left2.png");
	private static final ResourceLocation resLocRCtrlKey = new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/rctrl_key.png");
	private static final ResourceLocation resLocKeyTab = new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_tab.png");
	private static final ResourceLocation resLocLShiftKey= new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/lshift_key.png");
	private static final ResourceLocation resLocLCtrlKey = new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/lctrl_key.png");

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void event(RenderGameOverlayEvent.Pre event) {
		if(event.type == ElementType.HOTBAR) {
			if(mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBlockModel)
				 renderModelPlaceControls(event.resolution.getScaledWidth(), event.resolution.getScaledHeight(), mc.fontRenderer);
		}
	}
	
	private static void renderModelPlaceControls(int width, int height, FontRenderer fontrenderer) {
		if (Keyboard.isKeyDown(Keyboard.KEY_RCONTROL)) {
			if (!keyPress) {
				keyPress = true;
				isNotRender = !isNotRender;
			}
		} else
			keyPress = false;
		if (isNotRender)
			return;
		GL11.glPushMatrix();
		GL11.glDepthMask(false);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glColor4f(0, 0, 0, 0.5f);
		int xpos = -214;
		int ypos = 142;
		int x = width / 2 + xpos;
		int y = height - ypos;
		int xSize = -xpos * 2;
		int ySize = 94;
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x = width / 2 + xpos;
		y = height - ypos + 14;
		xSize = 16;
		ySize = 16;

		GL11.glColor4f(1, 1, 1, 1f);
		GL11.glEnable(GL11.GL_TEXTURE_2D);

		String s = "Режим установки модели - ";
		if (PlacementHighlightHandler.modelPreview != null && PlacementHighlightHandler.modelPreview.getType() != null) {
			s += PlacementHighlightHandler.modelPreview.getType().toString();
		}

		fontrenderer.drawStringWithShadow(s, width / 2f - fontrenderer.getStringWidth(s) / 2f, y - 12, Integer.MAX_VALUE);

		Utils.bindTexture(resLocKeyT);
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		fontrenderer.drawStringWithShadow(" - Открыть чат для", x + 20, y, Integer.MAX_VALUE);
		fontrenderer.drawStringWithShadow("редактирования", x + 26, y + 7, Integer.MAX_VALUE);
		Utils.bindTexture(resLocMouseLeft2);
		GuiRenderHelper.renderElement(x + 110, y, xSize, ySize);
		fontrenderer.drawStringWithShadow(" - Вращение мышью", x + 126, y + 4, Integer.MAX_VALUE);
		Utils.bindTexture(resLocRCtrlKey);
		GuiRenderHelper.renderElement(x + 216, y, xSize + 5, ySize);
		fontrenderer.drawStringWithShadow(" - Скрыть/показать подсказку", x + 240, y + 4, Integer.MAX_VALUE);
		Utils.bindTexture(resLocKeyTab);
		GuiRenderHelper.renderElement(x + 366, y, xSize + 5, ySize);
		fontrenderer.drawStringWithShadow(" - Размер", x + 388, y, Integer.MAX_VALUE);
		fontrenderer.drawStringWithShadow("мышью", x + 392, y + 7, Integer.MAX_VALUE);
		y += 16;
		Utils.bindTexture(resLocLShiftKey);
		GuiRenderHelper.renderElement(x, y, xSize + 20, ySize);
		fontrenderer.drawStringWithShadow(" - Горизонтальное вращение мышью", x + 40, y + 4, Integer.MAX_VALUE);
		Utils.bindTexture(resLocLCtrlKey);
		GuiRenderHelper.renderElement(x + 200, y, xSize + 5, ySize);
		fontrenderer.drawStringWithShadow(" - Вертикальное вращение мышью", x + 225, y + 4, Integer.MAX_VALUE);
		y += 16;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/lshift_key.png"));
		GuiRenderHelper.renderElement(x, y, xSize + 20, ySize);
		fontrenderer.drawStringWithShadow(" - Ускорение перемещения клавиатурой", x + 40, y + 4, Integer.MAX_VALUE);
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/lshift_key.png"));
		GuiRenderHelper.renderElement(x + 212, y, xSize + 20, ySize);
		fontrenderer.drawStringWithShadow(" - Вращение по 45 градусов клавиатурой", x + 252, y + 4, Integer.MAX_VALUE);
		y += 16;

		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_w.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_a.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_s.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_d.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_f.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_r.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		fontrenderer.drawStringWithShadow(" - Перемещение", x + 20, y + 4, Integer.MAX_VALUE);
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_left.png"));
		x += 90;
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_right.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_up.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_down.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_pageup.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		x += xSize;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/key_pagedown.png"));
		GuiRenderHelper.renderElement(x, y, xSize, ySize);
		fontrenderer.drawStringWithShadow(" - Вращение", x + 20, y + 4, Integer.MAX_VALUE);
		y += 16;
		x -= 250;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/ralt_key.png"));
		GuiRenderHelper.renderElement(x, y, xSize + 5, ySize);
		fontrenderer.drawStringWithShadow(" - Сброс перемещения", x + 25, y + 4, Integer.MAX_VALUE);
		x += 125;
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/lalt_key.png"));
		GuiRenderHelper.renderElement(x, y, xSize + 5, ySize);
		fontrenderer.drawStringWithShadow(" - Сброс вращения", x + 25, y + 4, Integer.MAX_VALUE);
		Utils.bindTexture(new ResourceLocation(CoreAdvancedBlockModelsCommon.MODID + ":textures/gui/icons/lshift_key.png"));
		GuiRenderHelper.renderElement(x + 108, y, xSize + 20, ySize);
		fontrenderer.drawStringWithShadow(" - Установка в центр блока", x + 148, y + 4, Integer.MAX_VALUE);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDepthMask(true);
		GL11.glPopMatrix();
	}

	private static void renderElement(float x, float y, float width, float height) {
		Tessellator t = Tessellator.instance;
		t.startDrawingQuads();
		t.addVertexWithUV(x, (double) y + height, 1, 0, 1.0F);
		t.addVertexWithUV(x + width, (double) y + height, 1, 1, 1.0F);
		t.addVertexWithUV(x + width, y, 1, 1, 0.0F);
		t.addVertexWithUV(x, y, 1, 0, 0.0F);
		t.draw();
	}
}
