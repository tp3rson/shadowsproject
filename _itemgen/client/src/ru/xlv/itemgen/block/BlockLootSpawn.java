package ru.xlv.itemgen.block;

import net.minecraft.block.BlockChest;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import ru.xlv.itemgen.tile.TileEntityLootSpawn;

public class BlockLootSpawn extends BlockChest {

    public BlockLootSpawn(String name) {
        super(0);
        setBlockTextureName(name);
        setBlockName(name);
        setCreativeTab(CreativeTabs.tabMisc);
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return new TileEntityLootSpawn();
    }
}
