package ru.xlv.itemgen.event;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.itemgen.tile.TileEntityLootSpawn;
import ru.xlv.mochar.inventory.MatrixInventoryController;
import ru.xlv.mochar.network.matrix.PacketMatrixInventoryTransactionGuiOpen;
import ru.xlv.mochar.network.matrix.PacketMatrixInventoryTransactionSync;

public class OpenLootSpawnEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void event(PlayerInteractEvent event) {
        if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK) {
            TileEntity tileEntity = event.world.getTileEntity(event.x, event.y, event.z);
            if (tileEntity instanceof TileEntityLootSpawn) {
                ((TileEntityLootSpawn) tileEntity).interact();
                MatrixInventoryController.INSTANCE.openMatrixInventory(event.entityPlayer, (IMatrixInventoryProvider) tileEntity);
                MatrixInventory transactionMatrixInventory = MatrixInventoryController.INSTANCE.getTransactionMatrixInventory(event.entityPlayer);
                if (transactionMatrixInventory != null) {
                    FlexPlayer.of(event.entityPlayer)
                            .notNull()
                            .sendPacket(new PacketMatrixInventoryTransactionGuiOpen(event.x, event.y, event.z))
                            .sendPacket(new PacketMatrixInventoryTransactionSync(transactionMatrixInventory));
                }
                event.setCanceled(true);
            }
        }
    }
}
