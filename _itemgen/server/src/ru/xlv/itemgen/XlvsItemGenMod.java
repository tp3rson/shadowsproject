package ru.xlv.itemgen;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import lombok.Getter;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.item.ItemStackFactory;
import ru.xlv.itemgen.block.BlockRegistry;
import ru.xlv.itemgen.event.OpenLootSpawnEventListener;
import ru.xlv.itemgen.tile.TileEntityLootSpawn;
import ru.xlv.itemgen.util.LootSpawnConfig;

import static ru.xlv.itemgen.XlvsItemGenMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
@Getter
public class XlvsItemGenMod {

    static final String MODID = "itemgen";

    @Mod.Instance(MODID)
    public static XlvsItemGenMod INSTANCE;

    private final LootSpawnConfig config = new LootSpawnConfig();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        config.load();
        BlockRegistry.register();
        GameRegistry.registerTileEntity(TileEntityLootSpawn.class, "TileEntityLootSpawn");
        CommonUtils.registerEvents(new OpenLootSpawnEventListener());
    }

    @Mod.EventHandler
    public void event(FMLPostInitializationEvent event) {
        LootSpawnConfig.LootSpawnModel lootSpawnConfig;
        int i = 0;
        while((lootSpawnConfig = config.getLootSpawnConfig(i)) != null) {
            lootSpawnConfig.getItems().forEach(itemStackModel -> {
                ItemStack itemStack = ItemStackFactory.create(itemStackModel.getUnlocalizedName(), itemStackModel.getAmount(), itemStackModel.getMetadata(), itemStackModel.getNbtJson());
                if(itemStack == null || itemStack.getItem() == null) {
                    try {
                        throw new RuntimeException("Bad generated item found: " + itemStackModel.getUnlocalizedName() + ". More:" + config);
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }
                }
            });
            i++;
        }
    }
}
