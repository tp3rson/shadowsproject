package ru.xlv.group.common;

import lombok.Data;

@Data
public class GroupInvite<K> {

    private final AbstractGroup<K> group;
    private final String target;
    private final long creationTimeMills;

}
