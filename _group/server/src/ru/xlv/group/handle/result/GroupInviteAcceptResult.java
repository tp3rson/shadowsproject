package ru.xlv.group.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.group.XlvsGroupMod;

@Getter
@RequiredArgsConstructor
public enum GroupInviteAcceptResult {

    SUCCESS(XlvsGroupMod.INSTANCE.getLocalization().responseAcceptInviteSuccessMessage),
    GROUP_IS_FULL(XlvsGroupMod.INSTANCE.getLocalization().responseAcceptInviteGroupIsFullMessage),
    INVITER_NOT_FOUND_OR_OFFLINE(XlvsGroupMod.INSTANCE.getLocalization().responseAcceptInviteInviterNotFoundOrOfflineMessage),
    UNKNOWN_ERROR(XlvsGroupMod.INSTANCE.getLocalization().responseAcceptInviteUnknownErrorMessage);

    private final String responseMessage;
}
