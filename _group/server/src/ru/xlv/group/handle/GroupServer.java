package ru.xlv.group.handle;

import ru.xlv.core.player.ServerPlayer;
import ru.xlv.group.common.AbstractGroup;

public class GroupServer extends AbstractGroup<ServerPlayer> {

    public GroupServer(ServerPlayer leader) {
        super(leader);
    }
}
