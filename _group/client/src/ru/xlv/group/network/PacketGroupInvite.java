package ru.xlv.group.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketGroupInvite implements IPacketCallbackEffective<String> {

    private String targetName, responseMessage;

    public PacketGroupInvite(String targetName) {
        this.targetName = targetName;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(targetName);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        responseMessage = bbis.readUTF();
    }

    @Nullable
    @Override
    public String getResult() {
        return responseMessage;
    }
}
