package ru.xlv.group.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.group.XlvsGroupMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketGroupInviteNew implements IPacketIn {
    @Override
    public void read(ByteBufInputStream data) throws IOException {
        XlvsGroupMod.INSTANCE.getGroupHandler().handleIncomingInvite(data.readUTF());
    }
}
