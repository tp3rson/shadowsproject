package ru.xlv.group;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.XlvsCore;
import ru.xlv.group.common.Group;
import ru.xlv.group.handle.GroupHandler;
import ru.xlv.group.network.*;

import static ru.xlv.group.XlvsGroupMod.MODID;

@Mod(
        name = "XvlsGroupMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsGroupMod {

    static final String MODID = "xlvsgroups";

    @Mod.Instance(MODID)
    public static XlvsGroupMod INSTANCE;

    @Setter
    @Getter
    private Group group;

    @Getter
    private GroupHandler groupHandler;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        groupHandler = new GroupHandler();
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketGroupChangeLeader(),
                new PacketGroupInvite(),
                new PacketGroupInviteAccept(),
                new PacketGroupKick(),
                new PacketGroupSync(),
                new PacketGroupInviteNew()
        );
    }
}
