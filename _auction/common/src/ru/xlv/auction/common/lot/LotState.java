package ru.xlv.auction.common.lot;

import javax.annotation.Nullable;

public enum LotState {

    ACTIVE,
    CANCELED,
    COMPLETED,
    TIMEOUT;

    public static boolean equalsOne(LotState lotState, @Nullable LotState... lotStates) {
        if(lotStates == null || lotStates.length == 0) return true;
        for (LotState state : lotStates) {
            if(state == lotState) return true;
        }
        return false;
    }
}
