package ru.xlv.auction.database;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;
import com.mongodb.client.result.UpdateResult;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import org.bson.Document;
import org.bson.conversions.Bson;
import ru.xlv.auction.common.lot.AuctionBet;
import ru.xlv.auction.common.lot.LotState;
import ru.xlv.auction.handle.lot.AuctionLot;
import ru.xlv.core.database.mongodb.ConfigMongoDB;
import ru.xlv.core.util.IConverter;
import ru.xlv.core.database.IDatabaseEventListener;
import ru.xlv.core.database.mongodb.MongoProvider;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import static com.mongodb.client.model.Filters.*;

public class DatabaseManager {

    private static final String LOT_UUID_KEY = "uuid";
    private static final String LOT_SELLER_KEY = "seller";
    private static final String LOT_COST_KEY = "cost";
    private static final String LOT_LOT_STATE_KEY = "lotState";
    private static final String LOT_TIMESTAMP_KEY = "timeStamp";
    private static final String LOT_ITEM_STACK_KEY = "itemStack";
    private static final String LOT_BETS_KEY = "bets";
    private static final String LOT_ID_KEY = "_id";

    private final IConverter<AuctionLot, Document> AUCTION_LOT_CONVERTER = new IConverter<AuctionLot, Document>() {
        @Override
        public Document convertTo(AuctionLot auctionLot) {
            NBTTagCompound nbtTagCompound = auctionLot.getItemStack().writeToNBT(new NBTTagCompound());
            return new Document()
                    .append(LOT_UUID_KEY, auctionLot.getUuid().toString())
                    .append(LOT_SELLER_KEY, auctionLot.getSeller())
                    .append(LOT_COST_KEY, auctionLot.getStartCost())
                    .append(LOT_LOT_STATE_KEY, auctionLot.getLotState().ordinal())
                    .append(LOT_TIMESTAMP_KEY, auctionLot.getTimestamp())
                    .append(LOT_ITEM_STACK_KEY, nbtTagCompound.toString());
        }

        @Override
        public AuctionLot convertFrom(Document document) {
            try {
                UUID uuid = UUID.fromString(document.getString(LOT_UUID_KEY));
                String seller = document.getString(LOT_SELLER_KEY);
                int cost = document.getInteger(LOT_COST_KEY);
                LotState lotState = LotState.values()[document.getInteger(LOT_LOT_STATE_KEY)];
                long timeStamp = document.getLong(LOT_TIMESTAMP_KEY);
                ItemStack itemStack = ItemStack.loadItemStackFromNBT((NBTTagCompound) JsonToNBT.func_150315_a(document.getString(LOT_ITEM_STACK_KEY)));
                AuctionLot auctionLot = new AuctionLot(uuid, seller, itemStack, cost, timeStamp);
                auctionLot.set_id(document.getString(LOT_ID_KEY));
                auctionLot.setLotState(lotState);
                return auctionLot;
            } catch (NBTException e) {
                e.printStackTrace();
            }
            return null;
        }
    };

    private ExecutorService executorService;

    private MongoProvider mongoProvider;
    private MongoCollection<Document> lotCollection;

    public void init(IDatabaseEventListener<AuctionLot> databaseEventListener) {
        executorService = Executors.newFixedThreadPool(16);
        mongoProvider = new MongoProvider(new ConfigMongoDB("config/auction/database.json"));
        mongoProvider.init();
        lotCollection = mongoProvider.getDatabase().getCollection("active");
        executorService.submit(() -> lotCollection.watch(
                Collections.singletonList(
                        Aggregates.match(
                                Filters.in("operationType", Arrays.asList("insert", "update", "replace", "delete"))
                        )
                )
        ).fullDocument(FullDocument.UPDATE_LOOKUP).forEach((Consumer<? super ChangeStreamDocument<Document>>) documentChangeStreamDocument -> {
            switch (documentChangeStreamDocument.getOperationType()) {
                case UPDATE:
                    databaseEventListener.onUpdate(AUCTION_LOT_CONVERTER.convertFrom(documentChangeStreamDocument.getFullDocument()));
                    break;
                case REPLACE:
                case DELETE:
                    databaseEventListener.onDelete(AUCTION_LOT_CONVERTER.convertFrom(documentChangeStreamDocument.getFullDocument()));
                    break;
                case INSERT:
                    databaseEventListener.onInsert(AUCTION_LOT_CONVERTER.convertFrom(documentChangeStreamDocument.getFullDocument()));
            }
        }));
    }

    public void shutdown() {
        mongoProvider.shutdown();
        executorService.shutdown();
    }

    public CompletableFuture<AuctionLot> placeLot(AuctionLot auctionLot) {
        return CompletableFuture.supplyAsync(() -> {
            Document document = AUCTION_LOT_CONVERTER.convertTo(auctionLot);
            if (document != null) {
                lotCollection.insertOne(document);
                Document result = lotCollection.find(eq(LOT_UUID_KEY, auctionLot.getUuid().toString())).first();
                return AUCTION_LOT_CONVERTER.convertFrom(result);
            }
            return null;
        }, executorService);
    }

    public CompletableFuture<Boolean> addBet(AuctionLot auctionLot, AuctionBet auctionBet) {
        return CompletableFuture.supplyAsync(() -> {
            List<AuctionBet> auctionBets = new ArrayList<>(auctionLot.getBets());
            auctionBets.add(auctionBet);
            UpdateResult updateResult = lotCollection.updateOne(eq(LOT_UUID_KEY, auctionLot.getUuid().toString()), new Document("$set", new Document(LOT_BETS_KEY, auctionBets)));
            return updateResult.getModifiedCount() > 0;
        }, executorService);
    }

    public CompletableFuture<List<AuctionLot>> getAllLots(String username, LotState... lotStates) {//pizdec
        return CompletableFuture.supplyAsync(() -> {
            List<Bson> bsons = new ArrayList<>();
            bsons.add(eq(LOT_SELLER_KEY, username));
            if(lotStates != null) {
                List<Bson> list = new ArrayList<>();
                for (LotState lotState : lotStates) {
                    list.add(eq(LOT_LOT_STATE_KEY, lotState.ordinal()));
                }
                bsons.add(or(list));
            }
            MongoCursor<Document> iterator = lotCollection.find(and(bsons)).iterator();
            List<AuctionLot> auctionLots = new ArrayList<>();
            while (iterator.hasNext()) {
                Document next = iterator.next();
                auctionLots.add(AUCTION_LOT_CONVERTER.convertFrom(next));
            }
            iterator.close();
            return auctionLots;
        }, executorService);
    }

    public CompletableFuture<List<AuctionLot>> getAllLots() {
        return CompletableFuture.supplyAsync(() -> {
            List<AuctionLot> auctionLots = new ArrayList<>();
            lotCollection.find().forEach((Consumer<? super Document>) document -> auctionLots.add(AUCTION_LOT_CONVERTER.convertFrom(document)));
            return auctionLots;
        }, executorService);
    }
}
