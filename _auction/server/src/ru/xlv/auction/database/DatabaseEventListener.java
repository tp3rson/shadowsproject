package ru.xlv.auction.database;

import ru.xlv.auction.XlvsAuctionMod;
import ru.xlv.auction.handle.lot.AuctionLot;
import ru.xlv.core.database.IDatabaseEventListener;

import java.util.Iterator;

public class DatabaseEventListener implements IDatabaseEventListener<AuctionLot> {
    @Override
    public void onInsert(AuctionLot object) {
        XlvsAuctionMod.INSTANCE.getAuctionHandler().getAuctionLotList().add(object);
    }

    @Override
    public void onDelete(AuctionLot object) {
        Iterator<AuctionLot> iterator = XlvsAuctionMod.INSTANCE.getAuctionHandler().getAuctionLotList().iterator();
        while (iterator.hasNext()) {
            AuctionLot next = iterator.next();
            if(next.equals(object)) {
                XlvsAuctionMod.INSTANCE.getAuctionHandler().returnLot(next).thenAccept(auctionReturnResult -> iterator.remove());
                break;
            }
        }
        XlvsAuctionMod.INSTANCE.getAuctionHandler().getAuctionLotList().removeIf(object::equals);
    }

    @Override
    public void onUpdate(AuctionLot object) {
        for (AuctionLot auctionLot : XlvsAuctionMod.INSTANCE.getAuctionHandler().getAuctionLotList()) {
            if(auctionLot.equals(object)) {
                auctionLot.getBets().clear();
                auctionLot.getBets().addAll(object.getBets());
                break;
            }
        }
    }
}
