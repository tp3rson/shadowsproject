package ru.xlv.auction.handle.lot;

import ru.xlv.auction.XlvsAuctionMod;

public enum TermType {

    FEW_DAYS(XlvsAuctionMod.INSTANCE.getConfig().termTypeFewDaysModifier),
    ONE_WEEK(XlvsAuctionMod.INSTANCE.getConfig().tempTypeOneWeekModifier),
    TWO_WEEKS(XlvsAuctionMod.INSTANCE.getConfig().tempTypeTwoWeeksModifier);

    private final double modifier;

    TermType(double modifier) {
        this.modifier = modifier;
    }

    public double getModifier() {
        return modifier;
    }
}
