//package ru.xlv.auction.handle.lot.result;
//
//import ru.xlv.auction.common.util.AuctionLocalization;
//
//public enum AuctionBuyResult {
//
//    SUCCESS(AuctionLocalization.RESPONSE_BUY_SUCCESS),
//    NOT_ENOUGH_MONEY(AuctionLocalization.RESPONSE_BUY_NOT_ENOUGH_MONEY),
//    DATABASE_ERROR(AuctionLocalization.RESPONSE_BUY_DATABASE_ERROR),
//    UNKNOWN(AuctionLocalization.RESPONSE_BUY_UNKNOWN),
//    NOT_ACTIVE_LOT(AuctionLocalization.RESPONSE_BUY_NOT_ACTIVE_LOT),
//    NO_TARGET_CATEGORY(AuctionLocalization.RESPONSE_BUY_NO_TARGET_CATEGORY),
//    CANNOT_REMOVE_LOT(AuctionLocalization.RESPONSE_BUY_CANNOT_REMOVE_LOT),
//    BUYER_NOT_FOUND(AuctionLocalization.RESPONSE_BUY_BUYER_NOT_FOUND),
//    SELLER_NOT_FOUND(AuctionLocalization.RESPONSE_BUY_SELLER_NOT_FOUND),
//    WITHDRAW_ERROR(AuctionLocalization.RESPONSE_BUY_WITHDRAW_ERROR),
//    DEPOSIT_ERROR(AuctionLocalization.RESPONSE_BUY_DEPOSIT_ERROR);
//
//    private final String message;
//
//    AuctionBuyResult(String message) {
//        this.message = message;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//}
