package ru.xlv.auction.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.xlv.auction.common.lot.SimpleAuctionLot;
import ru.xlv.auction.common.util.AuctionUtils;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketAuctionLotsSync implements IPacketCallbackEffective<PacketAuctionLotsSync.Result> {

    @Getter
    @Setter
    public static class Result {
        private boolean isSuccess;
        private List<SimpleAuctionLot> auctionLots;
        private String responseMessage;
    }

    private Result result;

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        boolean success = bbis.readBoolean();
        result = new Result();
        result.setSuccess(success);
        if(success) {
            List<SimpleAuctionLot> list = AuctionUtils.readAuctionLotList(bbis);
            result.setAuctionLots(list);
        } else {
            String responseMessage = bbis.readUTF();
            result.setResponseMessage(responseMessage);
        }
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {}

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }
}
