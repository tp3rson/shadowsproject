package ru.krogenit.guns.util;

import io.netty.buffer.ByteBufOutputStream;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.ItemMeleeWeapon;
import ru.xlv.core.common.item.quality.EnumItemQuality;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public class GunUtils {

    public static <T extends ItemGun> void writeGun(T itemGun, ByteBufOutputStream data) throws IOException {
        try {
            data.writeUTF(itemGun.getName());
            data.writeUTF(itemGun.getDisplayName());
            data.writeInt(itemGun.getCharacterAttributeBoostMap().size());
            for (CharacterAttributeType characterAttributeType : itemGun.getCharacterAttributeBoostMap().keySet()) {
                data.writeInt(characterAttributeType.ordinal());
                data.writeDouble(itemGun.getCharacterAttributeBoostMap().get(characterAttributeType).getValueAdd());
            }
            data.writeInt(itemGun.getRate());
            data.writeInt(itemGun.getMaxDamage());
            data.writeFloat(itemGun.getKnockback());
            data.writeFloat(itemGun.getReloadSpeed());
            data.writeFloat(itemGun.getUnloadSpeed());
            data.writeInt(itemGun.getBaseAccuracy().length);
            for (float i : itemGun.getBaseAccuracy()) {
                data.writeFloat(i);
            }
            data.writeInt(itemGun.getItemQuality().ordinal());
            data.writeInt(itemGun.getItemRarity().ordinal());
            data.writeUTF(itemGun.getDescription());
            data.writeInt(itemGun.getItemTags().size());
            for (EnumItemTag itemTag : itemGun.getItemTags()) {
                data.writeInt(itemTag.ordinal());
            }
        } catch (Exception e) {
            System.out.println("Ошибка отправки конфига оружия: " + itemGun.getUnlocalizedName());
            throw new IOException();
        }
    }

    public static void readGuns(ByteBufInputStream data) throws IOException {
        String name = data.readUTF();
        for (ItemGun itemGun : CoreGunsCommon.registeredGunList) {
            if(itemGun.getName().equals(name)) {
                itemGun.setDisplayName(data.readUTF());
                itemGun.getCharacterAttributeBoostMap().clear();
                itemGun.getItemTags().clear();
                int c = data.readInt();
                for (int i = 0; i < c; i++) {
                    CharacterAttributeType value = CharacterAttributeType.values()[data.readInt()];
                    CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                            .attributeType(value)
                            .valueAdd(data.readDouble())
                            .build();
                    itemGun.getCharacterAttributeBoostMap().put(value, characterAttributeMod);
                }
                itemGun.setRate(data.readInt());
                itemGun.setMaxDamage(data.readInt());
                itemGun.setKnockback(data.readFloat());
                itemGun.setReloadSpeed(data.readFloat());
                itemGun.setUnloadSpeed(data.readFloat());
                c = data.readInt();
                float[] ii = new float[c];
                for (int i = 0; i < c; i++) {
                    ii[i] = data.readFloat();
                }
                itemGun.setAccuracy(ii);
                itemGun.setItemQuality(EnumItemQuality.values()[data.readInt()]);
                itemGun.setItemRarity(EnumItemRarity.values()[data.readInt()]);
                itemGun.setDescription(data.readUTF());
                c = data.readInt();
                for (int i = 0; i < c; i++) {
                    itemGun.getItemTags().add(EnumItemTag.values()[data.readInt()]);
                }
                break;
            }
        }
    }

    public static <T extends ItemMeleeWeapon> void writeMelee(T itemGun, ByteBufOutputStream data) throws IOException {
        try {
            data.writeUTF(itemGun.getName());
            data.writeUTF(itemGun.getDisplayName());
            data.writeInt(itemGun.getCharacterAttributeBoostMap().size());
            for (CharacterAttributeType characterAttributeType : itemGun.getCharacterAttributeBoostMap().keySet()) {
                data.writeInt(characterAttributeType.ordinal());
                data.writeDouble(itemGun.getCharacterAttributeBoostMap().get(characterAttributeType).getValueAdd());
            }
            data.writeInt(itemGun.getMaxDamage());
            data.writeInt(itemGun.getItemQuality().ordinal());
            data.writeInt(itemGun.getItemRarity().ordinal());
            data.writeUTF(itemGun.getDescription());
            data.writeInt(itemGun.getItemTags().size());
            for (EnumItemTag itemTag : itemGun.getItemTags()) {
                data.writeInt(itemTag.ordinal());
            }
        } catch (Exception e) {
            System.out.println("Ошибка отправки конфига оружия: " + itemGun.getUnlocalizedName());
            throw new IOException();
        }
    }

    public static void readMelee(ByteBufInputStream data) throws IOException {
        String name = data.readUTF();
        for (ItemMeleeWeapon itemGun : CoreGunsCommon.registeredMeleeWeapons) {
            if(itemGun.getName().equals(name)) {
                itemGun.setDisplayName(data.readUTF());
                itemGun.getCharacterAttributeBoostMap().clear();
                itemGun.getItemTags().clear();
                int c = data.readInt();
                for (int i = 0; i < c; i++) {
                    CharacterAttributeType value = CharacterAttributeType.values()[data.readInt()];
                    CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                            .attributeType(value)
                            .valueAdd(data.readDouble())
                            .build();
                    itemGun.getCharacterAttributeBoostMap().put(value, characterAttributeMod);
                }
                itemGun.setMaxDamage(data.readInt());
                itemGun.setItemQuality(EnumItemQuality.values()[data.readInt()]);
                itemGun.setItemRarity(EnumItemRarity.values()[data.readInt()]);
                itemGun.setDescription(data.readUTF());
                c = data.readInt();
                for (int i = 0; i < c; i++) {
                    itemGun.getItemTags().add(EnumItemTag.values()[data.readInt()]);
                }
                break;
            }
        }
    }
}
