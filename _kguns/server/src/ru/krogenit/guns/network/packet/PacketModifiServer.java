package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import ru.krogenit.guns.CoreGunsServer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public class PacketModifiServer implements IPacketOut, IPacketInOnServer {

    @Override
    public void read(EntityPlayer p, ByteBufInputStream data) throws IOException {
        handleMessageOnServerSide(p);
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {

    }

    public void handleMessageOnServerSide(EntityPlayer p) {
        p.openGui(CoreGunsServer.instance, 0, p.worldObj, 0, 0, 0);
    }
}
