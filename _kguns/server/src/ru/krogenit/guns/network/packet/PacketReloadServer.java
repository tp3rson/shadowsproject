package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.krogenit.guns.item.ItemGun;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketReloadServer implements IPacketOut, IPacketInOnServer {


    @Override
    public void read(EntityPlayer p, ByteBufInputStream data) throws IOException {
        handleMessageOnServerSide(p);
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {

    }

    public void handleMessageOnServerSide(EntityPlayer p) {
        ItemStack item = p.getCurrentEquippedItem();
        if (item != null && item.getItem() instanceof ItemGun) {
            ItemGun weapon = (ItemGun) item.getItem();
            weapon.onReload(item, p.worldObj, p, true);
        }
    }

}
