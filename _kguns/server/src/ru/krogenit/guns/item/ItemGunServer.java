package ru.krogenit.guns.item;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.entity.EntityBulletServer;
import ru.krogenit.guns.network.packet.PacketBulletServer;
import ru.krogenit.guns.network.packet.PacketFlashlightServer;
import ru.krogenit.guns.util.config.ConfigGun;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.event.PlayerShootsWeaponEvent;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.result.AddItemResult;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.network.PacketHandler;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.network.PacketPlaySound;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.SoundType;
import ru.xlv.mochar.network.matrix.PacketMatrixPlayerInventorySync;

import java.util.ArrayList;
import java.util.List;

public class ItemGunServer extends ItemGun {

    public ItemGunServer(String name, ItemAmmo itemAmmo, int bulletsPerShot) {
        super(name);
        this.bulletsPerShot = bulletsPerShot;
        this.ammo = itemAmmo;
        ConfigGun configGun = new ConfigGun(name);
        configGun.load();
        addAttributeDamage(CharacterAttributeType.BALLISTIC_DAMAGE, configGun.getBallisticDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.CUT_DAMAGE, configGun.getCutDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.ENERGY_DAMAGE, configGun.getEnergyDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.THERMAL_DAMAGE, configGun.getThermalDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.FIRE_DAMAGE, configGun.getFireDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.ELECTRIC_DAMAGE, configGun.getElectricDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.TOXIC_DAMAGE, configGun.getToxicDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.RADIATION_DAMAGE, configGun.getRadiationDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.EXPLOSION_DAMAGE, configGun.getExplosionDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.FROZEN_DAMAGE, configGun.getFrozenDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.FRACTAL_DAMAGE, configGun.getFractalDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.PRICKLE_DAMAGE, configGun.getPrickleDamage() / bulletsPerShot);
        addAttributeDamage(CharacterAttributeType.PENETRATION, configGun.getPenetration());
        this.rate = configGun.getRate();
        setMaxDamage(configGun.getHardness());
        this.knockback = configGun.getKnockback();
        this.reloadSpeed = configGun.getReloadSpeed();
        this.unloadSpeed = configGun.getUnloadSpeed();
        setInvMatrixSize(configGun.getMatrixWidth(), configGun.getMatrixHeight());
        setItemQuality(configGun.getItemQuality());
        setItemRarity(configGun.getItemRarity());
        setDisplayName(configGun.getDisplayName());
        setSecondName(configGun.getSecondName());
        setDescription(configGun.getDescription());
        setAccuracy(configGun.getAccuracy());
        String[] split = configGun.getTags().split(",");
        for (String s : split) {
            if(s.length() > 0) getItemTags().add(EnumItemTag.valueOf(s.trim()));
        }
    }

    private void addAttributeDamage(CharacterAttributeType characterAttributeType, double value) {
        if(value > 0D) {
            CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder().attributeType(characterAttributeType).valueAdd(value).build();
            characterAttributeBoostMap.put(characterAttributeType, characterAttributeMod);
        }
    }

    public void onReload(ItemStack item, World w, EntityPlayer p, boolean removeAmmo) {
        boolean hasAmmo = getBoolean(item, "hasammo");
        if (hasAmmo) {
            if(CoreGunsCommon.IS_FULL_RELOAD) {
                ItemStack ammo = findAmmo(w, item, p, 1, this.ammo);
//                if (ammo == null) ammo = findAmmo(w, item, p, 0, this.ammo);
                if(ammo != null) {
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToAllAround(p, 16, new PacketPlaySound(p, SoundType.DEFAULT_DOMAIN + getLoadSound()));
                    putAmmo(w, item, p);
                    loadAmmo(item, ammo);
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(p, new PacketMatrixPlayerInventorySync(XlvsCore.INSTANCE.getPlayerManager().getPlayer(p)));
                }
            } else {
                putAmmo(w, item, p);
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToAllAround(p, 16, new PacketPlaySound(p, SoundType.DEFAULT_DOMAIN + getUnloadSound()));
            }
        } else {
            ItemStack ammoStack = findAmmo(w, item, p, 1, this.ammo);
//            if (ammoStack == null) ammoStack = findAmmo(w, item, p, 0, this.ammo);
            if(ammoStack != null) {
                loadAmmo(item, ammoStack);
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(p, new PacketMatrixPlayerInventorySync(XlvsCore.INSTANCE.getPlayerManager().getPlayer(p)));
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToAllAround(p, 16, new PacketPlaySound(p, SoundType.DEFAULT_DOMAIN + getLoadSound()));
            }
        }
    }

    protected ItemStack pullOutAmmo(ItemStack item) {
        ItemStack itemAmmo = new ItemStack(this.ammo);
        itemAmmo.setTagCompound(new NBTTagCompound());
        this.ammo.setAmmo(itemAmmo, getInteger(item, "ammo"));

        setInteger(item, "ammo", 0);
        setBoolean(item, "hasammo", false);

        return itemAmmo;
    }

    protected boolean putAmmo(World w, ItemStack item, EntityPlayer p) {
        ItemStack itemAmmo = pullOutAmmo(item);
        putAmmoToInventory(p, itemAmmo, true, false);
        return true;
    }

    private boolean putAmmoToInventory(EntityPlayer p, ItemStack item, boolean needDrop, boolean canStack) {
        ServerPlayer player = XlvsCore.INSTANCE.getPlayerManager().getPlayer(p);
        MatrixInventory matrixInventory = player.getSelectedCharacter().getMatrixInventory();
        if(matrixInventory.addItem(item) == AddItemResult.SUCCESS) return true;

//        InventoryPlayer inventory = p.inventory;
//        for(int i=0;i<inventory.mainInventory.length;i++) {
//            ItemStack pItem = inventory.mainInventory[i];
//            if(canStack && pItem != null && pItem.stackSize + item.stackSize <= pItem.getMaxStackSize()) {
//                pItem.stackSize += item.stackSize;
//                return true;
//            } else if(pItem == null) {
//                inventory.mainInventory[i] = item.copy();
//                return true;
//            }
//        }

        if(needDrop) {
            dropItem(p, item);
        }

        return false;
    }

    private void dropItem(EntityPlayer p, ItemStack item) {
        p.worldObj.spawnEntityInWorld(new EntityItem(p.worldObj, p.posX, p.posY - 0.30000001192092896D + p.getEyeHeight(), p.posZ, item));
    }

    private void loadAmmo(ItemStack itemStack, ItemStack ammoStack) {
        setInteger(itemStack, "ammo", ammo.getAmmo(ammoStack));
        setBoolean(itemStack, "hasammo", true);
    }

    protected ItemStack findAmmo(World w, ItemStack item, EntityPlayer p, int minAmmo, ItemAmmo ammo) {
        ItemStack foundedAmmo = null;
        int maxAmmoFound = 0;
        boolean fromDefaultInventory = false;
        int defaultInventoryIndex = 0;
        for (int i = 0; i < p.inventory.mainInventory.length; i++) {
            if (p.inventory.mainInventory[i] != null && p.inventory.mainInventory[i].getItem() == ammo) {
                ItemStack ammoStack = p.inventory.mainInventory[i];
                int newammo = ammo.getAmmo(ammoStack);
                if (newammo >= minAmmo && newammo >= maxAmmoFound) {
                    maxAmmoFound = newammo;
                    foundedAmmo = ammoStack;
                    fromDefaultInventory = true;
                    defaultInventoryIndex = i;
                }
            }
        }

        ServerPlayer player = XlvsCore.INSTANCE.getPlayerManager().getPlayer(p);
        MatrixInventory matrixInventory = player.getSelectedCharacter().getMatrixInventory();
        for(ItemStack itemStack : matrixInventory.getItems().values()) {
            if(itemStack != null) {
                if(itemStack.getItem() == ammo) {
                    int newammo = ammo.getAmmo(itemStack);
                    if (newammo >= minAmmo && newammo >= maxAmmoFound) {
                        maxAmmoFound = newammo;
                        foundedAmmo = itemStack;
                        fromDefaultInventory = false;
                    }
                }
            }
        }

        if(foundedAmmo != null) {
            if(fromDefaultInventory) {
                p.inventory.mainInventory[defaultInventoryIndex] = null;
                return foundedAmmo;
            } else {
                if(matrixInventory.removeItem(foundedAmmo)) return foundedAmmo;
            }
        }

        return null;
    }

    private final List<EntityBulletServer> shootBullets = new ArrayList<>();

    public void onItemLeftClick(ItemStack item, World w, EntityPlayer p, float playerAcc) {
        int ammo = getInteger(item, "ammo");
        if (canShoot(item, w, ammo)) {
            if (XlvsCoreCommon.EVENT_BUS.post(new PlayerShootsWeaponEvent(p, item))) {
                return;
            }
            if (!CoreGunsCommon.IS_UNLIMITED_AMMO) setInteger(item, "ammo", getInteger(item, "ammo") - 1);
            float damage = getWeaponDamage(item);
            float totalAcc = getAccuracy(item, playerAcc);

            shootBullets.clear();
            for(int i=0;i<bulletsPerShot;i++) {
                EntityBulletServer bullet = new EntityBulletServer(w, p, damage, totalAcc, damageMultiply);
                w.spawnEntityInWorld(bullet);
                shootBullets.add(bullet);
            }

            List<ServerPlayer> playerList = XlvsCore.INSTANCE.getPlayerManager().getPlayerList();
            PacketHandler packetHandler = XlvsCore.INSTANCE.getPacketHandler();
            for(ServerPlayer serverPlayer : playerList) {
                if (serverPlayer.isOnline()) {
                    EntityPlayer p1 = serverPlayer.getEntityPlayer();
                    if (p1 != p) {
                        packetHandler.sendPacketToPlayer(p1, new PacketFlashlightServer(p.getDisplayName(), ammo < 10));
                        for(EntityBulletServer bullet : shootBullets) packetHandler.sendPacketToPlayer(p1, new PacketBulletServer(bullet));
                    }
                }
            }
            onShoot(item, w);
        } else {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToAllAround(p, 16, new PacketPlaySound(p, SoundType.DEFAULT_DOMAIN + getNoAmmoSound()));
        }
    }
}
