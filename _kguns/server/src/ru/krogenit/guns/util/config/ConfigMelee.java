package ru.krogenit.guns.util.config;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;
import ru.xlv.core.common.item.quality.EnumItemQuality;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

@Getter
@ToString
public class ConfigMelee implements IConfigGson {

    private static final String CONFIG_PATH_FORMAT = "config/gun/%s_cfg.json";

    @Configurable(comment = "ballisticDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double ballisticDamage = 0D;
    @Configurable(comment = "cutDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double cutDamage = 0D;
    @Configurable(comment = "energyDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double energyDamage = 0D;
    @Configurable(comment = "thermalDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double thermalDamage = 0D;
    @Configurable(comment = "fireDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double fireDamage = 0D;
    @Configurable(comment = "electricDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double electricDamage = 0D;
    @Configurable(comment = "toxicDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double toxicDamage = 0D;
    @Configurable(comment = "radiationDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double radiationDamage = 0D;
    @Configurable(comment = "explosionDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double explosionDamage = 0D;
    @Configurable(comment = "frozenDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double frozenDamage = 0D;
    @Configurable(comment = "fractalDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double fractalDamage = 0D;
    @Configurable(comment = "prickleDamage. 2 - 1 сердце. Стандартное максимальное значение хп игрока: 100.0")
    private double prickleDamage = 0D;
    @Configurable(comment = "penetration.")
    private double penetration = 1D;
    @Configurable(comment = "weight")
    private double weight = 1D;

    @Configurable(comment = "Прочность предмета")
    private int hardness = 10000;

    @Configurable(comment = "Название предмета.")
    private String displayName = "";
    @Configurable(comment = "Второе название предмета.")
    private String secondName = "";

    @Configurable(comment = "Ширина предмета в клетках. Минимальное значение: 1")
    private int matrixWidth = 1;
    @Configurable(comment = "Высота предмета в клетках. Минимальное значение: 1")
    private int matrixHeight = 1;

    @Configurable(comment = "Качество предмета. Возможные варианты: GARBAGE - Мусорный,WORN_OUT - Изношенное,LOW_QUALITY - Низкокачественное,FMCG - Ширпотреб,BASIC - Базовое качество, HIGH - Повышенное качество, DISTINCTIVE - Отличительное качество, HIGHEST - Наивысшее качество, PERFECT - Идеальное качество, CUSTOMIZED - Заказное качество")
    private EnumItemQuality itemQuality = EnumItemQuality.BASIC;
    @Configurable(comment = "Редкость предмета. Возможные варианты: SLAG - Шлаковый, COMMON - Распространенный, UNCOMMON - Необычный, RARE - Редкий, EPIC - Эпический, SPECIAL - Особый, LEGENDARY - Легендарный")
    private EnumItemRarity itemRarity = EnumItemRarity.COMMON;

    @Configurable(comment = "Описание предмета")
    private String description = "";

    @Configurable(comment = "Лозунг предмета.")
    private String slogan = "";

    @Configurable(comment = "Теги предмета через запятую. Возможные варианты: PISTOL, SMG, AUTOMATIC, RIFLE, CARBINE, SNIPER, SHOTGUN, MACHINE_GUN, LASER_GUN, ENERGY_GUN, UNREGISTERED_ITEM, REGISTERED_ITEM, UNINSURED_ITEM, INSURED_ITEM, PERSONAL_CANDIDATE, PERSONAL")
    private String tags = "";

    @Getter(value = AccessLevel.NONE)
    protected final String itemName;

    public ConfigMelee(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public File getConfigFile() {
        return new File(String.format(CONFIG_PATH_FORMAT, itemName));
    }
}
