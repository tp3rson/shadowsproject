package ru.krogenit.guns.render.medicine;

import lombok.Getter;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.item.ItemMedicine;
import ru.krogenit.guns.render.*;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

public class ItemMedicineRenderer extends ItemAnimationHelper implements IItemRenderer, ICustomizableRenderer {

    private static final String CONFIG_PATH_FORMAT = "config/renderer/medicine/%s_cfg.json";

    protected final Model model;
    protected final ItemMedicine itemMedicine;
    private final TextureDDS diffuse;
    @Configurable
    @IgnoreObf
    protected List<RenderObject> renderObjects = new ArrayList<>();
    @Getter
    protected Map<String, RenderObject> renderObjectsByName = new HashMap<>();

    public ItemMedicineRenderer(Model model, ItemMedicine itemMedicine) {
        this.model = model;
        this.itemMedicine = itemMedicine;
        this.diffuse = new TextureDDS(new ResourceLocation("items", "textures/" + itemMedicine.getName() + "_d.dds"));
    }

    @Override
    public void init() {
        renderObjects.add(new RenderObject(EnumRenderObjectType.FIRST_PERSON.getName()));
        renderObjects.add(new RenderObject(EnumRenderObjectType.EQUIPPED.getName()));
        renderObjects.add(new RenderObject(EnumRenderObjectType.ENTITY.getName()));
        RenderObject e1 = new RenderObject(EnumRenderObjectType.INVENTORY.getName());
        e1.getPosition().x = 6.7f;
        e1.getPosition().y = 8.3f;
        e1.getRotation().z = 180;
        e1.getScale().x = 1.5f;
        e1.getScale().y = 1.5f;
        e1.getScale().z = 1.5f;
        renderObjects.add(e1);
        RenderObject item = new RenderObject("item");
        item.getPosition().x = 0.74f;
        item.getPosition().y = 0.47f;
        item.getPosition().z = 0.7f;
        item.getRotation().x = -21;
        item.getScale().x = 0.09f;
        item.getScale().y = 0.09f;
        item.getScale().z = 0.09f;
        renderObjects.add(item);
        RenderObject e = new RenderObject(EnumRenderObjectType.RIGHT_HAND.getName());
        e.getPosition().x = 0.5f;
        e.getPosition().y = 0.3f;
        e.getPosition().z = 0.4f;
        e.getRotation().x = 80f;
        e.getRotation().z = -20;
        renderObjects.add(e);
        load();
        for(RenderObject renderObject : renderObjects) {
            renderObjectsByName.put(renderObject.getName(), renderObject);
        }
    }

    private void renderItem() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(diffuse);
        model.render(shader);
        KrogenitShaders.finishCurrentShader();
    }

    public void renderFirstPerson(ItemStack itemStack) {
        glRotatef(27f, 0, 0, 1);
        glRotatef(86f, 0, 1, 0);
        setupMatrix(EnumRenderObjectType.FIRST_PERSON.getName());
        renderHand();
        setupMatrix("item");
        renderItem();
    }

    public void renderEquipped(ItemStack itemStack) {
        setupMatrix(EnumRenderObjectType.EQUIPPED.getName());
        renderItem();
    }

    public void renderAsEntity(ItemStack itemStack) {
        setupMatrix(EnumRenderObjectType.ENTITY.getName());
        renderItem();
    }

    public void renderInventory(ItemStack itemStack) {
        setupMatrix(EnumRenderObjectType.INVENTORY.getName());
        renderItem();
    }

    private void setupMatrix(String name) {
        RenderObject renderObject = renderObjectsByName.get(name);
        Vector3fConfigurable position = renderObject.getPosition();
        Vector3fConfigurable rotation = renderObject.getRotation();
        Vector3fConfigurable scale = renderObject.getScale();
        glTranslatef(position.x , position.y, position.z);
        glScaled(scale.x, scale.y, scale.z);
        glRotatef(rotation.x, 1, 0, 0);
        glRotatef(rotation.z, 0, 0, 1);
        glRotatef(rotation.y, 0, 1, 0);
    }

    private void renderHand() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
        glPushMatrix();
        setupMatrix(EnumRenderObjectType.RIGHT_HAND.getName());
        hand.renderRight();
        glPopMatrix();
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
        return false;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data) {
        switch (type) {
            case EQUIPPED_FIRST_PERSON:
                renderFirstPerson(itemStack);
                return;
            case EQUIPPED:
                renderEquipped(itemStack);
                return;
            case INVENTORY:
                renderInventory(itemStack);
                return;
            case ENTITY:
                renderAsEntity(itemStack);
        }
    }

    @Override
    public void renderItemPost(ItemRenderType type, ItemStack item, Object... data) {

    }

    @Override
    public Map<String, RenderObject> getRenderObjects() {
        return renderObjectsByName;
    }

    @Override
    public Map<String, List<AnimationNode>> getAnimations() {
        return null;
    }

    @Override
    public long getAnimationTime() {
        return 0;
    }

    @Override
    public File getConfigFile() {
        return new File(String.format(CONFIG_PATH_FORMAT, itemMedicine.getUnlocalizedName()));
    }
}
