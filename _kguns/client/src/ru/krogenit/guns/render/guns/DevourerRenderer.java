package ru.krogenit.guns.render.guns;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class DevourerRenderer extends AbstractItemGunRenderer {

    private final TextureDDS resourceLocationStatefulDiffuse;
    private final TextureDDS resourceLocationStatefulNormal;
    private final TextureDDS resourceLocationStatefulSpecular;
    private final TextureDDS resourceLocationStatefulGlassMap;
    private final TextureDDS resourceLocationStatefulEmission;
    private final TextureDDS scopeD;

    public DevourerRenderer(ItemGun gun) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/weapons/Devourer.obj")), gun);
        resourceLocationStatefulDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Devourer_D.dds"));
        resourceLocationStatefulNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Devourer_N.dds"));
        resourceLocationStatefulSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Devourer_S.dds"));
        resourceLocationStatefulGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Devourer_G.dds"));
        resourceLocationStatefulEmission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Devourer_E.dds"));
        scopeD = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/devourer/devourer_scope_d.dds"));
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.1f;
        glScalef(scale, scale, scale);
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Override
    protected void renderGun(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        int lensTexture = 0;
        boolean renderScopeDynamic = false;
        if (!KrogenitShaders.lightPass && !KrogenitShaders.gBufferPass && type != ItemRenderType.EQUIPPED) {
            if (type == ItemRenderType.EQUIPPED_FIRST_PERSON && aim.x > 0) {
                lensTexture = prepareLensTexture(aim.x * -10f, -0.05F, 1.05F, 0, 1);
                renderScopeDynamic = true;
            }
        }

        TextureLoaderDDS.bindTexture(resourceLocationStatefulDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulGlassMap, shader);
        TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulEmission, shader, 1.5f);
        gunModel.renderPart("devourer", shader);
        shader.setNormalMapping(false);
        shader.setEmissionMapping(false);
        shader.setSpecularMapping(false);
        shader.setGlossMapping(false);
        glActiveTexture(GL_TEXTURE0);

        if (!KrogenitShaders.lightPass && !KrogenitShaders.gBufferPass && type != ItemRenderType.EQUIPPED && renderScopeDynamic) {
            glDepthMask(false);
            glBindTexture(GL_TEXTURE_2D, lensTexture);
            shader.setColor(aim.x, aim.x, aim.x, 1f);
            shader.setLightMapping(false);
            gunModel.renderPart("glass", shader);
            shader.setColor(1, 1, 1, 1);
            glDepthMask(true);

            glDisable(GL_ALPHA_TEST);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glAlphaFunc(GL_GREATER, 0.000001f);
            TextureLoaderDDS.bindTexture(scopeD);
            gunModel.renderPart("glass", shader);
            shader.setLightMapping(true);
            glAlphaFunc(GL_GREATER, 0.5f);
            glDisable(GL_BLEND);
            glEnable(GL_ALPHA_TEST);
        } else {
            if(!KrogenitShaders.interfacePass) shader.setUseTexture(false);
            shader.setColor(0, 0, 0, 1f);
            gunModel.renderPart("glass", shader);
            shader.setColor(1f, 1f, 1f, 1f);
            if(!KrogenitShaders.interfacePass) shader.setUseTexture(true);
        }

        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void renderComponents(ItemStack item, ItemRenderType type) {

    }

    @Override
    public void createParticles(ItemStack item, ItemRenderType type, EntityPlayer p) {
        boolean shouldSpawnSmokde = true;

        Vector3f smokeVec;
        Vector3f sleeveSpawn;
        if (type == ItemRenderType.EQUIPPED || mc.gameSettings.thirdPersonView > 0) {
            smokeVec = new Vector3f(-0.3f+0.02f + (aim.x / 4f), -0.1f-0.08f, 0.4f + 0.08f);
            sleeveSpawn = new Vector3f(1.2f + 0.5f, aim.x/10f, -0.6f + aim.x/1.7f + 0.45f);
        }
        else {
            smokeVec = new Vector3f(-0.3f + (aim.x / 4f), -0.1f, 0.4f);
            sleeveSpawn = new Vector3f(1.2f, aim.x/10f, -0.6f + aim.x/1.7f);
        }

        super.createBasePaticles(item, type, sleeveSpawn, smokeVec, true, shouldSpawnSmokde, 10, p);
    }

    @Override
    public void updatePostShootAnimation() {
        isPlayPostShootAnim = false;
    }

    @Override
    public boolean isNeedPreAnimation() {
        return false;
    }
}
