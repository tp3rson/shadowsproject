package ru.krogenit.guns.render.guns;

import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.*;

public class ArkonistaRenderer extends AbstractItemGunRenderer {

    private final Model scope = new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/arkonista/Arkonista-Scope.obj"));

    @Getter private final TextureDDS resourceLocationStatefulGaussDiffuse;
    @Getter private final TextureDDS resourceLocationStatefulGlowDiffuse;
    @Getter private final TextureDDS resourceLocationStatefulGaussReticleDiffuse;

    public ArkonistaRenderer(ItemGun gun) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/arkonista/Arkonista.obj")), gun);
        resourceLocationStatefulGaussDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/arkonista/p3dm.ru_assaultrifle_gauss_3p.dds"));
        resourceLocationStatefulGlowDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/arkonista/p3dm.ru_assaultrifle_glow.dds"));
        resourceLocationStatefulGaussReticleDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/arkonista/p3dm.ru_assaultrifle_gauss_reticle_3p.dds"));
        postRender = true;
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.1f;
        glScalef(scale, scale, scale);
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Override
    protected void renderGun(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulGaussDiffuse);
        gunModel.renderPart("Arkonista", shader);
        gunModel.renderPart("Mag", shader);
        scope.renderPart("Scope", shader);

        if (!KrogenitShaders.lightPass && !KrogenitShaders.gBufferPass && type != ItemRenderType.EQUIPPED) {
            glColor4f(1f, 1f, 1f, 1f);
            if(!KrogenitShaders.interfacePass) {
                GL11.glAlphaFunc(GL_GREATER, 0.0001f);
                GL11.glEnable(GL_BLEND);
            }
            shader.setColor(1f, 1f, 1f, 1f);
            TextureLoaderDDS.bindTexture(resourceLocationStatefulGlowDiffuse);
            TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulGlowDiffuse, shader, 2f);
            gunModel.renderPart("Display", shader);
            GL13.glActiveTexture(GL13.GL_TEXTURE0);
            TextureLoaderDDS.bindTexture(resourceLocationStatefulGaussReticleDiffuse);
            TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulGaussReticleDiffuse, shader, 2f);
            GL11.glDisable(GL_CULL_FACE);
            scope.renderPart("Glass", shader);
            GL11.glEnable(GL_CULL_FACE);
            if(!KrogenitShaders.interfacePass) {
                GL11.glDisable(GL_BLEND);
                GL11.glAlphaFunc(GL_GREATER, 0.5f);
            }
            shader.setEmissionMapping(false);
        }
        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    protected void renderGunPost(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);

        if (!KrogenitShaders.lightPass) {
            GL11.glAlphaFunc(GL_GREATER, 0.0001f);
            GL11.glEnable(GL_BLEND);
            TextureLoaderDDS.bindTexture(resourceLocationStatefulGlowDiffuse);
            TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulGlowDiffuse, shader, 2f);
            gunModel.renderPart("Display", shader);
            GL13.glActiveTexture(GL13.GL_TEXTURE0);
            TextureLoaderDDS.bindTexture(resourceLocationStatefulGaussReticleDiffuse);
            TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulGaussReticleDiffuse, shader, 2f);
            GL11.glDisable(GL_CULL_FACE);
            scope.renderPart("Glass", shader);
            GL11.glEnable(GL_CULL_FACE);
            GL11.glDisable(GL_BLEND);
            GL11.glAlphaFunc(GL_GREATER, 0.5f);
            shader.setEmissionMapping(false);
        }

        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void renderComponents(ItemStack item, ItemRenderType type) {

    }

    @Override
    public void createParticles(ItemStack item, ItemRenderType type, EntityPlayer p) {
        boolean shouldSpawnSmokde = true;

        Vector3f smokeVec;
        Vector3f sleeveSpawn;
        if (type == ItemRenderType.EQUIPPED || mc.gameSettings.thirdPersonView > 0) {
            smokeVec = new Vector3f(-0.3f+0.02f + (aim.x / 4f), -0.1f-0.08f, 0.4f + 0.08f);
            sleeveSpawn = new Vector3f(1.2f + 0.5f, aim.x/10f, -0.6f + aim.x/1.7f + 0.45f);
        }
        else {
            smokeVec = new Vector3f(-0.3f + (aim.x / 4f), -0.1f, 0.4f);
            sleeveSpawn = new Vector3f(1.2f, aim.x/10f, -0.6f + aim.x/1.7f);
        }

        super.createBasePaticles(item, type, sleeveSpawn, smokeVec, true, shouldSpawnSmokde, 10, p);
    }

    @Override
    public void updatePostShootAnimation() {
        isPlayPostShootAnim = false;
    }

    @Override
    public boolean isNeedPreAnimation() {
        return false;
    }
}
