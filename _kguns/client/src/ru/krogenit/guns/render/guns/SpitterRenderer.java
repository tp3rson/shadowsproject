package ru.krogenit.guns.render.guns;

import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class SpitterRenderer extends AbstractItemGunRenderer {

    @Getter private final TextureDDS resourceLocationStatefulHoloSightDiffuse;
    @Getter private final TextureDDS resourceLocationStatefulHoloSightEmission;
    @Getter private final TextureDDS resourceLocationStatefulDiffuse;
    @Getter private final TextureDDS resourceLocationStatefulNormal;
    @Getter private final TextureDDS resourceLocationStatefulSpecular;
    @Getter private final TextureDDS resourceLocationStatefulGlassMap;
    @Getter private final TextureDDS resourceLocationStatefulEmission;
    @Getter private final TextureDDS resourceLocationStatefulMagDiffuse;
    @Getter private final TextureDDS resourceLocationStatefulMagNormal;
    @Getter private final TextureDDS resourceLocationStatefulMagSpecular;
    @Getter private final TextureDDS resourceLocationStatefulMagGlassMap;

    public SpitterRenderer(ItemGun gun) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/spitter/Spitter.obj")), gun);
        resourceLocationStatefulHoloSightDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/holosight01.tga.dds"));
        resourceLocationStatefulHoloSightEmission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/holosight01_emissive.tga.dds"));
        resourceLocationStatefulDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/SMG_Diffuse.dds"));
        resourceLocationStatefulNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/SMG_Normal.dds"));
        resourceLocationStatefulSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/SMG_Specular.dds"));
        resourceLocationStatefulGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/SMG_Glossiness.dds"));
        resourceLocationStatefulEmission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/SMG_Emissive.dds"));
        resourceLocationStatefulMagDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/MAG_Diffuse.dds"));
        resourceLocationStatefulMagNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/MAG_Normal.dds"));
        resourceLocationStatefulMagSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/MAG_Specular.dds"));
        resourceLocationStatefulMagGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/spitter/MAG_Glossiness.dds"));
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.1f;
        glScalef(scale, scale, scale);
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Override
    protected void renderGun(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        glActiveTexture(GL_TEXTURE0);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulGlassMap, shader);
        TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulEmission, shader, 5f);
        gunModel.renderPart("SMG", shader);
        glActiveTexture(GL_TEXTURE0);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulMagDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulMagNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulMagSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulMagGlassMap, shader);
        shader.setEmissionMapping(false);
        gunModel.renderPart("SMG.001", shader);
        gunModel.renderPart("MAG", shader);
        shader.setNormalMapping(false);
        shader.setSpecularMapping(false);
        shader.setGlossMapping(false);

        if(!KrogenitShaders.lightPass) {
            glActiveTexture(GL_TEXTURE0);
            TextureLoaderDDS.bindTexture(resourceLocationStatefulHoloSightDiffuse);
            TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulHoloSightEmission, shader, 5f);
            if(!KrogenitShaders.interfacePass) GL11.glEnable(GL_BLEND);
            gunModel.renderPart("glass01", shader);
            if(!KrogenitShaders.interfacePass) GL11.glDisable(GL_BLEND);
            shader.setEmissionMapping(false);
        }

        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void renderComponents(ItemStack item, ItemRenderType type) {

    }

    @Override
    public void createParticles(ItemStack item, ItemRenderType type, EntityPlayer p) {
        boolean shouldSpawnSmokde = true;

        Vector3f smokeVec;
        Vector3f sleeveSpawn;
        if (type == ItemRenderType.EQUIPPED || mc.gameSettings.thirdPersonView > 0) {
            smokeVec = new Vector3f(-0.3f+0.02f + (aim.x / 4f), -0.1f-0.08f, 0.4f + 0.08f);
            sleeveSpawn = new Vector3f(1.2f + 0.5f, aim.x/10f, -0.6f + aim.x/1.7f + 0.45f);
        }
        else {
            smokeVec = new Vector3f(-0.3f + (aim.x / 4f), -0.1f, 0.4f);
            sleeveSpawn = new Vector3f(1.2f, aim.x/10f, -0.6f + aim.x/1.7f);
        }

        super.createBasePaticles(item, type, sleeveSpawn, smokeVec, true, shouldSpawnSmokde, 10, p);
    }

    @Override
    public void updatePostShootAnimation() {
        isPlayPostShootAnim = false;
    }

    @Override
    public boolean isNeedPreAnimation() {
        return false;
    }
}
