package ru.krogenit.guns.render.guns;

import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.xlv.core.resource.ResourceLocationStateful;

import static org.lwjgl.opengl.GL11.glScalef;

public class BarracudaRenderer extends AbstractItemGunRenderer {

    @Getter private final TextureDDS resourceLocationStatefulDiffuse;
    @Getter private final TextureDDS resourceLocationStatefulNormal;
    @Getter private final TextureDDS resourceLocationStatefulSpecular;

    public BarracudaRenderer(ItemGun gun) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/weapons/Barracuda.obj")), gun);
        ResourceLocationStateful.Builder builder = ResourceLocationStateful.Builder.builder().withDomain(CoreGunsCommon.MODID).temporary();
        resourceLocationStatefulDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Barracuda_D.dds"));
        resourceLocationStatefulNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Barracuda_N.dds"));
        resourceLocationStatefulSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Barracuda_S.dds"));
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.15f;
        glScalef(scale, scale, scale);
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Override
    protected void renderGun(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        shader.setNormalMapping(true);
        shader.setSpecularMapping(true);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulSpecular, shader);
        gunModel.renderPart("Barracuda_Part2", shader);
        gunModel.renderPart("Barracuda_Part1", shader);
        gunModel.renderPart("Barracuda_Part3", shader);
        gunModel.renderPart("Barracuda_Ammo", shader);
        shader.setNormalMapping(false);
        shader.setSpecularMapping(false);
        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void renderComponents(ItemStack item, ItemRenderType type) {

    }

    @Override
    public void createParticles(ItemStack item, ItemRenderType type, EntityPlayer p) {
        boolean shouldSpawnSmokde = true;

        Vector3f smokeVec;
        Vector3f sleeveSpawn;
        if (type == ItemRenderType.EQUIPPED || mc.gameSettings.thirdPersonView > 0) {
            smokeVec = new Vector3f(-0.3f+0.02f + (aim.x / 4f), -0.1f-0.08f, 0.4f + 0.08f);
            sleeveSpawn = new Vector3f(1.2f + 0.5f, aim.x/10f, -0.6f + aim.x/1.7f + 0.45f);
        }
        else {
            smokeVec = new Vector3f(-0.3f + (aim.x / 4f), -0.1f, 0.4f);
            sleeveSpawn = new Vector3f(1.2f, aim.x/10f, -0.6f + aim.x/1.7f);
        }

        super.createBasePaticles(item, type, sleeveSpawn, smokeVec, true, shouldSpawnSmokde, 10, p);
    }

    @Override
    public void updatePostShootAnimation() {
        isPlayPostShootAnim = false;
    }

    @Override
    public boolean isNeedPreAnimation() {
        return false;
    }
}
