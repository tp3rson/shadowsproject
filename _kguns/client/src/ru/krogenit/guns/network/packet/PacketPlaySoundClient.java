package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketPlaySoundClient implements IPacketOut, IPacketIn {

    private String sound;

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        sound = data.readUTF();
        handleMessageOnServerSide();
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeUTF(sound);
    }

    public void handleMessageOnServerSide() {
        Minecraft.getMinecraft().theWorld.playSoundAtEntity(Minecraft.getMinecraft().thePlayer, sound, 1.0f, 1.0f);
    }
}
