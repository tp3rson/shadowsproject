package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketDamageClient implements IPacketIn, IPacketOut {

    private int entityId;
    private float damage;

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        damage = data.readFloat();
        entityId = data.readInt();
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeFloat(damage);
        data.writeInt(entityId);
    }
}
