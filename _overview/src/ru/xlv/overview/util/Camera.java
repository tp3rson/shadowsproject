package ru.xlv.overview.util;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import org.lwjgl.input.Mouse;
import ru.xlv.overview.XlvsOverviewMod;

public class Camera {

    private static boolean enabled = false;
    private static float cameraYaw;
    private static float cameraPitch;
    private static float playerYaw;
    private static float playerPitch;
    private static float originalYaw;
    private static float originalPitch;

    public static void setCamera() {
        Minecraft mc = Minecraft.getMinecraft();
        cameraYaw = playerYaw = originalYaw = mc.thePlayer.rotationYaw;
        cameraPitch = originalPitch = -mc.thePlayer.rotationPitch;
        playerPitch = cameraPitch;
        enabled = true;
    }

    private static void updateCamera() {
        Minecraft mc = Minecraft.getMinecraft();
        if ((!mc.inGameHasFocus) && (!enabled)) {
            return;
        }
        float f = mc.gameSettings.mouseSensitivity * 0.6F + 0.2F;
        float f1 = f * f * f * 8.0F;
        double dx = Mouse.getDX() * f1 * 0.15D;
        double dy = Mouse.getDY() * f1 * 0.15D;
        if (XlvsOverviewMod.keyOverview.getIsKeyPressed()) {
            cameraYaw = (float)(cameraYaw + dx);
            cameraPitch = (float)(cameraPitch + dy);
            cameraPitch = MathHelper.clamp_float(cameraPitch, -90.0F, 90.0F);
            if (XlvsOverviewMod.viewClamping) {
                cameraYaw = MathHelper.clamp_float(cameraYaw, originalYaw + -100.0F, originalYaw + 100.0F);
            }
        }
    }

    public static void resetCamera() {
        cameraYaw = originalYaw;
        cameraPitch = originalPitch;
        playerYaw = originalYaw;
        playerPitch = originalPitch;
        enabled = false;
    }

    public static void update(boolean start) {
        Minecraft mc = Minecraft.getMinecraft();
        Entity player = mc.renderViewEntity;
        if (player == null) {
            return;
        }
        if (enabled) {
            updateCamera();
            if (start) {
                player.rotationYaw = (player.prevRotationYaw = cameraYaw);
                player.rotationPitch = (player.prevRotationPitch = -cameraPitch);
            } else {
                player.rotationYaw = (mc.thePlayer.rotationYaw - cameraYaw + playerYaw);
                player.prevRotationYaw = (mc.thePlayer.prevRotationYaw - cameraYaw + playerYaw);

                player.rotationPitch = (-playerPitch);
                player.prevRotationPitch = (-playerPitch);
            }
        }
    }

    public static void cameraEnabled(boolean start) {
        Minecraft mc = Minecraft.getMinecraft();
        Entity player = mc.renderViewEntity;
        if ((player == null) && (!mc.inGameHasFocus)) {
            return;
        }
        if (mc.inGameHasFocus) {
            if (!enabled) {
                setCamera();
                enabled = true;
            }
            updateCamera0();
            if (start) {
                player.rotationYaw = (player.prevRotationYaw = cameraYaw);
                player.rotationPitch = (player.prevRotationPitch = -cameraPitch);
            } else {
                player.rotationYaw = (mc.thePlayer.rotationYaw - cameraYaw + playerYaw);
                player.prevRotationYaw = (mc.thePlayer.prevRotationYaw - cameraYaw + playerYaw);

                player.rotationPitch = (-playerPitch);
                player.prevRotationPitch = (-playerPitch);
            }
        }
    }

    private static void updateCamera0() {
        Minecraft mc = Minecraft.getMinecraft();
        if ((!mc.inGameHasFocus) && (!enabled)) {
            return;
        }
        if (mc.inGameHasFocus) {
            float f = mc.gameSettings.mouseSensitivity * 0.6F + 0.2F;
            float f1 = f * f * f * 8.0F;
            double dx = Mouse.getDX() * f1 * 0.15D;
            double dy = Mouse.getDY() * f1 * 0.15D;
            cameraYaw = (float)(cameraYaw + dx);
            cameraPitch = (float)(cameraPitch + dy);
            cameraPitch = MathHelper.clamp_float(cameraPitch, -90.0F, 90.0F);
            if (XlvsOverviewMod.viewClamping) {
                cameraYaw = MathHelper.clamp_float(cameraYaw, originalYaw + -100.0F, originalYaw + 100.0F);
            }
        }
    }
}